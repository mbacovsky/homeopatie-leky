<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Red" ID="ID_1845958965" CREATED="1341225352456" MODIFIED="1341225410566" LINK="../../Letn&#xed;%20&#x161;kola%202012%20-%20Podmitrov/L&#xe9;&#x10d;en&#xed;%20pomoc&#xed;%20barev.mm" COLOR="#cc0000" STYLE="fork">
<hook NAME="MapStyle" max_node_width="600"/>
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Red-aw" POSITION="right" ID="ID_299823961" CREATED="1341237929291" MODIFIED="1341237934696" COLOR="#996600">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Trituace" POSITION="right" ID="ID_1599084046" CREATED="1341225412137" MODIFIED="1341225426052" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="t&#x11b;&#x17e;kost" ID="ID_1397264728" CREATED="1341225484165" MODIFIED="1341225574597">
<font ITALIC="true"/>
</node>
<node TEXT="v&#xe1;&#x17e;nost" ID="ID_977274451" CREATED="1341225574851" MODIFIED="1341225581253">
<font ITALIC="true"/>
</node>
<node TEXT="osaplost" ID="ID_1971924375" CREATED="1341225674079" MODIFIED="1341225689425">
<font ITALIC="true"/>
</node>
<node TEXT="nucen&#xed; zastavit a odpo&#x10d;&#xed;vat" ID="ID_1137417535" CREATED="1341225690070" MODIFIED="1341226045082">
<font ITALIC="true"/>
</node>
<node TEXT="n&#xe1;vrat k podstat&#x11b; (ko&#x159;en&#x16f;m)" ID="ID_479307332" CREATED="1341226189829" MODIFIED="1341226616986">
<font ITALIC="true"/>
</node>
<node TEXT="hodn&#x11b; dovnit&#x159;" ID="ID_1684114420" CREATED="1341226344614" MODIFIED="1341226349887"/>
<node TEXT="zrozen&#xed;" ID="ID_428369606" CREATED="1341226350439" MODIFIED="1341226353288"/>
<node TEXT="bolest hlavy" ID="ID_268725751" CREATED="1341226444112" MODIFIED="1341226448316"/>
<node TEXT="horko v oblasti 1. &#x10d;akry" ID="ID_106254952" CREATED="1341226448597" MODIFIED="1341226455342"/>
<node TEXT="s&#xed;la sn&#xe1;&#x161;et fyzickou bolest" ID="ID_806253509" CREATED="1341226522019" MODIFIED="1341226537289"/>
<node TEXT="&quot;nikam nep&#x16f;jde&#x161;, doma bude&#x161;&quot;" ID="ID_500029038" CREATED="1341226645638" MODIFIED="1341226667909"/>
<node TEXT="bolesti v podb&#x159;i&#x161;ku" ID="ID_1443416927" CREATED="1341226768003" MODIFIED="1341226776291"/>
</node>
<node TEXT="Info" POSITION="right" ID="ID_951450832" CREATED="1341226250043" MODIFIED="1341226257135" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Saturn" ID="ID_332368376" CREATED="1341226635590" MODIFIED="1341226643697"/>
<node TEXT="t&#xf3;n" ID="ID_1110189916" CREATED="1341236555304" MODIFIED="1341236557566">
<node TEXT="C" ID="ID_282109390" CREATED="1341236558647" MODIFIED="1341236559323"/>
</node>
<node TEXT="miazma" ID="ID_1003070490" CREATED="1341236560361" MODIFIED="1341236563201">
<node TEXT="psora" ID="ID_238253022" CREATED="1341236564156" MODIFIED="1341236566089">
<node TEXT="p&#xe1;d do hmoty" ID="ID_399387579" CREATED="1341236574602" MODIFIED="1341236581361" COLOR="#999999">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
<node TEXT="625=?nm" ID="ID_153656311" CREATED="1341236538441" MODIFIED="1341236550868"/>
<node TEXT="nejpomalej&#x161;&#xed; vibrace" ID="ID_1088801569" CREATED="1341236588157" MODIFIED="1341236600490"/>
<node TEXT="nejhust&#x161;&#xed; energetick&#xe9; pole" ID="ID_494988303" CREATED="1341236600907" MODIFIED="1341236611576"/>
<node TEXT="ot&#xe1;zka &#x17e;ivota a smrti" ID="ID_493595660" CREATED="1341236631121" MODIFIED="1341236639828">
<node TEXT="z&#xe1;kladn&#xed; energie k p&#x159;e&#x17e;it&#xed;" ID="ID_848083275" CREATED="1341236643345" MODIFIED="1341236653089"/>
</node>
<node TEXT="v p&#x159;&#xed;tomnosti &#x10d;erven&#xe9; nelze z&#x16f;stat bez reakce" ID="ID_482374780" CREATED="1341236665315" MODIFIED="1341236685247"/>
<node TEXT="zvy&#x161;uje tepovou frekvenci" ID="ID_1079350949" CREATED="1341236685535" MODIFIED="1341236694881"/>
<node TEXT="vyplavuje adrenalin" ID="ID_493312172" CREATED="1341236695186" MODIFIED="1341236700781"/>
<node TEXT="prvn&#xed; barva, kterou lidsk&#xe9; okko vn&#xed;m&#xe1;" ID="ID_24259983" CREATED="1341236787783" MODIFIED="1341236797874"/>
</node>
<node TEXT="Symptomy" POSITION="right" ID="ID_406764323" CREATED="1341226241173" MODIFIED="1341226249155" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="krev" ID="ID_951422926" CREATED="1341236764590" MODIFIED="1341236765781">
<node TEXT="posiluje" ID="ID_1484639848" CREATED="1341236766742" MODIFIED="1341236769599"/>
</node>
<node TEXT="z&#xe1;n&#x11b;tliv&#xe9; procesy" ID="ID_337130404" CREATED="1341237410415" MODIFIED="1341237416603">
<node TEXT="z&#xe1;n&#x11b;ty kost&#xed;" ID="ID_1144273167" CREATED="1341237499032" MODIFIED="1341237503539"/>
</node>
<node TEXT="nedostatek &#x17e;eleza" ID="ID_897410223" CREATED="1341237416804" MODIFIED="1341237427825"/>
<node TEXT="sn&#xed;&#x17e;en&#xe1; imunita" ID="ID_1134011617" CREATED="1341237485630" MODIFIED="1341237491066">
<node TEXT="HIV" ID="ID_864082723" CREATED="1341237567028" MODIFIED="1341237568244"/>
<node TEXT="AIDS" ID="ID_1734341904" CREATED="1341237568525" MODIFIED="1341237571192"/>
</node>
<node TEXT="p&#xe1;liv&#xe9; bolesti" ID="ID_415300037" CREATED="1341237509639" MODIFIED="1341237514594"/>
<node TEXT="otekl&#xe9; uzliny" ID="ID_432165281" CREATED="1341237522332" MODIFIED="1341237526399"/>
<node TEXT="&#x161;patn&#xe1; cirkulace" ID="ID_1059969119" CREATED="1341237526680" MODIFIED="1341237532042"/>
<node TEXT="z&#xe1;cpa" ID="ID_723240967" CREATED="1341237532300" MODIFIED="1341237535232"/>
<node TEXT="hemeroidy" ID="ID_347954625" CREATED="1341237535489" MODIFIED="1341237538645"/>
<node TEXT="pot&#xed;&#x17e;e s mo&#x10d;en&#xed;m" ID="ID_1184031221" CREATED="1341237538918" MODIFIED="1341237544754"/>
<node TEXT="podporuje regeneraci" ID="ID_1332473825" CREATED="1341237551001" MODIFIED="1341237555964"/>
<node TEXT="demence" ID="ID_266362417" CREATED="1341237576314" MODIFIED="1341237578276"/>
<node TEXT="doln&#xed; kon&#x10d;etiny" ID="ID_1555834591" CREATED="1341237585619" MODIFIED="1341237593811"/>
<node TEXT="p&#x159;i porodu" ID="ID_262469410" CREATED="1341237853583" MODIFIED="1341237860483">
<node TEXT="stimuluje" ID="ID_1034210263" CREATED="1341237861292" MODIFIED="1341237863616"/>
<node TEXT="otev&#xed;r&#xe1; hrdlo d&#x11b;lo&#x17e;n&#xed;" ID="ID_1954167396" CREATED="1341237863921" MODIFIED="1341237870610"/>
</node>
<node TEXT="ztr&#xe1;ta &#x17e;ivotn&#xed;ho cyklu/rytmu" ID="ID_448839767" CREATED="1341237957941" MODIFIED="1341237970758"/>
<node TEXT="alzheimer" ID="ID_1642894777" CREATED="1341237971909" MODIFIED="1341237976098"/>
<node TEXT="poruchy MS cyklu" ID="ID_1633910869" CREATED="1341237994533" MODIFIED="1341238000056"/>
</node>
<node TEXT="Kontraindikace" POSITION="right" ID="ID_877262465" CREATED="1341226277578" MODIFIED="1341226288140" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="akutn&#xed; z&#xe1;n&#x11b;ty" ID="ID_1079122301" CREATED="1341237760223" MODIFIED="1341237771328"/>
<node TEXT="krv&#xe1;cen&#xed;" ID="ID_271481256" CREATED="1341237771960" MODIFIED="1341237777758"/>
<node TEXT="vysok&#xfd; krevn&#xed; tlak" ID="ID_1342704548" CREATED="1341237791772" MODIFIED="1341237797232"/>
<node TEXT="prudc&#xed; a n&#xe1;siln&#xed; lid&#xe9;" ID="ID_1355381940" CREATED="1341237797537" MODIFIED="1341237804789"/>
<node TEXT="potla&#x10d;en&#xfd; vztek" ID="ID_231870730" CREATED="1341237808571" MODIFIED="1341237814300">
<node TEXT="m&#x16f;&#x17e;e se dramaticky a rychle uvolnit" ID="ID_449953406" CREATED="1341237822729" MODIFIED="1341237833788" COLOR="#999999">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="termin&#xe1;ln&#xed; st&#xe1;dia" ID="ID_169525393" CREATED="1341237834712" MODIFIED="1341237840482"/>
</node>
<node TEXT="Res" POSITION="right" ID="ID_1518692802" CREATED="1341225427036" MODIFIED="1341225431952" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="RED: The first color in the homeopathic color remedies is RED. It is the densest of all colors. It has the longest wavelength and lowest energy level of all the visible colors. It is the first color the human eye sees and is often associated with passion, violence, life force, danger and vitality.  We use it for grounding and rooting in to life. Red is the color of life.&#xa;It is used in the homeopathic color remedies to help anchor the spirit in the physical plane. Whenever a person goes through life changes that take them away from their safety zone, or level of accustomed comfort this color grounds them soundly in their experience. Red is a color that has density and gravity. It works well when people are recovering from long term or acute illness, surgery, life change such as divorce, deaths of loved ones, shocks, traumas or house or job moves.&#xa; &#xa;Chakra: The Root Chakra: The Root Chakra is located at the base of the spine. It nourishes the adrenal cortex of the kidneys, which contains the fight or flight function of the kidneys. It deals with life challenges that focus on survival, community and family. Whenever we fall into the Victim archetype we lose our ground of being. Constellating the Mother archetype, learning to be our own good parent, sustaining a positive outlook to whatever our situation is, comes from a strong Root chakra. The qualities of the Root Chakra are: Patience, Security, Stability, Structure, Order and the Ability to make our dreams manifest into reality.&#xa;.&#xa;Mentals: ( 30c) This is a good remedy for people who are recovering from illness, going through a life change, or coming out of a serious shock. It is suited to spiritually involved people who do not live or think in a practical, rhythmic manner, people who are very detached from reality. It helps the dreamers, and indolent to refocus their attention on the world around them.&#xa;Contraindications: This remedy is not to be used where there is suppressed rage or anxiety. If you have a serious medical condition, please consult your doctor immediately.&#xa; &#xa;Physicals: ( 6x, 12c)This remedy can raise blood pressure and is good for weak , lifeless people who are exhausted, have poor circulation, are constipated, have piles, or varicose veins, rectal or urinary problems, are going into childbirth, are anemic, weak, slow to recuperate or immuno-deficient.&#xa;Contraindications: This remedy is not to be used for people with high blood pressure or who are violent or aggressive by nature. Do not repeat often and keep the potency below 30c.  If you have one of these conditions listed above see your doctor.&#xa; &#xa;General Symptoms: Red addresses a lack of connection with physical life. It works well for people who feel they are disconnected from family, community, and tribe. It brings one back to life with a greater certainty about one&#x2019;s purpose and place. It helps foster patience to let one&#x2019;s good unfold, it can be used for deep depression, even suicidal tendencies.&#xa;Contraindications: Do not give to people who have a history of violence. Consult a doctor for physical and mental conditions that may need treatment." ID="ID_53458703" CREATED="1341225462706" MODIFIED="1341225464661"/>
</node>
<node TEXT="T&#xe9;mata" POSITION="left" ID="ID_1269744966" CREATED="1341225415453" MODIFIED="1341225425703" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="nadbytek" ID="ID_1268926563" CREATED="1341236703979" MODIFIED="1341236707437">
<font BOLD="true"/>
<node TEXT="podr&#xe1;&#x17e;d&#x11b;n&#xed;" ID="ID_563264233" CREATED="1341236734527" MODIFIED="1341236739099"/>
<node TEXT="zlost" ID="ID_140337440" CREATED="1341236739484" MODIFIED="1341236741130"/>
<node TEXT="pocit t&#xed;hy v t&#x11b;le" ID="ID_859952813" CREATED="1341236741946" MODIFIED="1341236747570"/>
<node TEXT="p&#x159;&#xed;li&#x161; p&#x159;ipoutan&#xfd;, materi&#xe1;ln&#xed;" ID="ID_1440213352" CREATED="1341237381022" MODIFIED="1341237394802"/>
<node TEXT="nedok&#xe1;&#x17e;&#xed; distribuovat energii" ID="ID_821037135" CREATED="1341237395219" MODIFIED="1341237404678"/>
</node>
<node TEXT="p&#x159;&#xed;&#x10d;iny vy&#x10d;erp&#xe1;n&#xed;" ID="ID_1407052269" CREATED="1341237318296" MODIFIED="1341237378528">
<font BOLD="true"/>
<node TEXT="drastick&#xe9; zm&#x11b;ny" ID="ID_384173865" CREATED="1341237346999" MODIFIED="1341237354748">
<node TEXT="rozvod" ID="ID_1863124881" CREATED="1341237325384" MODIFIED="1341237327930"/>
<node TEXT="v&#xe1;lka" ID="ID_504307415" CREATED="1341237328291" MODIFIED="1341237330145"/>
<node TEXT="b&#xed;da" ID="ID_126798521" CREATED="1341237330434" MODIFIED="1341237332689"/>
<node TEXT="str&#xe1;d&#xe1;n&#xed;" ID="ID_863381055" CREATED="1341237334244" MODIFIED="1341237337155"/>
<node TEXT="zneu&#x17e;it&#xed;" ID="ID_171991171" CREATED="1341237337404" MODIFIED="1341237340362"/>
</node>
</node>
<node TEXT="dostatek" ID="ID_1716600993" CREATED="1341236880776" MODIFIED="1341236909437">
<font BOLD="true"/>
<node TEXT="schopnost &#x159;e&#x161;it &#x17e;ivot" ID="ID_1020201148" CREATED="1341236883624" MODIFIED="1341236889918"/>
<node TEXT="sebez&#xe1;chovn&#xe1;" ID="ID_1564043235" CREATED="1341236910481" MODIFIED="1341236916051"/>
<node TEXT="schopnost &#x17e;&#xed;t beze strachu a v p&#x159;&#xed;tomnosti" ID="ID_76729904" CREATED="1341236971239" MODIFIED="1341236983960"/>
</node>
<node TEXT="nedostatek" ID="ID_1569669181" CREATED="1341236821563" MODIFIED="1341236859012">
<font BOLD="true"/>
<node TEXT="unaven&#xfd; &#x17e;ivotem" ID="ID_1084770100" CREATED="1341236824929" MODIFIED="1341236831661"/>
<node TEXT="tence se vzd&#xe1;vat" ID="ID_194210369" CREATED="1341236832014" MODIFIED="1341236838830"/>
<node TEXT="nezako&#x159;en&#x11b;n&#xfd;" ID="ID_934600461" CREATED="1341237092548" MODIFIED="1341237096929"/>
<node TEXT="nem&#xe1; trp&#x11b;livost" ID="ID_1756299112" CREATED="1341237097250" MODIFIED="1341237102805"/>
<node TEXT="nedok&#xe1;&#x17e;e pro&#x17e;&#xed;vat emoce" ID="ID_281901490" CREATED="1341237103054" MODIFIED="1341237110048"/>
<node TEXT="c&#xed;t&#xed; se jako ob&#x11b;&#x165;" ID="ID_627549796" CREATED="1341237110313" MODIFIED="1341237118394"/>
<node TEXT="nevid&#xed; c&#xed;l ve sv&#xe9;m &#x17e;ivot&#x11b;" ID="ID_1427257511" CREATED="1341237118668" MODIFIED="1341237127370">
<node TEXT="deprese" ID="ID_1136654620" CREATED="1341237128530" MODIFIED="1341237130794"/>
</node>
<node TEXT="&#xfa;zkost o &#x17e;ivot" ID="ID_512242769" CREATED="1341237132919" MODIFIED="1341237138006"/>
<node TEXT="sebevra&#x17e;edn&#xe9; sklony" ID="ID_845718773" CREATED="1341237138279" MODIFIED="1341237145525"/>
<node TEXT="dlouhodob&#xfd; pocit ne&#x161;t&#x11b;st&#xed;" ID="ID_1382789454" CREATED="1341237148586" MODIFIED="1341237156463"/>
<node TEXT="nedostatek vit&#xe1;ln&#xed;ho tepla" ID="ID_22304209" CREATED="1341237230722" MODIFIED="1341237239212"/>
<node TEXT="studen&#xe9; ruce a nohy" ID="ID_1999248191" CREATED="1341237242097" MODIFIED="1341237246772"/>
<node TEXT="slabost" ID="ID_2351567" CREATED="1341237246981" MODIFIED="1341237249254"/>
<node TEXT="nejistota" ID="ID_1186077180" CREATED="1341237249503" MODIFIED="1341237251588"/>
<node TEXT="pomalost" ID="ID_1723633893" CREATED="1341237252437" MODIFIED="1341237254656"/>
<node TEXT="n&#xed;zk&#xe1; hladina energie" ID="ID_131218397" CREATED="1341237256719" MODIFIED="1341237264260"/>
<node TEXT="chyb&#xed; ukotven&#xed;" ID="ID_1903344434" CREATED="1341237283146" MODIFIED="1341237287778">
<node TEXT="p&#x159;&#xed;li&#x161;n&#xfd; z&#xe1;jem o duchovno" ID="ID_594732663" CREATED="1341237288731" MODIFIED="1341237297964"/>
</node>
</node>
<node TEXT="d&#xe1;v&#xe1;" ID="ID_1758132438" CREATED="1341236841939" MODIFIED="1341236859661">
<font BOLD="true"/>
<node TEXT="&#x17e;ivotn&#xed; energii" ID="ID_924438672" CREATED="1341236844389" MODIFIED="1341236848458"/>
<node TEXT="pozitivita" ID="ID_349483546" CREATED="1341236892542" MODIFIED="1341236895121"/>
<node TEXT="schopnost prosadit se" ID="ID_1480894806" CREATED="1341236895378" MODIFIED="1341236901958"/>
<node TEXT="sebep&#x159;ijet&#xed;" ID="ID_1485618363" CREATED="1341236918741" MODIFIED="1341236923251"/>
<node TEXT="pocit nez&#xe1;vislosti a sebesvobody" ID="ID_1280867259" CREATED="1341236924810" MODIFIED="1341236934469"/>
<node TEXT="schopnost postarat se o sebe" ID="ID_1519438432" CREATED="1341236939807" MODIFIED="1341236946575">
<node TEXT="zajistit si z&#xe1;kladn&#xed; pot&#x159;eby" ID="ID_907619914" CREATED="1341236949741" MODIFIED="1341236960000" COLOR="#999999">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="schopnost dot&#xe1;hnout v&#x11b;ci do konce" ID="ID_398788461" CREATED="1341237007107" MODIFIED="1341237017513"/>
<node TEXT="nevzd&#xe1;vat se/to" ID="ID_1851277565" CREATED="1341237018840" MODIFIED="1341237027338"/>
<node TEXT="d&#x16f;v&#x11b;ra v &#x17e;ivot" ID="ID_1060506925" CREATED="1341237028058" MODIFIED="1341237033409"/>
<node TEXT="schopnost prom&#x11b;nit sny v realitu" ID="ID_1692465932" CREATED="1341237033650" MODIFIED="1341237042938"/>
<node TEXT="naplnit sv&#x16f;j sud" ID="ID_866249383" CREATED="1341237045696" MODIFIED="1341237052240"/>
<node TEXT="&#x159;&#xe1;d po&#x159;&#xe1;dek" ID="ID_1199758919" CREATED="1341237160920" MODIFIED="1341237165971"/>
<node TEXT="strukturu" ID="ID_1776245140" CREATED="1341237166228" MODIFIED="1341237171896"/>
<node TEXT="d&#x16f;slednost" ID="ID_536232972" CREATED="1341237172273" MODIFIED="1341237175410"/>
<node TEXT="pravidelnost" ID="ID_1605689066" CREATED="1341237175668" MODIFIED="1341237178722"/>
<node TEXT="stabilitu" ID="ID_220562380" CREATED="1341237179011" MODIFIED="1341237183439"/>
<node TEXT="jistotu" ID="ID_59999510" CREATED="1341237184327" MODIFIED="1341237186432"/>
<node TEXT="pocit bezpe&#x10d;&#xed;" ID="ID_28697247" CREATED="1341237186673" MODIFIED="1341237190381"/>
<node TEXT="schopnost zachovat klid" ID="ID_1227372897" CREATED="1341237194488" MODIFIED="1341237199845"/>
<node TEXT="pom&#xe1;h&#xe1; rozb&#xed;t destruktivn&#xed; vzorce chov&#xe1;n&#xed;" ID="ID_1314358069" CREATED="1341237714608" MODIFIED="1341237728347"/>
</node>
<node TEXT="realita" ID="ID_810957691" CREATED="1341236852188" MODIFIED="1341236854477"/>
<node TEXT="p&#x159;&#xed;tomnost" ID="ID_382306417" CREATED="1341236860530" MODIFIED="1341236864131"/>
<node TEXT="hmota" ID="ID_1535559840" CREATED="1341236864715" MODIFIED="1341236866593"/>
<node TEXT="v&#xe1;&#x161;e&#x148;" ID="ID_21206647" CREATED="1341236771106" MODIFIED="1341236774306"/>
<node TEXT="touha" ID="ID_100700413" CREATED="1341236774523" MODIFIED="1341236776514"/>
<node TEXT="n&#xe1;sil&#xed;" ID="ID_1781224138" CREATED="1341236776746" MODIFIED="1341236779110"/>
<node TEXT="komunita" ID="ID_265216614" CREATED="1341236990764" MODIFIED="1341236993666"/>
<node TEXT="klan" ID="ID_22613452" CREATED="1341236994050" MODIFIED="1341236995475"/>
<node TEXT="n&#xe1;rod" ID="ID_1887138425" CREATED="1341236995724" MODIFIED="1341236997762"/>
<node TEXT="pohlcen&#xed; v duchovn&#xed;ch oblastech, kte&#x159;&#xed; zavrhuj&#xed; fyzi&#x10d;no" ID="ID_939624137" CREATED="1341237625878" MODIFIED="1341237645399"/>
<node TEXT="sn&#xed;lkov&#xe9;" ID="ID_962945345" CREATED="1341237645648" MODIFIED="1341237648590"/>
<node TEXT="lhostejn&#xed;" ID="ID_278126875" CREATED="1341237648839" MODIFIED="1341237652411"/>
<node TEXT="neschopn&#xed; pracovat a postarat se o sebe" ID="ID_334201691" CREATED="1341237652812" MODIFIED="1341237664004"/>
<node TEXT="odevzd&#xe1;vaj&#xed; energii druh&#xfd;m a maj&#xed; pocit, &#x17e;e nemaj&#xed; pr&#xe1;vo na &#x17e;ivot" ID="ID_1663827909" CREATED="1341237664741" MODIFIED="1341237686066"/>
<node TEXT="pocit zotro&#x10d;en&#xed;" ID="ID_778379938" CREATED="1341237686650" MODIFIED="1341237694336"/>
<node TEXT="pocit nesvobody" ID="ID_1116458366" CREATED="1341237694689" MODIFIED="1341237701999"/>
<node TEXT="d&#x11b;ti" ID="ID_1994675229" CREATED="1341237735974" MODIFIED="1341237739187">
<node TEXT="odlo&#x17e;en&#xe9;" ID="ID_1620843297" CREATED="1341237739988" MODIFIED="1341237743144"/>
<node TEXT="adoptovan&#xe9;" ID="ID_1277583387" CREATED="1341237743401" MODIFIED="1341237748640"/>
<node TEXT="zneu&#x17e;&#xed;van&#xe9;" ID="ID_1923413646" CREATED="1341237748838" MODIFIED="1341237753140"/>
</node>
</node>
<node TEXT="Souvisej&#xed;c&#xed; l&#xe9;ky" POSITION="left" ID="ID_870899867" CREATED="1341226259537" MODIFIED="1341226266640" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Calc" ID="ID_764053875" CREATED="1341237209817" MODIFIED="1341237212460"/>
<node TEXT="Kalia" ID="ID_1129106508" CREATED="1341237213908" MODIFIED="1341237220281"/>
<node TEXT="Ferr" ID="ID_1248221276" CREATED="1341238059017" MODIFIED="1341238060927"/>
<node TEXT="Cinabaris" ID="ID_81319630" CREATED="1341238061447" MODIFIED="1341238064042"/>
<node TEXT="Hell" ID="ID_1154097800" CREATED="1341238064403" MODIFIED="1341238068283"/>
<node TEXT="Bry" ID="ID_81746933" CREATED="1341238068668" MODIFIED="1341238070428"/>
<node TEXT="Arn" ID="ID_526540147" CREATED="1341238070717" MODIFIED="1341238072331"/>
<node TEXT="Acon" ID="ID_1768413419" CREATED="1341238085474" MODIFIED="1341238089485"/>
<node TEXT="Carb-v" ID="ID_380569737" CREATED="1341238103651" MODIFIED="1341238109646"/>
<node TEXT="Carb-a" ID="ID_578500319" CREATED="1341238110077" MODIFIED="1341238114740"/>
<node TEXT="Merc" ID="ID_601228308" CREATED="1341238115132" MODIFIED="1341238118070"/>
<node TEXT="Flouridy" ID="ID_1626299970" CREATED="1341238119206" MODIFIED="1341238123336"/>
<node TEXT="Berberis" ID="ID_1079347447" CREATED="1341238123761" MODIFIED="1341238126006"/>
<node TEXT="Bell" ID="ID_1449745981" CREATED="1341238129706" MODIFIED="1341238131059"/>
<node TEXT="Nux-v" ID="ID_362789805" CREATED="1341238152316" MODIFIED="1341238154967"/>
<node TEXT="Lach" ID="ID_1113891523" CREATED="1341238159042" MODIFIED="1341238163466"/>
<node TEXT="Naja" ID="ID_640887514" CREATED="1341238164242" MODIFIED="1341238165481"/>
<node TEXT="pavouci" ID="ID_1788228338" CREATED="1341238172496" MODIFIED="1341238175072"/>
<node TEXT="&#x161;korpioni" ID="ID_1615211711" CREATED="1341238178581" MODIFIED="1341238182352"/>
</node>
</node>
</map>
