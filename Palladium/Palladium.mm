<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Palladium" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1358066158114"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Podobn&#xe9; l&#xe9;ky" STYLE_REF="Subtopic" POSITION="left" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1357168625505">
<node TEXT="Plat" ID="ID_656130709" CREATED="1358065311595" MODIFIED="1358066664169">
<node TEXT="pov&#xfd;&#x161;en&#xfd; t&#xed;m, kdo jsem j&#xe1;" STYLE_REF="Differs" ID="ID_787318019" CREATED="1358065324040" MODIFIED="1358065336015"/>
<node TEXT="bude si sna&#x17e;it pozornost vynutit" STYLE_REF="Differs" ID="ID_318327427" CREATED="1358066395500" MODIFIED="1358066428416">
<node TEXT="Pall bude hledat ocen&#x11b;n&#xed; jinde" STYLE_REF="Comment" ID="ID_449130549" CREATED="1358066435371" MODIFIED="1358066454367"/>
</node>
</node>
<node TEXT="Lyc" ID="ID_1490733358" CREATED="1358066738638" MODIFIED="1358066740477">
<node TEXT="intriky" STYLE_REF="Differs" ID="ID_753674511" CREATED="1358066948243" MODIFIED="1358066955597"/>
<node TEXT="strategie" STYLE_REF="Differs" ID="ID_399183240" CREATED="1358066963167" MODIFIED="1358066966988"/>
</node>
<node TEXT="Puls" ID="ID_1035595732" CREATED="1358066743765" MODIFIED="1358066746123">
<node TEXT="chce b&#xfd;t chv&#xe1;len" STYLE_REF="Same" ID="ID_922871031" CREATED="1358066834292" MODIFIED="1358066867380">
<node TEXT="pot&#x159;ebuje pocit jistoty v rodin&#x11b;" STYLE_REF="Comment" ID="ID_1192373855" CREATED="1358066868848" MODIFIED="1358066884610"/>
</node>
</node>
<node TEXT="Carc" ID="ID_114864511" CREATED="1358066790732" MODIFIED="1358066792793">
<node TEXT="chce b&#xfd;t chv&#xe1;len" STYLE_REF="Same" ID="ID_793556952" CREATED="1358066793730" MODIFIED="1358066805789">
<node TEXT="nikdo ho nikdy nepochv&#xe1;lil" STYLE_REF="Comment" ID="ID_925879942" CREATED="1358066808678" MODIFIED="1358066885775"/>
</node>
</node>
<node TEXT="Clac-S" ID="ID_1145203957" CREATED="1358070728867" MODIFIED="1358070739123"/>
</node>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="left" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1357168653304">
<node TEXT="10. perioda" ID="ID_1748992019" CREATED="1358065272075" MODIFIED="1358066699316" STYLE="fork">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</node>
<node TEXT="pot&#x159;eba b&#xfd;t chv&#xe1;len" ID="ID_1675377710" CREATED="1358065668996" MODIFIED="1358068850111">
<font BOLD="true"/>
<node TEXT="je na tom z&#xe1;visl&#xfd;" ID="ID_1288750658" CREATED="1358066102129" MODIFIED="1358066106342"/>
<node TEXT="kdy&#x17e; to nem&#xe1;, vznik&#xe1; pocit opu&#x161;t&#x11b;nosti a osam&#x11b;losti" ID="ID_1705170122" CREATED="1358066126782" MODIFIED="1358066143083"/>
<node TEXT="miluje moc" ID="ID_1988254157" CREATED="1358067847187" MODIFIED="1358067852817"/>
<node TEXT="a&#x17e; d&#x11b;tinsky" ID="ID_50489710" CREATED="1358068045245" MODIFIED="1358068053118">
<node TEXT="sm&#x11b;&#x161;n&#x11b;" STYLE_REF="Comment" ID="ID_619442216" CREATED="1358068055980" MODIFIED="1358068060479"/>
</node>
</node>
<node TEXT="pot&#x159;ebuje skupinu (publikum)" ID="ID_1989355210" CREATED="1358065676590" MODIFIED="1358065687368">
<node TEXT="ale m&#xe1; m&#xe1;lo p&#x159;&#xe1;tel" ID="ID_1315731517" CREATED="1358067774013" MODIFIED="1358067782350"/>
</node>
<node TEXT="kdy&#x17e; m&#x11b; nebudete chv&#xe1;lit, tak p&#x16f;jdu jinam, kde budou" ID="ID_547200363" CREATED="1358066995465" MODIFIED="1358067019280"/>
<node TEXT="n&#x11b;kdy py&#x161;n&#xfd;" ID="ID_122375996" CREATED="1358065841995" MODIFIED="1358068682255">
<node TEXT="m&#x16f;&#x17e;e p&#x16f;sobit dikt&#xe1;torsky" ID="ID_1386086385" CREATED="1358067725245" MODIFIED="1358067744454"/>
</node>
<node TEXT="v&#x11b;&#x159;&#xed; si" ID="ID_1111038944" CREATED="1358068684593" MODIFIED="1358068829741">
<font BOLD="true"/>
</node>
<node TEXT="je odd&#x11b;len i t&#xed;m, &#x17e;e pot&#x159;euje b&#xfd;t nejlep&#x161;&#xed;" ID="ID_970768158" CREATED="1358066241058" MODIFIED="1358066267171"/>
<node TEXT="averze ke spole&#x10d;nosti" ID="ID_831115269" CREATED="1358066312042" MODIFIED="1358066317708">
<node TEXT="pot&#x159;eba b&#xfd;t s&#xe1;m aby se dobil" ID="ID_1973993956" CREATED="1358066318828" MODIFIED="1358066332888"/>
<node TEXT="spole&#x10d;nost, kter&#xe1; ho nevyzdvihuje" ID="ID_1745494727" CREATED="1358066334103" MODIFIED="1358066346402"/>
</node>
<node TEXT="pov&#xfd;&#x161;en&#xfd; t&#xed;m, co um&#xed;m" LOCALIZED_STYLE_REF="default" ID="ID_643443154" CREATED="1358065340210" MODIFIED="1358066086324">
<node TEXT="Pod&#xed;vejte co j&#xe1; um&#xed;m, proto jsem lep&#x161;&#xed;" STYLE_REF="Comment" ID="ID_209260759" CREATED="1358065399626" MODIFIED="1358065424816"/>
</node>
<node TEXT="jednoduchost" ID="ID_945958276" CREATED="1358066930263" MODIFIED="1358066939627"/>
<node TEXT="p&#x159;&#xed;most" ID="ID_1580853043" CREATED="1358066940124" MODIFIED="1358066943141"/>
<node TEXT="&#x17e;&#xe1;rlivost" ID="ID_1106544655" CREATED="1358067089792" MODIFIED="1358067095776">
<node TEXT="kdy&#x17e; je chv&#xe1;len n&#x11b;kdo jin&#xfd;" ID="ID_1959835815" CREATED="1358067096761" MODIFIED="1358067110956"/>
</node>
<node TEXT="v&#x161;echny okouzl&#xed;" ID="ID_949795422" CREATED="1358069486837" MODIFIED="1358069491999">
<node TEXT="dob&#x159;e vypad&#xe1;" ID="ID_1677959644" CREATED="1358067119737" MODIFIED="1358067124699"/>
<node TEXT="p&#x16f;sob&#xed; p&#x159;&#xe1;telsky, p&#x159;&#xed;v&#x11b;tiv&#x11b;" ID="ID_1142944210" CREATED="1358067125691" MODIFIED="1358067142384">
<node TEXT="nen&#xed; co vytknout" ID="ID_1285190569" CREATED="1358067142897" MODIFIED="1358067148626"/>
<node TEXT="chce tak p&#x16f;sobit, aby si z&#xed;skal lidi" ID="ID_1450551567" CREATED="1358067497241" MODIFIED="1358068837510">
<font BOLD="true"/>
</node>
</node>
<node TEXT="charisma" ID="ID_1558508653" CREATED="1358067116777" MODIFIED="1358067119464"/>
<node TEXT="&#x10d;asto je obdivov&#xe1;n" ID="ID_1471944406" CREATED="1358065980300" MODIFIED="1358065988952"/>
</node>
<node TEXT="nepochybuje o sob&#x11b;" ID="ID_527571069" CREATED="1358067162132" MODIFIED="1358067168095"/>
<node TEXT="citliv&#xfd; na pochvalu" ID="ID_1171854260" CREATED="1358067371070" MODIFIED="1358067377378">
<node TEXT="snadno se uraz&#xed;" ID="ID_574466160" CREATED="1358067378881" MODIFIED="1358067393352"/>
<node TEXT="netrp&#x11b;liv&#xfd;" ID="ID_1799937562" CREATED="1358067408337" MODIFIED="1358067412889"/>
</node>
<node TEXT="nechce p&#x16f;sobit namy&#x161;len&#x11b;, bude si to u&#x17e;&#xed;vat vnit&#x159;n&#x11b;" ID="ID_496591610" CREATED="1358067474756" MODIFIED="1358067492599"/>
<node TEXT="&#x10d;lov&#x11b;k kter&#xfd; za&#x17e;il pon&#xed;&#x17e;en&#xed; nebo z chud&#xfd;ch pom&#x11b;r&#x16f;" ID="ID_1833937385" CREATED="1358067787082" MODIFIED="1358067810378"/>
<node TEXT="kritika ho poni&#x17e;uje" ID="ID_74460892" CREATED="1358067923828" MODIFIED="1358067938667">
<node TEXT="ut&#xed;k&#xe1; od n&#xed;" ID="ID_208186579" CREATED="1358067939604" MODIFIED="1358067943706"/>
</node>
</node>
<node TEXT="Paladium" STYLE_REF="Remedy" POSITION="right" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1358066282011"/>
<node TEXT="Pall" STYLE_REF="Abbrev" POSITION="right" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1358066184084"/>
<node TEXT="Miazma" STYLE_REF="Subtopic" POSITION="right" ID="ID_31190142" CREATED="1357168609677" MODIFIED="1357168614539">
<node TEXT="Sykotick&#xe9;" ID="ID_584480872" CREATED="1358066284314" MODIFIED="1358066287817"/>
</node>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="right" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389">
<node TEXT="kov" ID="ID_1643951563" CREATED="1358066890196" MODIFIED="1358066891614"/>
</node>
<node TEXT="Trituace" STYLE_REF="Subtopic" POSITION="right" ID="ID_1275465186" CREATED="1357168572522" MODIFIED="1357168576351">
<node TEXT="z&#xe1;vra&#x165;" ID="ID_677079432" CREATED="1358069696791" MODIFIED="1358069709459">
<font ITALIC="true"/>
</node>
<node TEXT="to&#x10d;en&#xed; hlavy" ID="ID_1354208642" CREATED="1358069702981" MODIFIED="1358069711282">
<font ITALIC="true"/>
</node>
<node TEXT="zima" ID="ID_816512119" CREATED="1358070618528" MODIFIED="1358070629463"/>
<node TEXT="bu&#x161;en&#xed; srdce" ID="ID_1817216291" CREATED="1358070673599" MODIFIED="1358070677644"/>
<node TEXT="tr&#xe9;ma" ID="ID_1864419569" CREATED="1358070690164" MODIFIED="1358070693622">
<node TEXT="motiva&#x10d;n&#xed;" ID="ID_1826755798" CREATED="1358070716993" MODIFIED="1358070720916"/>
</node>
<node TEXT="&#xfa;zkost" ID="ID_952724859" CREATED="1358070682070" MODIFIED="1358070687415"/>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="right" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<node TEXT="pohlavn&#xed; org&#xe1;ny" ID="ID_1276772242" CREATED="1358069231199" MODIFIED="1358069391286">
<node TEXT="vaje&#x10d;n&#xed;ky" ID="ID_1751904753" CREATED="1358069224271" MODIFIED="1358069229033">
<node TEXT="cysty" ID="ID_227079645" CREATED="1358069303366" MODIFIED="1358069306881"/>
<node TEXT="karcinom" ID="ID_234525536" CREATED="1358069307568" MODIFIED="1358069324966"/>
</node>
<node TEXT="v&#xfd;toky" ID="ID_1529697647" CREATED="1358070380349" MODIFIED="1358070383216"/>
<node TEXT="z&#xe1;n&#x11b;ty" ID="ID_1376491658" CREATED="1358070383449" MODIFIED="1358070386484"/>
<node TEXT="prostata" ID="ID_907572240" CREATED="1358070510151" MODIFIED="1358070512582"/>
<node TEXT="poruchy erekce" ID="ID_519695573" CREATED="1358070512959" MODIFIED="1358070517679"/>
<node TEXT="varlata" ID="ID_1893072531" CREATED="1358070517968" MODIFIED="1358070521478"/>
<node TEXT="siln&#xe1; MS" ID="ID_927200761" CREATED="1358070562557" MODIFIED="1358070565761"/>
</node>
<node TEXT="bolesti hlavy" ID="ID_716350528" CREATED="1358070373657" MODIFIED="1358070378715"/>
<node TEXT="hluchota" ID="ID_41589318" CREATED="1358070424097" MODIFIED="1358070438493">
<node TEXT="strana na kter&#xe9; le&#x17e;&#xed;" ID="ID_1402068555" CREATED="1358070467907" MODIFIED="1358070473756"/>
</node>
<node TEXT="krk" ID="ID_1901313095" CREATED="1358070474818" MODIFIED="1358070477395">
<node TEXT="hlasivky" ID="ID_377238519" CREATED="1358070478276" MODIFIED="1358070480970"/>
<node TEXT="nachlazen&#xed;" ID="ID_647266100" CREATED="1358070481219" MODIFIED="1358070483920"/>
<node TEXT="dr&#xe1;&#x17e;d&#x11b;n&#xed; v hrdle" ID="ID_723209027" CREATED="1358070497942" MODIFIED="1358070506044"/>
</node>
<node TEXT="pl&#xed;ce" ID="ID_1114015376" CREATED="1358070486077" MODIFIED="1358070487763">
<node TEXT="astma" ID="ID_1616416504" CREATED="1358070488596" MODIFIED="1358070490122"/>
</node>
<node TEXT="z&#xe1;da" ID="ID_1879747945" CREATED="1358070527613" MODIFIED="1358070534326">
<node TEXT="bolesti" ID="ID_1395419260" CREATED="1358070535071" MODIFIED="1358070537352"/>
<node TEXT="ztuhlost" ID="ID_489526382" CREATED="1358070537568" MODIFIED="1358070540192"/>
</node>
<node TEXT="k&#x16f;&#x17e;e" ID="ID_431222698" CREATED="1358070546236" MODIFIED="1358070549082">
<node TEXT="ekz&#xe9;my" ID="ID_1810846921" CREATED="1358070549859" MODIFIED="1358070553108"/>
<node TEXT="sv&#x11b;d&#x11b;n&#xed;" ID="ID_1500335138" CREATED="1358070594404" MODIFIED="1358070597276">
<node TEXT="st&#x11b;hovav&#xe9;" ID="ID_1862205411" CREATED="1358070600650" MODIFIED="1358070604116"/>
</node>
<node TEXT="bradavi&#x10d;ky na kloubech ruky" ID="ID_1934651481" CREATED="1358070606209" MODIFIED="1358070615244"/>
</node>
<node TEXT="zimoum&#x159;iv&#xfd;" ID="ID_1742704709" CREATED="1358070568288" MODIFIED="1358070572082"/>
<node TEXT="pocit zv&#x11b;t&#x161;en&#xe9;ho t&#x11b;la" ID="ID_83736016" CREATED="1358070662274" MODIFIED="1358070671641"/>
</node>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="right" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690">
<node TEXT="" STYLE_REF="Agg" ID="ID_752495444" CREATED="1358070574850" MODIFIED="1358070576030">
<node TEXT="chlad" ID="ID_1092554918" CREATED="1358070579847" MODIFIED="1358070582539"/>
<node TEXT="vlhko" ID="ID_1503613406" CREATED="1358070582819" MODIFIED="1358070584960"/>
</node>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="right" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729"/>
</node>
</map>
