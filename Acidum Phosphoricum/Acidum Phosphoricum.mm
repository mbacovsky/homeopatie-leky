<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="     Acidum&#xa;Phosphoricum" ID="ID_1518478132" CREATED="1339328352510" MODIFIED="1395529319826" LINK="../../2.%20rocnik/2r-10.mm" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Ph-ac" POSITION="right" ID="ID_39383744" CREATED="1339328722903" MODIFIED="1395529337508" COLOR="#996600">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="kyselina fosfore&#x10d;n&#xe1;" POSITION="right" ID="ID_10572950" CREATED="1339328729570" MODIFIED="1339328745162" COLOR="#999900">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Trituace" POSITION="right" ID="ID_1681486672" CREATED="1339328748850" MODIFIED="1339328768919" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="odevzdanost" ID="ID_1511230170" CREATED="1339328752743" MODIFIED="1339328756378"/>
<node TEXT="lennost" ID="ID_1366660377" CREATED="1339328756870" MODIFIED="1339328758616"/>
<node TEXT="neklid" ID="ID_1086299883" CREATED="1339328758873" MODIFIED="1339328955513">
<font ITALIC="true"/>
<node TEXT="chv&#x11b;n&#xed;" ID="ID_630274564" CREATED="1339328980171" MODIFIED="1339328986102">
<font ITALIC="true"/>
</node>
</node>
<node TEXT="pasivita" ID="ID_95942463" CREATED="1339328990588" MODIFIED="1339328993491"/>
<node TEXT="stav vy&#x10d;erpan&#xe9;ho fosforu" ID="ID_144294274" CREATED="1339328996799" MODIFIED="1339329012461"/>
<node TEXT="tlak v b&#x159;i&#x161;e" ID="ID_919401808" CREATED="1339328760614" MODIFIED="1339328764530"/>
<node TEXT="nechce se mi zvedat ze &#x17e;idle" ID="ID_134418066" CREATED="1339328878895" MODIFIED="1339328892585"/>
<node TEXT="nechce se mi mluvit" ID="ID_495383610" CREATED="1339328894136" MODIFIED="1339328900965"/>
<node TEXT="p&#xe1;liv&#xe1; bolest v &#x10d;ele" ID="ID_297481843" CREATED="1339328902077" MODIFIED="1339328929820">
<font ITALIC="true"/>
</node>
<node TEXT="bolest za krkem" ID="ID_259549084" CREATED="1339328930338" MODIFIED="1339328936780">
<font ITALIC="true"/>
</node>
</node>
<node TEXT="Podobn&#xe9; l&#xe9;ky" POSITION="right" ID="ID_319713485" CREATED="1339329209289" MODIFIED="1339329216295" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="PHOS" ID="ID_1590427677" CREATED="1339329217095" MODIFIED="1339329219212">
<node TEXT="ve stavu vy&#x10d;erp&#xe1;n&#xed;" ID="ID_856776475" CREATED="1339329222449" MODIFIED="1339329233705" COLOR="#999999">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="rychle regeneruje" ID="ID_1485796713" CREATED="1339329234583" MODIFIED="1339329251813">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="hodiny a&#x17e; dny" ID="ID_1805870730" CREATED="1339329254440" MODIFIED="1339329263618" COLOR="#999999">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="nikoho nikam netla&#x10d;&#xed;" ID="ID_281582063" CREATED="1339329456282" MODIFIED="1339329466971">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="v&#x11b;t&#x161;&#xed; sn&#xed;lek" ID="ID_596928117" CREATED="1339330151857" MODIFIED="1339330158922">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node TEXT="IGN" ID="ID_1633103638" CREATED="1339330785871" MODIFIED="1339330789580">
<node TEXT="na zran&#x11b;n&#xed; reaguje emocemi" ID="ID_763215034" CREATED="1339330791307" MODIFIED="1339330827921">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node TEXT="NAT-M" ID="ID_1905420339" CREATED="1339330803977" MODIFIED="1339330810548">
<node TEXT="Na zran&#x11b;n&#xed; reaguje sta&#x17e;en&#xed;m se" ID="ID_818933402" CREATED="1339330812683" MODIFIED="1339330830788">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node TEXT="Symptomy" POSITION="right" ID="ID_1052233206" CREATED="1339331452528" MODIFIED="1339331500277" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="bolesti" ID="ID_814342226" CREATED="1339331456405" MODIFIED="1339331458949">
<node TEXT="hlavy" ID="ID_1753985182" CREATED="1339331460533" MODIFIED="1339331462578">
<node TEXT="t&#xed;ha na temeni z nervov&#xe9;ho vy&#x10d;erp&#xe1;n&#xed;, ze z&#xe1;rmutku" ID="ID_787409422" CREATED="1339331467537" MODIFIED="1339331494153"/>
</node>
<node TEXT="zad" ID="ID_1830987148" CREATED="1339331502439" MODIFIED="1339331505755"/>
<node TEXT="kon&#x10d;etin" ID="ID_969273481" CREATED="1339331506339" MODIFIED="1339331509593">
<node TEXT="r&#x16f;stov&#xe9; bolesti" ID="ID_1369204165" CREATED="1339331510906" MODIFIED="1339331517187"/>
<node TEXT="dloubav&#xe9; bolesti kon&#x10d;etinov&#xfd;ch nerv&#x16f;" ID="ID_744559521" CREATED="1339331525828" MODIFIED="1339331538640"/>
</node>
</node>
<node TEXT="pr&#x16f;jem" ID="ID_872503406" CREATED="1339331543158" MODIFIED="1339331547151">
<node TEXT="nebolestiv&#xfd;" ID="ID_1358173572" CREATED="1339331548000" MODIFIED="1339331551374"/>
<node TEXT="hojn&#xfd;" ID="ID_541083658" CREATED="1339331551903" MODIFIED="1339331553589"/>
<node TEXT="&#x17e;lut&#xe1; nebo b&#xed;l&#xe1; barva" ID="ID_1433660812" CREATED="1339331553766" MODIFIED="1339331566876"/>
</node>
<node TEXT="mo&#x10d;" ID="ID_570728094" CREATED="1339331568042" MODIFIED="1339331569803">
<node TEXT="hojn&#xe1;" ID="ID_1074297773" CREATED="1339331570621" MODIFIED="1339331572722"/>
<node TEXT="ml&#xe9;&#x10d;n&#xe1;" ID="ID_201802613" CREATED="1339331573275" MODIFIED="1339331576923"/>
</node>
<node TEXT="pocen&#xed;" ID="ID_1010632377" CREATED="1339331581724" MODIFIED="1339331583967">
<node TEXT="hojn&#xe9;" ID="ID_1674807403" CREATED="1339331584824" MODIFIED="1339331588410"/>
<node TEXT="v noci a r&#xe1;no" ID="ID_188664334" CREATED="1339331588715" MODIFIED="1339331593638"/>
</node>
<node TEXT="vlasy a ochlupen&#xed;" ID="ID_1654388170" CREATED="1339331601441" MODIFIED="1339331610316">
<node TEXT="vypad&#xe1;v&#xe1;n&#xed;" ID="ID_532844486" CREATED="1339331611006" MODIFIED="1339331614342"/>
</node>
<node TEXT="vy&#x10d;erp&#xe1;n&#xed;" ID="ID_1431785654" CREATED="1339331616682" MODIFIED="1339331620597">
<node TEXT="po infekci" ID="ID_618058009" CREATED="1339331621493" MODIFIED="1339331631358"/>
<node TEXT="chronick&#xfd; &#xfa;navov&#xfd; syndrom" ID="ID_1249251320" CREATED="1339331632534" MODIFIED="1339331644045"/>
<node TEXT="p&#x159;ehnan&#x11b; citliv&#xfd; na sv&#x11b;tlo hluk" ID="ID_198160739" CREATED="1339331646083" MODIFIED="1339331734277"/>
</node>
</node>
<node TEXT="Modality" POSITION="right" ID="ID_460379426" CREATED="1339331771837" MODIFIED="1339331820906" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="" ID="ID_975369806" CREATED="1339331761418" MODIFIED="1339331766755" COLOR="#000000">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="12"/>
<node TEXT="sexu&#xe1;ln&#xed; v&#xfd;st&#x159;elky" ID="ID_128747678" CREATED="1339331735425" MODIFIED="1339331744705"/>
<node TEXT="pohyb" ID="ID_68408518" CREATED="1339331744987" MODIFIED="1339331746878"/>
<node TEXT="r&#xe1;no" ID="ID_679639574" CREATED="1339331747145" MODIFIED="1339331749288"/>
<node TEXT="ztr&#xe1;ta t&#x11b;ln&#xed;ch tekutin" ID="ID_991357455" CREATED="1339331749609" MODIFIED="1339331758267"/>
</node>
<node TEXT="" ID="ID_1897982873" CREATED="1339331775819" MODIFIED="1339331777378" COLOR="#000000">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="12"/>
<node TEXT="klidem" ID="ID_545723290" CREATED="1339331778730" MODIFIED="1339331780924"/>
<node TEXT="osamot&#x11b;" ID="ID_1999649588" CREATED="1339331781214" MODIFIED="1395529368206"/>
<node TEXT="teplem" ID="ID_816646835" CREATED="1339331784334" MODIFIED="1339331786657"/>
<node TEXT="kr&#xe1;tk&#xfd;m odpo&#x10d;inkem" ID="ID_957538438" CREATED="1339331786938" MODIFIED="1339331793176"/>
<node TEXT="pr&#x16f;jmem" ID="ID_417084887" CREATED="1339331793409" MODIFIED="1339331796377"/>
<node TEXT="pohyb" ID="ID_586640349" CREATED="1339331827397" MODIFIED="1339331829714"/>
<node TEXT="ch&#x16f;ze" ID="ID_449845956" CREATED="1339331830011" MODIFIED="1339331834333"/>
</node>
</node>
<node TEXT="T&#xe9;mata" POSITION="left" ID="ID_1225486592" CREATED="1339329063462" MODIFIED="1339329070045" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="odpor ke spole&#x10d;nosti" ID="ID_48544604" CREATED="1339329139023" MODIFIED="1339329149703"/>
<node TEXT="Phos" ID="ID_118967047" CREATED="1339329310275" MODIFIED="1339329315620">
<node TEXT="aktivita sm&#x11b;rovan&#xe1; ke vztah&#x16f;m" ID="ID_1885937320" CREATED="1339329316708" MODIFIED="1339329373329"/>
<node TEXT="maskuje neodpu&#x161;t&#x11b;n&#xed;" ID="ID_822250024" CREATED="1339329373857" MODIFIED="1339329381378"/>
</node>
<node TEXT="Kyselina" ID="ID_411231107" CREATED="1339329338785" MODIFIED="1339329341629">
<node TEXT="t&#xe9;ma odpu&#x161;t&#x11b;n&#xed;" ID="ID_1265493844" CREATED="1339329342622" MODIFIED="1339329356662"/>
</node>
<node TEXT="chce si prosadit to sv&#xe9;" ID="ID_737888231" CREATED="1339329433490" MODIFIED="1339329445549">
<node TEXT="ve vztaz&#xed;ch" ID="ID_1399513616" CREATED="1339329449333" MODIFIED="1339329453004"/>
<node TEXT="m&#xe1; c&#xed;l, kter&#xe9;ho chce dos&#xe1;hnout" ID="ID_1278184366" CREATED="1339329476888" MODIFIED="1339329487263"/>
<node TEXT="m&#xe1; p&#x159;edstavu / o&#x10d;ek&#xe1;v&#xe1;n&#xed;" ID="ID_167588557" CREATED="1339329618457" MODIFIED="1339329662450"/>
<node TEXT="kdy&#x17e; se vztah rozpadne, neum&#xed; to odpustit" ID="ID_677620854" CREATED="1339329679483" MODIFIED="1339329691988"/>
<node TEXT="&#x17e;&#xe1;rlivost" ID="ID_275557543" CREATED="1339329826393" MODIFIED="1339329832144"/>
<node TEXT="za&#x161;&#x165;" ID="ID_1553328294" CREATED="1339329969387" MODIFIED="1339329975257"/>
<node TEXT="k&#x159;ivda" ID="ID_583871308" CREATED="1339329979284" MODIFIED="1339329997063"/>
<node TEXT="zarputilost" ID="ID_784635006" CREATED="1339330353162" MODIFIED="1339330356705"/>
<node TEXT="vytrvlost" ID="ID_1822024603" CREATED="1339330357026" MODIFIED="1339330360677"/>
<node TEXT="p&#x159;ehnan&#xe1; snaha" ID="ID_486181665" CREATED="1339330361110" MODIFIED="1339330369731"/>
<node TEXT="charismatick&#xfd;" ID="ID_362040720" CREATED="1339331271095" MODIFIED="1339331275801"/>
</node>
<node TEXT="zklaman&#xe1; l&#xe1;ska" ID="ID_895836495" CREATED="1339330564085" MODIFIED="1339330575276"/>
<node TEXT="ztr&#xe1;ta domova" ID="ID_1992430227" CREATED="1339330576140" MODIFIED="1339330580725"/>
<node TEXT="zm&#x11b;na &#x161;koly" ID="ID_1419539523" CREATED="1339330589266" MODIFIED="1339330593943"/>
<node TEXT="ztr&#xe1;ta soun&#xe1;le&#x17e;itosti" ID="ID_1644120212" CREATED="1339330684853" MODIFIED="1339330692722">
<node TEXT="Propojen&#xe1; dvoj&#x10d;ata, jedno si najde, &#xa;partnera, druh&#xe9; to nezvl&#xe1;dne" ID="ID_1593974846" CREATED="1339329844977" MODIFIED="1339330707737"/>
</node>
<node TEXT="z&#xe1;vislost" ID="ID_325237885" CREATED="1339329671953" MODIFIED="1339329675156"/>
<node TEXT="f&#xe1;ze zlomu" ID="ID_1790481854" CREATED="1339330169005" MODIFIED="1339330177526">
<node TEXT="kdy&#x17e; m&#xe1; pocit, &#x17e;e ud&#x11b;lal v&#x161;echno co &#x161;lo&#xa;a nep&#x159;ineslo to v&#x161;echno co cht&#x11b;l" ID="ID_446903844" CREATED="1339330179071" MODIFIED="1339330218670"/>
</node>
<node TEXT="na zran&#x11b;n&#xed; reaguje zv&#xfd;&#x161;en&#xed;m &#xfa;sil&#xed; o udr&#x17e;en&#xed; vztahu" ID="ID_526870424" CREATED="1339330846760" MODIFIED="1339330879049"/>
<node TEXT="stesk po domov&#x11b;" ID="ID_669058296" CREATED="1339330952315" MODIFIED="1339330958363">
<node TEXT="vzpom&#xed;n&#xe1;m na star&#xe9; &#x10d;asy" ID="ID_1953691595" CREATED="1339330996342" MODIFIED="1339331012742"/>
</node>
<node TEXT="vy&#x10d;erp&#xe1;n&#xed;" ID="ID_1397061706" CREATED="1339331074505" MODIFIED="1339331103929">
<font BOLD="true"/>
<node TEXT="&#xfa;navov&#xe9; syndromy" ID="ID_305398171" CREATED="1339331056521" MODIFIED="1339331063433"/>
<node TEXT="extr&#xe9;mn&#xed;" ID="ID_905050007" CREATED="1339331082947" MODIFIED="1339331087702"/>
<node TEXT="unaven&#xfd;" ID="ID_1096024944" CREATED="1339331118980" MODIFIED="1339331123266">
<node TEXT="odpov&#xed;d&#xe1; jedn&#xed;m slovem" ID="ID_675047591" CREATED="1339331124411" MODIFIED="1339331148164"/>
</node>
<node TEXT="velk&#xfd; smutek a deprese" ID="ID_497360579" CREATED="1339331165569" MODIFIED="1339331185969"/>
</node>
</node>
<node TEXT="Info" POSITION="left" ID="ID_107174254" CREATED="1339329090502" MODIFIED="1339329098526" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="podobn&#xe9; vy&#x10d;erpan&#xe9;mu fosforu" ID="ID_1137556064" CREATED="1339329118298" MODIFIED="1339329133782"/>
<node TEXT="regeneruje pomalu" ID="ID_954507076" CREATED="1339329196079" MODIFIED="1339329207926"/>
</node>
</node>
</map>
