<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Lac Lupinum" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1368263659187"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Vl&#x10d;&#xed; ml&#xe9;ko" STYLE_REF="Remedy" POSITION="left" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1368264153104"/>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="left" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389">
<node TEXT="Syndrom &#x10d;erven&#xe9; karkulky" ID="ID_1463375593" CREATED="1368264825864" MODIFIED="1368264835410">
<node TEXT="vyhuben&#xed; vlk&#x16f;, proto&#x17e;e si mysl&#xed;me, &#x17e;e jsou zl&#xed;" ID="ID_1801773902" CREATED="1368264836554" MODIFIED="1368264855340"/>
</node>
<node TEXT="matriarch&#xe1;ln&#xed; sme&#x10d;ka" ID="ID_54541348" CREATED="1368280589851" MODIFIED="1368280599296"/>
</node>
<node TEXT="Trituace" STYLE_REF="Subtopic" POSITION="left" ID="ID_1275465186" CREATED="1357168572522" MODIFIED="1357168576351">
<node TEXT="Smutek" ID="ID_1917599850" CREATED="1368263921337" MODIFIED="1368263930778">
<font ITALIC="true"/>
</node>
<node TEXT="Jsem v tom s&#xe1;m" ID="ID_1418501019" CREATED="1368264061675" MODIFIED="1368264106975">
<font ITALIC="true"/>
</node>
<node TEXT="Slin&#x11b;n&#xed;" ID="ID_615211584" CREATED="1368264107517" MODIFIED="1368264115532">
<font ITALIC="true"/>
</node>
<node TEXT="Zbyst&#x159;en&#xed; smysl&#x16f;" ID="ID_1708098071" CREATED="1368264116251" MODIFIED="1368264129792"/>
<node TEXT="Instinkt" ID="ID_1010238137" CREATED="1368264160058" MODIFIED="1368264162752"/>
<node TEXT="Nez&#xe1;vislost" ID="ID_1278135470" CREATED="1368264163016" MODIFIED="1368264167172"/>
<node TEXT="Vlk samot&#xe1;&#x159;" ID="ID_1905238320" CREATED="1368264167589" MODIFIED="1368264172582"/>
<node TEXT="Ostra&#x17e;itost" ID="ID_1518492573" CREATED="1368264212165" MODIFIED="1368264216543"/>
<node TEXT="Klid" ID="ID_1112878666" CREATED="1368264511315" MODIFIED="1368264518292"/>
<node TEXT="Kdy&#x17e; byl l&#xe9;k ve t&#x159;&#xed;d&#x11b; m&#x11b;la jsem pocit &#x161;tvan&#xe9;ho zv&#xed;&#x159;ete,&#xa;kdy&#x17e; se dostal ke m&#x11b;, c&#xed;tila jsem klid a soust&#x159;ed&#x11b;n&#xed;" ID="ID_1245931833" CREATED="1368264520881" MODIFIED="1368264614591"/>
<node TEXT="&#x17d;ivot je t&#x11b;&#x17e;k&#xfd;" ID="ID_354644790" CREATED="1368264746999" MODIFIED="1368264764295"/>
<node TEXT="osobnost" ID="ID_1068190993" CREATED="1368264918556" MODIFIED="1368264921826"/>
<node TEXT="d&#x16f;stojnost" ID="ID_1105524186" CREATED="1368264922123" MODIFIED="1368264925476"/>
<node TEXT="oproti psu chyb&#xed; oddanost" ID="ID_1713230481" CREATED="1368264925789" MODIFIED="1368264934785"/>
<node TEXT="chytr&#xfd;" ID="ID_101154700" CREATED="1368264943061" MODIFIED="1368264946047"/>
<node TEXT="bolest v srdci" ID="ID_147551569" CREATED="1368265027585" MODIFIED="1368265032901">
<node TEXT="t&#xed;ha" ID="ID_1675263787" CREATED="1368265019696" MODIFIED="1368265022704"/>
<node TEXT="stesk" ID="ID_454270000" CREATED="1368265037875" MODIFIED="1368265042374"/>
</node>
</node>
<node TEXT="Lac-lup" STYLE_REF="Abbrev" POSITION="right" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1368281992369"/>
<node TEXT="Miazma" STYLE_REF="Subtopic" POSITION="right" ID="ID_31190142" CREATED="1357168609677" MODIFIED="1357168614539"/>
<node TEXT="Podobn&#xe9; l&#xe9;ky" STYLE_REF="Subtopic" POSITION="right" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1357168625505">
<node TEXT="Med" ID="ID_395436882" CREATED="1368283033610" MODIFIED="1368283037966">
<node TEXT="sexualita" STYLE_REF="Same" ID="ID_284484598" CREATED="1368283042784" MODIFIED="1368283109097">
<node TEXT="promiskuitn&#xed;" STYLE_REF="Differs" ID="ID_1932076875" CREATED="1368283047611" MODIFIED="1368283088701"/>
</node>
</node>
</node>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="right" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1357168653304">
<node TEXT="vyhnanstv&#xed;" ID="ID_802881848" CREATED="1368278284336" MODIFIED="1368278289433"/>
<node TEXT="nep&#x159;ijet&#xed;" ID="ID_704227270" CREATED="1368278332447" MODIFIED="1368278342337">
<node TEXT="rodinou" ID="ID_1320235369" CREATED="1368278343306" MODIFIED="1368278346986"/>
<node TEXT="bl&#xed;zk&#xfd;mi" ID="ID_736923734" CREATED="1368278347586" MODIFIED="1368278350784"/>
<node TEXT="nepat&#x159;&#xed; mezi n&#x11b;" ID="ID_1827096115" CREATED="1368278393998" MODIFIED="1368278403293"/>
<node TEXT="p&#x159;ijde si jako vet&#x159;elec" ID="ID_1893843217" CREATED="1368278407711" MODIFIED="1368278414442"/>
</node>
<node TEXT="ob&#x11b;&#x165;" ID="ID_1711919559" CREATED="1368278635812" MODIFIED="1368278652197">
<font BOLD="true"/>
<node TEXT="skryt&#xe9;" ID="ID_199155343" CREATED="1368278670186" MODIFIED="1368278673467">
<node TEXT="ne vzdychav&#xe9;" STYLE_REF="Comment" ID="ID_314093077" CREATED="1368278676512" MODIFIED="1368278682526"/>
</node>
</node>
<node TEXT="neschopnost nav&#xe1;zat hlubok&#xfd; vztah" ID="ID_446253340" CREATED="1368279052170" MODIFIED="1368279064309">
<node TEXT="unikov&#xe9; cesty" ID="ID_935745324" CREATED="1368278995483" MODIFIED="1368279000605">
<node TEXT="kon&#xed;&#x10d;ky" ID="ID_1382265468" CREATED="1368279001549" MODIFIED="1368279004739"/>
<node TEXT="po&#x10d;&#xed;ta&#x10d;e" ID="ID_365542054" CREATED="1368279005068" MODIFIED="1368279008634"/>
<node TEXT="nev&#x11b;ra" ID="ID_561771190" CREATED="1368279009019" MODIFIED="1368279011573"/>
<node TEXT="hospoda" ID="ID_1860470324" CREATED="1368279018188" MODIFIED="1368279020366"/>
</node>
</node>
<node TEXT="typ James Bond" ID="ID_1696257848" CREATED="1368279472026" MODIFIED="1368282021227">
<font BOLD="false"/>
<node TEXT="elegance" ID="ID_1252495107" CREATED="1368279436193" MODIFIED="1368279444847"/>
<node TEXT="p&#x16f;sob&#xed; seri&#xf3;zn&#x11b;" ID="ID_23193749" CREATED="1368279532544" MODIFIED="1368279538848"/>
<node TEXT="citlivost" ID="ID_1730059451" CREATED="1368279445854" MODIFIED="1368279449770"/>
<node TEXT="&#x161;arm" ID="ID_1989640939" CREATED="1368279450097" MODIFIED="1368279452584"/>
<node TEXT="neocho&#x10d;itelnost" ID="ID_456712066" CREATED="1368279421443" MODIFIED="1368279429547"/>
<node TEXT="problematick&#xfd; vztah s matkou nebo man&#x17e;elkou" ID="ID_1782277425" CREATED="1368279484589" MODIFIED="1368279507543"/>
<node TEXT="sukni&#x10d;k&#xe1;&#x159;" ID="ID_650350800" CREATED="1368279511642" MODIFIED="1368279517977"/>
<node TEXT="vypadaj&#xed; emo&#x10d;n&#x11b; otev&#x159;en&#xed;" ID="ID_506705267" CREATED="1368279556519" MODIFIED="1368279566785">
<node TEXT="k sob&#x11b; v&#xe1;s nepust&#xed;" ID="ID_1843736639" CREATED="1368279576197" MODIFIED="1368279584263"/>
<node TEXT="ale nejsou" ID="ID_84348104" CREATED="1368279570734" MODIFIED="1368279574822"/>
</node>
<node TEXT="neza&#x159;aditeln&#xed;" ID="ID_1329921531" CREATED="1368279622090" MODIFIED="1368279629120">
<node TEXT="samorost" ID="ID_1670930781" CREATED="1368279633663" MODIFIED="1368282007575">
<font BOLD="true"/>
</node>
</node>
<node TEXT="c&#xed;t&#xed; se vyd&#x11b;len&#x11b;" ID="ID_351525486" CREATED="1368279653192" MODIFIED="1368279660963">
<node TEXT="outsider" ID="ID_1054327451" CREATED="1368279666317" MODIFIED="1368279671373">
<font BOLD="true"/>
</node>
</node>
</node>
<node TEXT="nep&#x159;ijde jim d&#x16f;le&#x17e;it&#xe9; m&#xed;t d&#x11b;ti" ID="ID_798855402" CREATED="1368279937407" MODIFIED="1368280019300"/>
<node TEXT="nen&#xed; velk&#xfd; pracant" ID="ID_1743322683" CREATED="1368280029845" MODIFIED="1368280035213"/>
<node TEXT="p&#x159;ecitliv&#x11b;lost na kritiku" ID="ID_1514328322" CREATED="1368280206002" MODIFIED="1368280214597">
<node TEXT="ut&#xed;kaj&#xed; od toho" ID="ID_160527118" CREATED="1368280224560" MODIFIED="1368280241498"/>
<node TEXT="neh&#xe1;daj&#xed; se" ID="ID_1267821653" CREATED="1368280242067" MODIFIED="1368280246449"/>
</node>
<node TEXT="tou&#x17e;&#xed; po spole&#x10d;nosti a z&#xe1;rove&#x148; k n&#xed; m&#xe1; odpor" ID="ID_1734598508" CREATED="1368280296028" MODIFIED="1368280370574"/>
<node TEXT="samorost, co nikam nepat&#x159;&#xed;" ID="ID_1766744797" CREATED="1368280371223" MODIFIED="1368280377739"/>
<node TEXT="sexualita" ID="ID_272152293" CREATED="1368281222622" MODIFIED="1368281227894">
<node TEXT="bav&#xed; ho vzru&#x161;en&#xed;" ID="ID_70192723" CREATED="1368281228840" MODIFIED="1368281235647"/>
<node TEXT="r&#xe1;d si hraje" ID="ID_1695304828" CREATED="1368281236104" MODIFIED="1368281239789"/>
<node TEXT="nap&#x11b;t&#xed;" ID="ID_1265278043" CREATED="1368281245749" MODIFIED="1368281249990"/>
</node>
<node TEXT="odd&#x11b;len&#xed; se" ID="ID_250118605" CREATED="1368281312645" MODIFIED="1368281316935"/>
<node TEXT="s probl&#xe9;m&#x16f; se vyvl&#xe9;kaj&#xed; neagresivn&#x11b;" ID="ID_1593436676" CREATED="1368282090354" MODIFIED="1368282104058"/>
<node TEXT="neboj&#xed; se smrti" ID="ID_1717476531" CREATED="1368282142409" MODIFIED="1368282160539">
<node TEXT="&#xfa;pln&#x11b; by nevadilo, kdy by zem&#x159;el" ID="ID_1280614861" CREATED="1368282164079" MODIFIED="1368282199970"/>
</node>
<node TEXT="nehod&#xed; se pro rodinn&#xfd; &#x17e;ivot" ID="ID_1607046510" CREATED="1368282210712" MODIFIED="1368282219352"/>
<node TEXT="horolezci, cestovatel&#xe9;, dobrodruzi" ID="ID_158795867" CREATED="1368282375453" MODIFIED="1368282444416"/>
<node TEXT="Ml&#xe9;ka" STYLE_REF="Subsubtopic" ID="ID_63219480" CREATED="1368265409651" MODIFIED="1368265414405">
<node TEXT="t&#xe9;mata na za&#x10d;&#xe1;tku &#x17e;ivota" ID="ID_271100304" CREATED="1368265416998" MODIFIED="1368265532949"/>
<node TEXT="propojen&#xed; s matkou" ID="ID_317698216" CREATED="1368265573841" MODIFIED="1368265580802">
<node TEXT="z&#xe1;kladn&#xed; pocit bezpe&#x10d;&#xed;" ID="ID_1270709665" CREATED="1368265581660" MODIFIED="1368265587855"/>
<node TEXT="b&#xfd;t na zemi je bezpe&#x10d;n&#xe9;" ID="ID_876544516" CREATED="1368265598465" MODIFIED="1368265839888">
<font BOLD="true"/>
</node>
<node TEXT="d&#x16f;v&#x11b;ra v &#x17e;ivot" ID="ID_485381341" CREATED="1368265627668" MODIFIED="1368265844199">
<font BOLD="true"/>
</node>
<node TEXT="kdy&#x17e; nen&#xed;" ID="ID_1790083871" CREATED="1368265656008" MODIFIED="1368265680138">
<node TEXT="ztr&#xe1;ta d&#x16f;v&#x11b;ry" ID="ID_86322395" CREATED="1368265681722" MODIFIED="1368265708850"/>
<node TEXT="ztr&#xe1;ta pocitu bezpe&#x10d;&#xed;" ID="ID_1035192719" CREATED="1368265711598" MODIFIED="1368265719435"/>
<node TEXT="hled&#xe1; pocit bezpe&#x10d;&#xed; d&#x16f;v&#x11b;ry" ID="ID_806414147" CREATED="1368265747461" MODIFIED="1368265765505">
<node TEXT="cel&#xfd; &#x17e;ivot" ID="ID_1797365263" CREATED="1368265859381" MODIFIED="1368265863221"/>
</node>
<node TEXT="strach" ID="ID_1174645139" CREATED="1368265765721" MODIFIED="1368265767701"/>
<node TEXT="st&#xe1;hne se do sebe" ID="ID_449977774" CREATED="1368265781243" MODIFIED="1368265796493">
<node TEXT="strach z opakovan&#xe9;ho odm&#xed;tnut&#xed;, kter&#xe9; nastalo poprv&#xe9;" ID="ID_1562716141" CREATED="1368265807135" MODIFIED="1368265823345"/>
</node>
<node TEXT="u zv&#xed;&#x159;at jde o &#x17e;ivot" ID="ID_609336712" CREATED="1368265905803" MODIFIED="1368265913087"/>
<node TEXT="cel&#xfd; &#x17e;ivot se sna&#x17e;&#xed; nav&#xe1;zat tento prim&#xe1;rn&#xed; kontakt" ID="ID_1487046616" CREATED="1368266064243" MODIFIED="1368266085971"/>
<node TEXT="vznikaj&#xed; sekund&#xe1;rn&#xed; traumata" ID="ID_1904149605" CREATED="1368266049899" MODIFIED="1368266060642"/>
</node>
</node>
<node TEXT="strach otev&#x159;&#xed;t se vztahu" ID="ID_190602651" CREATED="1368266437910" MODIFIED="1368267440782">
<font BOLD="true"/>
<node TEXT="velk&#xe1; bolest" ID="ID_868199682" CREATED="1368266450783" MODIFIED="1368266455233"/>
<node TEXT="cht&#x11b;j&#xed; vztah, ale zarove&#x148; z n&#x11b;ho maj&#xed; obrovsk&#xfd; strach" ID="ID_6065531" CREATED="1368266469654" MODIFIED="1368266491985"/>
</node>
<node TEXT="prvotn&#xed; trauma vzn&#xed;k&#xe1;, kdy&#x17e; nen&#xed; nav&#xe1;z&#xe1;na vazba" ID="ID_242443269" CREATED="1368266820551" MODIFIED="1368266840729">
<node TEXT="porucha kojen&#xed;" ID="ID_1409300073" CREATED="1368266841801" MODIFIED="1368266845658"/>
<node TEXT="nep&#x159;ijet&#xed;" ID="ID_950731450" CREATED="1368266845938" MODIFIED="1368266852702">
<node TEXT="matka Lac" ID="ID_602652115" CREATED="1368266853862" MODIFIED="1368266858526"/>
</node>
<node TEXT="..." ID="ID_715642499" CREATED="1368266862438" MODIFIED="1368266864041"/>
</node>
<node TEXT="c&#xed;t&#xed; se osam&#x11b;l&#xed;" ID="ID_118908543" CREATED="1368267216337" MODIFIED="1368267224520">
<node TEXT="vnit&#x159;n&#x11b;" ID="ID_1811649193" CREATED="1368267226631" MODIFIED="1368267234606"/>
<node TEXT="jsou si v&#x11b;domi, &#x17e;e jim n&#x11b;co ve vztaz&#xed;ch chyb&#xed;" ID="ID_80885476" CREATED="1368267239844" MODIFIED="1368304691332"/>
<node TEXT="c&#xed;t&#xed; se bez opory" ID="ID_1003491791" CREATED="1368268115911" MODIFIED="1368268124683">
<node TEXT="oporu neum&#xed; ale p&#x159;ijmout" ID="ID_1584578499" CREATED="1368268125747" MODIFIED="1368268132886"/>
</node>
<node TEXT="&#x10d;asto svobodn&#xe9; matky" ID="ID_205083536" CREATED="1368268191342" MODIFIED="1368268198925"/>
</node>
<node TEXT="empatie" ID="ID_1210512665" CREATED="1368267276649" MODIFIED="1368267280697">
<node TEXT="soucit" ID="ID_1816581050" CREATED="1368267283693" MODIFIED="1368267286104"/>
<node TEXT="schopnost c&#xed;t&#x11b;n&#xed;" ID="ID_860943595" CREATED="1368267286528" MODIFIED="1368267292041"/>
<node TEXT="pom&#xe1;h&#xe1;n&#xed; lidem" ID="ID_1146089709" CREATED="1368267297603" MODIFIED="1368267307297">
<node TEXT="kompenzuj&#xed; si t&#xed;m to, po &#x10d;em tou&#x17e;&#xed;" ID="ID_834657488" CREATED="1368267310908" MODIFIED="1368267334145"/>
<node TEXT="svoje trauma &#x159;e&#x161;&#xed; pomoc&#xed; ostatn&#xed;m" ID="ID_1992130540" CREATED="1368267354211" MODIFIED="1368267367972"/>
</node>
</node>
<node TEXT="emo&#x10d;n&#xed;" ID="ID_687469648" CREATED="1368267982840" MODIFIED="1368268401005">
<font BOLD="true"/>
<node TEXT="vztekaj&#xed; se" ID="ID_1168449374" CREATED="1368267988636" MODIFIED="1368268001843">
<node TEXT="za t&#xed;m strach" ID="ID_817542430" CREATED="1368268028344" MODIFIED="1368268036699"/>
<node TEXT="l&#xed;tost" ID="ID_1800181513" CREATED="1368268040893" MODIFIED="1368268043960"/>
<node TEXT="pocit viny" ID="ID_1058494137" CREATED="1368268047730" MODIFIED="1368268052755"/>
</node>
<node TEXT="impulzivn&#xed;" ID="ID_1690201711" CREATED="1368268004121" MODIFIED="1368268007683"/>
</node>
<node TEXT="zranitelnost" ID="ID_1673891811" CREATED="1368268010430" MODIFIED="1368268014035"/>
<node TEXT="pocit ob&#x11b;ti" ID="ID_1419252616" CREATED="1368268106450" MODIFIED="1368268110600"/>
<node TEXT="probl&#xe9;m s vytvo&#x159;en&#xed;m hranic" ID="ID_1302414404" CREATED="1368268304414" MODIFIED="1368268530479">
<font BOLD="true"/>
<node TEXT="p&#x159;eb&#xed;raj&#xed; zodopv&#x11b;dnost za ostatn&#xed;" ID="ID_1120352481" CREATED="1368268317833" MODIFIED="1368268332858"/>
<node TEXT="na sebe jsou velmi tvrd&#xed;" ID="ID_667532727" CREATED="1368268333330" MODIFIED="1368268341179"/>
<node TEXT="pomoc nedok&#xe1;&#x17e;ou odm&#xed;tnout" ID="ID_1780626450" CREATED="1368268348809" MODIFIED="1368268368256"/>
<node TEXT="d&#xed;t&#x11b; nem&#xe1; hranice, ty mu ur&#x10d;uje matka" ID="ID_439365071" CREATED="1368268545770" MODIFIED="1368268562138">
<node TEXT="kdy&#x17e; to neprob&#x11b;hne, nenau&#x10d;&#xed; se to" ID="ID_1230444262" CREATED="1368268591444" MODIFIED="1368268615796"/>
</node>
<node TEXT="naivn&#xed; otev&#x159;enost" ID="ID_497357280" CREATED="1368268628896" MODIFIED="1368268634983"/>
</node>
<node TEXT="maj&#xed; tendenci c&#xed;tit se zodpov&#x11b;dn&#xed; za v&#x161;echno a za v&#x161;echny" ID="ID_1984888000" CREATED="1368267389926" MODIFIED="1368267407038">
<node TEXT="pocit viny" ID="ID_197611041" CREATED="1368267410098" MODIFIED="1368267740467">
<font BOLD="true"/>
<node TEXT="vztahova&#x10d;nost" ID="ID_290392839" CREATED="1368267482257" MODIFIED="1368267489407"/>
<node TEXT="m&#x16f;&#x17e;u za to j&#xe1;" ID="ID_1395781974" CREATED="1368267489839" MODIFIED="1368267496731"/>
<node TEXT="pocit &#x161;patnosti" ID="ID_566193453" CREATED="1368267508673" MODIFIED="1368267513826"/>
<node TEXT="pocit nicoty" ID="ID_1151163351" CREATED="1368267514242" MODIFIED="1368267517957"/>
</node>
</node>
<node TEXT="velk&#xfd; potenci&#xe1;l" ID="ID_1967509821" CREATED="1368267635781" MODIFIED="1368267642129">
<node TEXT="nevyu&#x17e;ije ho" ID="ID_1483358813" CREATED="1368267642945" MODIFIED="1368267647323"/>
</node>
<node TEXT="vypad&#xe1; velmi karcinogenn&#x11b;" ID="ID_1502409727" CREATED="1368267710149" MODIFIED="1368267720199"/>
<node TEXT="nec&#xed;t&#xed; se na zemi doma" ID="ID_533318213" CREATED="1368266873366" MODIFIED="1368266886476">
<node TEXT="b&#xfd;t tady je t&#x11b;&#x17e;k&#xfd;" ID="ID_178570726" CREATED="1368266887189" MODIFIED="1368266894498"/>
<node TEXT="stoj&#xed; m&#x11b; to hodn&#x11b; &#xfa;sil&#xed;, abych tady v&#x16f;bec byl" ID="ID_1917556226" CREATED="1368266894858" MODIFIED="1368266914669"/>
<node TEXT="inklinuje k duchovn&#xed;m v&#x11b;cem" ID="ID_154833707" CREATED="1368266924911" MODIFIED="1368266940222">
<node TEXT="sv&#x11b;t naho&#x159;e je mu bli&#x17e;&#x161;&#xed; ne&#x17e; tento" ID="ID_1694328659" CREATED="1368266941070" MODIFIED="1368266966094"/>
</node>
<node TEXT="v&#xed;c &#x17e;ije ve sv&#xe9;m vnit&#x159;n&#xed;m sv&#x11b;t&#x11b;" ID="ID_1930759908" CREATED="1368267812001" MODIFIED="1368267825592"/>
</node>
<node TEXT="podv&#x11b;dom&#x11b; si vytv&#xe1;&#x159;&#xed; situace, kter&#xe9; mu umo&#x17e;&#x148;uj&#xed; pro&#x17e;&#xed;vat ty traumata znova" ID="ID_108415102" CREATED="1368268205141" MODIFIED="1368268258175"/>
<node TEXT="siln&#xfd; vztah k sexualit&#x11b;" ID="ID_1820295682" CREATED="1368281205682" MODIFIED="1368281212676"/>
<node TEXT="mladistv&#xfd; vzhled" ID="ID_1166029777" CREATED="1368281359025" MODIFIED="1368281365930">
<node TEXT="p&#x16f;sob&#xed; nezrale" ID="ID_76683311" CREATED="1368281367585" MODIFIED="1368281373175"/>
</node>
<node TEXT="snadn&#xe1; ob&#x11b;&#x165; zneu&#x17e;&#xed;v&#xe1;n&#xed;" ID="ID_769606143" CREATED="1368288920996" MODIFIED="1368288931983"/>
<node TEXT="p&#x159;itahuje si obvi&#x148;ov&#xe1;n&#xed; od ostatn&#xed;ch" ID="ID_1183102568" CREATED="1368290524519" MODIFIED="1368290556877"/>
</node>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="right" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<node TEXT="Gynekologick&#xe9; pot&#xed;&#x17e;e" ID="ID_237667163" CREATED="1368282454252" MODIFIED="1368283139104"/>
<node TEXT="RS" ID="ID_149957578" CREATED="1368283130332" MODIFIED="1368283143883"/>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="right" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729"/>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="right" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690"/>
</node>
</map>
