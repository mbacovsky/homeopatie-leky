<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Calcarea Phosphorica" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1362837099274"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Calc-p" STYLE_REF="Abbrev" POSITION="left" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1362837471967"/>
<node TEXT="Fosfore&#x10d;nan v&#xe1;penat&#xfd;" STYLE_REF="Remedy" POSITION="left" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1362837419776"/>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="left" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389">
<node TEXT="Phos" STYLE_REF="Subsubtopic" ID="ID_230233876" CREATED="1362838816601" MODIFIED="1362838824692">
<node TEXT="komunikace" ID="ID_1311434973" CREATED="1362838836892" MODIFIED="1362838840213"/>
<node TEXT="cestov&#xe1;n&#xed;" ID="ID_1267535344" CREATED="1362838840564" MODIFIED="1362838848980"/>
<node TEXT="p&#x159;&#xe1;tel&#xe9;" ID="ID_1476672196" CREATED="1362838849277" MODIFIED="1362838852396"/>
</node>
<node TEXT="Calc" STYLE_REF="Subsubtopic" ID="ID_173421724" CREATED="1362838819342" MODIFIED="1362838825063">
<node TEXT="nejistota" ID="ID_1105274316" CREATED="1362838856286" MODIFIED="1362838861558"/>
<node TEXT="plachost" ID="ID_630800647" CREATED="1362838861959" MODIFIED="1362838865077"/>
<node TEXT="stabilita" ID="ID_1568881950" CREATED="1362838865614" MODIFIED="1362838873643"/>
<node TEXT="bezpe&#x10d;&#xed;" ID="ID_641935405" CREATED="1362838874546" MODIFIED="1362838877936"/>
<node TEXT="domov" ID="ID_985555997" CREATED="1362838878585" MODIFIED="1362838880318"/>
</node>
<node TEXT="Leont&#xfd;nka" ID="ID_1115987425" CREATED="1362839889377" MODIFIED="1362839897532">
<node TEXT="A&#x165; &#x17e;ij&#xed; duchov&#xe9;" STYLE_REF="Comment" ID="ID_340041714" CREATED="1362839898501" MODIFIED="1362839907635"/>
</node>
<node TEXT="lze pou&#x17e;&#xed;vat paraleln&#x11b; s tk&#xe1;&#x148;ovou sol&#xed;" ID="ID_1710831892" CREATED="1362841589929" MODIFIED="1362841606925"/>
</node>
<node TEXT="Trituace" STYLE_REF="Subtopic" POSITION="left" ID="ID_1275465186" CREATED="1357168572522" MODIFIED="1357168576351">
<node TEXT="bolest kolen" ID="ID_93563966" CREATED="1362837336117" MODIFIED="1362837364650">
<font ITALIC="true"/>
</node>
<node TEXT="bolest ky&#x10d;l&#xed;" ID="ID_1310460631" CREATED="1362837342047" MODIFIED="1362837365938">
<font ITALIC="true"/>
</node>
<node TEXT="tlak za &#x10d;elem" ID="ID_1186132767" CREATED="1362837347386" MODIFIED="1362837367530">
<font ITALIC="true"/>
</node>
<node TEXT="pocit osam&#x11b;n&#xed;" ID="ID_28697288" CREATED="1362837354146" MODIFIED="1362837369440">
<font ITALIC="true"/>
</node>
<node TEXT="palpitace" ID="ID_1117419248" CREATED="1362837359195" MODIFIED="1362837378388">
<font ITALIC="true"/>
</node>
</node>
<node TEXT="Podobn&#xe9; l&#xe9;ky" STYLE_REF="Subtopic" POSITION="left" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1357168625505">
<node TEXT="Phos" ID="ID_623840597" CREATED="1362838465642" MODIFIED="1362838468414">
<node TEXT="z&#xe1;&#x159;&#xed;" STYLE_REF="Differs" ID="ID_442454053" CREATED="1362838469607" MODIFIED="1362838486415">
<node TEXT="calc-p p&#x16f;sob&#xed; pla&#x161;e" STYLE_REF="Comment" ID="ID_1063874939" CREATED="1362838488175" MODIFIED="1362838499400"/>
</node>
<node TEXT="nevytv&#xe1;&#x159;&#xed; u&#x17e;&#x161;&#xed; vztahy" STYLE_REF="Differs" ID="ID_156941621" CREATED="1362839487830" MODIFIED="1362839499759"/>
<node TEXT="nepot&#x159;ebuje tolik pocit bezpe&#x10d;&#xed;" STYLE_REF="Differs" ID="ID_494669472" CREATED="1362839538222" MODIFIED="1362839557416"/>
</node>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="left" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<node TEXT="huben&#xed;, vysoc&#xed;" ID="ID_1190569618" CREATED="1362838109313" MODIFIED="1362838117024">
<node TEXT="rachytida" ID="ID_150150905" CREATED="1362841638865" MODIFIED="1362841645049"/>
</node>
<node TEXT="bolesti b&#x159;&#xed;&#x161;ka" ID="ID_1137717311" CREATED="1362839259280" MODIFIED="1362839264975"/>
<node TEXT="averze k mate&#x159;sk&#xe9;mu ml&#xe9;ku" ID="ID_1681799199" CREATED="1362841169252" MODIFIED="1362841186750"/>
<node TEXT="kosti" ID="ID_567006876" CREATED="1362841212263" MODIFIED="1362841216501">
<node TEXT="r&#x16f;stov&#xe9; bolesti" ID="ID_1848002845" CREATED="1362841217510" MODIFIED="1362841222754"/>
<node TEXT="rychl&#xfd; r&#x16f;st" ID="ID_1522928005" CREATED="1362841223195" MODIFIED="1362841227514"/>
<node TEXT="pomal&#xfd; r&#x16f;st" ID="ID_1664996430" CREATED="1362841227770" MODIFIED="1362841231767"/>
<node TEXT="kazen&#xed; zub&#x16f;" ID="ID_1048334930" CREATED="1362841232592" MODIFIED="1362841238157"/>
<node TEXT="zlomeniny" ID="ID_1185749722" CREATED="1362841537929" MODIFIED="1362841541784"/>
<node TEXT="k&#x159;ehk&#xe9; kosti" ID="ID_1410033526" CREATED="1362841543359" MODIFIED="1362841548020"/>
<node TEXT="osteopor&#xf3;za" ID="ID_220457187" CREATED="1362841548339" MODIFIED="1362841554329"/>
</node>
<node TEXT="krev" ID="ID_1770850152" CREATED="1362841239004" MODIFIED="1362841240916">
<node TEXT="bledost" ID="ID_1597856172" CREATED="1362841612029" MODIFIED="1362841626831"/>
<node TEXT="anemie" ID="ID_1126953158" CREATED="1362841615386" MODIFIED="1362841631512"/>
</node>
<node TEXT="lymfatick&#xfd; syst&#xe9;m" ID="ID_803713748" CREATED="1362841242458" MODIFIED="1362841248140"/>
<node TEXT="v pubert&#x11b; rychle vyrostou" ID="ID_1050555184" CREATED="1362841261750" MODIFIED="1362841272592"/>
<node TEXT="r&#xfd;my" ID="ID_12201934" CREATED="1362841743002" MODIFIED="1362841747918"/>
<node TEXT="zan&#x11b;t pr&#x16f;du&#x161;ek" ID="ID_505207143" CREATED="1362841748431" MODIFIED="1362841754755"/>
<node TEXT="lymfatick&#xe9; &#x17e;l&#xe1;zy" ID="ID_1855906424" CREATED="1362841755141" MODIFIED="1362841761439"/>
<node TEXT="nep&#x159;ib&#xed;r&#xe1; ani kdy&#x17e; j&#xed;" ID="ID_189230556" CREATED="1362841763588" MODIFIED="1362841777089"/>
<node TEXT="bolesti hlavy z p&#x159;et&#xed;&#x17e;en&#xed;" ID="ID_1864165556" CREATED="1362841777505" MODIFIED="1362841785956"/>
</node>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="right" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1357168653304">
<node TEXT="sandno a se z&#xe1;palem navazuj&#xed; kontakty" ID="ID_1604979715" CREATED="1362837969089" MODIFIED="1362837988056">
<node TEXT="ale m&#xe1; pochybnosti, jestli p&#x16f;sob&#xed; dob&#x159;e" ID="ID_456384817" CREATED="1362837989489" MODIFIED="1362838001538"/>
</node>
<node TEXT="kdy&#x17e; je doma chce j&#xed;t ven a naopak" ID="ID_251767812" CREATED="1362838003310" MODIFIED="1362838016378">
<node TEXT="rozpor" ID="ID_118502168" CREATED="1362838017871" MODIFIED="1362838020323"/>
<node TEXT="nespokojenost" ID="ID_987836950" CREATED="1362838020746" MODIFIED="1362838025524"/>
</node>
<node TEXT="povrchn&#x11b; um&#xed; dob&#x159;e komunikovat, ale t&#x11b;&#x17e;ko hled&#xe1; k ostatn&#xed;m d&#x16f;v&#x11b;ru pro hlub&#x161;&#xed; vztahy" ID="ID_1342023703" CREATED="1362838193095" MODIFIED="1362838329838"/>
<node TEXT="nejistota" ID="ID_1368659954" CREATED="1362838332217" MODIFIED="1362838339368">
<font BOLD="true"/>
<node TEXT="vnit&#x159;n&#xed;" ID="ID_1936132092" CREATED="1362838452154" MODIFIED="1362838456287"/>
</node>
<node TEXT="citlivost" ID="ID_169730129" CREATED="1362838447017" MODIFIED="1362838450709">
<node TEXT="zranitelnost" ID="ID_1971697101" CREATED="1362840993027" MODIFIED="1362840998978"/>
<node TEXT="empatie" ID="ID_252966418" CREATED="1362839042123" MODIFIED="1362839044659"/>
</node>
<node TEXT="uraz&#xed; se" ID="ID_1247575923" CREATED="1362841011175" MODIFIED="1362841014852"/>
<node TEXT="siln&#xe1; v&#x16f;le" ID="ID_542143963" CREATED="1362838693924" MODIFIED="1362838703124">
<node TEXT="pracovitost" ID="ID_95565916" CREATED="1362838940206" MODIFIED="1362838943636"/>
</node>
<node TEXT="pochybnosti" ID="ID_592808336" CREATED="1362838418540" MODIFIED="1362838422378">
<node TEXT="je dost dobr&#xe9; to co d&#x11b;l&#xe1;m?" ID="ID_1607967577" CREATED="1362838424939" MODIFIED="1362838435437">
<node TEXT="pr&#xe1;ce" ID="ID_1009705906" CREATED="1362838436311" MODIFIED="1362838438522"/>
<node TEXT="vztahy" ID="ID_1489803573" CREATED="1362838438828" MODIFIED="1362838443292"/>
</node>
</node>
<node TEXT="maj&#xed; na sebe vysok&#xe9; n&#xe1;roky" ID="ID_1572150944" CREATED="1362838673552" MODIFIED="1362838680964"/>
<node TEXT="v&#xfd;&#x10d;itky" ID="ID_210006723" CREATED="1362838530256" MODIFIED="1362838533377"/>
<node TEXT="jsou citliv&#xed; na to co si o nich okol&#xed; mysl&#xed;" ID="ID_1034441021" CREATED="1362838564169" MODIFIED="1362838594850">
<node TEXT="pr&#xe1;ce" ID="ID_1862658025" CREATED="1362838595955" MODIFIED="1362838607669"/>
<node TEXT="&#x161;kola" ID="ID_1102447508" CREATED="1362838608548" MODIFIED="1362838613822"/>
<node TEXT="v&#x11b;domosti" ID="ID_128310150" CREATED="1362838614205" MODIFIED="1362838618520"/>
</node>
<node TEXT="C: mus&#xed;m to ud&#x11b;lat P: ale b&#x11b;&#x17e; ven, to se pak zvl&#xe1;dne C: ale pak to nebude vpo&#x159;&#xe1;dku P: v&#x17e;dy&#x165; je to jedno" ID="ID_267689411" CREATED="1362838919114" MODIFIED="1362840705771"/>
<node TEXT="ustra&#x161;en&#xe9; d&#x11b;ti ze &#x11b;koly" ID="ID_228021539" CREATED="1362839245893" MODIFIED="1362839255948"/>
<node TEXT="m&#xe1; r&#xe1;d pohyb" ID="ID_1585580958" CREATED="1362839278003" MODIFIED="1362839282063"/>
<node TEXT="upov&#xed;dan&#xed;" ID="ID_1796670132" CREATED="1362839781355" MODIFIED="1362839800734"/>
<node TEXT="rozt&#x11b;kan&#xed;" ID="ID_962194604" CREATED="1362839777862" MODIFIED="1362839781020"/>
<node TEXT="netrp&#x11b;liv&#xed;" ID="ID_1805859145" CREATED="1362839767919" MODIFIED="1362839777446"/>
<node TEXT="k&#x159;ehkost" ID="ID_844579275" CREATED="1362839846910" MODIFIED="1362839855608"/>
<node TEXT="citliv&#xe1; na kritiku" ID="ID_1188141286" CREATED="1362839955968" MODIFIED="1362839967323"/>
<node TEXT="idealismus" ID="ID_1411170801" CREATED="1362839967738" MODIFIED="1362839974615"/>
<node TEXT="probl&#xe9;m &#x10d;elit re&#xe1;ln&#xe9; situace" ID="ID_599324586" CREATED="1362839976188" MODIFIED="1362839986290"/>
<node TEXT="nerad kontaktn&#xed; sporty" ID="ID_989619083" CREATED="1362840011309" MODIFIED="1362840018025">
<node TEXT="chyb&#xed; sout&#x11b;&#x17e;ivost" ID="ID_273999791" CREATED="1362840018858" MODIFIED="1362840026711"/>
</node>
<node TEXT="vyh&#xfd;b&#xe1;n&#xed; se konflikt&#x16f;m" ID="ID_1631112225" CREATED="1362840096313" MODIFIED="1362840106162">
<node TEXT="nechce je &#x159;e&#x161;it" ID="ID_1489338831" CREATED="1362840116379" MODIFIED="1362840121336"/>
</node>
<node TEXT="d&#xed;t&#x11b;, hrozn&#x11b; chce jet na t&#xe1;bor" ID="ID_741838579" CREATED="1362840129243" MODIFIED="1362840145780">
<node TEXT="po 2 dnech chce jet dom&#x16f;" ID="ID_1589482441" CREATED="1362840147332" MODIFIED="1362840677359"/>
<node TEXT="p&#x159;ije&#x10f;te nebo si vymysl&#xed;m n&#x11b;co hor&#x161;&#xed;ho" ID="ID_1481424632" CREATED="1362840179004" MODIFIED="1362840212148"/>
</node>
<node TEXT="zasn&#x11b;nost" ID="ID_1269225512" CREATED="1362840557933" MODIFIED="1362840560858">
<node TEXT="denn&#xed; sn&#x11b;n&#xed;" ID="ID_1743967381" CREATED="1362840562154" MODIFIED="1362840568972"/>
</node>
<node TEXT="z&#xe1;visl&#xed;" ID="ID_916574368" CREATED="1362840900682" MODIFIED="1362840911328"/>
<node TEXT="lehce manipulovateln&#xed;" ID="ID_1064226931" CREATED="1362840915793" MODIFIED="1362840923766">
<node TEXT="v rozumn&#xe9; m&#xed;&#x159;e jim to nemus&#xed; vadit" ID="ID_1944325548" CREATED="1362840925397" MODIFIED="1362840941199"/>
<node TEXT="m&#x16f;&#x17e;e t&#x11b;&#x17e;it z toho, &#x17e;e je vede n&#x11b;kdo, kdo se vyzn&#xe1;" ID="ID_476907766" CREATED="1362840941640" MODIFIED="1362840991399"/>
</node>
<node TEXT="probl&#xe9;my se soust&#x159;ed&#x11b;n&#xed;m" ID="ID_121975340" CREATED="1362841072493" MODIFIED="1362841079386">
<node TEXT="d&#x11b;l&#xe1; chyby" ID="ID_429718002" CREATED="1362841064605" MODIFIED="1362841070431"/>
<node TEXT="rozt&#x11b;kanost" ID="ID_116932356" CREATED="1362841080249" MODIFIED="1362841087231"/>
<node TEXT="zapom&#xed;n&#xe1;" ID="ID_1190891109" CREATED="1362841087655" MODIFIED="1362841090782"/>
</node>
</node>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="right" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690">
<node TEXT="" STYLE_REF="Agg" ID="ID_1163087020" CREATED="1362841849511" MODIFIED="1362841897439">
<node TEXT="vlhkem" ID="ID_955464217" CREATED="1362841851522" MODIFIED="1362841854347"/>
<node TEXT="zm&#x11b;nou po&#x10d;as&#xed;" ID="ID_638396224" CREATED="1362841855235" MODIFIED="1362841860996"/>
<node TEXT="psych. n&#xe1;mahou" ID="ID_1326824642" CREATED="1362841861300" MODIFIED="1362841888059"/>
</node>
<node TEXT="" STYLE_REF="Amel" ID="ID_1698621594" CREATED="1362841898854" MODIFIED="1362841919452">
<node TEXT="v l&#xe9;t&#x11b;" ID="ID_931141021" CREATED="1362841901843" MODIFIED="1362841905976"/>
<node TEXT="tepl&#xfd;m po&#x10d;as&#xed;m" ID="ID_1218508233" CREATED="1362841906800" MODIFIED="1362841912102"/>
</node>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="right" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729"/>
</node>
</map>
