<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Indigo" ID="ID_934562003" CREATED="1341220905398" MODIFIED="1341220957748" LINK="../../Letn&#xed;%20&#x161;kola%202012%20-%20Podmitrov/L&#xe9;&#x10d;en&#xed;%20pomoc&#xed;%20barev.mm" COLOR="#cc0000" STYLE="fork">
<hook NAME="MapStyle" max_node_width="600"/>
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Trituace" POSITION="right" ID="ID_1595045094" CREATED="1341220963073" MODIFIED="1341220967153" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="vhled dovnit&#x159;" ID="ID_154833067" CREATED="1341220968281" MODIFIED="1341220998079">
<font ITALIC="true"/>
</node>
<node TEXT="pocit jistoty/veden&#xed;" ID="ID_329846666" CREATED="1341220981535" MODIFIED="1341220999447">
<font ITALIC="true"/>
</node>
<node TEXT="jsem v&#x11b;t&#x161;&#xed;, p&#x159;esahuji svoje t&#x11b;lo" ID="ID_166058572" CREATED="1341221160921" MODIFIED="1341221173615">
<font ITALIC="true"/>
</node>
<node TEXT="tlak a&#x17e; tup&#xe1; bolest v oblasti t&#x159;et&#xed;ho oka" ID="ID_1692640586" CREATED="1341221227957" MODIFIED="1341221256857">
<font ITALIC="true"/>
</node>
<node TEXT="hloubka" ID="ID_494628550" CREATED="1341221734912" MODIFIED="1341221737538"/>
<node TEXT="odosobn&#x11b;n&#xed;" ID="ID_109453629" CREATED="1341222339126" MODIFIED="1341222342887"/>
<node TEXT="d&#x16f;v&#x11b;ra" ID="ID_667466251" CREATED="1341221737986" MODIFIED="1341221741491"/>
<node TEXT="v&#x11b;d&#x11b;t" ID="ID_1029056320" CREATED="1341221742189" MODIFIED="1341221745192">
<node TEXT="beze slov" ID="ID_1394839519" CREATED="1341221803869" MODIFIED="1341221806001"/>
</node>
<node TEXT="pocit, &#x17e;e let&#xed;m v&#xfd;&#x161;" ID="ID_1855037896" CREATED="1341221790989" MODIFIED="1341221801911"/>
<node TEXT="mraven&#x10d;en&#xed;" ID="ID_1113373061" CREATED="1341221833043" MODIFIED="1341221838057"/>
<node TEXT="kan&#xe1;l nahoru" ID="ID_973867219" CREATED="1341221841582" MODIFIED="1341221846197"/>
</node>
<node TEXT="Info" POSITION="right" ID="ID_852651383" CREATED="1341222018480" MODIFIED="1341222107398" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="430-500nm" ID="ID_1539315575" CREATED="1341222006252" MODIFIED="1341225108443"/>
<node TEXT="t&#xf3;n" ID="ID_608760339" CREATED="1341222021767" MODIFIED="1341222024429">
<node TEXT="A" ID="ID_1746734420" CREATED="1341222025638" MODIFIED="1341222026466"/>
</node>
<node TEXT="&#x10d;akra" ID="ID_778779003" CREATED="1341222028251" MODIFIED="1341222033109">
<node TEXT="t&#x159;et&#xed; oko" ID="ID_419590419" CREATED="1341222033814" MODIFIED="1341222036937"/>
<node TEXT="&#x159;&#xed;d&#xed;c&#xed; centrum" ID="ID_1130939918" CREATED="1341222041873" MODIFIED="1341222046864"/>
<node TEXT="rozv&#xed;j&#xed; se od puberty, vrchol&#xed; asi ve 28 letech" ID="ID_1608930367" CREATED="1341222346798" MODIFIED="1341222381640"/>
<node TEXT="vy&#x10d;erpan&#xe1; 3 &#x10d;akra brzd&#xed; v&#xfd;voj 3. oka" ID="ID_1108539317" CREATED="1341222400461" MODIFIED="1341222432084"/>
</node>
<node TEXT="za&#x17e;&#xed;v&#xe1;-li d&#xed;t&#x11b; nestabilitu, nebezpe&#x10d;&#xed;, 6. &#x10d;akra se vyvine velmi rychle,&#xa;vyvine si vn&#xed;mavost aby se uchr&#xe1;nilo" ID="ID_22592344" CREATED="1341222469609" MODIFIED="1341222809151"/>
<node TEXT="tonikum celkov&#xe9;ho zdrav&#xed;" ID="ID_309715081" CREATED="1341222873703" MODIFIED="1341222883377"/>
<node TEXT="pod&#xe1;v&#xe1; se za soumraku nebo rozb&#x159;esku" ID="ID_1251350691" CREATED="1341223641891" MODIFIED="1341223653540"/>
</node>
<node TEXT="Souvisej&#xed;c&#xed; l&#xe9;ky" POSITION="right" ID="ID_1557884020" CREATED="1341223665186" MODIFIED="1341223682289" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Sep" ID="ID_1127899432" CREATED="1341223671028" MODIFIED="1341223673106"/>
<node TEXT="lith" ID="ID_214018930" CREATED="1341223673651" MODIFIED="1341223675952"/>
<node TEXT="Cobalt" ID="ID_1408031651" CREATED="1341223676928" MODIFIED="1341223679816"/>
<node TEXT="Lach" ID="ID_530605440" CREATED="1341223683508" MODIFIED="1341223692000"/>
<node TEXT="Crot" ID="ID_435697753" CREATED="1341223692257" MODIFIED="1341223693935"/>
<node TEXT="Bell" ID="ID_1910977914" CREATED="1341223698418" MODIFIED="1341223699952"/>
<node TEXT="Picric-ac" ID="ID_1126080109" CREATED="1341223701383" MODIFIED="1341223708954"/>
<node TEXT="Cupr" ID="ID_1456087615" CREATED="1341223710194" MODIFIED="1341223712958"/>
<node TEXT="Phos" ID="ID_1007502744" CREATED="1341223716327" MODIFIED="1341223717645"/>
<node TEXT="Camp" ID="ID_1136090221" CREATED="1341223765675" MODIFIED="1341223793539"/>
<node TEXT="Bapt" ID="ID_939274917" CREATED="1341223839919" MODIFIED="1341223841621"/>
</node>
<node TEXT="Res" POSITION="right" ID="ID_1822508598" CREATED="1341221007392" MODIFIED="1341223661067" COLOR="#0066cc" STYLE="fork" HGAP="-25" VSHIFT="19">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="INDIGO BLUE&#xa; &#xa;This color is cool and chilly. It represents the intellect and symbolizes emotional detachment. It influences the Pituitary gland, which stimulates grown and controls reproductive cycles in the body.  It is the color of the higher mind and heals what is inflamed and impassioned.&#xa;It acts as an anesthetic, and soothes and gives cooling relief from inflammation.&#xa;It is the color that is most favored in the world for fashion and decoration. It is known as the color of discernment.  It calms frayed nerves and anxious states. It is used to cleanse the psychic forces of the body.  If you have a serious medical condition, please consult your doctor before taking this remedy.&#xa; &#xa; &#xa;Chakra: The Brow Chakra:&#xa;This center is known as the control center in yoga. It is located between the eyebrows in the middle of the forehead. It functions as an antenna allowing us access to both inner truth and perceived awareness about people and situations. &#xa;It controls many vital functions and is our conscious link to the life choices we make for ourselves. It helps us cultivate wisdom from the trials and sufferings of our lives, and find the good, the truth and the freedom we need to consciously evolve.&#xa;The Brow chakra rules our ability to discern who and what are for our highest good. It s primary archetype is the Wise Person who is able to harness knowledge and make wholesome choices for their good and the good of those for whom they are responsible.&#xa;The negative archetype of this chakra is the Intellectual, the one who only wants the facts of the situation and avoids knowing their inner truth, or intuitive perception.&#xa;The qualities of the Brow Chakra are: wisdom, discernment, knowledge, intuition and imagination.&#xa; &#xa;Mental; This color remedy is useful for those who need clarity and organization in their thinking. It helps the mind focus and elucidates matters which weigh on the mind. It is good for tired and overused minds, as it brings in other dimensions besides thinking into the choice process.&#xa;Contraindications: It is not suited for people who analyze constantly, and who are always in their heads. It would only accentuate this. If you have a serious medical condition, please consult your doctor before using this treatment.&#xa; &#xa;Physical: This remedy can lower blood pressure and act as a clamant. It can work as an anesthetic and help relieve pain. It acts as natural blood purification and can alleviate lymphatic congestion which causes inflammation. It is useful for swollen ankles, boils, carbuncles, skin irritations, and can soothe wounds.&#xa;Contraindications: If you have a serious medical condition, please consult your doctor before taking this treatment.&#xa; &#xa;General Symptoms: Indigo homeopathic remedy is used for anxiety and fear states, or whenever the emotions run high. It cools, relaxes, and helps the emotions become detached. It is good for agitated emotional states.&#xa;Contraindications: It is not good for depressed states and can make a person more depressed and dissociated from her /his feelings and body. Please do not take this remedy if you are depressed. Please consult your doctor." ID="ID_1499332614" CREATED="1341221041614" MODIFIED="1341221044333"/>
</node>
<node TEXT="Indi-aw" POSITION="left" ID="ID_187178277" CREATED="1341221060783" MODIFIED="1341221070241" COLOR="#996600">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="T&#xe9;mata" POSITION="left" ID="ID_153897445" CREATED="1341222075029" MODIFIED="1341222082130" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="oce&#xe1;n" ID="ID_1670316211" CREATED="1341222083196" MODIFIED="1341222094241"/>
<node TEXT="soumrak a rozb&#x159;esk" ID="ID_1934986811" CREATED="1341222067554" MODIFIED="1341222073496"/>
<node TEXT="barva l&#xe9;&#x10d;en&#xed;" ID="ID_976336167" CREATED="1341222061982" MODIFIED="1341222066778"/>
<node TEXT="energie kosmu a univerz&#xe1;ln&#xed; barvy" ID="ID_538547059" CREATED="1341222049727" MODIFIED="1341222060088"/>
<node TEXT="sv&#x11b;t mysli" ID="ID_199176900" CREATED="1341222187568" MODIFIED="1341222194273">
<node TEXT="moudrost" ID="ID_617520659" CREATED="1341222244606" MODIFIED="1341222246996"/>
<node TEXT="v&#x11b;d&#x11b;n&#xed;" ID="ID_1562939825" CREATED="1341222247541" MODIFIED="1341222250271"/>
<node TEXT="intuice" ID="ID_879653632" CREATED="1341222252118" MODIFIED="1341222254282"/>
<node TEXT="obrazotvornost" ID="ID_454571345" CREATED="1341222254504" MODIFIED="1341222258089"/>
<node TEXT="imaginace" ID="ID_1700459010" CREATED="1341222258426" MODIFIED="1341222260464"/>
<node TEXT="vizualizace" ID="ID_931903954" CREATED="1341222260761" MODIFIED="1341222262995"/>
</node>
<node TEXT="schopnost rozli&#x161;ovat" ID="ID_257680140" CREATED="1341222200433" MODIFIED="1341222207002">
<node TEXT="co chceme a co ne" ID="ID_460028678" CREATED="1341222220050" MODIFIED="1341222223570"/>
<node TEXT="co je dobr&#xe9; a co ne" ID="ID_957912695" CREATED="1341222223923" MODIFIED="1341222228957"/>
<node TEXT="d&#xe1;v&#xe1; schopnost &#x159;&#xed;kat ne" ID="ID_1617336670" CREATED="1341222229222" MODIFIED="1341222235881"/>
</node>
<node TEXT="d&#xe1;v&#xe1;" ID="ID_975719084" CREATED="1341222160018" MODIFIED="1341222647772">
<font BOLD="true"/>
<node TEXT="pocit odstupu" ID="ID_1577038336" CREATED="1341222165423" MODIFIED="1341222169104"/>
<node TEXT="vzd&#xe1;len&#xed; se" ID="ID_1360107389" CREATED="1341222169377" MODIFIED="1341222172821">
<node TEXT="od emoc&#xed;" ID="ID_295656281" CREATED="1341222178577" MODIFIED="1341222181623"/>
</node>
<node TEXT="schopnost p&#x159;em&#x11b;nit sny v realitu" ID="ID_1457777545" CREATED="1341222275961" MODIFIED="1341222286483"/>
<node TEXT="schopnost pou&#x10d;it se s pro&#x17e;it&#xfd;ch t&#x11b;&#x17e;kost&#xed;" ID="ID_1452026262" CREATED="1341222290144" MODIFIED="1341222303755"/>
<node TEXT="jasn&#xe1; a soust&#x159;ed&#x11b;n&#xe1; mysl" ID="ID_1019009147" CREATED="1341222311441" MODIFIED="1341222321872"/>
<node TEXT="schopnost nadhledu" ID="ID_1814526689" CREATED="1341222323617" MODIFIED="1341222329183"/>
<node TEXT="lep&#x161;&#xed; pozorovac&#xed; schopnosti" ID="ID_289551803" CREATED="1341223583388" MODIFIED="1341223593398"/>
<node TEXT="schopnost analyzovat" ID="ID_1069795531" CREATED="1341223593639" MODIFIED="1341223599300"/>
</node>
<node TEXT="nadbytek" ID="ID_641403465" CREATED="1341222648363" MODIFIED="1341222652260">
<font BOLD="true"/>
<node TEXT="moc p&#x159;em&#xfd;&#x161;l&#xed;" ID="ID_1871870493" CREATED="1341222653124" MODIFIED="1341222658941"/>
<node TEXT="m&#xe1;lo &#x17e;ije" ID="ID_836292145" CREATED="1341222659517" MODIFIED="1341222663152"/>
<node TEXT="zapom&#xed;n&#xe1; na sv&#x11b;t okolo" ID="ID_375741982" CREATED="1341222663465" MODIFIED="1341222669415"/>
<node TEXT="odpout&#xe1;n&#xed; od t&#x11b;la" ID="ID_1148927170" CREATED="1341222673243" MODIFIED="1341222679911"/>
</node>
<node TEXT="nedostatek" ID="ID_1879395927" CREATED="1341223456729" MODIFIED="1341223461021">
<font BOLD="true"/>
<node TEXT="&#xfa;zkostn&#xe9; stavy" ID="ID_374769129" CREATED="1341223462012" MODIFIED="1341223466866"/>
<node TEXT="strach" ID="ID_1660474737" CREATED="1341223467179" MODIFIED="1341223468456"/>
<node TEXT="pohlcen&#xfd; v probl&#xe9;mech" ID="ID_458967859" CREATED="1341223468681" MODIFIED="1341223474553"/>
<node TEXT="chyb&#xed; nadhled nad emoc&#xed;" ID="ID_900041842" CREATED="1341223474778" MODIFIED="1341223491126"/>
<node TEXT="t&#x11b;&#x17e;ko si h&#xe1;j&#xed; sv&#xe9; hranice" ID="ID_1023292871" CREATED="1341223514404" MODIFIED="1341223522493"/>
</node>
</node>
<node TEXT="Symptomy" POSITION="left" ID="ID_956575443" CREATED="1341222816503" MODIFIED="1341222831644" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="hore&#x10d;ka" ID="ID_20253475" CREATED="1341222821347" MODIFIED="1341222824282">
<node TEXT="ochalzuje" ID="ID_1214800937" CREATED="1341222825602" MODIFIED="1341222829637" COLOR="#999999">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="dislexie" ID="ID_1564645216" CREATED="1341223534859" MODIFIED="1341223537913"/>
<node TEXT="hysterie" ID="ID_249360063" CREATED="1341223539472" MODIFIED="1341223541616"/>
<node TEXT="zmaten&#xed; mysli" ID="ID_1236343002" CREATED="1341223551522" MODIFIED="1341223556339"/>
<node TEXT="p&#x159;&#xed;li&#x161;n&#xe1; starost o druh&#xe9;" ID="ID_786342656" CREATED="1341223560758" MODIFIED="1341223567679"/>
<node TEXT="vysok&#xfd; krevn&#xed; tlak" ID="ID_1913676413" CREATED="1341222832831" MODIFIED="1341222840466"/>
<node TEXT="ovliv&#x148;uje lymf. syst&#xe9;m" ID="ID_128716703" CREATED="1341222845100" MODIFIED="1341222863467"/>
<node TEXT="regeneruje" ID="ID_198338877" CREATED="1341222864323" MODIFIED="1341222867589"/>
<node TEXT="&#x10d;ist&#xed; krev" ID="ID_317729500" CREATED="1341222869260" MODIFIED="1341222873356"/>
<node TEXT="u&#x161;i" ID="ID_723311253" CREATED="1341222892301" MODIFIED="1341222894590"/>
<node TEXT="nos" ID="ID_1846475376" CREATED="1341222894911" MODIFIED="1341222896114"/>
<node TEXT="ti&#x161;&#xed; bolesti" ID="ID_1055842656" CREATED="1341222902027" MODIFIED="1341222906894"/>
<node TEXT="bolesti hlavy" ID="ID_82826786" CREATED="1341222907406" MODIFIED="1341222911521"/>
<node TEXT="uklid&#x148;uje sv&#x11b;d&#x11b;n&#xed;" ID="ID_1153825257" CREATED="1341222919358" MODIFIED="1341222927926">
<node TEXT="vlasov&#xe1; &#x10d;&#xe1;st hlavy" ID="ID_1380844122" CREATED="1341222932665" MODIFIED="1341222941014"/>
</node>
<node TEXT="otekl&#xe9; klouby" ID="ID_1844354210" CREATED="1341222941908" MODIFIED="1341222946750"/>
<node TEXT="abscesy" ID="ID_1403359440" CREATED="1341222947103" MODIFIED="1341222949735"/>
<node TEXT="v&#x159;edy" ID="ID_319970256" CREATED="1341222950208" MODIFIED="1341222952357"/>
<node TEXT="podr&#xe1;&#x17e;d&#x11b;n&#xe1; poko&#x17e;ka" ID="ID_1121345794" CREATED="1341222952678" MODIFIED="1341222962319"/>
<node TEXT="neplodnost" ID="ID_215466347" CREATED="1341222966274" MODIFIED="1341222972032"/>
<node TEXT="astma" ID="ID_1670365225" CREATED="1341223011042" MODIFIED="1341223013224"/>
<node TEXT="TBC" ID="ID_479683922" CREATED="1341223013529" MODIFIED="1341223015209"/>
<node TEXT="epilepsie" ID="ID_1070469054" CREATED="1341223021009" MODIFIED="1341223029503"/>
<node TEXT="zklidn&#x11b;n&#xed; vnit&#x159;n&#xed;ch org&#xe1;n&#x16f;" ID="ID_902657524" CREATED="1341223029904" MODIFIED="1341223041368"/>
<node TEXT="kongesce v panvi" ID="ID_1919661346" CREATED="1341223041774" MODIFIED="1341223047361"/>
<node TEXT="chronick&#xe9; degenerativn&#xed; choroby" ID="ID_1119612316" CREATED="1341223047601" MODIFIED="1341223060830">
<node TEXT="alzheimer" ID="ID_1508808121" CREATED="1341223062047" MODIFIED="1341223067643"/>
<node TEXT="parkinson" ID="ID_1381367727" CREATED="1341223067964" MODIFIED="1341223070824"/>
<node TEXT="becht&#x11b;rev" ID="ID_1572997147" CREATED="1341223071137" MODIFIED="1341223189420"/>
</node>
<node TEXT="anestetikum" ID="ID_123028822" CREATED="1341223206570" MODIFIED="1341223210217"/>
</node>
<node TEXT="Kontraindikace" POSITION="left" ID="ID_1266948192" CREATED="1341223607147" MODIFIED="1341223614041" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="deprese" ID="ID_1766541875" CREATED="1341223614891" MODIFIED="1341223618693"/>
<node TEXT="p&#x159;em&#xed;ra anal&#xfd;zy a hloub&#xe1;n&#xed;" ID="ID_374468297" CREATED="1341223619173" MODIFIED="1341223635555"/>
</node>
</node>
</map>
