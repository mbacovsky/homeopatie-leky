<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Magenta" ID="ID_407330046" CREATED="1341245857525" MODIFIED="1341245905730" LINK="../../Letn&#xed;%20&#x161;kola%202012%20-%20Podmitrov/L&#xe9;&#x10d;en&#xed;%20pomoc&#xed;%20barev.mm" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<hook NAME="MapStyle" max_node_width="600"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Info" POSITION="right" ID="ID_628948737" CREATED="1341246157174" MODIFIED="1341246879134" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="t&#xf3;n" ID="ID_1214082271" CREATED="1341246772250" MODIFIED="1341246777838">
<node TEXT="C" ID="ID_1316148778" CREATED="1341246779366" MODIFIED="1341246780454"/>
</node>
<node TEXT="&#x10d;akra" ID="ID_873448544" CREATED="1341246785002" MODIFIED="1341246788734">
<node TEXT="Alta Major" ID="ID_1564406475" CREATED="1341246789590" MODIFIED="1341246796737"/>
</node>
<node TEXT="&#x10d;erven&#xe1;+zelen&#xe1;+fialov&#xe1;" ID="ID_528718918" CREATED="1341246887375" MODIFIED="1341246942093"/>
<node TEXT="l&#xe9;&#x10d;&#xed; ob&#x11b;ti zneu&#x17e;it&#xed; a zanedb&#xe1;van&#xe9; d&#x11b;ti" ID="ID_684625450" CREATED="1341247075829" MODIFIED="1341247093436"/>
<node TEXT="barva pro zvl&#xe1;&#x161;tn&#xed; p&#x159;&#xed;le&#x17e;itosti" ID="ID_412870370" CREATED="1341246985653" MODIFIED="1341247167761"/>
<node TEXT="hlubok&#xe9; naz&#x159;en&#xed; na&#x161;&#xed; duchovn&#xed; podstaty" ID="ID_1528928306" CREATED="1341246994995" MODIFIED="1341247015276"/>
<node TEXT="posiluje vitalitu" ID="ID_912408312" CREATED="1341247386971" MODIFIED="1341247391339"/>
<node TEXT="povzbuzuje" ID="ID_1222181782" CREATED="1341247393405" MODIFIED="1341247397861">
<node TEXT="star&#x161;&#xed; lidi" ID="ID_405429269" CREATED="1341247398647" MODIFIED="1341247402136"/>
</node>
<node TEXT="p&#x159;itahuje velk&#xe9; myslitele" ID="ID_134625528" CREATED="1341247409181" MODIFIED="1341247416603"/>
</node>
<node TEXT="Souvisej&#xed;c&#xed; l&#xe9;ky" POSITION="right" ID="ID_1200010782" CREATED="1341247532850" MODIFIED="1341247539431" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Stronc" ID="ID_968973219" CREATED="1341247541039" MODIFIED="1341247543710"/>
<node TEXT="Kalia" ID="ID_1332322021" CREATED="1341247544230" MODIFIED="1341247545851"/>
<node TEXT="Mangany" ID="ID_1170486035" CREATED="1341247546220" MODIFIED="1341247550009"/>
<node TEXT="Digitalis" ID="ID_384083618" CREATED="1341247555466" MODIFIED="1341247558812"/>
<node TEXT="Niob" ID="ID_1097124438" CREATED="1341247560387" MODIFIED="1341247624984"/>
<node TEXT="Molybden" ID="ID_740712999" CREATED="1341247567988" MODIFIED="1341247576389"/>
</node>
<node TEXT="Res" POSITION="right" ID="ID_588033475" CREATED="1341245916668" MODIFIED="1341245928753" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="MAGENTA&#xa;This color represents the highest level of creativity and is associated with the realm of the collective unconscious. It is a mixture of red, green and violet, and encompasses the energy of the life force of red, the peace and harmony of green and the serenity of violet. It is a color that is used in healing to stimulate the adrenals, the heart and sexuality. It is said to strengthen the heart muscles and stabilize the heart&#x2019;s rhythm. It is the color which helps people find their spiritual insight and realistically view their problems from an elevated perspective. Do not take this remedy if you have a serious medical condition without consulting your doctor.&#xa; &#xa;Chakra: The Alta Major Chakra&#xa;This chakra sits about one foot above the head. It relates to our link to the collective mind of humanity, both in its most powerful way and in its most negative. The Alta Major Chakra hold information about the past , in particularly past life karma, and the contractual agreements we made with our higher self and others before incarnating in this life time.  This chakra is our telepathic link to all knowledge and activity and acts as a communication link which makes our ideas and thoughts interchangeable. It is the center higher truth.&#xa; &#xa;Mental; Great thinkers gravitate to this color. It awakens latent creativity, and at the same time stimulates the sexual realm. This energy can be transmuted into creative expression through this color. It elevates the thinking towards the higher realms of awareness while maintaining a grounded and realistic approach to life.&#xa;Contraindications:  Do not take this remedy if you have a serious medical condition. Please consult your doctor first.&#xa; &#xa; &#xa;Physical: This is a good heart tonic. It is good for impotency, and is used for frigidity and low libido.&#xa;Contraindications:  Magenta should not be used on hysterical patients or anyone with a serious medical condition. Please always consult your doctor before taking a remedy...&#xa; &#xa; &#xa;General Symptoms: Magenta is given when there is lack of insight or when the emotions are too strongly engaged and an overview of the presenting problems is necessary. It suits people who have trouble envisioning a larger horizon.&#xa;Contraindications: None." ID="ID_604590259" CREATED="1341245931443" MODIFIED="1341245933279"/>
</node>
<node TEXT="Proving" POSITION="right" ID="ID_788843360" CREATED="1341342046792" MODIFIED="1341342051880" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="C12 do vody" ID="ID_1488289131" CREATED="1341401230960" MODIFIED="1341401241833"/>
<node TEXT="2h po" ID="ID_466580686" CREATED="1341401160634" MODIFIED="1341401212611">
<node TEXT="chce se mi j&#xed;t ven" ID="ID_878782191" CREATED="1341342165887" MODIFIED="1341342170977"/>
<node TEXT="chci b&#xfd;t s&#xe1;m" ID="ID_749312284" CREATED="1341342054447" MODIFIED="1341342078153"/>
<node TEXT="nechci &#x159;e&#x161;it &#x17e;&#xe1;dn&#xe9; probl&#xe9;my" ID="ID_643254831" CREATED="1341342078889" MODIFIED="1341342100171"/>
<node TEXT="pot&#x159;ebuj&#xed; klid na sebe" ID="ID_1269039937" CREATED="1341342100596" MODIFIED="1341342109596"/>
<node TEXT="v&#x161;echno co mus&#xed;m &#x159;e&#x161;it m&#x11b; obt&#x11b;&#x17e;uje" ID="ID_1083620348" CREATED="1341342145761" MODIFIED="1341342158891"/>
<node TEXT="chce se mi sp&#xe1;t" ID="ID_1471085498" CREATED="1341342159196" MODIFIED="1341342165422"/>
</node>
<node TEXT="15h po" ID="ID_486199880" CREATED="1341401102811" MODIFIED="1341401224231">
<node TEXT="nic nem&#xe1; smysl" ID="ID_1105757590" CREATED="1341401120554" MODIFIED="1354007169201"/>
<node TEXT="chce se mi ut&#xe9;ct od lid&#xed;" ID="ID_374501985" CREATED="1341401122987" MODIFIED="1341401131432"/>
<node TEXT="i do v&#x11b;c&#xed; kter&#xe9; m&#x11b; norm&#xe1;ln&#x11b; bav&#xed; se mus&#xed;m nutit" ID="ID_754190992" CREATED="1341401132528" MODIFIED="1341401149052"/>
<node TEXT="jsem podra&#x17e;d&#x11b;n&#xfd;" ID="ID_1137180122" CREATED="1341401149844" MODIFIED="1341401157979"/>
<node TEXT="chce se mi nad&#xe1;vat" ID="ID_1397027490" CREATED="1341401180660" MODIFIED="1341401694179"/>
</node>
</node>
<node TEXT="Mage-aw" POSITION="left" ID="ID_1511911178" CREATED="1341247462498" MODIFIED="1341247466539" COLOR="#996600">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Trituace" POSITION="left" ID="ID_637431767" CREATED="1341245909993" MODIFIED="1341245915992" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="pocit ochrany" ID="ID_987034001" CREATED="1341246562344" MODIFIED="1341246568978"/>
<node TEXT="pocit opory" ID="ID_929941925" CREATED="1341246720709" MODIFIED="1341246734900"/>
<node TEXT="pohoda, klid" ID="ID_1163318961" CREATED="1341246660191" MODIFIED="1354007260022">
<font ITALIC="true"/>
</node>
<node TEXT="pocit d&#x16f;v&#x11b;ry" ID="ID_1424077535" CREATED="1341246569275" MODIFIED="1341246709626"/>
<node TEXT="k&#x159;&#xed;&#x17e;enec mezi n&#x11b;hou a radost&#xed;" ID="ID_1193964896" CREATED="1341246599383" MODIFIED="1341246614927"/>
<node TEXT="nekone&#x10d;no" ID="ID_1292071738" CREATED="1341246632684" MODIFIED="1341246637805"/>
</node>
<node TEXT="T&#xe9;mata" POSITION="left" ID="ID_669761944" CREATED="1341246879598" MODIFIED="1341246884854" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="nejvy&#x161;&#x161;&#xed; z&#xe1;mery v &#x17e;ivot&#x11b;" ID="ID_1841723017" CREATED="1341246959406" MODIFIED="1341246966861"/>
<node TEXT="kr&#xe1;lovsk&#xe1; barva" ID="ID_1436710040" CREATED="1341246943141" MODIFIED="1341246948178"/>
<node TEXT="nejvy&#x161;&#x161;&#xed; kreativita" ID="ID_1725732940" CREATED="1341246948564" MODIFIED="1341246957163"/>
<node TEXT="nedostatek" ID="ID_1560145285" CREATED="1341247474448" MODIFIED="1341247507392">
<font BOLD="true"/>
<node TEXT="ztr&#xe1;ta vhledu" ID="ID_1679841499" CREATED="1341247478228" MODIFIED="1341247482932"/>
<node TEXT="p&#x159;&#xed;li&#x161;n&#xe1; emo&#x10d;n&#xed; anga&#x17e;ovanost" ID="ID_1985878110" CREATED="1341247483461" MODIFIED="1341247494463"/>
<node TEXT="nevid&#xed; sv&#x16f;j c&#xed;l" ID="ID_1904780336" CREATED="1341247494768" MODIFIED="1341247503638"/>
</node>
<node TEXT="d&#xe1;v&#xe1;" ID="ID_1591504080" CREATED="1341247099068" MODIFIED="1341247509072">
<font BOLD="true"/>
<node TEXT="porozum&#x11b;n&#xed;" ID="ID_1249033257" CREATED="1341247102627" MODIFIED="1341247107641"/>
<node TEXT="lidstv&#xed;" ID="ID_1042609346" CREATED="1341247107874" MODIFIED="1341247110451"/>
<node TEXT="spiritu&#xe1;ln&#xed; p&#x159;ijet&#xed;" ID="ID_403614472" CREATED="1341247110684" MODIFIED="1341247117119"/>
<node TEXT="napojen&#xed; na universum" ID="ID_1658393119" CREATED="1341247117352" MODIFIED="1341247128904"/>
<node TEXT="&#x161;ir&#x161;&#xed; souvislosti" ID="ID_115489260" CREATED="1341247129321" MODIFIED="1341247137118"/>
<node TEXT="moudrost a vhled do situace" ID="ID_1770645261" CREATED="1341247195546" MODIFIED="1341247201989"/>
<node TEXT="vizion&#xe1;&#x159;sk&#xe9; uva&#x17e;ov&#xe1;n&#xed;" ID="ID_1356330750" CREATED="1341247215091" MODIFIED="1341247223762"/>
<node TEXT="propojen&#xed; obou hemisf&#xe9;r" ID="ID_1394222707" CREATED="1341247236528" MODIFIED="1341247243578"/>
<node TEXT="d&#x16f;v&#x11b;ru v budoucnost" ID="ID_1879442668" CREATED="1341247244930" MODIFIED="1341247252349"/>
<node TEXT="spojuje duchovn&#xed; a lidsk&#xe9; rozm&#x11b;ry" ID="ID_608733132" CREATED="1341247262454" MODIFIED="1341247272189"/>
<node TEXT="probouz&#xed; latentn&#xed; kreativitu" ID="ID_103576262" CREATED="1341247421573" MODIFIED="1341247428803"/>
<node TEXT="my&#x161;len&#xed; a vn&#xed;mavost a sou&#x10d;asn&#x11b; uzem&#x11b;n&#xed;" ID="ID_935652472" CREATED="1341247434093" MODIFIED="1341247448247"/>
</node>
<node TEXT="kolektivn&#xed; nev&#x11b;dom&#xed;" ID="ID_809803201" CREATED="1341247141531" MODIFIED="1341247148485"/>
</node>
<node TEXT="Symptomy" POSITION="left" ID="ID_52211119" CREATED="1341247277706" MODIFIED="1341247286623" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="stimuluje nadledvinky" ID="ID_647076104" CREATED="1341247287753" MODIFIED="1341247294112"/>
<node TEXT="srdce" ID="ID_1798836168" CREATED="1341247294441" MODIFIED="1341247297341">
<node TEXT="posiluje sval" ID="ID_1142647286" CREATED="1341247297838" MODIFIED="1341247304093"/>
<node TEXT="rytmus" ID="ID_977856901" CREATED="1341247304390" MODIFIED="1341247306897"/>
</node>
<node TEXT="sexualita" ID="ID_237359606" CREATED="1341247309397" MODIFIED="1341247312185">
<node TEXT="poruchy funkc&#xed; &#x17e;ensk&#xfd;ch org&#xe1;n&#x16f;" ID="ID_889525064" CREATED="1341247316438" MODIFIED="1341247327598"/>
<node TEXT="frigidita" ID="ID_1335744715" CREATED="1341247327990" MODIFIED="1341247331426"/>
<node TEXT="neplodnost" ID="ID_503757665" CREATED="1341247332011" MODIFIED="1341247334733"/>
<node TEXT="endometri&#xf3;za" ID="ID_1361704547" CREATED="1341247335014" MODIFIED="1341247339560"/>
<node TEXT="opo&#x17e;d&#x11b;n&#xe1; menstruace" ID="ID_1318612155" CREATED="1341247339801" MODIFIED="1341247351049"/>
<node TEXT="impotence" ID="ID_716985648" CREATED="1341247351332" MODIFIED="1341247354007"/>
<node TEXT="n&#xed;zk&#xe9; libido" ID="ID_904766748" CREATED="1341247354256" MODIFIED="1341247358876"/>
</node>
<node TEXT="nervov&#xe1; sousatva" ID="ID_837972012" CREATED="1341247362518" MODIFIED="1341247368559">
<node TEXT="stabilizuje" ID="ID_530736970" CREATED="1341247369744" MODIFIED="1341247377468"/>
</node>
</node>
<node TEXT="Kontraindikace" POSITION="left" ID="ID_1396760254" CREATED="1341246218395" MODIFIED="1341246227337" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="hysterie" ID="ID_754939246" CREATED="1341247511461" MODIFIED="1341247516698"/>
</node>
</node>
</map>
