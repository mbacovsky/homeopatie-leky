<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Yellow" ID="ID_1481122687" CREATED="1341154101745" MODIFIED="1341154438871" COLOR="#cc0000" STYLE="fork" LINK="../../Letn&#xed;%20&#x161;kola%202012%20-%20Podmitrov/L&#xe9;&#x10d;en&#xed;%20pomoc&#xed;%20barev.mm">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<hook NAME="MapStyle" max_node_width="600"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Yell-am" POSITION="right" ID="ID_1534626677" CREATED="1341154804618" MODIFIED="1341154831618" COLOR="#996600">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Symptomy" POSITION="right" ID="ID_135948407" CREATED="1341156141827" MODIFIED="1341156217978" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="koliky" ID="ID_123784624" CREATED="1341156147630" MODIFIED="1341156149465"/>
<node TEXT="&#x17e;lu&#x10d;n&#xed;kov&#xe9; z&#xe1;chvaty" ID="ID_1687518087" CREATED="1341156149730" MODIFIED="1341156156501"/>
<node TEXT="poruchy tr&#xe1;ven&#xed;" ID="ID_899152249" CREATED="1341156156718" MODIFIED="1341156162043">
<node TEXT="celiakie" ID="ID_709083655" CREATED="1341156164206" MODIFIED="1341156167068"/>
<node TEXT="p&#x159;ekyselen&#xed;" ID="ID_1492003681" CREATED="1341156315134" MODIFIED="1341156319493"/>
<node TEXT="p&#xe1;len&#xed; &#x17e;&#xe1;hy" ID="ID_1630808562" CREATED="1341156319782" MODIFIED="1341156325952"/>
<node TEXT="&#x17e;alude&#x10d;n&#xed; v&#x159;edy" ID="ID_979356292" CREATED="1341156326265" MODIFIED="1341156332157"/>
</node>
<node TEXT="poruchy p&#x159;&#xed;jmu potravy" ID="ID_1967329985" CREATED="1341156335891" MODIFIED="1341156343614"/>
<node TEXT="DM" ID="ID_1460099679" CREATED="1341156167524" MODIFIED="1341156175199"/>
<node TEXT="rakovina" ID="ID_345352614" CREATED="1341156181859" MODIFIED="1341156185863"/>
<node TEXT="zrak" ID="ID_741662436" CREATED="1341156219459" MODIFIED="1341156221482">
<node TEXT="prav&#xe9; oko" ID="ID_1430797128" CREATED="1341156222595" MODIFIED="1341156226146"/>
</node>
<node TEXT="pam&#x11b;&#x165;" ID="ID_1310319493" CREATED="1341156352828" MODIFIED="1341156356435"/>
<node TEXT="my&#x161;len&#xed;" ID="ID_1448145376" CREATED="1341156359895" MODIFIED="1341156363342"/>
<node TEXT="intelekt" ID="ID_1319795233" CREATED="1341156365381" MODIFIED="1341156369162"/>
</node>
<node TEXT="Odpov&#xed;daj&#xed;c&#xed; l&#xe9;ky" POSITION="right" ID="ID_1667350972" CREATED="1341156928374" MODIFIED="1341156959042" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Suplh" ID="ID_1863123862" CREATED="1341156937153" MODIFIED="1341156939741"/>
<node TEXT="Sil" ID="ID_1232713078" CREATED="1341156940068" MODIFIED="1341156941439"/>
<node TEXT="Ars" ID="ID_510589033" CREATED="1341156941829" MODIFIED="1341156944309"/>
<node TEXT="Fluoridy" ID="ID_38870133" CREATED="1341156944749" MODIFIED="1341156950128"/>
<node TEXT="Antim" ID="ID_116446516" CREATED="1341156950424" MODIFIED="1341156953051"/>
<node TEXT="Cadm" ID="ID_229592287" CREATED="1341156953556" MODIFIED="1341156955562"/>
<node TEXT="Oxyg" ID="ID_576976959" CREATED="1341156960054" MODIFIED="1341156969314"/>
<node TEXT="Nitro" ID="ID_1200391411" CREATED="1341156969835" MODIFIED="1341156977678"/>
<node TEXT="Heliantus" ID="ID_966919860" CREATED="1341156978206" MODIFIED="1341156981424"/>
<node TEXT="Bell-p" ID="ID_962050965" CREATED="1341156982911" MODIFIED="1341156987239"/>
<node TEXT="Berberis" ID="ID_1499984753" CREATED="1341156991043" MODIFIED="1341156993895"/>
<node TEXT="Junip" ID="ID_390690776" CREATED="1341156997739" MODIFIED="1341157000238"/>
<node TEXT="Lycop" ID="ID_675662092" CREATED="1341157010784" MODIFIED="1341157014361"/>
<node TEXT="Chell" ID="ID_1195776310" CREATED="1341157015393" MODIFIED="1341157022832"/>
<node TEXT="Aur" ID="ID_1946708198" CREATED="1341157027483" MODIFIED="1341157030226"/>
<node TEXT="Merc" ID="ID_656288856" CREATED="1341157030818" MODIFIED="1341157032408"/>
</node>
<node TEXT="Res" POSITION="right" ID="ID_183484059" CREATED="1341295834668" MODIFIED="1341295837046" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="YELLOW&#xa;Yellow is the color with the brightest light. It is associated with the sun and with the metal gold, the object in physical reality which shines the brightest. It is a diffuse energy and can create confusion because it has no boundaries. People can become disoriented when in the presence of too much light.&#xa;It is the color associated with personal power and triumph. It is linked to worth and value. Yellow lightens a dark mood or a dark day. It can generate feelings of euphoria after the heaviness of red of the energy buzz of orange.&#xa;It is associated with the element of fire that aids digestion and assimilation. It is interesting that yellow foods all are important to digestion such as lemon, grapefruit and corn. If you have a serious medical condition, consult your doctor before taking these remedies.&#xa; &#xa;Chakra: The Solar Plexus:  This chakra is located in the nerve ganglions between the stomach and the sternum. It is a center that revolves around our sense of personal identity and issues of personal power and worth.  When people acknowledge their self worth they begin to negotiate in the world as a Warrior archetype and no longer fall into the Servant archetype. They take personal responsibility for how they want their lives to unfold and are no longer at the mercy of how and what others dictate.&#xa;The qualities of the Solar Plexus chakra are: self worth, self esteem, confidence, personal power, and freedom of choice.&#xa; &#xa;Mentals:  Yellow is the color of gut responses, or lower intelligence. This color can stimulate the mind to be clear and focused. It charges the gut responses and carries this impulse into the higher mind to be analyzed and thought over.  Yellow can help increase the memory and thinking processes, and provide a rich potential for clarity and effectiveness.&#xa;Contraindication:  Too much yellow can cause a dissociated feeling from an over expansive mind. Do not take this color remedy if you have a serious medical condition. Please consult your doctor.&#xa; &#xa;Physicals: Yellow relates to the organs of digestion and can help with physical problems of breaking down food content. If the liver or gallbladder is weak this color can add additional strength. If there is chronic weakness from any form of liver disease, or gallbladder problems this color can tonify the area with energy and vitality. It is an excellent remedy for acid stomach, Celiac disease, which is the inability to breakdown gluten, and can be used concurrently with other remedies. If you have a serious medical condition, please consult your doctor before using this treatment.&#xa;Contraindications: Yellow should be used as an astringent and a decongestant. It should be used in daytime only as it can cause sleep disturbance by bringing too much energy into the system.&#xa; &#xa;General Symptoms: Yellow is a color that can be used by anyone with lack of confidence, problems with self worth or personal identity. It is well suited to people who give their life force over to others for their approval or affection.  Yellow strengthens the egoic forces and stimulates confidence and encourages personal empowerment.&#xa;Contraindications: This remedy should not be used by people who are overly confident or have an excessively developed ego. It should not be used at night.&#xa;Do not use this remedy if you have a serious medical condition. Please consult your doctor." ID="ID_1472710284" CREATED="1341295867128" MODIFIED="1341295868643"/>
</node>
<node TEXT="Trituace" POSITION="left" ID="ID_141431202" CREATED="1341154835514" MODIFIED="1341154841112" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="dynamika" ID="ID_611742784" CREATED="1341154887389" MODIFIED="1341154904720"/>
<node TEXT="el&#xe1;n" ID="ID_872513725" CREATED="1341154905081" MODIFIED="1341154915099"/>
<node TEXT="cht&#xed;t n&#x11b;co &#x159;e&#x161;it" ID="ID_601850061" CREATED="1341154916872" MODIFIED="1341154938258">
<font ITALIC="true"/>
</node>
<node TEXT="vyrovnanost" ID="ID_1920390326" CREATED="1341154940549" MODIFIED="1341154944172"/>
<node TEXT="euforie" ID="ID_1741334180" CREATED="1341154944445" MODIFIED="1341154954466"/>
</node>
<node TEXT="Info" POSITION="left" ID="ID_1199327134" CREATED="1341155229344" MODIFIED="1341156029497" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="nejjasn&#x11b;j&#x161;&#xed; ze v&#x161;ech barev" ID="ID_1312263398" CREATED="1341155233157" MODIFIED="1341155242316"/>
<node TEXT="kreativn&#xed; lid&#xe9;" ID="ID_1763756514" CREATED="1341155276197" MODIFIED="1341155281979"/>
<node TEXT="t&#xf3;n" ID="ID_939687460" CREATED="1341155252688" MODIFIED="1341155258287"/>
<node TEXT="miazma" ID="ID_724331318" CREATED="1341155259079" MODIFIED="1341155260827">
<node TEXT="syfilitick&#xe9;" ID="ID_391567699" CREATED="1341155261684" MODIFIED="1341155274474"/>
</node>
<node TEXT="kov" ID="ID_1417355883" CREATED="1341155433849" MODIFIED="1341155435220">
<node TEXT="zlato" ID="ID_1013725599" CREATED="1341155436476" MODIFIED="1341155438656"/>
</node>
<node TEXT="vitam&#xed;ny" ID="ID_793560296" CREATED="1341155441248" MODIFIED="1341155443695">
<node TEXT="A, C" ID="ID_294690481" CREATED="1341155444688" MODIFIED="1341155447075"/>
</node>
<node TEXT="barva c&#xed;sa&#x159;&#x16f;" ID="ID_389623353" CREATED="1341155450424" MODIFIED="1341155455826"/>
<node TEXT="archetyp" ID="ID_832799898" CREATED="1341155492467" MODIFIED="1341155502698">
<node TEXT="v&#xe1;le&#x10d;n&#xed;ka" ID="ID_453599775" CREATED="1341155503987" MODIFIED="1341155506846"/>
</node>
<node TEXT="barva sign&#xe1;ln&#xed;" ID="ID_1193717955" CREATED="1341155510951" MODIFIED="1341155518992"/>
<node TEXT="varov&#xe1;n&#xed;" ID="ID_1035720938" CREATED="1341155519736" MODIFIED="1341155525314"/>
<node TEXT="d&#xe1;v&#xe1;" ID="ID_956354269" CREATED="1341155559333" MODIFIED="1341155561314">
<node TEXT="pocit nad&#x11b;je" ID="ID_317105863" CREATED="1341155538294" MODIFIED="1341155564973"/>
<node TEXT="v&#xed;ra v lep&#x161;&#xed; budoucnost" ID="ID_1990014903" CREATED="1341155565326" MODIFIED="1341155572032"/>
<node TEXT="schopnost transformovat s&#xed;lu do reality l&#xe1;sky" ID="ID_1297971635" CREATED="1341155936910" MODIFIED="1341155953390"/>
</node>
<node TEXT="komplement&#xe1;rn&#xed; barva" ID="ID_1746652820" CREATED="1341155747388" MODIFIED="1341155755347">
<node TEXT="purpurov&#xe1;" ID="ID_851607960" CREATED="1341155756332" MODIFIED="1341155762818"/>
</node>
<node TEXT="org&#xe1;ny" ID="ID_1641072905" CREATED="1341156088873" MODIFIED="1341156092818">
<node TEXT="j&#xe1;tra" ID="ID_626145658" CREATED="1341156093795" MODIFIED="1341156096023"/>
<node TEXT="slinivka" ID="ID_1857239747" CREATED="1341156096384" MODIFIED="1341156098947"/>
<node TEXT="&#x17e;lu&#x10d;n&#xed;k" ID="ID_1129599967" CREATED="1341156099300" MODIFIED="1341156102963"/>
<node TEXT="tenk&#xe9; st&#x159;evo" ID="ID_1183059794" CREATED="1341156103324" MODIFIED="1341156107588"/>
</node>
</node>
<node TEXT="T&#xe9;mata" POSITION="left" ID="ID_798219385" CREATED="1341155310817" MODIFIED="1341156028411" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="touha po &#xfa;sp&#x11b;chu" ID="ID_1255581062" CREATED="1341155285038" MODIFIED="1341155301284">
<node TEXT="a&#x17e; k sebezni&#x10d;en&#xed;" ID="ID_1499031153" CREATED="1341155302173" MODIFIED="1341155309057"/>
</node>
<node TEXT="vlastn&#xed; sebehodnota" ID="ID_980701407" CREATED="1341155320057" MODIFIED="1341155327053"/>
<node TEXT="vz&#xed;t &#x17e;ivot do vlastn&#xed;ch rukou" ID="ID_1547200726" CREATED="1341155327316" MODIFIED="1341155338783"/>
<node TEXT="svoboda volby" ID="ID_1074672046" CREATED="1341155339032" MODIFIED="1341155342971"/>
<node TEXT="autorita" ID="ID_1141661190" CREATED="1341155343284" MODIFIED="1341155346210"/>
<node TEXT="integrita" ID="ID_1003746342" CREATED="1341155346499" MODIFIED="1341155352211"/>
<node TEXT="s&#xed;la osobnosti" ID="ID_1688391164" CREATED="1341155352540" MODIFIED="1341155355973"/>
<node TEXT="zdrav&#xe1; hrdost" ID="ID_975836724" CREATED="1341155357852" MODIFIED="1341155365351"/>
<node TEXT="nad&#x11b;je" ID="ID_1704558028" CREATED="1341155368600" MODIFIED="1341155375920"/>
<node TEXT="v&#xed;ra" ID="ID_665090757" CREATED="1341155365600" MODIFIED="1341155367408"/>
<node TEXT="osv&#xed;cen&#xed;" ID="ID_442420092" CREATED="1341155476094" MODIFIED="1341155479156"/>
<node TEXT="slunce" ID="ID_1796739580" CREATED="1341155389373" MODIFIED="1341155391526"/>
<node TEXT="vyrovnan&#xe9;" ID="ID_854248538" CREATED="1341155820965" MODIFIED="1341155824804">
<node TEXT="svoboda" ID="ID_1162301120" CREATED="1341155813987" MODIFIED="1341155829072"/>
<node TEXT="moc" ID="ID_948981048" CREATED="1341155829465" MODIFIED="1341155830852"/>
<node TEXT="um&#xed; s&#xed;lu pou&#x17e;&#xed;t, kdy&#x17e; je t&#x159;eba" ID="ID_1905887074" CREATED="1341155831230" MODIFIED="1341155842067"/>
</node>
<node TEXT="nedostatek" ID="ID_459913873" CREATED="1341155879462" MODIFIED="1341155883447">
<node TEXT="manipulovateln&#xfd;" ID="ID_348689374" CREATED="1341155578525" MODIFIED="1341155584234"/>
<node TEXT="zra&#x148;ovan&#xfd;" ID="ID_376037929" CREATED="1341155584714" MODIFIED="1341155589009"/>
<node TEXT="zneu&#x17e;&#xed;van&#xfd;" ID="ID_1493181081" CREATED="1341155589369" MODIFIED="1341155596018"/>
<node TEXT="dost&#xe1;v&#xe1; se do kompromituj&#xed;c&#xed;ch situac&#xed;" ID="ID_963256686" CREATED="1341155596499" MODIFIED="1341155609022"/>
<node TEXT="&#x10d;in&#xed; nespr&#xe1;vn&#xe1; rozhodnut&#xed;" ID="ID_1395484330" CREATED="1341155609232" MODIFIED="1341155620188"/>
<node TEXT="neum&#xed; bojovat za vlastn&#xed; pr&#xe1;va" ID="ID_815344171" CREATED="1341155620437" MODIFIED="1341155630528"/>
<node TEXT="odevzdali s&#xed;lu druh&#xfd;m" ID="ID_1562325777" CREATED="1341155630841" MODIFIED="1341155637016">
<node TEXT="nech&#xe1;vaj&#xed; je rozhodovat" ID="ID_215407680" CREATED="1341155644404" MODIFIED="1341155655598"/>
</node>
<node TEXT="pod&#x159;izuj&#xed; se" ID="ID_1529598626" CREATED="1341155656764" MODIFIED="1341155663073"/>
<node TEXT="jsou &#x10d;asto poni&#x17e;ovan&#xed;" ID="ID_1335042910" CREATED="1341155663419" MODIFIED="1341155670728"/>
<node TEXT="strachy" ID="ID_288036755" CREATED="1341155676129" MODIFIED="1341155678938"/>
<node TEXT="&#xfa;zkosti pochyby" ID="ID_346100975" CREATED="1341155679266" MODIFIED="1341155929154"/>
<node TEXT="starosti" ID="ID_1405085750" CREATED="1341155684618" MODIFIED="1341155687541"/>
<node TEXT="zmatek" ID="ID_659558610" CREATED="1341155690833" MODIFIED="1341155693667"/>
<node TEXT="snadno ztr&#xe1;c&#xed; osbn&#xed; hranice" ID="ID_1054801431" CREATED="1341155699804" MODIFIED="1341155723853"/>
</node>
<node TEXT="p&#x159;ebytek" ID="ID_1238972258" CREATED="1341155724948" MODIFIED="1341155731533">
<node TEXT="povy&#x161;uje se" ID="ID_801299151" CREATED="1341155732518" MODIFIED="1341155735700"/>
<node TEXT="chce moc a s&#xed;lu" ID="ID_259436540" CREATED="1341155735981" MODIFIED="1341155741601"/>
</node>
<node TEXT="pot&#x159;ebuj&#xed; lid&#xe9;, kte&#x159;&#xed; byli opu&#x161;t&#x11b;n&#xed; nebo za&#x17e;ili ztr&#xe1;tu" ID="ID_214519362" CREATED="1341155770582" MODIFIED="1341155792424"/>
<node TEXT="nalezen&#xed; osobn&#xed; s&#xed;ly" ID="ID_1566023621" CREATED="1341155795938" MODIFIED="1341155807834"/>
<node TEXT="blokovan&#xe1;" ID="ID_1998553126" CREATED="1341155844886" MODIFIED="1341155848729">
<node TEXT="slu&#x17e;ebn&#xed;k" ID="ID_1209033972" CREATED="1341155849809" MODIFIED="1341155854267"/>
<node TEXT="posera" ID="ID_35715738" CREATED="1341155854659" MODIFIED="1341155856679"/>
<node TEXT="agrese" ID="ID_1929202784" CREATED="1341155857000" MODIFIED="1341155860849"/>
<node TEXT="vztek" ID="ID_853609396" CREATED="1341155861186" MODIFIED="1341155862999"/>
<node TEXT="&#xfa;zkost" ID="ID_1754305122" CREATED="1341155863257" MODIFIED="1341155866283"/>
<node TEXT="zbab&#x11b;lec" ID="ID_1969448264" CREATED="1341155871516" MODIFIED="1341155874626"/>
</node>
<node TEXT="moudrost" ID="ID_784549456" CREATED="1341156375450" MODIFIED="1341156379812"/>
</node>
<node TEXT="Kontraindikace" POSITION="left" ID="ID_1682134391" CREATED="1341156739551" MODIFIED="1341156745801" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="na noc" ID="ID_906786690" CREATED="1341156746697" MODIFIED="1341156765237"/>
<node TEXT="egoist&#xe9;" ID="ID_1608646500" CREATED="1341156765582" MODIFIED="1341156769420"/>
</node>
</node>
</map>
