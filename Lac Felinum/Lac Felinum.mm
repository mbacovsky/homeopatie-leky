<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Lac Felinum" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1368285404292"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Ko&#x10d;i&#x10d;&#xed; ml&#xe9;ko" STYLE_REF="Remedy" POSITION="left" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1368285267890"/>
<node TEXT="Lac-F" STYLE_REF="Abbrev" POSITION="left" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1368285272091"/>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="left" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389">
<node TEXT="zdom&#xe1;cn&#x11b;la a p&#x159;itom si zachovala svobodu" ID="ID_955025932" CREATED="1368287861760" MODIFIED="1368287874037"/>
<node TEXT="je bolestiv&#xe9; vyta&#x17e;en&#xed; penisu" ID="ID_1625807170" CREATED="1368289194201" MODIFIED="1368289228973"/>
<node TEXT="samec se ji p&#x159;i aktu zakousne do krku a dr&#x17e;&#xed; ji" ID="ID_1661835898" CREATED="1368289284562" MODIFIED="1368289299824"/>
<node TEXT="odpor k vod&#x11b;" ID="ID_1798281937" CREATED="1368289638422" MODIFIED="1368289642683"/>
<node TEXT="nem&#xe1; cyklickou ovulaci" ID="ID_1326763" CREATED="1368343234763" MODIFIED="1368343250448">
<node TEXT="p&#x159;i kopulaci mus&#xed; b&#xfd;t hodn&#x11b;&#xa;dr&#xe1;&#x17e;d&#x11b;n&#xe1; aby do&#x161;lo k ovulaci" ID="ID_632383734" CREATED="1368343276807" MODIFIED="1368343405824"/>
</node>
</node>
<node TEXT="Podobn&#xe9; l&#xe9;ky" STYLE_REF="Subtopic" POSITION="left" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1357168625505">
<node TEXT="Sep" ID="ID_867709531" CREATED="1368288413953" MODIFIED="1368288415497">
<node TEXT="chce p&#x16f;sobit nez&#xe1;visle" STYLE_REF="Same" ID="ID_1808368613" CREATED="1368288416513" MODIFIED="1368288424694"/>
<node TEXT="v&#xfd;konnost" STYLE_REF="Same" ID="ID_1644777428" CREATED="1368288427174" MODIFIED="1368288431515"/>
<node TEXT="pragmati&#x10d;nost" STYLE_REF="Differs" ID="ID_709592858" CREATED="1368288436014" MODIFIED="1368288442753"/>
<node TEXT="men&#x161;&#xed; kontaktnost" STYLE_REF="Differs" ID="ID_801740674" CREATED="1368288459199" MODIFIED="1368288486736"/>
<node TEXT="unaven&#xe1;" STYLE_REF="Differs" ID="ID_733335248" CREATED="1368288539659" MODIFIED="1368288547725"/>
<node TEXT="r&#xe1;da tan&#x10d;&#xed;" STYLE_REF="Same" ID="ID_1177872115" CREATED="1368288617992" MODIFIED="1368288623659"/>
<node TEXT="sex jako v&#xfd;konnost, povinnost" STYLE_REF="Differs" ID="ID_459452328" CREATED="1368288666607" MODIFIED="1368288677808"/>
<node TEXT="ko&#x10d;ka je v&#xed;c mate&#x159;sk&#xe1;, &#x17e;ensk&#xe1; a kontaktn&#xed;" STYLE_REF="Comment" ID="ID_1416916015" CREATED="1368288566812" MODIFIED="1368288587446"/>
</node>
<node TEXT="Ign" ID="ID_573004048" CREATED="1368291706575" MODIFIED="1368291709684">
<node TEXT="hysterie" STYLE_REF="Same" ID="ID_247462603" CREATED="1368291710822" MODIFIED="1368291725544"/>
<node TEXT="&#x17e;ensk&#xe1; energie" STYLE_REF="Same" ID="ID_1785914416" CREATED="1368291715090" MODIFIED="1368291724805"/>
</node>
</node>
<node TEXT="Trituace" STYLE_REF="Subtopic" POSITION="left" ID="ID_1275465186" CREATED="1357168572522" MODIFIED="1357168576351">
<node TEXT="soust&#x159;ed&#x11b;nost" ID="ID_1407000283" CREATED="1368285282257" MODIFIED="1368285542547">
<font ITALIC="true"/>
</node>
<node TEXT="&#x161;kodolibost" ID="ID_772831386" CREATED="1368285519533" MODIFIED="1368285540644">
<font ITALIC="true"/>
</node>
<node TEXT="v&#x161;echno jde samo a hodn&#x11b; se toho stihne" ID="ID_743825883" CREATED="1368285552105" MODIFIED="1368285591621"/>
<node TEXT="roz&#x10d;iluj&#xed; m&#x11b; chlupat&#xfd; ko&#x165;ata" ID="ID_1485373043" CREATED="1368285720113" MODIFIED="1368285756351"/>
<node TEXT="kdy&#x17e; p&#x159;ijdu do restaurace, ka&#x17e;d&#xfd; se na m&#x11b; d&#xed;v&#xe1;" ID="ID_1067372627" CREATED="1368285909133" MODIFIED="1368285960114"/>
<node TEXT="m&#x16f;j d&#x16f;m je moje teritorium" ID="ID_1944384052" CREATED="1368286431315" MODIFIED="1368286443352">
<node TEXT="mus&#xed;m &#x10d;lov&#x11b;ku hodn&#x11b; v&#x11b;&#x159;it, abych ho pustila dovnit&#x159;" ID="ID_1586539859" CREATED="1368286444576" MODIFIED="1368286473424"/>
</node>
<node TEXT="moje rodina je m&#x16f;j man&#x17e;el" ID="ID_553238456" CREATED="1368286480077" MODIFIED="1368286496362">
<node TEXT="d&#x11b;ti ne, ty odejdou" ID="ID_1784163350" CREATED="1368286502838" MODIFIED="1368286512825"/>
</node>
<node TEXT="strach, &#x17e;e ji chceme uv&#xe1;zat na &#x159;et&#x11b;zu" ID="ID_1442015171" CREATED="1368287054589" MODIFIED="1368287067296"/>
<node TEXT="zranitelnost" ID="ID_409445404" CREATED="1368287432780" MODIFIED="1368287437983"/>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="left" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<node TEXT="emo&#x10d;n&#xed; zran&#x11b;n&#xed; po zn&#xe1;siln&#x11b;n&#xed;" ID="ID_1360975235" CREATED="1368289523710" MODIFIED="1368289534297"/>
<node TEXT="zneu&#x17e;it&#xed;" ID="ID_1065614464" CREATED="1368289717586" MODIFIED="1368289724092"/>
<node TEXT="hlava &#x208a; hrdlo" ID="ID_287107874" CREATED="1368291816983" MODIFIED="1368291861207">
<node TEXT="ang&#xed;ny" ID="ID_77083542" CREATED="1368291824422" MODIFIED="1368291828027"/>
<node TEXT="&#x161;t&#xed;tn&#xe1; &#x17e;l&#xe1;za" ID="ID_612404745" CREATED="1368291828778" MODIFIED="1368291834772"/>
<node TEXT="spazmy &#x161;&#xed;je" ID="ID_1721418714" CREATED="1368291835421" MODIFIED="1368291839811"/>
<node TEXT="bolesti hlavy" ID="ID_405762541" CREATED="1368291842823" MODIFIED="1368291847154"/>
<node TEXT="zuby" ID="ID_1876050425" CREATED="1368291867696" MODIFIED="1368291870015"/>
<node TEXT="d&#xe1;sn&#x11b;" ID="ID_790279022" CREATED="1368291870336" MODIFIED="1368291873029"/>
<node TEXT="o&#x10d;i" ID="ID_916451863" CREATED="1368291904552" MODIFIED="1368291907232">
<node TEXT="z&#xe1;n&#x11b;ty" ID="ID_1204774876" CREATED="1368291908080" MODIFIED="1368291911274"/>
<node TEXT="p&#xe1;len&#xed;" ID="ID_1206812128" CREATED="1368291911650" MODIFIED="1368291914213"/>
<node TEXT="v&#xfd;toky" ID="ID_1017194467" CREATED="1368291914637" MODIFIED="1368291918067"/>
</node>
<node TEXT="slin&#x11b;n&#xed;" ID="ID_1833824903" CREATED="1368291929209" MODIFIED="1368291931865"/>
<node TEXT="z&#xe1;n&#x11b;ty st&#x159;edou&#x161;&#xed;" ID="ID_68723910" CREATED="1368291957779" MODIFIED="1368291965067"/>
<node TEXT="ztr&#xe1;ta hlasu" ID="ID_1513965123" CREATED="1368292128697" MODIFIED="1368292132974"/>
</node>
<node TEXT="pl&#xed;ce" ID="ID_990636881" CREATED="1368292008150" MODIFIED="1368292009995">
<node TEXT="bronchytidy" ID="ID_607432815" CREATED="1368291965756" MODIFIED="1368291971385"/>
<node TEXT="z&#xe1;paly plic" ID="ID_1307064553" CREATED="1368291991836" MODIFIED="1368291996226"/>
</node>
<node TEXT="Gyn" ID="ID_997163510" CREATED="1368291937473" MODIFIED="1368291978192">
<node TEXT="bolestiv&#xe1; MS" ID="ID_405222044" CREATED="1368291941090" MODIFIED="1368291985223"/>
<node TEXT="bulky v prsou" ID="ID_819399511" CREATED="1368291985752" MODIFIED="1368291989922"/>
</node>
<node TEXT="z&#xe1;cpa" ID="ID_1301362458" CREATED="1368291945678" MODIFIED="1368291949289"/>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="left" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729"/>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="left" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690"/>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="right" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1357168653304">
<node TEXT="&#x17e;ensk&#xe1; energie" ID="ID_1703895935" CREATED="1368287259994" MODIFIED="1368287265688"/>
<node TEXT="rafinovanost" ID="ID_1388574773" CREATED="1368287290476" MODIFIED="1368287294289"/>
<node TEXT="ml&#xe9;ko" ID="ID_1122776129" CREATED="1368287828370" MODIFIED="1368287833497">
<font BOLD="true"/>
</node>
<node TEXT="tou&#x17e;&#xed; po svobod&#x11b; i po v&#xfd;hod&#xe1;ch domova" ID="ID_358633287" CREATED="1368287834016" MODIFIED="1368287851972">
<node TEXT="je to o pom&#x11b;ru mezi t&#xed;m" ID="ID_799991400" CREATED="1368287885593" MODIFIED="1368287901844"/>
</node>
<node TEXT="touha po svobod&#x11b;" ID="ID_620758453" CREATED="1368288203969" MODIFIED="1368288210121">
<font BOLD="true"/>
</node>
<node TEXT="t&#xe9;ma svobody a z&#xe1;vislosti" ID="ID_398681487" CREATED="1368288218331" MODIFIED="1368288233450"/>
<node TEXT="tendence odporovat" ID="ID_1824609394" CREATED="1368288260657" MODIFIED="1368288266087"/>
<node TEXT="bu&#x10f; je uzav&#x159;en&#xe1; nebo zcela oddan&#xe1;" ID="ID_558100181" CREATED="1368288351688" MODIFIED="1368288369822">
<node TEXT="pak se c&#xed;t&#xed; nesvobodn&#xe1;" ID="ID_771774211" CREATED="1368288370719" MODIFIED="1368288379096"/>
</node>
<node TEXT="hravost" ID="ID_724425721" CREATED="1368288631904" MODIFIED="1368288634953"/>
<node TEXT="sexualita" ID="ID_1582201654" CREATED="1368288635504" MODIFIED="1368292038194">
<font BOLD="true"/>
<node TEXT="zneu&#x17e;&#xed;v&#xe1;n&#xed;" ID="ID_1985985271" CREATED="1368288956241" MODIFIED="1368288968237"/>
<node TEXT="sny o zn&#xe1;siln&#x11b;n&#xed;" ID="ID_1396288350" CREATED="1368288982305" MODIFIED="1368288987870"/>
<node TEXT="v&#xfd;m&#x11b;na t&#x11b;la za trochu citu" ID="ID_605870254" CREATED="1368289325166" MODIFIED="1368289343678"/>
<node TEXT="nab&#xed;z&#xed; t&#x11b;lo jako jedinou svoji hodnotu" ID="ID_52219039" CREATED="1368292052741" MODIFIED="1368292069573"/>
</node>
<node TEXT="tanec" ID="ID_1714740863" CREATED="1368288649924" MODIFIED="1368288652274"/>
<node TEXT="nem&#xe1; sebe&#xfa;ctu" ID="ID_825398209" CREATED="1368288707777" MODIFIED="1368288714176">
<node TEXT="p&#x159;itahuje chlapy, kte&#x159;&#xed; se k n&#xed; nechovaj&#xed; dob&#x159;e" ID="ID_1524121195" CREATED="1368288715260" MODIFIED="1368288733734"/>
<node TEXT="nach&#xe1;z&#xed; si tyrana" ID="ID_862316546" CREATED="1368288744012" MODIFIED="1368288752904"/>
<node TEXT="nev&#x11b;&#x159;&#xed; si" ID="ID_1053998863" CREATED="1368288849367" MODIFIED="1368288855358"/>
<node TEXT="najde si st&#x159;&#xed;pek pozitivn&#xed;ho" ID="ID_523447373" CREATED="1368288871375" MODIFIED="1368288900148"/>
<node TEXT="stane se objektem zneu&#x17e;&#xed;v&#xe1;n&#xed;" ID="ID_1733117128" CREATED="1368288900740" MODIFIED="1368288909898"/>
<node TEXT="najde si trosku (kter&#xe1; j&#xed; neuspokojuje), proto&#x17e;e na v&#xed;c nem&#xe1;" ID="ID_1974953942" CREATED="1368289428375" MODIFIED="1368289457171"/>
<node TEXT="mus&#xed;m b&#xfd;t r&#xe1;da, &#x17e;e se mnou v&#x16f;bec n&#x11b;kdo je" ID="ID_400056587" CREATED="1368290050911" MODIFIED="1368290064762"/>
</node>
<node TEXT="pon&#xed;&#x17e;en&#xed;" ID="ID_720458614" CREATED="1368289508173" MODIFIED="1368289511786"/>
<node TEXT="kous&#xe1;n&#xed;" ID="ID_1669422562" CREATED="1368289585676" MODIFIED="1368289592047"/>
<node TEXT="sny o tonut&#xed;" ID="ID_1873263786" CREATED="1368289646882" MODIFIED="1368289667404"/>
<node TEXT="atraktivn&#xed; vzhled" ID="ID_1874094722" CREATED="1368289733379" MODIFIED="1368289810473"/>
<node TEXT="&#x10d;ist&#xe1; &#x17e;enskost, kter&#xe1; m&#x16f;&#x17e;e b&#xfd;t zran&#x11b;n&#xe1;" ID="ID_53773369" CREATED="1368289833998" MODIFIED="1368289853336"/>
<node TEXT="kompenzace" ID="ID_1748824113" CREATED="1368290182796" MODIFIED="1368290186755">
<node TEXT="promiskuita" ID="ID_937210526" CREATED="1368290175808" MODIFIED="1368290179017">
<node TEXT="pocit ne&#x161;t&#x11b;st&#xed;" ID="ID_1411065182" CREATED="1368290194385" MODIFIED="1368290199854"/>
</node>
<node TEXT="alkohol" ID="ID_286215642" CREATED="1368290162409" MODIFIED="1368290169036"/>
<node TEXT="l&#xe9;ky" ID="ID_501470740" CREATED="1368290169563" MODIFIED="1368290172515"/>
<node TEXT="drogy" ID="ID_385763267" CREATED="1368290173019" MODIFIED="1368290175447"/>
<node TEXT="feministky" ID="ID_82717288" CREATED="1368290219914" MODIFIED="1368290226232"/>
</node>
<node TEXT="strach z mu&#x17e;&#x16f;" ID="ID_1725058810" CREATED="1368290568492" MODIFIED="1368290580853">
<node TEXT="jejich agresivity" ID="ID_249700610" CREATED="1368290581852" MODIFIED="1368290586916"/>
</node>
<node TEXT="strach z opu&#x161;t&#x11b;n&#xed;" ID="ID_523540412" CREATED="1368290660077" MODIFIED="1368290665667"/>
<node TEXT="strach z ostr&#xfd;ch p&#x159;edm&#x11b;t&#x16f;" ID="ID_98421402" CREATED="1368290666985" MODIFIED="1368290674952">
<node TEXT="jehly" ID="ID_1783761955" CREATED="1368290683699" MODIFIED="1368290686515"/>
<node TEXT="no&#x17e;e" ID="ID_1002319752" CREATED="1368290687483" MODIFIED="1368290689075"/>
</node>
<node TEXT="strach z lid&#xed;" ID="ID_4850647" CREATED="1368290695920" MODIFIED="1368290732285">
<node TEXT="kompenzace zv&#xed;&#x159;aty" ID="ID_583141592" CREATED="1368290733429" MODIFIED="1368290741915"/>
</node>
<node TEXT="ve spole&#x10d;nosti p&#x16f;sob&#xed; pla&#x161;e" ID="ID_311130995" CREATED="1368290753369" MODIFIED="1368290783136"/>
<node TEXT="mysl&#xed; si, &#x17e;e jsou nehezc&#xed;" ID="ID_662432278" CREATED="1368290885652" MODIFIED="1368290910352">
<node TEXT="hloup&#xed;" ID="ID_56253064" CREATED="1368290912222" MODIFIED="1368290914855"/>
<node TEXT="nedostate&#x10d;n&#xed;" ID="ID_1586858621" CREATED="1368290915248" MODIFIED="1368290918687"/>
</node>
<node TEXT="strach &#x17e;e ud&#x11b;laj&#xed; chybu" ID="ID_851653135" CREATED="1368290919772" MODIFIED="1368290926878">
<node TEXT="&#x17e;e v&#x161;echno d&#x11b;laj&#xed; blb&#x11b;" ID="ID_624624334" CREATED="1368290927958" MODIFIED="1368290934926"/>
</node>
<node TEXT="kdy&#x17e; je zbl&#xe1;zn&#x11b;n&#xe1; do chlapa, jdou d&#x11b;ti stranou" ID="ID_870803332" CREATED="1368291315279" MODIFIED="1368291333955">
<node TEXT="dok&#xe1;zala by tolerovat i zn&#xe1;sil&#x148;ov&#xe1;n&#xed; d&#x11b;t&#xed;" ID="ID_397795981" CREATED="1368291393378" MODIFIED="1368291409470"/>
</node>
<node TEXT="perfekcionisti" ID="ID_435386905" CREATED="1368291477290" MODIFIED="1368291481767"/>
<node TEXT="maj&#xed; na sebe p&#x159;ehnan&#xe9; n&#xe1;roky" ID="ID_992356978" CREATED="1368291482647" MODIFIED="1368291491388"/>
<node TEXT="v&#xfd;konnost" ID="ID_254584218" CREATED="1368291500952" MODIFIED="1368291505945"/>
<node TEXT="rychlost" ID="ID_1964181142" CREATED="1368291506401" MODIFIED="1368291512934"/>
<node TEXT="snadno se dostanou do stavu vy&#x10d;erp&#xe1;n&#xed;" ID="ID_1770557629" CREATED="1368291536835" MODIFIED="1368291566947">
<node TEXT="vyho&#x159;en&#xed;" ID="ID_1423608480" CREATED="1368291570759" MODIFIED="1368291575456"/>
<node TEXT="histerie" ID="ID_859144932" CREATED="1368291669283" MODIFIED="1368291672170"/>
</node>
<node TEXT="pocity viny" ID="ID_15239355" CREATED="1368291661765" MODIFIED="1368291665748"/>
<node TEXT="smyslnost" ID="ID_1224112936" CREATED="1368291729616" MODIFIED="1368291732980"/>
<node TEXT="hravost" ID="ID_988343614" CREATED="1368291746521" MODIFIED="1368291749681"/>
<node TEXT="pohodl&#xed;&#x10d;ko" ID="ID_1936837602" CREATED="1368291750369" MODIFIED="1368291755505"/>
</node>
</node>
</map>
