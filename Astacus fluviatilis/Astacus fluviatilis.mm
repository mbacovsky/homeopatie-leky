<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Astacus fluviatilis" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1399066697969"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="left" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<node TEXT="k&#x159;e&#x10d;e" ID="ID_1423670199" CREATED="1398600340211" MODIFIED="1398600346699">
<node TEXT="cel&#xe9;ho t&#x11b;la" ID="ID_1864542062" CREATED="1398600394776" MODIFIED="1398600398441"/>
</node>
<node TEXT="pocen&#xed;" ID="ID_838824115" CREATED="1398600399484" MODIFIED="1398600403824">
<node TEXT="v&#xfd;razn&#xe9;" ID="ID_1779692750" CREATED="1398600405221" MODIFIED="1398600410648"/>
<node TEXT="u d&#x11b;t&#xed;" ID="ID_1684468870" CREATED="1398600416827" MODIFIED="1398600419763"/>
<node TEXT="v noci" ID="ID_1675752719" CREATED="1398600420149" MODIFIED="1398600422190"/>
</node>
<node TEXT="imunitn&#xed; syst&#xe9;m" ID="ID_758261214" CREATED="1398600425335" MODIFIED="1398600434200">
<node TEXT="p&#x159;ecitliv&#x11b;l&#xfd;" ID="ID_813825303" CREATED="1398600435115" MODIFIED="1398600440797"/>
<node TEXT="zdu&#x159;el&#xe9; uzliny" ID="ID_943069316" CREATED="1398600441057" MODIFIED="1398600448172"/>
<node TEXT="vysok&#xe1; hore&#x10d;ka" ID="ID_744537699" CREATED="1398600448406" MODIFIED="1398600453357"/>
<node TEXT="intenzivn&#xed; reakce" ID="ID_1986573393" CREATED="1398600453609" MODIFIED="1398600458110"/>
</node>
<node TEXT="vyr&#xe1;&#x17e;ky" ID="ID_187525421" CREATED="1398601157997" MODIFIED="1398601161539">
<node TEXT="kop&#x159;ivka" ID="ID_1451013120" CREATED="1398601165457" MODIFIED="1398601168617"/>
<node TEXT="u d&#x11b;ti" ID="ID_1515305355" CREATED="1398601168976" MODIFIED="1398601171296"/>
<node TEXT="p&#x159;i jatern&#xed;ch onemocn&#x11b;n&#xed;ch" ID="ID_694538931" CREATED="1398601171637" MODIFIED="1398601179557"/>
<node TEXT="slunce zhor&#x161;." ID="ID_562679780" CREATED="1398601206822" MODIFIED="1398601215535"/>
<node TEXT="po ml&#xed;ku" ID="ID_150798436" CREATED="1398601216716" MODIFIED="1398601219457"/>
</node>
<node TEXT="z&#xe1;da" ID="ID_1273826991" CREATED="1398601232575" MODIFIED="1398601234428">
<node TEXT="bolesti" ID="ID_1883725603" CREATED="1398601235907" MODIFIED="1398601237978"/>
</node>
<node TEXT="b&#x159;icho" ID="ID_1686066485" CREATED="1398601240893" MODIFIED="1398601243690">
<node TEXT="bolesti" ID="ID_1411702850" CREATED="1398601244775" MODIFIED="1398601247787"/>
</node>
</node>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="left" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690">
<node TEXT="" STYLE_REF="Agg" ID="ID_796467899" CREATED="1398601224589" MODIFIED="1398601226344">
<node TEXT="ml&#xe9;ko" ID="ID_254113793" CREATED="1398601227252" MODIFIED="1398601229108"/>
<node TEXT="slunce" ID="ID_864951922" CREATED="1398601252109" MODIFIED="1398601254062"/>
<node TEXT="vejce" ID="ID_1820363324" CREATED="1398601261881" MODIFIED="1398601263784"/>
<node TEXT="pohyb" ID="ID_1485404775" CREATED="1399067189483" MODIFIED="1399067192377"/>
</node>
</node>
<node TEXT="" POSITION="left" ID="ID_887681493" CREATED="1399066651810" MODIFIED="1399067224058" HGAP="-150" VSHIFT="80">
<hook URI="astacus_astacus_ff6967.jpg" SIZE="0.7033998" NAME="ExternalObject"/>
</node>
<node TEXT="Astacus fluviatilis" STYLE_REF="Remedy" POSITION="right" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1398600119176"/>
<node TEXT="Rak &#x159;&#xed;&#x10d;n&#xed;" STYLE_REF="Remedy" POSITION="right" ID="ID_74924311" CREATED="1398600132414" MODIFIED="1398600160435"/>
<node TEXT="Astac" STYLE_REF="Abbrev" POSITION="right" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1398600128858"/>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="right" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389">
<node TEXT="krat&#x161;&#xed; klepeta ne&#x17e; humr" ID="ID_1568122103" CREATED="1398600167043" MODIFIED="1398600180683"/>
<node TEXT="&#x161;patn&#x11b; sn&#xe1;&#x161;&#xed; zimu" ID="ID_246329498" CREATED="1398600181220" MODIFIED="1398600189138">
<node TEXT="zhrabe se bahna" ID="ID_1437775310" CREATED="1398600191323" MODIFIED="1398600216887"/>
</node>
<node TEXT="dok&#xe1;&#x17e;e &#x17e;&#xed;t i mimo vodn&#xed; prost&#x159;ed&#xed;" ID="ID_1484518442" CREATED="1398600219405" MODIFIED="1398600229047"/>
<node TEXT="svl&#xed;k&#xe1; se z k&#x16f;&#x17e;e" ID="ID_1869764274" CREATED="1398600282887" MODIFIED="1398600291121"/>
</node>
<node TEXT="Podobn&#xe9; l&#xe9;ky" STYLE_REF="Subtopic" POSITION="right" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1357168625505">
<node TEXT="Hom" ID="ID_24486916" CREATED="1398600636363" MODIFIED="1398600638518">
<node TEXT="pot&#x159;eba ochrany" STYLE_REF="Same" ID="ID_1538573160" CREATED="1398600641552" MODIFIED="1398600652787"/>
</node>
</node>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="right" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1357168653304">
<node TEXT="pot&#x159;eba ochrany" ID="ID_926777106" CREATED="1398600590677" MODIFIED="1398600600335">
<node TEXT="talisman" ID="ID_1940144709" CREATED="1398600601456" MODIFIED="1398600603774"/>
<node TEXT="konkr&#xe9;tn&#xed; p&#x159;edm&#x11b;t" ID="ID_1940555019" CREATED="1398600607227" MODIFIED="1398600613383">
<node TEXT="ply&#x161;ov&#xfd; medv&#xed;dek" ID="ID_1707943063" CREATED="1398600614476" MODIFIED="1398600620491"/>
<node TEXT="panenka" ID="ID_1069459886" CREATED="1398600620733" MODIFIED="1398600624604"/>
<node TEXT="hra&#x10d;ka" ID="ID_1134103900" CREATED="1398600625285" MODIFIED="1398600628114"/>
</node>
<node TEXT="m&#xe1; k n&#x11b;mu &#xfa;zk&#xfd; vztah" ID="ID_491417882" CREATED="1398600671214" MODIFIED="1398600679354"/>
<node TEXT="&#x10d;asto to taj&#xed;" ID="ID_1206971991" CREATED="1398600772587" MODIFIED="1398600777695">
<node TEXT="necht&#x11b;j&#xed; b&#xfd;t pova&#x17e;ov&#xe1;ni za pov&#x11b;r&#x10d;iv&#xe9;" ID="ID_902153425" CREATED="1398600779093" MODIFIED="1398600792424"/>
</node>
<node TEXT="obsese" ID="ID_1221050513" CREATED="1398600815497" MODIFIED="1398600831506">
<node TEXT="z&#xe1;vislost na talismanu" ID="ID_988299513" CREATED="1398600834839" MODIFIED="1398600842046"/>
</node>
</node>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="right" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729">
<node TEXT="Mind; company; desire for" ID="ID_1957835128" CREATED="1398600952393" MODIFIED="1398600952393"/>
<node TEXT="Mind; company; desire for; friend, friends, of" ID="ID_828915015" CREATED="1398600958217" MODIFIED="1398600958217"/>
<node TEXT="Mind; suspiciousness, mistrustfulness; company of friends, but needs" ID="ID_689124925" CREATED="1399066851441" MODIFIED="1399066851441"/>
<node TEXT="Mind; anger" ID="ID_1232116207" CREATED="1398600969134" MODIFIED="1398600969134"/>
<node TEXT="Mind; rage, fury" ID="ID_1171150914" CREATED="1398600978497" MODIFIED="1398600978497"/>
<node TEXT="Mind; sensitive, oversensitive; music, to" ID="ID_900592254" CREATED="1398600992186" MODIFIED="1398600992186"/>
<node TEXT="Mind; music; desires; loud" ID="ID_834420724" CREATED="1398601009776" MODIFIED="1398601009776"/>
<node TEXT="Mind; fear; dogs, of" ID="ID_592442521" CREATED="1398601061791" MODIFIED="1398601061791"/>
<node TEXT="Mind; fear; fire" ID="ID_1411500215" CREATED="1398601077907" MODIFIED="1398601077907"/>
<node TEXT="Mind; liar" ID="ID_1044348828" CREATED="1398601091794" MODIFIED="1398601091794"/>
<node TEXT="Mind; dreams; pursued, of being; murderer, being a" ID="ID_275422763" CREATED="1399066759679" MODIFIED="1399066759679"/>
<node TEXT="Mind; dreams; repeating; anxious; frame by frame" ID="ID_1864055894" CREATED="1399066777214" MODIFIED="1399066777214"/>
<node TEXT="Mind; fear; noon; three pm., until" ID="ID_1988807862" CREATED="1399066804394" MODIFIED="1399066804394"/>
<node TEXT="Mind; hide; oneself, desire to; room, in his" ID="ID_825701721" CREATED="1399066833357" MODIFIED="1399066833357"/>
<node TEXT="Mind; stupefaction, as if intoxicated" ID="ID_1632012091" CREATED="1399066909198" MODIFIED="1399066909198"/>
<node TEXT="Mind; discontented; everything, with" ID="ID_1812073562" CREATED="1399066924519" MODIFIED="1399066924519"/>
<node TEXT="Skin; eruptions" ID="ID_561615053" CREATED="1399066953302" MODIFIED="1399066953302"/>
<node TEXT="Skin; eruptions; urticaria, nettle-rash" ID="ID_1640033072" CREATED="1399066960724" MODIFIED="1399066960724"/>
<node TEXT="Skin; eruptions; urticaria, nettle-rash; food, after" ID="ID_1884102127" CREATED="1399066966813" MODIFIED="1399066966813"/>
<node TEXT="Skin; itching; scratch, must; bleeds, until it" ID="ID_389808301" CREATED="1399067007764" MODIFIED="1399067007764"/>
<node TEXT="Generalities; ulcers" ID="ID_799493012" CREATED="1399067131796" MODIFIED="1399067131796"/>
<node TEXT="Skin; redness" ID="ID_939698459" CREATED="1399067016071" MODIFIED="1399067016071"/>
<node TEXT="Skin; itching; warmth; agg." ID="ID_1026559436" CREATED="1399067032468" MODIFIED="1399067032468"/>
<node TEXT="Generalities; pain; cramping" ID="ID_605301062" CREATED="1399067089337" MODIFIED="1399067089337"/>
<node TEXT="Generalities; pain; neuralgic" ID="ID_8734596" CREATED="1399067094991" MODIFIED="1399067094991"/>
<node TEXT="Generalities; pain; pressing" ID="ID_1266848825" CREATED="1399067099917" MODIFIED="1399067099917"/>
<node TEXT="Generalities; pain; tearing" ID="ID_1501520418" CREATED="1399067105992" MODIFIED="1399067105992"/>
<node TEXT="Generalities; pain; motion; agg." ID="ID_576316585" CREATED="1399067160346" MODIFIED="1399067160346"/>
<node TEXT="Generalities; sun; agg.; sunburn" ID="ID_181597833" CREATED="1399067114224" MODIFIED="1399067114224"/>
</node>
</node>
</map>
