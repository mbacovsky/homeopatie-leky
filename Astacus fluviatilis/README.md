
Astacus fluviatilis
==============

![mapa](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Astacus%20fluviatilis/Astacus%20fluviatilis.svg)

 - [PDF](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Astacus%20fluviatilis/Astacus%20fluviatilis.pdf)
 - [Map](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Astacus%20fluviatilis/Astacus%20fluviatilis.mm)
 - [SVG](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Astacus%20fluviatilis/Astacus%20fluviatilis.svg)

Rubriky
-------
 - Generalities; pain; cramping 
 - Generalities; pain; motion; agg. 
 - Generalities; pain; neuralgic 
 - Generalities; pain; pressing 
 - Generalities; pain; tearing 
 - Generalities; sun; agg.; sunburn 
 - Generalities; ulcers 
 - Mind; anger 
 - Mind; company; desire for 
 - Mind; company; desire for; friend, friends, of 
 - Mind; discontented; everything, with 
 - Mind; dreams; pursued, of being; murderer, being a 
 - Mind; dreams; repeating; anxious; frame by frame 
 - Mind; fear; dogs, of 
 - Mind; fear; fire 
 - Mind; fear; noon; three pm., until 
 - Mind; hide; oneself, desire to; room, in his 
 - Mind; liar [[Op](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Opium)]
 - Mind; music; desires; loud 
 - Mind; rage, fury [[Hom](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Homarus gammarus)]
 - Mind; sensitive, oversensitive; music, to 
 - Mind; stupefaction, as if intoxicated 
 - Mind; suspiciousness, mistrustfulness; company of friends, but needs 
 - Skin; eruptions 
 - Skin; eruptions; urticaria, nettle-rash 
 - Skin; eruptions; urticaria, nettle-rash; food, after 
 - Skin; itching; scratch, must; bleeds, until it 
 - Skin; itching; warmth; agg. 
 - Skin; redness 
    