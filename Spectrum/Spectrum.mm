<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Spectrum" ID="ID_872387895" CREATED="1341243485430" MODIFIED="1341243515239" LINK="../../Letn&#xed;%20&#x161;kola%202012%20-%20Podmitrov/L&#xe9;&#x10d;en&#xed;%20pomoc&#xed;%20barev.mm" COLOR="#cc0000" STYLE="fork">
<hook NAME="MapStyle" max_node_width="600"/>
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Trituace" POSITION="right" ID="ID_287355303" CREATED="1341243516864" MODIFIED="1341243523573" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="celistvost" ID="ID_1548813728" CREATED="1341243976033" MODIFIED="1341244012259">
<font ITALIC="true"/>
</node>
<node TEXT="kruh se uzav&#x159;el" ID="ID_756027942" CREATED="1341243980821" MODIFIED="1341244013721">
<font ITALIC="true"/>
</node>
<node TEXT="nad&#x159;azenost" ID="ID_389357412" CREATED="1341243999872" MODIFIED="1341244015336">
<font ITALIC="true"/>
</node>
<node TEXT="sloup" ID="ID_1196930468" CREATED="1341244156422" MODIFIED="1341244159937"/>
<node TEXT="hork&#xe9; ruce" ID="ID_1778818658" CREATED="1341244163773" MODIFIED="1341244169854"/>
<node TEXT="horko" ID="ID_492120872" CREATED="1341244170607" MODIFIED="1341244188604">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="koncentrace" ID="ID_1317960382" CREATED="1341244190411" MODIFIED="1341244200749"/>
<node TEXT="vytr&#x17e;en&#xed; z reality" ID="ID_1526196130" CREATED="1341244201973" MODIFIED="1341244207635"/>
<node TEXT="sv&#x11b;d&#x11b;n&#xed;" ID="ID_905180099" CREATED="1341244211831" MODIFIED="1341244217363">
<node TEXT="obli&#x10d;ej" ID="ID_487223764" CREATED="1341244218443" MODIFIED="1341244223201"/>
<node TEXT="vlasy" ID="ID_705349273" CREATED="1341244223473" MODIFIED="1341244225710"/>
</node>
<node TEXT="iritace" ID="ID_939173527" CREATED="1341244312110" MODIFIED="1341244314630"/>
<node TEXT="intenzita" ID="ID_392578242" CREATED="1341244367179" MODIFIED="1341244398905"/>
</node>
<node TEXT="T&#xe9;mata" POSITION="right" ID="ID_778799743" CREATED="1341244407026" MODIFIED="1341244428943" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="imunita" ID="ID_1646070809" CREATED="1341244410792" MODIFIED="1341244413176"/>
<node TEXT="d&#xe1;v&#xe1;" ID="ID_1649680176" CREATED="1341244490183" MODIFIED="1341244493083">
<node TEXT="energii t&#x11b;lu i duchu" ID="ID_483574690" CREATED="1341244493892" MODIFIED="1341244499861"/>
<node TEXT="vitalita vn&#xed;mavost" ID="ID_863138879" CREATED="1341244649431" MODIFIED="1341244654013"/>
<node TEXT="klid a rovnov&#xe1;ha po dlouho trvaj&#xed;c&#xed; pr&#xe1;ci" ID="ID_158907169" CREATED="1341244806908" MODIFIED="1341244822089"/>
</node>
</node>
<node TEXT="Kontraindikace" POSITION="left" ID="ID_4304229" CREATED="1341244827141" MODIFIED="1341244847544" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="na noc" ID="ID_1116775290" CREATED="1341244835690" MODIFIED="1341244840012"/>
</node>
<node TEXT="Info" POSITION="left" ID="ID_240434042" CREATED="1341244399719" MODIFIED="1341244428492" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="akord" ID="ID_1243968050" CREATED="1341244403213" MODIFIED="1341244610264" COLOR="#659919">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<node TEXT="inspiruje holistick&#xe9; l&#xe9;&#x10d;en&#xed;" ID="ID_488178556" CREATED="1341244524068" MODIFIED="1341244533414"/>
<node TEXT="vn&#xe1;&#x161;&#xed; akord do v&#x161;eho kon&#xe1;n&#xed;" ID="ID_1985205016" CREATED="1341244533711" MODIFIED="1341244543874"/>
<node TEXT="posiluje kultivovanost" ID="ID_565891599" CREATED="1341244549539" MODIFIED="1341244556200"/>
<node TEXT="odhodl&#xe1;n&#xed;" ID="ID_373763378" CREATED="1341244556465" MODIFIED="1341244565231"/>
<node TEXT="otv&#xed;r&#xe1; tv&#x16f;r&#x10d;&#xed; &#x159;e&#x161;en&#xed; v&#x161;ech situac&#xed;" ID="ID_442583410" CREATED="1341244567086" MODIFIED="1341244581431"/>
</node>
<node TEXT="lze pod&#xe1;vat dlouh&#xfd; &#x10d;as bez zhor&#x161;en&#xed;" ID="ID_831603590" CREATED="1341244505508" MODIFIED="1341244520129"/>
</node>
<node TEXT="Res" POSITION="left" ID="ID_1361145322" CREATED="1341244956649" MODIFIED="1341244985010" COLOR="#0066cc" STYLE="fork" HGAP="-48" VSHIFT="39">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="SPECTRUM&#xa;This is a combination of all the colors in the homeopathic color remedy kit. It is the rainbow of colors and brings a multitude of energy and healing with it. It offers the stimulation, tonification and sedation that all the colors bring. It has a special affinity to auto-immune breakdown and helps revive tired, stressed out systems. It can be taken daily for many months to help recover from chronic illness or injury.&#xa;It combines the energy of all the chakras. Do not take this remedy without consulting your doctor first.&#xa; &#xa;Mental: this remedy is good for reviving tired minds and is excellent for recovering one&#x2019;s equilibrium after taxing work experiences, or stressful events where one would feel drained and empty.&#xa;Contraindications: Do not take this remedy without consulting your doctor before hand.&#xa; &#xa;Physical: This remedy has been used to boost energy in many different disease states. It has helped in pregnancy, labor, and post partum care. It is given for any degenerative disease state and helped people with immunosuppressive diseases have more vitality at their command. Anyone with a long term chronic disease, where severe pathology exists, feels better from taking this remedy.&#xa;Contraindications: Do not take this remedy without consulting your doctor first.&#xa; &#xa;General Symptoms:  This remedy is useful for people who suffer from nervous exhaustion and emotional upset. It is a remedy that offers relief. It helps people handle change and trauma in their lives.&#xa;Contraindications:  Because of the stimulating colors it may be best to take this remedy in the morning or during day light. Do not take this remedy without consulting your doctor first." ID="ID_590471749" CREATED="1341244962687" MODIFIED="1341244964986"/>
</node>
<node TEXT="Symptomy" POSITION="right" ID="ID_1338960282" CREATED="1341244449750" MODIFIED="1341244453991" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="imunita" ID="ID_202941551" CREATED="1341244459664" MODIFIED="1341244462384">
<node TEXT="AIDS" ID="ID_1563963336" CREATED="1341244467211" MODIFIED="1341244469064"/>
</node>
<node TEXT="stres a vy&#x10d;erp&#xe1;n&#xed;" ID="ID_1562224267" CREATED="1341244613022" MODIFIED="1341244618311"/>
<node TEXT="n&#xe1;sledky alopatick&#xfd;ch l&#xe9;k&#x16f;" ID="ID_671025702" CREATED="1341244629025" MODIFIED="1341244640479"/>
<node TEXT="z&#xe1;vislosti" ID="ID_1555987398" CREATED="1341244658180" MODIFIED="1341244676202">
<node TEXT="drogy" ID="ID_841996786" CREATED="1341244663326" MODIFIED="1341244665290"/>
<node TEXT="alkohol" ID="ID_1419218837" CREATED="1341244665564" MODIFIED="1341244669599"/>
</node>
<node TEXT="z&#xe1;n&#x11b;ty" ID="ID_270012101" CREATED="1341244679471" MODIFIED="1341244682601">
<node TEXT="lymfatick&#xfd;ch uzlin" ID="ID_123339052" CREATED="1341244683778" MODIFIED="1341244689367"/>
</node>
<node TEXT="t&#x11b;hotenstv&#xed;" ID="ID_902741147" CREATED="1341244690380" MODIFIED="1341244699423"/>
<node TEXT="porod" ID="ID_17283783" CREATED="1341244699688" MODIFIED="1341244701358"/>
<node TEXT="poporodn&#xed; p&#xe9;&#x10d;e" ID="ID_1852045865" CREATED="1341244701631" MODIFIED="1341244706805"/>
<node TEXT="degenerativn&#xed; onemocn&#x11b;n&#xed;" ID="ID_351216060" CREATED="1341244707142" MODIFIED="1341244717637"/>
<node TEXT="&#xfa;navov&#xfd; syndrom" ID="ID_862014799" CREATED="1341244717878" MODIFIED="1341244739105"/>
<node TEXT="postvirov&#xfd; syndrom" ID="ID_1730021311" CREATED="1341244739403" MODIFIED="1341244744761"/>
<node TEXT="nervov&#xe9; vy&#x10d;erp&#xe1;n&#xed;" ID="ID_1845843996" CREATED="1341244745018" MODIFIED="1341244751523"/>
<node TEXT="emocion&#xe1;ln&#xed; zran&#x11b;n&#xed;" ID="ID_1051266858" CREATED="1341244751763" MODIFIED="1341244757569"/>
<node TEXT="emo&#x10d;n&#xed; zm&#x11b;ny a traumata" ID="ID_1123938382" CREATED="1341244760398" MODIFIED="1341244771721"/>
<node TEXT="posiluj&#xed; unavenou mysl" ID="ID_159688596" CREATED="1341244776389" MODIFIED="1341244781795"/>
<node TEXT="tonikum po pr&#xe1;ci s velk&#xfd;mi skupinami lid&#xed;" ID="ID_1129617205" CREATED="1341244788315" MODIFIED="1341244800665"/>
</node>
</node>
</map>
