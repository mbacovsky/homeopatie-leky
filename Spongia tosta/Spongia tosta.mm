<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Spongia tosta" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1398588782772"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="left" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389">
<node TEXT="shluk bun&#x11b;k, kter&#xe9; spole&#x10d;n&#x11b; filtruj&#xed; vodu" ID="ID_1148935744" CREATED="1398588816055" MODIFIED="1398588896233"/>
<node TEXT="kdy&#x17e; ji propas&#xed;rujete p&#x159;es jemn&#xe9; s&#xed;tko, funguje d&#xe1;l" ID="ID_1779131485" CREATED="1398588897496" MODIFIED="1398588915776"/>
<node TEXT="&#x17e;&#xed;han&#xe1; houba" ID="ID_1809359462" CREATED="1398592284230" MODIFIED="1398592297689"/>
</node>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="left" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690">
<node TEXT="" STYLE_REF="Agg" ID="ID_1950421047" CREATED="1398592267629" MODIFIED="1398592271491">
<node TEXT="horko" ID="ID_716711518" CREATED="1398592272703" MODIFIED="1398592276357"/>
<node TEXT="teplo" ID="ID_913451139" CREATED="1398592276707" MODIFIED="1398592278843"/>
<node TEXT="t&#x11b;sn&#xe9; oble&#x10d;en&#xed; u krku" ID="ID_170986771" CREATED="1398592554689" MODIFIED="1398592561189"/>
</node>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="left" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729">
<node TEXT="Mind; anguish; heart complaints, in" ID="ID_1470123023" CREATED="1398591789725" MODIFIED="1398591789725"/>
<node TEXT="Mind; anxiety; night; agg." ID="ID_220122083" CREATED="1398591808275" MODIFIED="1398591808275"/>
<node TEXT="Mind; anxiety; night; agg.; palpitation, with" ID="ID_277511545" CREATED="1398591816081" MODIFIED="1398591816081"/>
<node TEXT="Mind; anxiety; midnight; after" ID="ID_1253657859" CREATED="1398591839900" MODIFIED="1398591839900"/>
<node TEXT="Mind; anxiety; cough; during" ID="ID_75525795" CREATED="1398591852579" MODIFIED="1398591852579"/>
<node TEXT="Mind; anxiety; cramps, with" ID="ID_1922877916" CREATED="1398591858627" MODIFIED="1398591858627"/>
<node TEXT="Mind; anxiety; croup, in" ID="ID_824431228" CREATED="1398591866928" MODIFIED="1398591866928"/>
<node TEXT="Mind; anxiety; driving him from place to place; dyspnea, in" ID="ID_275678480" CREATED="1398591881195" MODIFIED="1398591881195"/>
<node TEXT="Mind; anxiety; driving him from place to place" ID="ID_19704768" CREATED="1398591886935" MODIFIED="1398591886935"/>
<node TEXT="Mind; anxiety; dyspnea, in" ID="ID_1027885558" CREATED="1398591896216" MODIFIED="1398591896216"/>
<node TEXT="Mind; anxiety; faintness; with" ID="ID_164225652" CREATED="1398591904008" MODIFIED="1398591904008"/>
<node TEXT="Mind; anxiety; flushes of heat, with" ID="ID_182551256" CREATED="1398591911409" MODIFIED="1398591911409"/>
<node TEXT="Mind; anxiety; heart complaints, in" ID="ID_824170129" CREATED="1398591922515" MODIFIED="1398591922515"/>
<node TEXT="Mind; anxiety; pain, with; heart, in and about" ID="ID_1248026902" CREATED="1398591992343" MODIFIED="1398591992343"/>
<node TEXT="Mind; anxiety; palpitation; with" ID="ID_982094333" CREATED="1398592000615" MODIFIED="1398592000615"/>
<node TEXT="Mind; anxiety; perspiration; with" ID="ID_1204349614" CREATED="1398592008609" MODIFIED="1398592008609"/>
<node TEXT="Mind; anxiety; respiration; difficult, anxious, with" ID="ID_996159256" CREATED="1398592022368" MODIFIED="1398592022368"/>
<node TEXT="Mind; anxiety; sleep; during" ID="ID_473731923" CREATED="1398592032688" MODIFIED="1398592032688"/>
<node TEXT="Mind; anxiety; weakness, with" ID="ID_1705728895" CREATED="1398592042212" MODIFIED="1398592042212"/>
<node TEXT="Mind; anxiety; heart region" ID="ID_850076546" CREATED="1398592067132" MODIFIED="1398592067132"/>
<node TEXT="Mind; anxiety; heart region; pain in heart, with; cramping" ID="ID_982974078" CREATED="1398592077536" MODIFIED="1398592077536"/>
<node TEXT="Mind; anxiety; fainting, as from" ID="ID_1743237918" CREATED="1398592111672" MODIFIED="1398592111672"/>
<node TEXT="Mind; anxiety; sudden, paroxysmal" ID="ID_759665546" CREATED="1398592125076" MODIFIED="1398592125076"/>
<node TEXT="Mind; anxiety; sudden, paroxysmal; heart complaints, in" ID="ID_1471278539" CREATED="1398592132716" MODIFIED="1398592132716"/>
<node TEXT="Mind; anxiety; sudden, paroxysmal; throat" ID="ID_1192389147" CREATED="1398592139353" MODIFIED="1398592139353"/>
<node TEXT="Mind; closing eyes; agg." ID="ID_958194212" CREATED="1398592199318" MODIFIED="1398592199318"/>
<node TEXT="Mind; consolation; ailments from, agg." ID="ID_704645363" CREATED="1398592213717" MODIFIED="1398592213717"/>
<node TEXT="Mind; despair; heat agg." ID="ID_298773452" CREATED="1398592227318" MODIFIED="1398592227318"/>
<node TEXT="Mind; faintness, fainting, with" ID="ID_1489861348" CREATED="1398592243103" MODIFIED="1398592243103"/>
<node TEXT="Mind; fear; midnight; after" ID="ID_708645339" CREATED="1398592260771" MODIFIED="1398592260771"/>
<node TEXT="Mind; fear; palpitation, with" ID="ID_138730004" CREATED="1398592311085" MODIFIED="1398592311085"/>
<node TEXT="Mind; fear; perspiration, with" ID="ID_893585322" CREATED="1398592319081" MODIFIED="1398592319081"/>
<node TEXT="Mind; fear; death, of; suffocation, from" ID="ID_972091038" CREATED="1398592333094" MODIFIED="1398592333094"/>
<node TEXT="Mind; fear; death, of; termination, of fatal" ID="ID_656932437" CREATED="1398592339672" MODIFIED="1398592339672"/>
<node TEXT="Mind; fear; disease, of; incurable, of being" ID="ID_1166571029" CREATED="1398592350238" MODIFIED="1398592350238"/>
<node TEXT="Mind; fear; disease, of; terminal, fatal" ID="ID_294478001" CREATED="1398592463433" MODIFIED="1398592463433"/>
<node TEXT="Mind; fear; happen; something will; bad, evil" ID="ID_1912847521" CREATED="1398592471771" MODIFIED="1398592471771"/>
<node TEXT="Mind; fear; panic attacks, overpowering" ID="ID_1114620101" CREATED="1398592480323" MODIFIED="1398592480323"/>
<node TEXT="Mind; fear; suffocation, of" ID="ID_1676551702" CREATED="1398592533218" MODIFIED="1398592533218"/>
<node TEXT="Mind; fear; suffocation, of; night" ID="ID_726906053" CREATED="1398592549796" MODIFIED="1398592549796"/>
<node TEXT="Mind; fear; suffocation, of; palpitation, with, wakes after midnight" ID="ID_1750781769" CREATED="1398592572721" MODIFIED="1398592572721"/>
<node TEXT="Mind; inconsolable" ID="ID_1476318685" CREATED="1398592682573" MODIFIED="1398592682573"/>
<node TEXT="Mind; mildness" ID="ID_1605367924" CREATED="1398592753692" MODIFIED="1398592753692"/>
<node TEXT="Mind; prostration of mind; weakness, with bodily" ID="ID_1935513123" CREATED="1398594823693" MODIFIED="1398594823693"/>
<node TEXT="Mind; restlessness, nervousness; respiratory complaints, with" ID="ID_401488132" CREATED="1398594872826" MODIFIED="1398594872826"/>
<node TEXT="Mind; sighing" ID="ID_1510124926" CREATED="1398594888924" MODIFIED="1398594888924"/>
<node TEXT="Mind; starting, startled" ID="ID_1678847766" CREATED="1398597424946" MODIFIED="1398597424946"/>
<node TEXT="Mind; starting, startled; sleep; during" ID="ID_1417109547" CREATED="1398602457233" MODIFIED="1398602457233"/>
<node TEXT="Mind; sudden manifestations" ID="ID_1403024599" CREATED="1398602470562" MODIFIED="1398602470562"/>
<node TEXT="Mind; thinking; complaints, of; agg." ID="ID_1237546779" CREATED="1398602486745" MODIFIED="1398602486745"/>
<node TEXT="Mind; thinking; aversion to" ID="ID_1722953227" CREATED="1398602518357" MODIFIED="1398602518357"/>
<node TEXT="Mind; trifles; ailments from, agg." ID="ID_1092920381" CREATED="1398602532742" MODIFIED="1398602532742"/>
<node TEXT="Mind; weary of life; inflammation of testes, in chronic" ID="ID_676989715" CREATED="1398602550297" MODIFIED="1398602550297"/>
<node TEXT="Mind; weeping, tearful mood; alternating with; cheerfulness" ID="ID_1448760394" CREATED="1398602567856" MODIFIED="1398602567856"/>
</node>
<node TEXT="" POSITION="left" ID="ID_1601982950" CREATED="1398643359733" MODIFIED="1398643375943">
<hook URI="Spongia%20officinalis%202.JPG" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Spongia tosta" STYLE_REF="Remedy" POSITION="right" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1398589053746"/>
<node TEXT="Spong" STYLE_REF="Abbrev" POSITION="right" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1398588790551"/>
<node TEXT="Miazma" STYLE_REF="Subtopic" POSITION="right" ID="ID_31190142" CREATED="1357168609677" MODIFIED="1357168614539"/>
<node TEXT="Trituace" STYLE_REF="Subtopic" POSITION="right" ID="ID_1275465186" CREATED="1357168572522" MODIFIED="1357168576351"/>
<node TEXT="Podobn&#xe9; l&#xe9;ky" STYLE_REF="Subtopic" POSITION="right" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1357168625505">
<node TEXT="Acon" ID="ID_308914795" CREATED="1398589288442" MODIFIED="1398589291697">
<node TEXT="&#x161;ok" STYLE_REF="Differs" ID="ID_1928203071" CREATED="1398589292971" MODIFIED="1398589296707"/>
<node TEXT="vyd&#x11b;&#x161;en&#xfd;" STYLE_REF="Same" ID="ID_349096593" CREATED="1398589299752" MODIFIED="1398589338257">
<node TEXT="k smrti" STYLE_REF="Differs" ID="ID_991306996" CREATED="1398589340023" MODIFIED="1398589354578"/>
</node>
<node TEXT="nemluv&#xed;" STYLE_REF="Differs" ID="ID_1590904439" CREATED="1398589319680" MODIFIED="1398589324678"/>
</node>
<node TEXT="Stram" ID="ID_1696548800" CREATED="1398589492112" MODIFIED="1398589495276">
<node TEXT="temn&#xe9;" STYLE_REF="Differs" ID="ID_7890932" CREATED="1398589496675" MODIFIED="1398589517043"/>
<node TEXT="hlubok&#xe9;" STYLE_REF="Differs" ID="ID_1858596584" CREATED="1398589509573" MODIFIED="1398589520137"/>
<node TEXT="ohro&#x17e;en&#xed; zven&#x10d;&#xed;" STYLE_REF="Differs" ID="ID_1671562117" CREATED="1398589540291" MODIFIED="1398589548035"/>
</node>
<node TEXT="Bar-c" ID="ID_1127217408" CREATED="1398591276511" MODIFIED="1398591285162">
<node TEXT="nepot&#x159;ebuje tolik oporu" STYLE_REF="Differs" ID="ID_1391718002" CREATED="1398591290818" MODIFIED="1398591435352"/>
<node TEXT="nen&#xed; panick&#xe1;" STYLE_REF="Differs" ID="ID_639035976" CREATED="1398591356244" MODIFIED="1398591378898"/>
</node>
</node>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="right" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1357168653304">
<node TEXT="jednoduchost" ID="ID_339589295" CREATED="1398589274835" MODIFIED="1398589279941"/>
<node TEXT="jasn&#xe9; symptomy" ID="ID_1674623591" CREATED="1398589280953" MODIFIED="1398589286214"/>
<node TEXT="&#xfa;zkost" ID="ID_1495256552" CREATED="1398589586085" MODIFIED="1398589591082">
<node TEXT="vystra&#x161;en&#xe9; d&#xed;t&#x11b;" ID="ID_809463299" CREATED="1398589357716" MODIFIED="1398589363537"/>
<node TEXT="nesnese samotu" ID="ID_42259716" CREATED="1398590044118" MODIFIED="1398590048947"/>
<node TEXT="&#x17e;e zem&#x159;e" ID="ID_1990945049" CREATED="1398592147085" MODIFIED="1398592173547"/>
<node TEXT="o druh&#xe9;" ID="ID_372525390" CREATED="1398592159968" MODIFIED="1398592163339"/>
<node TEXT="n&#xe1;hle" ID="ID_891530834" CREATED="1398592164017" MODIFIED="1398592170213"/>
</node>
<node TEXT="hled&#xe1; a pot&#x159;ebuje oporu" ID="ID_1316060723" CREATED="1398589644086" MODIFIED="1398589904970">
<font BOLD="true"/>
<node TEXT="nev&#x11b;&#x159;&#xed; ve vlastn&#xed; s&#xed;lu a integritu" ID="ID_162396058" CREATED="1398589601374" MODIFIED="1398589682003"/>
<node TEXT="jako shluk bun&#x11b;k" ID="ID_830640955" CREATED="1398589624393" MODIFIED="1398589635118"/>
<node TEXT="kdy&#x17e; ji nenach&#xe1;z&#xed;, panika&#x159;&#xed;" ID="ID_1153218096" CREATED="1398590250240" MODIFIED="1398590261058"/>
<node TEXT="hypochondrie" ID="ID_4043707" CREATED="1398591245020" MODIFIED="1398591249150"/>
<node TEXT="vazba na terapeuty" ID="ID_1067307063" CREATED="1398591249562" MODIFIED="1398591255242"/>
<node TEXT="spousty l&#xe9;k&#x16f;" ID="ID_1775562789" CREATED="1398591255699" MODIFIED="1398591262934"/>
</node>
<node TEXT="udr&#x17e;uje si odstup" ID="ID_494015262" CREATED="1398590074724" MODIFIED="1398590080508">
<node TEXT="vyh&#xfd;b&#xe1; se" ID="ID_753514046" CREATED="1398590081808" MODIFIED="1398590089559">
<node TEXT="ciz&#xed;m lidem" ID="ID_562023519" CREATED="1398590091271" MODIFIED="1398590094777"/>
</node>
</node>
<node TEXT="ztr&#xe1;ta &quot;hlavn&#xed;ho&quot; &#x10d;lov&#x11b;ka" ID="ID_870427345" CREATED="1398590290068" MODIFIED="1398590338678"/>
<node TEXT="probl&#xe9;m s nez&#xe1;vislost&#xed;" ID="ID_1322377650" CREATED="1398590379732" MODIFIED="1398590395962">
<node TEXT="nezralost" ID="ID_463276671" CREATED="1398590398362" MODIFIED="1398590401267"/>
</node>
<node TEXT="&#x10d;ist&#xe1; du&#x161;e" ID="ID_696974148" CREATED="1398590423332" MODIFIED="1398590428437">
<node TEXT="&#x10d;ist&#xfd;" ID="ID_509205494" CREATED="1398589732340" MODIFIED="1398589736168"/>
<node TEXT="naivn&#xed;" ID="ID_867728911" CREATED="1398589736626" MODIFIED="1398589738651"/>
<node TEXT="up&#x159;&#xed;mn&#xfd;" ID="ID_1623070571" CREATED="1398589738929" MODIFIED="1398589742471"/>
</node>
<node TEXT="jistota" ID="ID_783249242" CREATED="1398590497207" MODIFIED="1398590499832">
<node TEXT="nem&#x11b;nn&#xe9; zab&#x11b;hl&#xe9; zp&#x16f;soby" ID="ID_783010600" CREATED="1398590471574" MODIFIED="1398590486425"/>
<node TEXT="jist&#xe9; zam&#x11b;stn&#xe1;n&#xed;" ID="ID_1305049879" CREATED="1398590464694" MODIFIED="1398590471126"/>
</node>
<node TEXT="ambice" ID="ID_174050023" CREATED="1398590505145" MODIFIED="1398590508896">
<node TEXT="nem&#xe1;" ID="ID_534053" CREATED="1398590510687" MODIFIED="1398590512567"/>
<node TEXT="konzervativn&#xed;" ID="ID_545846647" CREATED="1398590816767" MODIFIED="1398590820240"/>
</node>
<node TEXT="pohyb" ID="ID_954462821" CREATED="1398590839068" MODIFIED="1398590861793">
<node TEXT="nerad" ID="ID_343044585" CREATED="1398590864775" MODIFIED="1398590866963"/>
<node TEXT="sport jim nic ne&#x159;&#xed;k&#xe1;" ID="ID_790480686" CREATED="1398590867528" MODIFIED="1398590878463"/>
<node TEXT="necestuj&#xed;" ID="ID_744529051" CREATED="1398590878787" MODIFIED="1398590882938"/>
</node>
<node TEXT="voda" ID="ID_1469486353" CREATED="1398590912066" MODIFIED="1398590915298">
<node TEXT="koupe se" ID="ID_894601308" CREATED="1398590916285" MODIFIED="1398590920654"/>
<node TEXT="pot&#xe1;p&#xed; se" ID="ID_970079330" CREATED="1398590920977" MODIFIED="1398590924484"/>
<node TEXT="plave" ID="ID_979488954" CREATED="1398590924753" MODIFIED="1398590926925"/>
<node TEXT="bezpe&#x10d;n&#xe9;" ID="ID_201137012" CREATED="1398590928537" MODIFIED="1398590937872"/>
</node>
<node TEXT="nas&#xe1;v&#xe1;" ID="ID_435360299" CREATED="1398590574329" MODIFIED="1398591720745">
<node TEXT="starosti" ID="ID_1539236827" CREATED="1398590585073" MODIFIED="1398590588841"/>
<node TEXT="emoce" ID="ID_1596639876" CREATED="1398591711885" MODIFIED="1398591715372"/>
<node TEXT="voda = emoce" ID="ID_1322229228" CREATED="1398590589298" MODIFIED="1398590599745"/>
</node>
<node TEXT="zm&#x11b;ny v &#x17e;ivot&#x11b;" ID="ID_584676387" CREATED="1398591099929" MODIFIED="1398591105616">
<node TEXT="&#x161;patn&#x11b; se vyrovn&#xe1;v&#xe1;" ID="ID_558352722" CREATED="1398591107005" MODIFIED="1398591116759"/>
<node TEXT="hypochondrie" ID="ID_675310702" CREATED="1398591219771" MODIFIED="1398591224150"/>
</node>
<node TEXT="ukl&#xe1;daj&#xed;" ID="ID_1926799235" CREATED="1398591928623" MODIFIED="1398591935738">
<node TEXT="dob&#x159;e si pamatuje" ID="ID_1163956572" CREATED="1398591937163" MODIFIED="1398591958021"/>
<node TEXT="&#x161;patn&#xe9; z&#xe1;&#x17e;itky" ID="ID_1487230315" CREATED="1398591945490" MODIFIED="1398591952536"/>
</node>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="right" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1398643414282" HGAP="-30" VSHIFT="10">
<node TEXT="panika" ID="ID_1832050252" CREATED="1398589205835" MODIFIED="1398589210573">
<node TEXT="stavy akutn&#xed; paniky a &#xfa;zkosti" ID="ID_834351512" CREATED="1398589212474" MODIFIED="1398589223273">
<font BOLD="true"/>
</node>
<node TEXT="komunikuje" ID="ID_151095490" CREATED="1398589365525" MODIFIED="1398589370678">
<node TEXT="Acon" STYLE_REF="Comment" ID="ID_999115740" CREATED="1398589373312" MODIFIED="1398589380913"/>
</node>
</node>
<node TEXT="autismus" ID="ID_1650116359" CREATED="1398590113937" MODIFIED="1398590122322"/>
<node TEXT="srdce" ID="ID_523943300" CREATED="1398588985407" MODIFIED="1398588991652">
<font BOLD="true"/>
<node TEXT="endokarditidy" ID="ID_971610735" CREATED="1398592590272" MODIFIED="1398592594190"/>
<node TEXT="angina pectoris" ID="ID_1016437030" CREATED="1398592594415" MODIFIED="1398592598846"/>
<node TEXT="hypertrofie srdce" ID="ID_1676770542" CREATED="1398592599070" MODIFIED="1398592604996">
<node TEXT="astma" ID="ID_1341772325" CREATED="1398592631629" MODIFIED="1398592635158"/>
</node>
<node TEXT="nedostate&#x10d;nost chlopn&#xed;" ID="ID_1864812920" CREATED="1398592613878" MODIFIED="1398592624237"/>
<node TEXT="usazen&#xed; v&#xe1;pn&#xed;ku na chlopn&#xed;ch" ID="ID_723168761" CREATED="1398592636931" MODIFIED="1398592648259"/>
<node TEXT="du&#x161;nost" ID="ID_1906153023" CREATED="1398592695761" MODIFIED="1398592701216"/>
<node TEXT="strach" ID="ID_1119651084" CREATED="1398592707320" MODIFIED="1398592709361"/>
</node>
<node TEXT="periodicita" ID="ID_302361203" CREATED="1398590673558" MODIFIED="1398590677845">
<node TEXT="probl&#xe9;my se opakuj&#xed;" ID="ID_1934345894" CREATED="1398590679154" MODIFIED="1398590689425"/>
<node TEXT="n&#x11b;jakou dobu ano, n&#x11b;jakou dobu ne" ID="ID_608691741" CREATED="1398590689784" MODIFIED="1398590731576"/>
</node>
<node TEXT="spazmy" ID="ID_1989642667" CREATED="1398590734937" MODIFIED="1398590737633">
<node TEXT="d&#xfd;chac&#xed; cesty" ID="ID_45844572" CREATED="1398590739425" MODIFIED="1398590747569"/>
<node TEXT="bezd&#x16f;vodn&#xe9;" ID="ID_976924856" CREATED="1398590748053" MODIFIED="1398590752721"/>
</node>
<node TEXT="d&#xfd;ch&#xe1;n&#xed;" ID="ID_660599550" CREATED="1398590760880" MODIFIED="1398592415789">
<node TEXT="ka&#x161;el" ID="ID_1342928431" CREATED="1398588969702" MODIFIED="1398588991020">
<font BOLD="true"/>
<node TEXT="krup&#xf3;zn&#xed;" ID="ID_139333199" CREATED="1398588974472" MODIFIED="1398588979975"/>
<node TEXT="laryngitida" ID="ID_460959401" CREATED="1398588980316" MODIFIED="1398588984139"/>
<node TEXT="chrapot p&#x159;edem" ID="ID_1714831565" CREATED="1398592422590" MODIFIED="1398592444943"/>
<node TEXT="z&#xe1;chavty &#x10d;ern&#xe9;ho ka&#x161;le" ID="ID_102387968" CREATED="1398589158154" MODIFIED="1398589166552"/>
</node>
<node TEXT="paral&#xfd;zy" ID="ID_1092472496" CREATED="1398590778926" MODIFIED="1398590783091"/>
<node TEXT="nepr&#x16f;chodnost" ID="ID_1518916280" CREATED="1398592401263" MODIFIED="1398592407687"/>
</node>
<node TEXT="pocen&#xed;" ID="ID_1088899402" CREATED="1398592084141" MODIFIED="1398592086192">
<node TEXT="studen&#xfd;" ID="ID_58497187" CREATED="1398592087554" MODIFIED="1398592093026"/>
</node>
<node TEXT="pocit, &#x17e;e v m&#xed;stnosti je p&#xe1;liv&#xfd; vzduch" ID="ID_1474610162" CREATED="1398592355165" MODIFIED="1398592368102"/>
<node TEXT="varlata" ID="ID_1023300732" CREATED="1398592368586" MODIFIED="1398592372164">
<node TEXT="ch&#xe1;movody" ID="ID_1255731629" CREATED="1398592373553" MODIFIED="1398592379204"/>
<node TEXT="otoky" ID="ID_1772490358" CREATED="1398592390849" MODIFIED="1398592393495"/>
<node TEXT="zatvrdliny" ID="ID_1430384315" CREATED="1398592393836" MODIFIED="1398592397034"/>
</node>
<node TEXT="&#x10d;erven&#xe9; tv&#xe1;&#x159;e" ID="ID_335549747" CREATED="1398592486460" MODIFIED="1398592492813"/>
<node TEXT="hore&#x10d;ka" ID="ID_1660741597" CREATED="1398592493626" MODIFIED="1398592496914">
<node TEXT="prudk&#xe1;" ID="ID_1474697448" CREATED="1398592498295" MODIFIED="1398592504252"/>
<node TEXT="&#x17e;&#xed;ze&#x148;" ID="ID_1091628716" CREATED="1398592505505" MODIFIED="1398592509082"/>
<node TEXT="strach" ID="ID_327384616" CREATED="1398592509611" MODIFIED="1398592511636"/>
<node TEXT="zimnice na za&#x10d;&#xe1;tku" ID="ID_1669476682" CREATED="1398592512030" MODIFIED="1398592517245"/>
</node>
<node TEXT="n&#xe1;m&#x11b;s&#xed;&#x10d;nost" ID="ID_374436325" CREATED="1398592518879" MODIFIED="1398592523693"/>
<node TEXT="zv&#xfd;razn&#x11b;n&#xe9; c&#xe9;vy" ID="ID_298416365" CREATED="1398592688764" MODIFIED="1398592694920"/>
<node TEXT="klouby" ID="ID_1790410684" CREATED="1398592713004" MODIFIED="1398592717953">
<node TEXT="revma" ID="ID_1354751467" CREATED="1398592719118" MODIFIED="1398592723310"/>
<node TEXT="otoky" ID="ID_734475193" CREATED="1398592723767" MODIFIED="1398592727298"/>
</node>
</node>
</node>
</map>
