
Spongia tosta
==============

![mapa](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Spongia%20tosta/Spongia%20tosta.svg)

 - [PDF](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Spongia%20tosta/Spongia%20tosta.pdf)
 - [Map](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Spongia%20tosta/Spongia%20tosta.mm)
 - [SVG](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Spongia%20tosta/Spongia%20tosta.svg)

Rubriky
-------
 - Mind; anguish; heart complaints, in 
 - Mind; anxiety; cough; during 
 - Mind; anxiety; cramps, with 
 - Mind; anxiety; croup, in 
 - Mind; anxiety; driving him from place to place 
 - Mind; anxiety; driving him from place to place; dyspnea, in 
 - Mind; anxiety; dyspnea, in 
 - Mind; anxiety; fainting, as from 
 - Mind; anxiety; faintness; with 
 - Mind; anxiety; flushes of heat, with 
 - Mind; anxiety; heart complaints, in 
 - Mind; anxiety; heart region 
 - Mind; anxiety; heart region; pain in heart, with; cramping 
 - Mind; anxiety; midnight; after 
 - Mind; anxiety; night; agg. 
 - Mind; anxiety; night; agg.; palpitation, with 
 - Mind; anxiety; pain, with; heart, in and about 
 - Mind; anxiety; palpitation; with [[Arg-n](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/ArgentumNitricum)]
 - Mind; anxiety; perspiration; with 
 - Mind; anxiety; respiration; difficult, anxious, with 
 - Mind; anxiety; sleep; during 
 - Mind; anxiety; sudden, paroxysmal 
 - Mind; anxiety; sudden, paroxysmal; heart complaints, in 
 - Mind; anxiety; sudden, paroxysmal; throat 
 - Mind; anxiety; weakness, with 
 - Mind; closing eyes; agg. 
 - Mind; consolation; ailments from, agg. 
 - Mind; despair; heat agg. 
 - Mind; faintness, fainting, with 
 - Mind; fear; death, of; suffocation, from 
 - Mind; fear; death, of; termination, of fatal 
 - Mind; fear; disease, of; incurable, of being 
 - Mind; fear; disease, of; terminal, fatal 
 - Mind; fear; happen; something will; bad, evil 
 - Mind; fear; midnight; after 
 - Mind; fear; palpitation, with 
 - Mind; fear; panic attacks, overpowering [[Arg-n](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/ArgentumNitricum)]
 - Mind; fear; perspiration, with 
 - Mind; fear; suffocation, of 
 - Mind; fear; suffocation, of; night 
 - Mind; fear; suffocation, of; palpitation, with, wakes after midnight 
 - Mind; inconsolable 
 - Mind; mildness [[Carc](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Carcinosin)]
 - Mind; prostration of mind; weakness, with bodily 
 - Mind; restlessness, nervousness; respiratory complaints, with 
 - Mind; sighing 
 - Mind; starting, startled [[Cor-r](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Corallium rubrum), [Murx](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Murex purpurea)]
 - Mind; starting, startled; sleep; during [[Alum](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Alumina)]
 - Mind; sudden manifestations 
 - Mind; thinking; aversion to [[Aster](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Asterias rubens)]
 - Mind; thinking; complaints, of; agg. 
 - Mind; trifles; ailments from, agg. [[Ars](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/ArsenicumAlbum)]
 - Mind; weary of life; inflammation of testes, in chronic 
 - Mind; weeping, tearful mood; alternating with; cheerfulness 
    