<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Rosa Damascena" COLOR="#cc0000" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Main Topic" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1374655464664">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<stylenode LOCALIZED_TEXT="styles.predefined" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" POSITION="right">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<stylenode LOCALIZED_TEXT="default" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" POSITION="right">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" POSITION="right">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="SansSerif" SIZE="20" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="SansSerif" SIZE="18" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="SansSerif" SIZE="16" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="SansSerif" SIZE="14" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Ros-d" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Abbrev" POSITION="left" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1374655636761">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="R&#x16f;&#x17e;e dama&#x161;sk&#xe1;" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Remedy" POSITION="left" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1374655626003">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Info" COLOR="#0066cc" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Subtopic" POSITION="left" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<node TEXT="Rosaceae" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1287486954" CREATED="1374661582247" MODIFIED="1374661589644">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="jahody" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_101285474" CREATED="1374661590884" MODIFIED="1374661593429">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="p&#x159;in&#xe1;&#x161;&#xed; u&#x17e;itek" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_540185791" CREATED="1374661593950" MODIFIED="1374661602783">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="r&#x16f;&#x17e;ov&#xe1; voda" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_59598621" CREATED="1374662184564" MODIFIED="1374662188859">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="adstringent" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_331970600" CREATED="1374662189995" MODIFIED="1374662193266">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="p&#x11b;stuje se v R&#x16f;&#x17e;ov&#xe9;m &#xfa;dol&#xed; (Bulharsko)" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1396129422" CREATED="1374662230551" MODIFIED="1374662308103">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="obsahuje kyanovod&#xed;k" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_40324420" CREATED="1374662330413" MODIFIED="1374662337599">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="t&#xe9;mata smrti" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Comment" ID="ID_911524716" CREATED="1374662338504" MODIFIED="1374662350758">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="HCN" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1598426959" CREATED="1374688216430" MODIFIED="1374688219102">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="H" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1353914059" CREATED="1374688223580" MODIFIED="1374688226486">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="prudk&#xfd;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1535280401" CREATED="1374688228772" MODIFIED="1374688232189">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="ot&#xe1;zka &#x17e;ivota a smrti" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1475225022" CREATED="1374688232470" MODIFIED="1374688239192">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="ne&#x17e;&#xed;t" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1763669639" CREATED="1374688240750" MODIFIED="1374688243028">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="C" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_564450003" CREATED="1374688244313" MODIFIED="1374688245455">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="fyz. struktura" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1289255122" CREATED="1374688246487" MODIFIED="1374688250439">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="pevn&#xe1; nebo labiln&#xed; existence" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1858694059" CREATED="1374688250720" MODIFIED="1374688259374">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="hroz&#xed;c&#xed; smrt" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_4459908" CREATED="1374688260237" MODIFIED="1374688264139">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="strach ze smrti" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1114220173" CREATED="1374688264659" MODIFIED="1374688269009">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="N" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1801765347" CREATED="1374688272395" MODIFIED="1374688274119">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="expanze" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_894266369" CREATED="1374688275152" MODIFIED="1374688278405">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="zab&#xed;r&#xe1; p&#x159;&#xed;li&#x161; mnoho m&#xed;sta" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_147687021" CREATED="1374688280325" MODIFIED="1374688287835">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="asertivita" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_448297447" CREATED="1374688288666" MODIFIED="1374688291645">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
</node>
<node TEXT="symbol" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_795638062" CREATED="1374662372054" MODIFIED="1374662376678">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Rosenkruci&#xe1;ni" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1174191164" CREATED="1374662377607" MODIFIED="1374662410285">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="Templ&#xe1;&#x159;i" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_894804969" CREATED="1374662384852" MODIFIED="1374662406408">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="Ro&#x17e;umberkov&#xe9;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_544062294" CREATED="1374662389098" MODIFIED="1374662402352">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="Film: Pt&#xe1;ci v trn&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1492092423" CREATED="1374688088591" MODIFIED="1374688108829">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="R&#x16f;&#x17e;ovit&#xe9;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_747572766" CREATED="1374686874907" MODIFIED="1374686879893">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="sladkost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1148442985" CREATED="1374686880686" MODIFIED="1374686882900">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="du&#x161;en&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_942119235" CREATED="1374686901781" MODIFIED="1374686904567">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="trny" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_295579938" CREATED="1374686883269" MODIFIED="1374686885293">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="ochrana" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1311047607" CREATED="1374686886644" MODIFIED="1374686892229">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="&#x161;r&#xe1;my" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1993524766" CREATED="1374686892589" MODIFIED="1374686896496">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="srdce" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_302799308" CREATED="1374686905939" MODIFIED="1374686909223">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Scholten: bolest zlomen&#xe9;ho srdce" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1885866929" CREATED="1374686931813" MODIFIED="1374686942564">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="l&#xe1;ska" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1459828347" CREATED="1374686985626" MODIFIED="1374686988151">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="sexualita" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1501998854" CREATED="1374686988575" MODIFIED="1374686991124">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="odpor k mu&#x17e;&#x16f;m" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_754149860" CREATED="1374687012790" MODIFIED="1374687020251">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="n&#xe1;sledek emo&#x10d;n&#xed;ho zran&#x11b;n&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1882306184" CREATED="1374687023576" MODIFIED="1374687041925">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="idealizovan&#xe1; l&#xe1;ska" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_969375923" CREATED="1374687137292" MODIFIED="1374687150515">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="cht&#x11b;j&#xed; po sv&#xe9;m partnerovi ve&#x161;kerou l&#xe1;sku" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_667831590" CREATED="1374687150995" MODIFIED="1374687168935">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="cht&#x11b;j&#xed; i nab&#xed;dnout" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_193638693" CREATED="1374687233245" MODIFIED="1374687242950">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="&#xfa;nava" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1315281126" CREATED="1374687255598" MODIFIED="1374687263276">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="nenapln&#x11b;n&#xe1; o&#x10d;ek&#xe1;van&#xed; vy&#x10d;erp&#xe1;vaj&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_121316355" CREATED="1374687264140" MODIFIED="1374687293810">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="fyz" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_567242222" CREATED="1374687304616" MODIFIED="1374687306802">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="mdloby" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1387244073" CREATED="1374687307658" MODIFIED="1374687310341">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="du&#x161;nost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1660055217" CREATED="1374687313562" MODIFIED="1374687316779">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="astma" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_358169196" CREATED="1374687310693" MODIFIED="1374687313290">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="bolesti hlavy" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1159217559" CREATED="1374687320454" MODIFIED="1374687323794">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="p&#xe1;len&#xed; v puse" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1549801309" CREATED="1374687345762" MODIFIED="1374687349597">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="sucho v puse" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1009662226" CREATED="1374687349901" MODIFIED="1374687355309">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="neurologick&#xe9; probl&#xe9;my" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_684969277" CREATED="1374687370938" MODIFIED="1374687377673">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="tiky" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_63139777" CREATED="1374687382981" MODIFIED="1374687384916">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="z&#xe1;&#x161;kuby" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1504536028" CREATED="1374687385221" MODIFIED="1374687388437">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="koma" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_975506661" CREATED="1374687388685" MODIFIED="1374687390078">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="srdce" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_780210164" CREATED="1374687402462" MODIFIED="1374687404452">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="arytmie" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_864357767" CREATED="1374687405613" MODIFIED="1374687408125">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="infarkty" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1532105985" CREATED="1374687408477" MODIFIED="1374687411771">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="hypertenze" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_126862988" CREATED="1374687412028" MODIFIED="1374687417587">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="hupotenze" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_379462123" CREATED="1374687417875" MODIFIED="1374687421016">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="dilatace srdce" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1590643212" CREATED="1374687421296" MODIFIED="1374687425185">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="cyan&#xf3;za" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_866755928" CREATED="1374687508043" MODIFIED="1374687514186">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="promodr&#xe1;n&#xed; rt&#x16f;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1841827701" CREATED="1374687515808" MODIFIED="1374687521191">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="kone&#x10d;ky prst&#x16f;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_638107791" CREATED="1374687521703" MODIFIED="1374687527383">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="asfyxie u novorozenc&#x16f;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_307314214" CREATED="1374687528519" MODIFIED="1374687536356">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="chu&#x165; na peckoviny" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_885054414" CREATED="1374687560109" MODIFIED="1374687573591">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
<node TEXT="Scholten" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1594947155" CREATED="1374688485303" MODIFIED="1374688487812">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="St&#xe1;dia" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1590684355" CREATED="1374688488996" MODIFIED="1374688494201">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="1" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1834645696" CREATED="1374688508650" MODIFIED="1374688510550">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Kys, kyanovod&#xed;kov&#xe1;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1249686315" CREATED="1374688511878" MODIFIED="1374688519926">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="2" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1563910225" CREATED="1374688639945" MODIFIED="1374688645264">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Geum Urbanum" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1212624198" CREATED="1374688646249" MODIFIED="1374688658574">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="3" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1424399286" CREATED="1374688701832" MODIFIED="1374688704267">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Potentilla" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_43538828" CREATED="1374688705292" MODIFIED="1374688719960">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="4" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_478464022" CREATED="1374688730202" MODIFIED="1374688732721">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Sorbus domestica" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_483062927" CREATED="1374688733769" MODIFIED="1374688738322">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="5" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_358597501" CREATED="1374688740119" MODIFIED="1374688741564">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Malus communis" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_827882272" CREATED="1374688742516" MODIFIED="1374688748607">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="Malus pumila" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1909308422" CREATED="1374688762951" MODIFIED="1374688774111">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="6" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1440599817" CREATED="1374688791884" MODIFIED="1374688794663">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Krvavec, Totem" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1981133673" CREATED="1374688796526" MODIFIED="1374688805338">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="7" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1915985385" CREATED="1374688807015" MODIFIED="1374688810963">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Alchemila vulgaris" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1608266799" CREATED="1374688812058" MODIFIED="1374688820213">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="Agrimonia eupathoria" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_462861485" CREATED="1374688820788" MODIFIED="1374688835893">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="8" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1890156150" CREATED="1374688837425" MODIFIED="1374688839294">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Prunus spinosa" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1304606611" CREATED="1374688840302" MODIFIED="1374688843877">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="9" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1439430845" CREATED="1374688862153" MODIFIED="1374688863973">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Cidonia vulgaris" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1894439222" CREATED="1374688865365" MODIFIED="1374688870901">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="10" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1422755149" CREATED="1374688495113" MODIFIED="1374688496947">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Rosa canina" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_59073604" CREATED="1374688498251" MODIFIED="1374688502830">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="Rosa Damascena" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1858610582" CREATED="1374688503446" MODIFIED="1374688507268">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="11" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_876977711" CREATED="1374688928258" MODIFIED="1374688931458">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Amigdala Amala" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1232067098" CREATED="1374688932451" MODIFIED="1374688939938">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="12" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_569279088" CREATED="1374688916013" MODIFIED="1374688918654">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Crata egus" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_900304416" CREATED="1374688919782" MODIFIED="1374688924331">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="13" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_993084326" CREATED="1374688942882" MODIFIED="1374688946107">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Rubus fruticocus" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_727993957" CREATED="1374688949464" MODIFIED="1374688958011">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="14" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1439733881" CREATED="1374688959288" MODIFIED="1374688962600">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Spirea ulmaria" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_526520280" CREATED="1374688963512" MODIFIED="1374688983687">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="16" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1288177386" CREATED="1374688984485" MODIFIED="1374688986287">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Fragalia" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1686983776" CREATED="1374688987327" MODIFIED="1374688997416">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="17" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1007677278" CREATED="1374689002508" MODIFIED="1374689003934">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Laurocerasus" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_852002440" CREATED="1374689008993" MODIFIED="1374689016709">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Podobn&#xe9; l&#xe9;ky" COLOR="#0066cc" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Subtopic" POSITION="left" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1357168625505">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<node TEXT="Nat-m" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_946346518" CREATED="1374661357285" MODIFIED="1374661361762">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="miner&#xe1;ln&#xed; l&#xe9;k" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Differs" ID="ID_1829419506" CREATED="1374661366519" MODIFIED="1374661403027">
<icon BUILTIN="button_cancel"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="pragmati&#x10d;t&#x11b;j&#x161;&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Differs" ID="ID_237714334" CREATED="1374687632725" MODIFIED="1374687649100">
<icon BUILTIN="button_cancel"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="ros-d je romanti&#x10d;t&#x11b;j&#x161;&#xed;" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Comment" ID="ID_1595396710" CREATED="1374687652307" MODIFIED="1374687669865">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="jedovatost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Differs" ID="ID_1267599406" CREATED="1374687726379" MODIFIED="1374687735184">
<icon BUILTIN="button_cancel"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="chci vztah, ale za t&#x11b;chto podm&#xed;nek" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Comment" ID="ID_1501058031" CREATED="1374687736361" MODIFIED="1374687755541">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
<node TEXT="Ant-c" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_716438800" CREATED="1374687683286" MODIFIED="1374687703784">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="romantick&#xfd;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Same" ID="ID_1320432915" CREATED="1374687705167" MODIFIED="1374687709794">
<icon BUILTIN="button_ok"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="p&#xed;&#x161;e ver&#x161;e" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Same" ID="ID_1491217335" CREATED="1374687710285" MODIFIED="1374687716023">
<icon BUILTIN="button_ok"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="Puls" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1400682496" CREATED="1374687958812" MODIFIED="1374687961178">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="d&#x11b;tskost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Differs" ID="ID_1959767864" CREATED="1374687962226" MODIFIED="1374687970342">
<icon BUILTIN="button_cancel"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="manipulativnost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Differs" ID="ID_832407626" CREATED="1374687970803" MODIFIED="1374687982359">
<icon BUILTIN="button_cancel"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="nem&#xe1; ide&#xe1;ln&#xed; sv&#x11b;t" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Differs" ID="ID_276540769" CREATED="1374687988413" MODIFIED="1374687998163">
<icon BUILTIN="button_cancel"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="Sulph" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1501807027" CREATED="1374688317635" MODIFIED="1374688415304">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="&#x10d;erven&#xe9; rty" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Differs" ID="ID_1725632633" CREATED="1374688333335" MODIFIED="1374688381150">
<icon BUILTIN="button_cancel"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="r&#x16f;&#x17e;e m&#xe1; modr&#xe9;" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Comment" ID="ID_1140563867" CREATED="1374688383462" MODIFIED="1374688389658">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
</node>
<node TEXT="Miazma" COLOR="#0066cc" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Subtopic" POSITION="left" ID="ID_562805365" CREATED="1374688062163" MODIFIED="1374688065744">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<node TEXT="malarick&#xe9;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1271761933" CREATED="1374688066935" MODIFIED="1374688075296">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="Trituace" COLOR="#0066cc" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Subtopic" POSITION="right" ID="ID_1275465186" CREATED="1357168572522" MODIFIED="1357168576351">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<node TEXT="l&#xe1;ska" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1766726590" CREATED="1374661053486" MODIFIED="1374661112730">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="ide&#xe1;ln&#xed; vztah" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1080557392" CREATED="1374661718418" MODIFIED="1374661725299">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="a&#x17e; ryt&#xed;&#x159;sk&#xfd;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1834877252" CREATED="1374661726827" MODIFIED="1374661732750">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="ale i rodi&#x10d;ovsk&#xfd; nebo jin&#xfd;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1951435804" CREATED="1374661766252" MODIFIED="1374661775494">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="ide&#xe1;ln&#xed; l&#xe1;ska" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_120848154" CREATED="1374661735073" MODIFIED="1374661741297">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="kv&#x11b;t vypad&#xe1; jako &#x17e;ensk&#xe9; pohlav&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_464269946" CREATED="1374661113259" MODIFIED="1374661141799">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="panna Maria" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_261083070" CREATED="1374661143037" MODIFIED="1374661274696">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="r&#x16f;&#x17e;e bez trn&#x16f;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_936277027" CREATED="1374661278979" MODIFIED="1374661284251">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="o&#x10d;ista" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1559944082" CREATED="1374661289074" MODIFIED="1374661292244">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="r&#xfd;ma" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1165372526" CREATED="1374661982886" MODIFIED="1374661986027">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="ka&#x161;el" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_645357344" CREATED="1374661986339" MODIFIED="1374662003834">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="otok krku" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_309173609" CREATED="1374662004553" MODIFIED="1374662007806">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="alergick&#xe1; reakce" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_111602783" CREATED="1374662008086" MODIFIED="1374662014846">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="ochrana" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1470548487" CREATED="1374661303858" MODIFIED="1374661307107">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="trny" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_298137685" CREATED="1374661308809" MODIFIED="1374661310872">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="pasivn&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1275496189" CREATED="1374661312926" MODIFIED="1374661315216">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="c&#xed;t&#xed;m se nah&#xe1;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_714357418" CREATED="1374661325116" MODIFIED="1374661329031">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="zranitelnost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_44465367" CREATED="1374662132246" MODIFIED="1374662136555">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="p&#x159;izp&#x16f;sobivost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1629134287" CREATED="1374662136907" MODIFIED="1374662143280">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="ale neschopnost ust&#xe1;t si s&#xe1;m sebe" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1400081543" CREATED="1374662144105" MODIFIED="1374662153419">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="v&#x16f;n&#x11b;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1876234414" CREATED="1374661340135" MODIFIED="1374661348045">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="nedbal&#xe1; elegance" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1272554926" CREATED="1374661433348" MODIFIED="1374661439126">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="klid" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_177143097" CREATED="1374661614678" MODIFIED="1374661615992">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="smrt" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1516867785" CREATED="1374661625018" MODIFIED="1374661627346">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="ztr&#xe1;ta" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_592585184" CREATED="1374661631908" MODIFIED="1374661635357">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="doprovod na posledn&#xed; cest&#x11b;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1521713128" CREATED="1374661635925" MODIFIED="1374661642608">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="n&#x11b;ha" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1554610605" CREATED="1374661644915" MODIFIED="1374661646634">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="p&#xe9;&#x10d;e" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_164045612" CREATED="1374661660139" MODIFIED="1374661665846">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="here&#x10d;ka Vanessa Paris" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1868362634" CREATED="1374661670232" MODIFIED="1374662120189">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="jem&#x148;ou&#x10d;k&#xe1;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1905844039" CREATED="1374661691921" MODIFIED="1374661699478">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="chaotick&#xe1;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_736920015" CREATED="1374661699949" MODIFIED="1374661703225">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="pot&#x159;ebuje velkou &#xfa;dr&#x17e;bu" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1749033312" CREATED="1374661703554" MODIFIED="1374661711586">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="voda" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_755284176" CREATED="1374661865196" MODIFIED="1374661868394">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="jako bych byl v baz&#xe9;nu, ale vodu nec&#xed;t&#xed;m" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_443739908" CREATED="1374661871319" MODIFIED="1374661921063">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="true"/>
</node>
<node TEXT="prostor, volnost, pr&#xe1;zdnota" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_870554918" CREATED="1374661925437" MODIFIED="1374661941650">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="true"/>
</node>
</node>
<node TEXT="d&#xfd;ch&#xe1;n&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1058426479" CREATED="1374661952558" MODIFIED="1374661956517">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="nejde nadechnout, vydechnout" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_439077701" CREATED="1374661957478" MODIFIED="1374661966354">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="tlak na hrudi" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_667367315" CREATED="1374661971893" MODIFIED="1374661976376">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="rozpraskan&#xe1; p&#x16f;da" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1611733915" CREATED="1374662209583" MODIFIED="1374662215415">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="nedostatek vl&#xe1;hy" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1701161835" CREATED="1374662198918" MODIFIED="1374662204421">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="bolest hlavy" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1518126582" CREATED="1374662413000" MODIFIED="1374662417333">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="migr&#xe9;na" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_33216945" CREATED="1374662443235" MODIFIED="1374662450988">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="bodav&#xe1; bolest za &#x10d;elem" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_354677871" CREATED="1374662451516" MODIFIED="1374662480647">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="p&#xe1;liv&#xe9; bolesti" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1392195072" CREATED="1374662481620" MODIFIED="1374662487355">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="prav&#xe1; strana" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_726184558" CREATED="1374662487652" MODIFIED="1374662491716">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="T&#xe9;mata" COLOR="#0066cc" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Subtopic" POSITION="right" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1357168653304">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<node TEXT="dovol&#xed; odlo&#x17e;it minulost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1918504549" CREATED="1374661888104" MODIFIED="1374661899156">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="soci&#xe1;ln&#xed; charakter" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_459352988" CREATED="1374689426259" MODIFIED="1374689432121">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="n&#xe1;pomocn&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1232066254" CREATED="1374689433801" MODIFIED="1374689438922">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="podporuj&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_983386000" CREATED="1374689456812" MODIFIED="1374689460312">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="slou&#x17e;&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_613760301" CREATED="1374689460672" MODIFIED="1374689463419">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="udr&#x17e;uj&#xed; si p&#x159;im&#x11b;&#x159;en&#xfd; odstup" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_71379942" CREATED="1374689466208" MODIFIED="1374689474873">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="archetypy" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_948251925" CREATED="1374689143522" MODIFIED="1374689154367">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="panna maria" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1762452608" CREATED="1374689155519" MODIFIED="1374689168320">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="uvol&#x148;uje bolest prost&#x159;ednictv&#xed;m l&#xe1;sky" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_775181692" CREATED="1374689177229" MODIFIED="1374689187955">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="rozenkruci&#xe1;ni" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1714924132" CREATED="1374689169295" MODIFIED="1374689176725">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="venu&#x161;e" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_836282546" CREATED="1374689193324" MODIFIED="1374689195855">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="afrodita" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1122395230" CREATED="1374689199107" MODIFIED="1374689202177">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="jistota" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_363174129" CREATED="1374689216193" MODIFIED="1374689233154">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="bezpe&#x10d;&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_134334899" CREATED="1374689218866" MODIFIED="1374689221746">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="harmonie" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_534021089" CREATED="1374689222123" MODIFIED="1374689226543">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="kr&#xe1;sa" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_707437125" CREATED="1374689226832" MODIFIED="1374689229245">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="smysl pro um&#x11b;n&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_389472335" CREATED="1374689238233" MODIFIED="1374689242216">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="estetika" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_575888329" CREATED="1374689251356" MODIFIED="1374689253363">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="netaktnost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_900084836" CREATED="1374689266197" MODIFIED="1374689274632">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="nicotnost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_995527079" CREATED="1374689275072" MODIFIED="1374689277685">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="nesn&#xe1;ze" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1200317558" CREATED="1374689278309" MODIFIED="1374689280910">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="nepo&#x159;&#xe1;dek" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1752929537" CREATED="1374689281159" MODIFIED="1374689284036">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
<node TEXT="zahoj&#xed; emo&#x10d;n&#xed; &#x161;r&#xe1;my" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_966071534" CREATED="1374661900275" MODIFIED="1374661913197">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="touha po svobod&#x11b;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1513947195" CREATED="1374661945593" MODIFIED="1374661950290">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="&#x161;&#xed;pkov&#xe1; r&#x16f;&#x17e;enka" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1985390951" CREATED="1374686358699" MODIFIED="1374687209517">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="harmonie" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1340997830" CREATED="1374686391973" MODIFIED="1374687799657">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
<node TEXT="hled&#xe1;n&#xed; m&#xed;ru, klidu" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1282747175" CREATED="1374686394971" MODIFIED="1374686403835">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="hled&#xe1;n&#xed; rovnov&#xe1;hy mezi t&#x11b;lem a du&#x161;&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1023374641" CREATED="1374686406336" MODIFIED="1374686425392">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="t&#xe9;ma l&#xe1;sky" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_80117695" CREATED="1374686425769" MODIFIED="1374687801161">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
<node TEXT="t&#xe9;ma tepla" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_507960372" CREATED="1374686434444" MODIFIED="1374686438516">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="t&#xe9;ma partner&#x16f;, d&#x11b;t&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_730673105" CREATED="1374686448702" MODIFIED="1374686457102">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="soun&#xe1;le&#x17e;itost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1834152150" CREATED="1374686462295" MODIFIED="1374686477683">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="chce l&#xe1;sku" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_748333900" CREATED="1374661789897" MODIFIED="1374661796265">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="v ide&#xe1;ln&#xed; podob&#x11b;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1676837858" CREATED="1374661796993" MODIFIED="1374661801221">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="v&#x161;echno nebo nic" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1260388546" CREATED="1374688142879" MODIFIED="1374688147426">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
</node>
<node TEXT="j&#xe1;, moje v&#x16f;le" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1486882146" CREATED="1374686488260" MODIFIED="1374687801922">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
<node TEXT="terpeny" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Comment" ID="ID_815549111" CREATED="1374686499466" MODIFIED="1374686503006">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
<node TEXT="Carbon" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1216330183" CREATED="1374686514008" MODIFIED="1374686521524">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="polarita" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_837462927" CREATED="1374686532715" MODIFIED="1374686535577">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="nemohu prosadit svou v&#x16f;li" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_661396785" CREATED="1374686536989" MODIFIED="1374686544808">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="slou&#x17e;&#xed;m" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1824446709" CREATED="1374686546903" MODIFIED="1374686550038">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="pom&#xe1;h&#xe1;m" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1153086855" CREATED="1374686550271" MODIFIED="1374686554086">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="j&#xe1; bych cht&#x11b;l ale..." COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1130006302" CREATED="1374689560531" MODIFIED="1374689568358">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="potla&#x10d;en&#xed; j&#xe1;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_918556215" CREATED="1374689608012" MODIFIED="1374689616882">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="hrdost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1785239507" CREATED="1374689725657" MODIFIED="1374689728843">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="egoismus" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_217585109" CREATED="1374689743395" MODIFIED="1374689746491">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
<node TEXT="&#x161;patn&#xe1; schopnost koncentrace" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1876980295" CREATED="1374686559779" MODIFIED="1374687802611">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
<node TEXT="soust&#x159;ed&#x11b;n&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_118267203" CREATED="1374686592327" MODIFIED="1374686596534">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="smutek" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1038672611" CREATED="1374686599847" MODIFIED="1374687803092">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
<node TEXT="poh&#x159;by" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_672544260" CREATED="1374686604852" MODIFIED="1374686607878">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="n&#x11b;koho jsme ztratili" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_815317211" CREATED="1374686608262" MODIFIED="1374686614512">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="d&#xe1;v&#xe1;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_626796999" CREATED="1374689511740" MODIFIED="1374689516768">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="s&#xed;lu" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_303259227" CREATED="1374689517592" MODIFIED="1374689519287">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="hou&#x17e;evnatost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1750209358" CREATED="1374689519520" MODIFIED="1374689523789">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="vytrvalost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1855467988" CREATED="1374689524038" MODIFIED="1374689527527">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="v&#xed;ru v sebe sama" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_809488471" CREATED="1374689527800" MODIFIED="1374689533950">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="5 hlavn&#xed;ch t&#xe9;mat" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_71143363" CREATED="1374686670153" MODIFIED="1374686676664">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="5" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1389530633" CREATED="1374686678695" MODIFIED="1374686681203">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="&#x10d;&#xed;slo svobody" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1605378748" CREATED="1374686682074" MODIFIED="1374686687889">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="&#x10d;lov&#x11b;ka" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_783098336" CREATED="1374686688241" MODIFIED="1374686693055">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="pentagram" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1518975095" CREATED="1374686694694" MODIFIED="1374686697962">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="archetyp&#xe1;ln&#xed; energie" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_545300575" CREATED="1374686712994" MODIFIED="1374686719387">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
<node TEXT="Symptomy" COLOR="#0066cc" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Subtopic" POSITION="right" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<node TEXT="du&#x161;en&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_459829077" CREATED="1374687931631" MODIFIED="1374687935257">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="z&#xe1;chvat z emoc&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_292286839" CREATED="1374687945534" MODIFIED="1374687952737">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="promodr&#xe1;v&#xe1;n&#xed; rt&#x16f;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_286277821" CREATED="1374687935705" MODIFIED="1374687940812">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="zmatenost, nesoust&#x159;ed&#x11b;nost" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1407421696" CREATED="1374688183223" MODIFIED="1374688195652">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="alergie" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_46753903" CREATED="1374689300730" MODIFIED="1374689309893">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="na v&#x16f;ni r&#x16f;&#x17e;&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_150899600" CREATED="1374689316014" MODIFIED="1374689320953">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="senn&#xe1; r&#xfd;ma" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1945890933" CREATED="1374689374950" MODIFIED="1374689379855">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="sluch" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_51756104" CREATED="1374689321910" MODIFIED="1374689330063">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="poruchy" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1795070702" CREATED="1374689330896" MODIFIED="1374689343845">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="&#x161;elesty" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1288798878" CREATED="1374689334768" MODIFIED="1374689340987">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="z&#xe1;n&#x11b;t eustachovy trubice" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_766875190" CREATED="1374689350326" MODIFIED="1374689358317">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="sv&#x11b;d&#x11b;n&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_118355035" CREATED="1374689395948" MODIFIED="1374689398566">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="nos" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_623445022" CREATED="1374689383840" MODIFIED="1374689385203">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="sv&#x11b;d&#x11b;n&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_256240042" CREATED="1374689386132" MODIFIED="1374689389044">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="p&#xed;ch&#xe1;n&#xed;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_750827061" CREATED="1374689389324" MODIFIED="1374689394075">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="bolest hlavy" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1300844356" CREATED="1374689621178" MODIFIED="1374689625966">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="jednostrann&#xe9;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_420171784" CREATED="1374689631256" MODIFIED="1374689635682">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="v&#x11b;t&#x161;inou prav&#xe1;" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_1107605449" CREATED="1374689641563" MODIFIED="1374689647320">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
</node>
<node TEXT="Rubriky" COLOR="#0066cc" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Subtopic" POSITION="right" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<node TEXT="Mind; men, aversion to" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600" MIN_WIDTH="1" ID="ID_32862848" CREATED="1374687124980" MODIFIED="1374687124980">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="Modality" COLOR="#0066cc" STYLE="fork" MAX_WIDTH="600" MIN_WIDTH="1" STYLE_REF="Subtopic" POSITION="right" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
</node>
</node>
</map>
