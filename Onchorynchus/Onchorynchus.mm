<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Onchorynchus" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1398443137875"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="left" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389">
<node TEXT="narod&#xed; se v &#x159;ece" ID="ID_1285685307" CREATED="1398443266424" MODIFIED="1398443272510"/>
<node TEXT="putuje do mo&#x159;e" ID="ID_1888097773" CREATED="1398443272788" MODIFIED="1398443279259"/>
<node TEXT="proti proudu se dere &#x159;ekou zp&#x11b;t,&#xa;aby se rozmno&#x17e;il" ID="ID_1756891413" CREATED="1398443279770" MODIFIED="1398443318753"/>
<node TEXT="analogie se spermi&#xed;" ID="ID_904438493" CREATED="1398443391411" MODIFIED="1398443397782"/>
<node TEXT="p&#x159;i provingu ot&#x11b;hotn&#x11b;lo n&#x11b;kolik proverek" ID="ID_1262820282" CREATED="1398444352205" MODIFIED="1398444382797"/>
<node TEXT="rozmno&#x17e;uje se v nej&#x10d;ist&#x161;&#xed;ch &#x159;ek&#xe1;ch" ID="ID_498596632" CREATED="1398444621564" MODIFIED="1398444649162"/>
</node>
<node TEXT="Trituace" STYLE_REF="Subtopic" POSITION="left" ID="ID_1275465186" CREATED="1357168572522" MODIFIED="1357168576351">
<node TEXT="velk&#xfd; p&#x159;&#xed;liv energie" ID="ID_809735625" CREATED="1398445533424" MODIFIED="1398445540430"/>
</node>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="left" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1357168653304">
<node TEXT="neplodnost" ID="ID_1044290456" CREATED="1398444177685" MODIFIED="1398444190233">
<font BOLD="true"/>
<node TEXT="touha b&#xfd;t t&#x11b;hotn&#xe1;" ID="ID_1713032948" CREATED="1398443341955" MODIFIED="1398443352044"/>
<node TEXT="urputn&#xe9; usilov&#xe1;n&#xed; o po&#x10d;et&#xed;" ID="ID_1654049796" CREATED="1398443325061" MODIFIED="1398444581838">
<font BOLD="true"/>
</node>
<node TEXT="budu m&#xed;t miminko i za cenu, &#x17e;e nebudu zdrav&#xe1;" ID="ID_527746214" CREATED="1398444587003" MODIFIED="1398444611840"/>
<node TEXT="hled&#xe1; osudov&#xe9;ho partnera" ID="ID_873753779" CREATED="1398444246594" MODIFIED="1398444258956"/>
<node TEXT="partne&#x159;i si nesednou" ID="ID_466276402" CREATED="1398444259511" MODIFIED="1398444269735"/>
<node TEXT="&#x17e;eny, kter&#xe9; hledaj&#xed; svoji druhou polovinu" ID="ID_881149677" CREATED="1398446555064" MODIFIED="1398446571772"/>
<node TEXT="vnit&#x159;n&#x11b; um&#xed;r&#xe1; z&#xe1;rmutkem, &#x17e;e nem&#x16f;&#x17e;e m&#xed;t d&#xed;t&#x11b;" ID="ID_893623769" CREATED="1398446747739" MODIFIED="1398446787265"/>
<node TEXT="za&#x165;atost" ID="ID_1286311407" CREATED="1398446787758" MODIFIED="1398446791554"/>
</node>
<node TEXT="domov" ID="ID_1698053669" CREATED="1398444193828" MODIFIED="1398445691013">
<font BOLD="true"/>
<node TEXT="touha se vr&#xe1;tit dom&#x16f;" ID="ID_147160976" CREATED="1398444204271" MODIFIED="1398444216011">
<node TEXT="stoj&#xed; ho to stra&#x161;n&#x11b; moc energie" ID="ID_7540572" CREATED="1398444687917" MODIFIED="1398444699800"/>
</node>
<node TEXT="jako m&#xed;sto, kde &#x17e;ili m&#xed; p&#x159;edkov&#xe9;" ID="ID_663839518" CREATED="1398444217586" MODIFIED="1398444235628"/>
<node TEXT="ve vztahu s n&#x11b;k&#xfd;m" ID="ID_1414575705" CREATED="1398445728338" MODIFIED="1398445736736"/>
<node TEXT="partner" ID="ID_1653548501" CREATED="1398445749689" MODIFIED="1398445753469"/>
<node TEXT="rodina" ID="ID_587233523" CREATED="1398445754060" MODIFIED="1398445756190"/>
</node>
<node TEXT="voda" ID="ID_455608632" CREATED="1398445379504" MODIFIED="1398445701766">
<node TEXT="plavou" ID="ID_1612958818" CREATED="1398445390282" MODIFIED="1398445397525"/>
<node TEXT="p&#x159;&#xed;liv" ID="ID_1321844949" CREATED="1398445398053" MODIFIED="1398445401122"/>
<node TEXT="odliv" ID="ID_1456988182" CREATED="1398445402106" MODIFIED="1398445404205"/>
<node TEXT="oce&#xe1;ny" ID="ID_1799691111" CREATED="1398445406378" MODIFIED="1398445410967"/>
</node>
<node TEXT="svoboda" ID="ID_609650132" CREATED="1398445441193" MODIFIED="1398445443694">
<node TEXT="touha po" ID="ID_1161002492" CREATED="1398445445413" MODIFIED="1398445450190"/>
<node TEXT="touha po pohybu" ID="ID_1777850984" CREATED="1398445450719" MODIFIED="1398445455844"/>
<node TEXT="cestov&#xe1;n&#xed;" ID="ID_234947151" CREATED="1398445456310" MODIFIED="1398445460162"/>
<node TEXT="ch&#x16f;ze" ID="ID_91392226" CREATED="1398445460575" MODIFIED="1398445464899"/>
</node>
<node TEXT="&#xfa;pln&#x11b;k" ID="ID_852042683" CREATED="1398445490266" MODIFIED="1398445494086">
<node TEXT="reaguje na" ID="ID_221845918" CREATED="1398445516733" MODIFIED="1398445520204"/>
</node>
<node TEXT="miluje rychlou j&#xed;zdu autem" ID="ID_1555275135" CREATED="1398445607870" MODIFIED="1398445617393"/>
<node TEXT="osudov&#xfd; partner" ID="ID_1741842244" CREATED="1398445761496" MODIFIED="1398445767917">
<node TEXT="n&#x11b;kdo s k&#xfd;m m&#xe1;m siln&#xfd; duchovn&#xed; kontakt" ID="ID_179941374" CREATED="1398446264487" MODIFIED="1398446278439"/>
<node TEXT="n&#x11b;kdo, kdo je mi p&#x159;edur&#x10d;en" ID="ID_88427762" CREATED="1398446279227" MODIFIED="1398446289260"/>
<node TEXT="n&#x11b;kdo, kdo si ke m&#x11b; najde cestu" ID="ID_894988788" CREATED="1398446289770" MODIFIED="1398446298934"/>
</node>
<node TEXT="l&#xe1;ska" ID="ID_1853077150" CREATED="1398445887821" MODIFIED="1398445891591">
<node TEXT="osudov&#xe1;" ID="ID_359860302" CREATED="1398445897233" MODIFIED="1398445911388"/>
<node TEXT="znovu se zamiloval" ID="ID_1396859141" CREATED="1398445913535" MODIFIED="1398445925280"/>
<node TEXT="jen na n&#xed; z&#xe1;le&#x17e;&#xed;" ID="ID_1649177325" CREATED="1398445925791" MODIFIED="1398445933021"/>
<node TEXT="strach, &#x17e;e nenajdu opravdovou l&#xe1;sku" ID="ID_1193411311" CREATED="1398445947929" MODIFIED="1398445966291"/>
<node TEXT="posedlost po rodin&#x11b; a d&#xed;t&#x11b;ti" ID="ID_1750850950" CREATED="1398446105728" MODIFIED="1398446114813"/>
</node>
<node TEXT="patologick&#xfd; vztah mezi&#xa;matkou a dcerou" ID="ID_27818534" CREATED="1398446861488" MODIFIED="1398810791930">
<node TEXT="nevra&#x17e;ivost" ID="ID_730074504" CREATED="1398446876691" MODIFIED="1398446881750"/>
<node TEXT="vztek" ID="ID_1003451680" CREATED="1398446882010" MODIFIED="1398446884208"/>
<node TEXT="ha&#x161;te&#x159;ivost" ID="ID_563331472" CREATED="1398446884432" MODIFIED="1398446889198"/>
<node TEXT="dcera se c&#xed;t&#xed; ovl&#xe1;d&#xe1;na" ID="ID_1474688784" CREATED="1398446889700" MODIFIED="1398446900243"/>
<node TEXT="matka m&#xe1; pocit, &#x17e;e dcera je neschopn&#xe1;" ID="ID_106403777" CREATED="1398446901602" MODIFIED="1398446916698"/>
<node TEXT="&#x17e;&#xe1;dn&#xe1; ze stran nenaplnila o&#x10d;ek&#xe1;v&#xe1;n&#xed;" ID="ID_1934729519" CREATED="1398446920855" MODIFIED="1398446933339"/>
<node TEXT="vz&#xe1;jemn&#xfd; chlad" ID="ID_295573143" CREATED="1398446936013" MODIFIED="1398446943288"/>
</node>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="left" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<node TEXT="neplodnost" ID="ID_1879636128" CREATED="1398443700636" MODIFIED="1398443706236"/>
<node TEXT="sny o rovn&#xe9; cest&#x11b;" ID="ID_1363455728" CREATED="1398445637412" MODIFIED="1398445658081">
<node TEXT="cesta je jasn&#xe1;" ID="ID_91735007" CREATED="1398445659478" MODIFIED="1398445665312"/>
</node>
<node TEXT="l&#xe9;k pro lidi, kte&#x159;&#xed; pot&#x159;ebuj&#xed; zapustit ko&#x159;eny v nov&#xe9;m m&#xed;st&#x11b;" ID="ID_1401082579" CREATED="1398445835493" MODIFIED="1398445857371"/>
<node TEXT="harmonizuje t&#xe9;mata, kter&#xe1; se mohou t&#xe1;hnout mnoha &#x17e;ivoty" ID="ID_175400198" CREATED="1398446489340" MODIFIED="1398446510540"/>
<node TEXT="paraziti" ID="ID_554752566" CREATED="1398447197891" MODIFIED="1398447203358"/>
<node TEXT="hlava" ID="ID_1977540446" CREATED="1398447165480" MODIFIED="1398447169197">
<node TEXT="v&#x161;i" ID="ID_1827956510" CREATED="1398447170639" MODIFIED="1398447173683"/>
<node TEXT="sv&#x11b;d&#x11b;n&#xed;" ID="ID_1330617621" CREATED="1398447174131" MODIFIED="1398447178466"/>
<node TEXT="ekz&#xe9;my" ID="ID_254452529" CREATED="1398447206176" MODIFIED="1398447212009"/>
</node>
<node TEXT="o&#x10d;i" ID="ID_464914598" CREATED="1398447345318" MODIFIED="1398447348040">
<node TEXT="opuchlost" ID="ID_1100572415" CREATED="1398447349581" MODIFIED="1398447353663">
<node TEXT="jako po dlouh&#xe9;m pl&#xe1;&#x10d;i" ID="ID_1393432143" CREATED="1398447362416" MODIFIED="1398447369580"/>
</node>
<node TEXT="slzen&#xed;" ID="ID_1078586119" CREATED="1398447354531" MODIFIED="1398447360326"/>
</node>
<node TEXT="u&#x161;i" ID="ID_446402944" CREATED="1398447372031" MODIFIED="1398447374665">
<node TEXT="zbyst&#x159;en&#xfd; sluch" ID="ID_926364584" CREATED="1398447376259" MODIFIED="1398447382633"/>
</node>
<node TEXT="nos" ID="ID_1803312898" CREATED="1398447401384" MODIFIED="1398447417630">
<node TEXT="sinusitidy" ID="ID_1681731009" CREATED="1398447421512" MODIFIED="1398447425734"/>
</node>
<node TEXT="krk" ID="ID_1402926348" CREATED="1398447427144" MODIFIED="1398447429313">
<node TEXT="pocit z&#x16f;&#x17e;en&#xed;" ID="ID_1851945693" CREATED="1398447430567" MODIFIED="1398447439599"/>
<node TEXT="knedl&#xed;k" ID="ID_675395728" CREATED="1398447465354" MODIFIED="1398447469036"/>
</node>
<node TEXT="&#x17e;aludek" ID="ID_1177717350" CREATED="1398447495325" MODIFIED="1398447498980">
<node TEXT="vl&#x10d;&#xed; hlad" ID="ID_85511126" CREATED="1398447500556" MODIFIED="1398447505382"/>
</node>
<node TEXT="vztah k oran&#x17e;ov&#xe9; barv&#x11b;" ID="ID_1540760533" CREATED="1398447511654" MODIFIED="1398447539648"/>
<node TEXT="pohlavn&#xed; org&#xe1;ny" ID="ID_1576903506" CREATED="1398447540293" MODIFIED="1398447547286">
<node TEXT="endometri&#xf3;za" ID="ID_310107306" CREATED="1398447553601" MODIFIED="1398447562318"/>
<node TEXT="cysty na vaje&#x10d;n&#xed;c&#xed;ch" ID="ID_1238984837" CREATED="1398447562606" MODIFIED="1398447569445"/>
<node TEXT="vyr&#xe1;&#x17e;ky" ID="ID_1729215603" CREATED="1398447569795" MODIFIED="1398447574738"/>
<node TEXT="sv&#x11b;d&#x11b;n&#xed;" ID="ID_1509746366" CREATED="1398447575052" MODIFIED="1398447578261"/>
<node TEXT="neplodnost" ID="ID_1049822238" CREATED="1398447578548" MODIFIED="1398447586678"/>
<node TEXT="menstruace" ID="ID_473757745" CREATED="1398447586938" MODIFIED="1398447612692">
<node TEXT="nepravideln&#xe1;" ID="ID_203145751" CREATED="1398447613955" MODIFIED="1398447621289"/>
<node TEXT="sra&#x17e;eniny" ID="ID_962016794" CREATED="1398447622372" MODIFIED="1398447626421"/>
</node>
</node>
</node>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="left" ID="ID_1042712692" CREATED="1398810554182" MODIFIED="1398810561005">
<node TEXT="" STYLE_REF="Agg" ID="ID_405126700" CREATED="1398810563480" MODIFIED="1398810567322">
<node TEXT="hudba" ID="ID_1161629319" CREATED="1398810571403" MODIFIED="1398810642299"/>
<node TEXT="hluk" ID="ID_1902147184" CREATED="1398810642801" MODIFIED="1398810646011"/>
<node TEXT="v noci" ID="ID_872170610" CREATED="1398810646575" MODIFIED="1398810649885"/>
<node TEXT="ne&#x161;&#x165;astn&#xe1; l&#xe1;ska" ID="ID_1515052425" CREATED="1398810702636" MODIFIED="1398810715532"/>
</node>
</node>
<node TEXT="Onchorynchys tshawytscha" STYLE_REF="Remedy" POSITION="right" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1398808724189"/>
<node TEXT="Losos &#x10d;avi&#x10d;a" STYLE_REF="Remedy" POSITION="right" ID="ID_226903468" CREATED="1398443189883" MODIFIED="1398808740618"/>
<node TEXT="Onc-t" STYLE_REF="Abbrev" POSITION="right" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1398444660399"/>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="right" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729">
<node TEXT="Mind; anger; trifles, about" ID="ID_1950513336" CREATED="1398809429891" MODIFIED="1398809429891"/>
<node TEXT="Mind; anger; violent" ID="ID_1487691488" CREATED="1398809438377" MODIFIED="1398809438377"/>
<node TEXT="Mind; anxiety; money, financial matters, about" ID="ID_139498308" CREATED="1398809460854" MODIFIED="1398809460854"/>
<node TEXT="Mind; bathing; amel., warm" ID="ID_351838467" CREATED="1398809498309" MODIFIED="1398809498309"/>
<node TEXT="Mind; busy; fruitlessly" ID="ID_1185472959" CREATED="1398809527624" MODIFIED="1398809527624"/>
<node TEXT="Mind; censorious, critical" ID="ID_642692403" CREATED="1398809543004" MODIFIED="1398809543004"/>
<node TEXT="Mind; childless, ailments from being" ID="ID_1099425718" CREATED="1398809571565" MODIFIED="1398809571565"/>
<node TEXT="Mind; children; desires to; have, to beget, to nurture" ID="ID_1143543222" CREATED="1398809582920" MODIFIED="1398809582920"/>
<node TEXT="Mind; clairvoyance" ID="ID_1579894479" CREATED="1398809600037" MODIFIED="1398809600037"/>
<node TEXT="Mind; colors; charmed by" ID="ID_598606872" CREATED="1398809620000" MODIFIED="1398809620000"/>
<node TEXT="Mind; colors; desires" ID="ID_324984939" CREATED="1398809631237" MODIFIED="1398809631237"/>
<node TEXT="Mind; company; aversion to; solitude, desire for" ID="ID_949804355" CREATED="1398809652962" MODIFIED="1398809652962"/>
<node TEXT="Mind; concentration; impossible" ID="ID_502875219" CREATED="1398809670752" MODIFIED="1398809670752"/>
<node TEXT="Mind; confusion of mind; location, about" ID="ID_67003553" CREATED="1398809690124" MODIFIED="1398809690124"/>
<node TEXT="Mind; confusion of mind; location, about;&#xa;loses his way in well known streets" ID="ID_138305685" CREATED="1398809699122" MODIFIED="1398810910174"/>
<node TEXT="Mind; country, desire for, to go into the; mountains" ID="ID_669522735" CREATED="1398809723505" MODIFIED="1398809723505"/>
<node TEXT="Mind; cursing, swearing, desires; anger; with" ID="ID_972962661" CREATED="1398809741087" MODIFIED="1398809741087"/>
<node TEXT="Mind; dance, desires to" ID="ID_89109071" CREATED="1398809755065" MODIFIED="1398809755065"/>
<node TEXT="Mind; delusions, imaginations; animal, he or she is a; wolf" ID="ID_1252526766" CREATED="1398809784472" MODIFIED="1398809784472"/>
<node TEXT="Mind; delusions, imaginations; failure, he is a" ID="ID_792694591" CREATED="1398809800794" MODIFIED="1398809800794"/>
<node TEXT="Mind; delusions, imaginations; lost" ID="ID_1848663944" CREATED="1398809837998" MODIFIED="1398809837998"/>
<node TEXT="Mind; delusions, imaginations; queen, she is a" ID="ID_548475410" CREATED="1398809849638" MODIFIED="1398809849638"/>
<node TEXT="Mind; delusions, imaginations; trapped, he is" ID="ID_1287769994" CREATED="1398809857623" MODIFIED="1398809857623"/>
<node TEXT="Mind; delusions, imaginations; victim, she is a" ID="ID_1622642250" CREATED="1398809866878" MODIFIED="1398809866878"/>
<node TEXT="Mind; dreams; child, children; babies; nursing" ID="ID_1096118893" CREATED="1398809896768" MODIFIED="1398809896768"/>
<node TEXT="Mind; dreams; driving a; car" ID="ID_48522063" CREATED="1398809921352" MODIFIED="1398809921352"/>
<node TEXT="Mind; dreams; efforts, unsuccessful" ID="ID_1310513197" CREATED="1398809932219" MODIFIED="1398809932219"/>
<node TEXT="Mind; dreams; harmony, of" ID="ID_268922885" CREATED="1398809956572" MODIFIED="1398809956572"/>
<node TEXT="Mind; dreams; house, houses; big" ID="ID_578499170" CREATED="1398809966511" MODIFIED="1398809966511"/>
<node TEXT="Mind; dreams; house, houses; family house, their" ID="ID_26246915" CREATED="1398809974564" MODIFIED="1398809974564"/>
<node TEXT="Mind; dreams; journey, travelling" ID="ID_371913542" CREATED="1398809986122" MODIFIED="1398809986122"/>
<node TEXT="Mind; dreams; love; lost" ID="ID_1868351482" CREATED="1398809998383" MODIFIED="1398809998383"/>
<node TEXT="Mind; dreams; menses" ID="ID_1328022872" CREATED="1398810008272" MODIFIED="1398810008272"/>
<node TEXT="Mind; dreams; pregnant, of being" ID="ID_819791617" CREATED="1398810015477" MODIFIED="1398810015477"/>
<node TEXT="Mind; dreams; railway lines and roads" ID="ID_1889738828" CREATED="1398810024054" MODIFIED="1398810024054"/>
<node TEXT="Mind; dreams; soul mate, of" ID="ID_589036439" CREATED="1398810036352" MODIFIED="1398810036352"/>
<node TEXT="Mind; freedom; desire for" ID="ID_935791247" CREATED="1398810071477" MODIFIED="1398810071477"/>
<node TEXT="Mind; grief; sterility, from" ID="ID_1913431895" CREATED="1398810109643" MODIFIED="1398810109643"/>
<node TEXT="Mind; high-spirited" ID="ID_1044738612" CREATED="1398810132561" MODIFIED="1398810132561"/>
<node TEXT="Mind; home; go, desire to" ID="ID_846391139" CREATED="1398810150575" MODIFIED="1398810150575"/>
<node TEXT="Mind; home; go, desire to; ancestral" ID="ID_1419468000" CREATED="1398810154932" MODIFIED="1398810154932"/>
<node TEXT="Mind; inquisitive; observing others" ID="ID_1960233935" CREATED="1398810183690" MODIFIED="1398810183690"/>
<node TEXT="Mind; love; exalted" ID="ID_821568460" CREATED="1398810204408" MODIFIED="1398810204408"/>
<node TEXT="Mind; love; love-sick" ID="ID_251356491" CREATED="1398810211027" MODIFIED="1398810211027"/>
<node TEXT="Mind; mischievous" ID="ID_1513680806" CREATED="1398810309326" MODIFIED="1398810309326"/>
<node TEXT="Mind; play; desire to, playful" ID="ID_957344657" CREATED="1398810323841" MODIFIED="1398810323841"/>
<node TEXT="Mind; positiveness" ID="ID_1773885367" CREATED="1398810342950" MODIFIED="1398810342950"/>
<node TEXT="Mind; noise; agg." ID="ID_443983387" CREATED="1398810357854" MODIFIED="1398810357854"/>
<node TEXT="Mind; pregnant, desire to be" ID="ID_1304301642" CREATED="1398810384370" MODIFIED="1398810384370"/>
<node TEXT="Mind; sensitive, oversensitive; noise, to" ID="ID_1824500966" CREATED="1398810403125" MODIFIED="1398810403125"/>
<node TEXT="Mind; sympathetic, compassionate, too" ID="ID_521051653" CREATED="1398810421758" MODIFIED="1398810421758"/>
<node TEXT="Mind; time; loss of conception of" ID="ID_1812426760" CREATED="1398810439420" MODIFIED="1398810439420"/>
<node TEXT="Mind; time; timelessness" ID="ID_493827690" CREATED="1398810450640" MODIFIED="1398810450640"/>
<node TEXT="Mind; travel, desire to" ID="ID_909525783" CREATED="1398810465913" MODIFIED="1398810465913"/>
<node TEXT="Mind; violence, vehemence" ID="ID_1656420718" CREATED="1398810485411" MODIFIED="1398810485411"/>
<node TEXT="Mind; walk, walking; must; straight lines" ID="ID_627078250" CREATED="1398810506741" MODIFIED="1398810506741"/>
<node TEXT="Mind; water; desire for" ID="ID_1653042342" CREATED="1398810521950" MODIFIED="1398810521950"/>
</node>
<node TEXT="" POSITION="right" ID="ID_1599363688" CREATED="1398809118123" MODIFIED="1398810922286" HGAP="-50" VSHIFT="10">
<hook URI="Chinook-salmon-or-king-salmon-Oncorhynchus-tshawytscha-Lx321.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</map>
