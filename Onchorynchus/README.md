
Onchorynchus
==============

![mapa](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Onchorynchus/Onchorynchus.svg)

 - [PDF](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Onchorynchus/Onchorynchus.pdf)
 - [Map](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Onchorynchus/Onchorynchus.mm)
 - [SVG](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Onchorynchus/Onchorynchus.svg)

Rubriky
-------
 - Mind; anger; trifles, about [[Ars](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/ArsenicumAlbum)]
 - Mind; anger; violent [[Hom](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Homarus gammarus)]
 - Mind; anxiety; money, financial matters, about [[Aq-mar](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Aqua marina)]
 - Mind; bathing; amel., warm 
 - Mind; busy; fruitlessly 
 - Mind; censorious, critical [[Ars](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/ArsenicumAlbum)]
 - Mind; childless, ailments from being 
 - Mind; children; desires to; have, to beget, to nurture 
 - Mind; clairvoyance [[Verat-v](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Veratrum viride)]
 - Mind; colors; charmed by 
 - Mind; colors; desires 
 - Mind; company; aversion to; solitude, desire for 
 - Mind; concentration; impossible 
 - Mind; confusion of mind; location, about 
 - Mind; confusion of mind; location, about;
loses his way in well known streets 
 - Mind; country, desire for, to go into the; mountains 
 - Mind; cursing, swearing, desires; anger; with 
 - Mind; dance, desires to 
 - Mind; delusions, imaginations; animal, he or she is a; wolf 
 - Mind; delusions, imaginations; failure, he is a [[Aq-mar](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Aqua marina)]
 - Mind; delusions, imaginations; lost [[Arg-n](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/ArgentumNitricum)]
 - Mind; delusions, imaginations; queen, she is a 
 - Mind; delusions, imaginations; trapped, he is 
 - Mind; delusions, imaginations; victim, she is a 
 - Mind; dreams; child, children; babies; nursing 
 - Mind; dreams; driving a; car 
 - Mind; dreams; efforts, unsuccessful 
 - Mind; dreams; harmony, of 
 - Mind; dreams; house, houses; big 
 - Mind; dreams; house, houses; family house, their 
 - Mind; dreams; journey, travelling 
 - Mind; dreams; love; lost 
 - Mind; dreams; menses 
 - Mind; dreams; pregnant, of being 
 - Mind; dreams; railway lines and roads 
 - Mind; dreams; soul mate, of 
 - Mind; freedom; desire for 
 - Mind; grief; sterility, from 
 - Mind; high-spirited 
 - Mind; home; go, desire to 
 - Mind; home; go, desire to; ancestral 
 - Mind; inquisitive; observing others 
 - Mind; love; exalted 
 - Mind; love; love-sick 
 - Mind; mischievous 
 - Mind; noise; agg. 
 - Mind; play; desire to, playful 
 - Mind; positiveness 
 - Mind; pregnant, desire to be 
 - Mind; sensitive, oversensitive; noise, to 
 - Mind; sympathetic, compassionate, too 
 - Mind; time; loss of conception of [[Arg-n](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/ArgentumNitricum)]
 - Mind; time; timelessness 
 - Mind; travel, desire to 
 - Mind; violence, vehemence 
 - Mind; walk, walking; must; straight lines 
 - Mind; water; desire for 
    