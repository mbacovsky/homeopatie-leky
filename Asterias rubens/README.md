
Asterias rubens
==============

![mapa](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Asterias%20rubens/Asterias%20rubens.svg)

 - [PDF](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Asterias%20rubens/Asterias%20rubens.pdf)
 - [Map](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Asterias%20rubens/Asterias%20rubens.mm)
 - [SVG](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Asterias%20rubens/Asterias%20rubens.svg)

Rubriky
-------
 - Chest; cancer; painful; lancinating, mammae 
 - Chest; cancer; ulcerating, mammae 
 - Female; cancer; uterus 
 - Mind; afternoon; agg. [[Aq-mar](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Aqua marina)]
 - Mind; alternating states; emotional 
 - Mind; anguish; afternoon 
 - Mind; anxiety; weeping; amel. 
 - Mind; children; aversion to 
 - Mind; climacteric period, in [[Murx](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Murex purpurea)]
 - Mind; coition; agg. 
 - Mind; contradiction; ailments from, agg. 
 - Mind; contradiction; intolerant of [[Tarent](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/TarentulaHispanica)]
 - Mind; deeds; desire to perform; useful 
 - Mind; delusions, imaginations; home; away from, is 
 - Mind; delusions, imaginations; stranger, strangers; amongst 
 - Mind; delusions, imaginations; stranger, strangers; control of, under 
 - Mind; despair; sexual desire, from 
 - Mind; estranged 
 - Mind; estranged; climacteric period, in 
 - Mind; excitement, excitable; alternating with; sadness 
 - Mind; excitement, excitable; amel. 
 - Mind; excitement, excitable; contradiction, from least 
 - Mind; fear; misfortune, of; hysteria, in, weeping amel. 
 - Mind; irritability; mental exertion agg. 
 - Mind; irritability; nervous 
 - Mind; night; agg. [[Cor-r](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Corallium rubrum)]
 - Mind; nymphomania 
 - Mind; prostration of mind; mental exertion agg. 
 - Mind; quarrelsomeness, scolding 
 - Mind; restlessness, nervousness; mental exertion; agg. 
 - Mind; sadness; weeping; amel. 
 - Mind; satyriasis 
 - Mind; thinking; aversion to [[Spong](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Spongia tosta)]
 - Mind; weeping, tearful mood; amel. 
 - Mind; weeping, tearful mood; excitement, emotional, agg. 
 - Mind; women; mental, emotional complaints in [[Tarent](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/TarentulaHispanica)]
 - Mind; work; mental; aversion to 
    