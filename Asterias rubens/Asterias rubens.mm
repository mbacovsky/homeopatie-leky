<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Asterias rubens" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1398518018207"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Trituace" STYLE_REF="Subtopic" POSITION="left" ID="ID_1275465186" CREATED="1357168572522" MODIFIED="1357168576351">
<node TEXT="st&#x159;&#xed;d&#xe1;n&#xed; n&#xe1;lad" ID="ID_414160189" CREATED="1398518091340" MODIFIED="1398518103990"/>
<node TEXT="podr&#xe1;&#x17e;d&#x11b;nost" ID="ID_594219644" CREATED="1398518757812" MODIFIED="1398518763939"/>
</node>
<node TEXT="Podobn&#xe9; l&#xe9;ky" STYLE_REF="Subtopic" POSITION="left" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1357168625505">
<node TEXT="Sep" ID="ID_1580837758" CREATED="1398519065031" MODIFIED="1398519069395">
<node TEXT="d&#x11b;l&#xe1; v&#x11b;ci sama, proto&#x17e;e je to pro ni jednodu&#x161;&#x161;&#xed;" STYLE_REF="Differs" ID="ID_801805794" CREATED="1398519073160" MODIFIED="1398519096970"/>
<node TEXT="p&#x159;edst&#xed;ran&#xe1; nez&#xe1;vislost" STYLE_REF="Differs" ID="ID_1390823359" CREATED="1398519216241" MODIFIED="1398519228421"/>
<node TEXT="aktivita, pohyb" STYLE_REF="Same" ID="ID_1005730638" CREATED="1398520469939" MODIFIED="1398520479009"/>
</node>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="left" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<node TEXT="prsy" ID="ID_83137014" CREATED="1398518026790" MODIFIED="1398518033272">
<node TEXT="karcinom" ID="ID_1245527932" CREATED="1398518034811" MODIFIED="1398518044672">
<node TEXT="lev&#xe9;" ID="ID_92723830" CREATED="1398518060273" MODIFIED="1398518062955">
<node TEXT="prav&#xe9; - Conium" STYLE_REF="Comment" ID="ID_795586838" CREATED="1398518064396" MODIFIED="1398518074080"/>
</node>
</node>
<node TEXT="vp&#xe1;&#x10d;en&#xe1; bradavka" ID="ID_1004616843" CREATED="1398518048612" MODIFIED="1398518086593"/>
<node TEXT="zatvrdliny" ID="ID_1066114831" CREATED="1398520920791" MODIFIED="1398520927797"/>
<node TEXT="cysty" ID="ID_1900800878" CREATED="1398520935104" MODIFIED="1398520937596"/>
<node TEXT="otoky u mu&#x17e;&#x16f;" ID="ID_946472118" CREATED="1398520938322" MODIFIED="1398520964767"/>
<node TEXT="dlouho koj&#xed;" ID="ID_682495459" CREATED="1398521045271" MODIFIED="1398521051209">
<node TEXT="i 3 roky" STYLE_REF="Comment" ID="ID_813035738" CREATED="1398521059041" MODIFIED="1398521064158"/>
</node>
<node TEXT="po operaci, n&#xed;zk&#xe1; potence, m&#x11b;s&#xed;c" ID="ID_1316547349" CREATED="1398521351843" MODIFIED="1398521380481"/>
</node>
<node TEXT="sv&#x11b;d&#x11b;n&#xed;" ID="ID_1979774554" CREATED="1398520974232" MODIFIED="1398520977137">
<node TEXT="ekz&#xe9;my" ID="ID_1755951611" CREATED="1398520978803" MODIFIED="1398520983392"/>
<node TEXT="kop&#x159;ivky" ID="ID_627853770" CREATED="1398520983617" MODIFIED="1398520987564"/>
<node TEXT="i bez vyr&#xe1;&#x17e;ek" ID="ID_164406518" CREATED="1398520987816" MODIFIED="1398520995490"/>
<node TEXT="po ml&#xe9;ku" ID="ID_124061580" CREATED="1398520997002" MODIFIED="1398521002121"/>
</node>
<node TEXT="sp&#xe1;nek" ID="ID_487420326" CREATED="1398521067074" MODIFIED="1398521069911">
<node TEXT="&#x161;patn&#x11b; sp&#xed;" ID="ID_308209215" CREATED="1398521071470" MODIFIED="1398521076909">
<node TEXT="z &#xfa;navy" ID="ID_435065877" CREATED="1398521079532" MODIFIED="1398521083044"/>
</node>
<node TEXT="erotick&#xe9; sny" ID="ID_7540375" CREATED="1398521086570" MODIFIED="1398521090456"/>
</node>
<node TEXT="kardio" ID="ID_1247714823" CREATED="1398521292730" MODIFIED="1398521297830">
<node TEXT="infarkt" ID="ID_1491655084" CREATED="1398521298370" MODIFIED="1398521303089"/>
</node>
<node TEXT="gyn" ID="ID_234248819" CREATED="1398521305206" MODIFIED="1398521306937">
<node TEXT="klimakterium" ID="ID_1519490140" CREATED="1398521308371" MODIFIED="1398521313069">
<node TEXT="n&#xe1;valy" ID="ID_641010782" CREATED="1398521319441" MODIFIED="1398521321954"/>
<node TEXT="z&#x10d;erven&#xe1;n&#xed;" ID="ID_534962281" CREATED="1398521322277" MODIFIED="1398521326342"/>
<node TEXT="z&#xe1;vrat&#x11b;" ID="ID_1436566808" CREATED="1398521326674" MODIFIED="1398521329829"/>
</node>
</node>
<node TEXT="opo&#x17e;d&#x11b;n&#xfd; r&#x16f;st u d&#x11b;t&#xed;" ID="ID_1621434506" CREATED="1398521335688" MODIFIED="1398521343978"/>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="left" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729">
<node TEXT="Mind; alternating states; emotional" ID="ID_1487652396" CREATED="1398521662178" MODIFIED="1398521662178"/>
<node TEXT="Mind; afternoon; agg." ID="ID_862934926" CREATED="1398521679958" MODIFIED="1398521679958"/>
<node TEXT="Mind; night; agg." ID="ID_1786046934" CREATED="1398521699379" MODIFIED="1398521699379"/>
<node TEXT="Mind; anguish; afternoon" ID="ID_911354966" CREATED="1398521744431" MODIFIED="1398521744431"/>
<node TEXT="Mind; anxiety; weeping; amel." ID="ID_1001446880" CREATED="1398521759742" MODIFIED="1398521759742"/>
<node TEXT="Mind; children; aversion to" ID="ID_656282794" CREATED="1398521778647" MODIFIED="1398521778647"/>
<node TEXT="Mind; climacteric period, in" ID="ID_600826911" CREATED="1398521793605" MODIFIED="1398521793605"/>
<node TEXT="Mind; coition; agg." ID="ID_1582125598" CREATED="1398521806089" MODIFIED="1398521806089"/>
<node TEXT="Mind; contradiction; ailments from, agg." ID="ID_1960507293" CREATED="1398521820624" MODIFIED="1398521820624"/>
<node TEXT="Mind; contradiction; intolerant of" ID="ID_734128974" CREATED="1398521827973" MODIFIED="1398521827973"/>
<node TEXT="Mind; deeds; desire to perform; useful" ID="ID_1907226006" CREATED="1398521846629" MODIFIED="1398521846629"/>
<node TEXT="Mind; delusions, imaginations; stranger, strangers; control of, under" ID="ID_1450951249" CREATED="1398521872260" MODIFIED="1398521872260"/>
<node TEXT="Mind; delusions, imaginations; home; away from, is" ID="ID_1871328070" CREATED="1398521890511" MODIFIED="1398521890511"/>
<node TEXT="Mind; delusions, imaginations; stranger, strangers; amongst" ID="ID_569266932" CREATED="1398521916073" MODIFIED="1398521916073"/>
<node TEXT="Mind; despair; sexual desire, from" ID="ID_459135535" CREATED="1398521930410" MODIFIED="1398521930410"/>
<node TEXT="Mind; estranged" ID="ID_1523581537" CREATED="1398521940711" MODIFIED="1398521940711"/>
<node TEXT="Mind; estranged; climacteric period, in" ID="ID_262266316" CREATED="1398521946831" MODIFIED="1398521946831"/>
<node TEXT="Mind; excitement, excitable; alternating with; sadness" ID="ID_509885550" CREATED="1398522009643" MODIFIED="1398522009643"/>
<node TEXT="Mind; excitement, excitable; amel." ID="ID_355093373" CREATED="1398522061658" MODIFIED="1398522061658"/>
<node TEXT="Mind; excitement, excitable; contradiction, from least" ID="ID_1033682568" CREATED="1398522093398" MODIFIED="1398522093398"/>
<node TEXT="Mind; fear; misfortune, of; hysteria, in, weeping amel." ID="ID_529834208" CREATED="1398522125454" MODIFIED="1398522125454"/>
<node TEXT="Mind; fear; misfortune, of; hysteria, in, weeping amel." ID="ID_204754092" CREATED="1398522145463" MODIFIED="1398522145463"/>
<node TEXT="Mind; irritability; mental exertion agg." ID="ID_670467257" CREATED="1398522196501" MODIFIED="1398522196501"/>
<node TEXT="Mind; irritability; nervous" ID="ID_1370147489" CREATED="1398522214047" MODIFIED="1398522214047"/>
<node TEXT="Mind; nymphomania" ID="ID_360627140" CREATED="1398522228645" MODIFIED="1398522228645"/>
<node TEXT="Mind; prostration of mind; mental exertion agg." ID="ID_630045227" CREATED="1398522243486" MODIFIED="1398522243486"/>
<node TEXT="Mind; quarrelsomeness, scolding" ID="ID_1173865698" CREATED="1398522256884" MODIFIED="1398522256884"/>
<node TEXT="Mind; restlessness, nervousness; mental exertion; agg." ID="ID_460854948" CREATED="1398522273985" MODIFIED="1398522273985"/>
<node TEXT="Mind; sadness; weeping; amel." ID="ID_1192343355" CREATED="1398522295193" MODIFIED="1398522295193"/>
<node TEXT="Mind; satyriasis" ID="ID_299718394" CREATED="1398522309258" MODIFIED="1398522309258"/>
<node TEXT="Mind; thinking; aversion to" ID="ID_774476030" CREATED="1398522347457" MODIFIED="1398522347457"/>
<node TEXT="Mind; weeping, tearful mood; amel." ID="ID_773271740" CREATED="1398522364436" MODIFIED="1398522364436"/>
<node TEXT="Mind; weeping, tearful mood; excitement, emotional, agg." ID="ID_1067067039" CREATED="1398522374091" MODIFIED="1398522374091"/>
<node TEXT="Mind; women; mental, emotional complaints in" ID="ID_1246687594" CREATED="1398522404640" MODIFIED="1398522404640"/>
<node TEXT="Mind; work; mental; aversion to" ID="ID_969314055" CREATED="1398522412409" MODIFIED="1398522412409"/>
<node TEXT="Female; cancer; uterus" ID="ID_639576081" CREATED="1398522427892" MODIFIED="1398522427892"/>
<node TEXT="Chest; cancer; painful; lancinating, mammae" ID="ID_1820054866" CREATED="1398522440271" MODIFIED="1398522440271"/>
<node TEXT="Chest; cancer; ulcerating, mammae" ID="ID_81542591" CREATED="1398522459578" MODIFIED="1398522459578"/>
</node>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="left" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690"/>
<node TEXT="Hv&#x11b;zdice mo&#x159;sk&#xe1;" STYLE_REF="Remedy" POSITION="right" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1398520781076"/>
<node TEXT="Aster" STYLE_REF="Abbrev" POSITION="right" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1398519259527"/>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="right" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389">
<node TEXT="kdy&#x17e; p&#x159;ijde o rameno, doroste" ID="ID_37340452" CREATED="1398518741334" MODIFIED="1398518753409"/>
<node TEXT="je schopn&#xe1; se reprodukovat sama" ID="ID_1984716299" CREATED="1398518765702" MODIFIED="1398518798676"/>
<node TEXT="v dob&#x11b; rozmno&#x17e;ov&#xe1;n&#xed; obsahuje asi 100000 vaj&#xed;&#x10d;ek" ID="ID_315365329" CREATED="1398518827093" MODIFIED="1398518856023"/>
<node TEXT="ve st&#x159;edu m&#xe1; &#xfa;sta, kter&#xe1; slou&#x17e;&#xed; jako &#x17e;aludek" ID="ID_1550860730" CREATED="1398518856373" MODIFIED="1398518883429"/>
</node>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="right" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1357168653304">
<node TEXT="j&#xe1; sama" ID="ID_1886673831" CREATED="1398518804024" MODIFIED="1398519111887">
<font BOLD="true"/>
<node TEXT="v&#x161;echno dok&#xe1;&#x17e;u" ID="ID_1153808193" CREATED="1398518978249" MODIFIED="1398518985154"/>
<node TEXT="uk&#xe1;&#x17e;u ostatn&#xed;m" ID="ID_352569209" CREATED="1398518990446" MODIFIED="1398518997559"/>
<node TEXT="&#x17e;ivotn&#xed; moto" ID="ID_1437157094" CREATED="1398519152226" MODIFIED="1398519171742"/>
<node TEXT="nesnese b&#xfd;t z&#xe1;visl&#xe1;" ID="ID_649909272" CREATED="1398519335554" MODIFIED="1398519345607"/>
<node TEXT="um&#xed;n&#x11b;n&#xe1;" ID="ID_1066098304" CREATED="1398519681701" MODIFIED="1398519686143"/>
<node TEXT="probl&#xe9;m s n&#x11b;k&#xfd;m n&#x11b;co tvo&#x159;it" ID="ID_405688562" CREATED="1398520201239" MODIFIED="1398520229420"/>
<node TEXT="d&#x11b;l&#xe1; dojem siln&#xe9;ho" ID="ID_399347350" CREATED="1398520373591" MODIFIED="1398520381736"/>
<node TEXT="v&#xfd;konn&#xed;" ID="ID_1513603383" CREATED="1398520390083" MODIFIED="1398520408697"/>
<node TEXT="i p&#x159;es pokro&#x10d;ilou patologii nevn&#xed;m&#xe1; v&#xe1;&#x17e;nost situace" ID="ID_1831435613" CREATED="1398520418764" MODIFIED="1398520466168"/>
</node>
<node TEXT="chladn&#xe1;" ID="ID_773627096" CREATED="1398519000836" MODIFIED="1398519004642"/>
<node TEXT="tvrd&#xe1;" ID="ID_1381306833" CREATED="1398519005492" MODIFIED="1398519008386"/>
<node TEXT="dr&#x17e;&#xed; si odstup" ID="ID_851937641" CREATED="1398519174985" MODIFIED="1398519186926"/>
<node TEXT="kdy&#x17e; je nespokojen&#xe1;, jedn&#xe1;" ID="ID_1450836808" CREATED="1398519308886" MODIFIED="1398519317337"/>
<node TEXT="partnery st&#x159;&#xed;d&#xe1; s nez&#xe1;jmem" ID="ID_1822846609" CREATED="1398519390086" MODIFIED="1398519399323"/>
<node TEXT="partner je ve vztahu &quot;do po&#x10d;tu&quot;" ID="ID_1187844007" CREATED="1398519399914" MODIFIED="1398519416041"/>
<node TEXT="vnit&#x159;n&#xed; konflikty" ID="ID_1100075352" CREATED="1398519511151" MODIFIED="1398519517769"/>
<node TEXT="n&#x11b;kde uvnit&#x159; nesn&#xe1;&#x161;&#xed; sama sebe" ID="ID_1613773557" CREATED="1398519519862" MODIFIED="1398519542353"/>
<node TEXT="pocit, &#x17e;e" ID="ID_1698219600" CREATED="1398519773928" MODIFIED="1398519781859">
<node TEXT="zap&#xe1;ch&#xe1;" ID="ID_160653947" CREATED="1398519783078" MODIFIED="1398519787126">
<node TEXT="vlasy" ID="ID_305503734" CREATED="1398519795203" MODIFIED="1398519796961"/>
<node TEXT="genit&#xe1;lie" ID="ID_731213826" CREATED="1398519797248" MODIFIED="1398519800823"/>
</node>
<node TEXT="je &#x161;kared&#xe1;" ID="ID_1058657247" CREATED="1398519787520" MODIFIED="1398519793041"/>
</node>
<node TEXT="zv&#xfd;&#x161;en&#xe1; sexu&#xe1;ln&#xed; touha" ID="ID_14624237" CREATED="1398519825688" MODIFIED="1398519839026">
<node TEXT="nastaven&#xe1;, &#x17e;e nikoho nepot&#x159;ebuje" STYLE_REF="Comment" ID="ID_1268804673" CREATED="1398519874480" MODIFIED="1398519889928"/>
<node TEXT="potla&#x10d;ovan&#xe1;" ID="ID_1877725518" CREATED="1398519845957" MODIFIED="1398519873103"/>
<node TEXT="sex jako sport" ID="ID_319839721" CREATED="1398519928668" MODIFIED="1398519931974"/>
</node>
<node TEXT="podr&#xe1;&#x17e;d&#x11b;n&#xfd;" ID="ID_1661592173" CREATED="1398520240751" MODIFIED="1398520255967">
<node TEXT="zlostn&#xfd;" ID="ID_1312379635" CREATED="1398520257562" MODIFIED="1398520262288"/>
<node TEXT="pot&#x159;eba se h&#xe1;dat" ID="ID_517798837" CREATED="1398520266911" MODIFIED="1398520272541"/>
</node>
<node TEXT="pl&#xe1;&#x10d;e" ID="ID_1001692441" CREATED="1398520274170" MODIFIED="1398520280837"/>
<node TEXT="konzervativn&#xed;" ID="ID_1615383453" CREATED="1398520339581" MODIFIED="1398520344679">
<node TEXT="nerada zm&#x11b;nu" ID="ID_1855880330" CREATED="1398520345978" MODIFIED="1398520351274"/>
<node TEXT="nerada nov&#xe9; v&#x11b;ci" ID="ID_561640230" CREATED="1398520351714" MODIFIED="1398520361701"/>
</node>
<node TEXT="aktivita" ID="ID_603872676" CREATED="1398520484649" MODIFIED="1398520662020">
<node TEXT="cvi&#x10d;en&#xed;" ID="ID_1811126100" CREATED="1398520489469" MODIFIED="1398520492658"/>
<node TEXT="sport" ID="ID_261113683" CREATED="1398520493141" MODIFIED="1398520494731"/>
<node TEXT="pot&#x159;eba uk&#xe1;zat svoji nez&#xe1;vislost" ID="ID_1715474216" CREATED="1398520541398" MODIFIED="1398520553769"/>
<node TEXT="p&#x159;ehnan&#xe1;" ID="ID_1910840739" CREATED="1398520663492" MODIFIED="1398520668281"/>
<node TEXT="p&#x159;epracovanost" ID="ID_195655148" CREATED="1398520668702" MODIFIED="1398520673119"/>
</node>
</node>
<node TEXT="" POSITION="right" ID="ID_889927505" CREATED="1398640201163" MODIFIED="1398640225641">
<hook URI="ZLB-091003%20Gewone%20zeester,%20Asterias%20rubens%202.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</map>
