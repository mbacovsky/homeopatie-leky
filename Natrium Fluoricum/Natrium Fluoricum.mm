<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Natrium Fluoricum" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1368358985628"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Fluorid sodn&#xfd;" STYLE_REF="Remedy" POSITION="right" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1368359024205"/>
<node TEXT="Nat-f" STYLE_REF="Abbrev" POSITION="right" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1368359009326"/>
<node TEXT="Miazma" STYLE_REF="Subtopic" POSITION="right" ID="ID_31190142" CREATED="1357168609677" MODIFIED="1357168614539"/>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="right" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389"/>
<node TEXT="Trituace" STYLE_REF="Subtopic" POSITION="right" ID="ID_1275465186" CREATED="1357168572522" MODIFIED="1357168576351">
<node TEXT="bolest zub&#x16f; (v&#x161;ech)" ID="ID_167977275" CREATED="1368359195234" MODIFIED="1368359208556">
<font ITALIC="true"/>
</node>
<node TEXT="bolest v kolenou" ID="ID_1764724259" CREATED="1368359209896" MODIFIED="1368359220162">
<font ITALIC="true"/>
</node>
<node TEXT="&#xfa;nava" ID="ID_160375366" CREATED="1368359650320" MODIFIED="1368359677007">
<font ITALIC="true"/>
</node>
<node TEXT="trudomyslnost" ID="ID_280872605" CREATED="1368359659660" MODIFIED="1368359678926">
<font ITALIC="true"/>
</node>
<node TEXT="sta&#x17e;en&#xed; se" ID="ID_1720041006" CREATED="1368359695372" MODIFIED="1368359701201">
<font ITALIC="true"/>
</node>
</node>
<node TEXT="Podobn&#xe9; l&#xe9;ky" STYLE_REF="Subtopic" POSITION="right" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1357168625505"/>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="right" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1357168653304">
<node TEXT="Nat" STYLE_REF="Subsubtopic" ID="ID_1296985197" CREATED="1368359706587" MODIFIED="1368359713919">
<node TEXT="sta&#x17e;en&#xed; se" ID="ID_1311958399" CREATED="1368359718703" MODIFIED="1368359724005"/>
<node TEXT="smutek" ID="ID_924784263" CREATED="1368359724310" MODIFIED="1368359727072"/>
<node TEXT="odm&#xed;tnut&#xed;" ID="ID_1491408674" CREATED="1368359731363" MODIFIED="1368359734432"/>
<node TEXT="citlivost" ID="ID_1724277981" CREATED="1368359734704" MODIFIED="1368359738125"/>
<node TEXT="zadr&#x17e;ov&#xe1;n&#xed;" ID="ID_1973960949" CREATED="1368359738452" MODIFIED="1368359742884"/>
<node TEXT="uzav&#x159;enost" ID="ID_1048975765" CREATED="1368360696103" MODIFIED="1368360702266">
<font BOLD="true"/>
</node>
</node>
<node TEXT="velk&#xfd; rozpor" ID="ID_668571927" CREATED="1368359685023" MODIFIED="1368359689202">
<node TEXT="pocit viny z n&#x11b;&#x10d;ho zak&#xe1;zan&#xe9;ho" ID="ID_974396862" CREATED="1368359803088" MODIFIED="1368360035961">
<node TEXT="incest" ID="ID_557265431" CREATED="1368359841400" MODIFIED="1368359843115"/>
</node>
</node>
<node TEXT="velk&#xe1; touha do toho j&#xed;t" ID="ID_738570221" CREATED="1368359866689" MODIFIED="1368359935471">
<node TEXT="ale neud&#x11b;l&#xe1; to" ID="ID_1617759840" CREATED="1368359941460" MODIFIED="1368359949366"/>
<node TEXT="ud&#x11b;l&#xe1; to" ID="ID_1296948785" CREATED="1368359959703" MODIFIED="1368359964739">
<node TEXT="ale mus&#xed; to utajnit" ID="ID_1843801290" CREATED="1368359967385" MODIFIED="1368359974397"/>
<node TEXT="tr&#xe1;p&#xed; se z toho" ID="ID_715620449" CREATED="1368359975844" MODIFIED="1368359981361"/>
</node>
</node>
<node TEXT="tajn&#x16f;stk&#xe1;&#x159;" ID="ID_1718147651" CREATED="1368360038104" MODIFIED="1368360043247">
<node TEXT="n&#x11b;co zatajuje, aby neshodil dojem, kter&#xfd; chce vytv&#xe1;&#x159;et" ID="ID_1901638380" CREATED="1368360044319" MODIFIED="1368360065066"/>
<node TEXT="kdy&#x17e; se to prozrad&#xed;" ID="ID_253086342" CREATED="1368360109464" MODIFIED="1368360128279">
<node TEXT="ztrat&#xed; svoje hodnocen&#xed;" ID="ID_1480144809" CREATED="1368360129463" MODIFIED="1368360150532"/>
</node>
<node TEXT="dvoj&#xed; &#x17e;ivot" ID="ID_630299257" CREATED="1368360156075" MODIFIED="1368360159905"/>
</node>
<node TEXT="udr&#x17e;uje si odstup" ID="ID_347846947" CREATED="1368360191944" MODIFIED="1368360199686">
<node TEXT="tam m&#xe1; schovan&#xe9; smutky" ID="ID_174912048" CREATED="1368360201757" MODIFIED="1368360236054"/>
<node TEXT="a svoje tajnosti" ID="ID_1652863194" CREATED="1368360238476" MODIFIED="1368360249670"/>
</node>
<node TEXT="pocit osam&#x11b;losti" ID="ID_322241238" CREATED="1368360453624" MODIFIED="1368360458252">
<node TEXT="neschpnost nav&#xe1;zat bl&#xed;zk&#xfd; hlub&#x161;&#xed; vztah" ID="ID_728825738" CREATED="1368360474421" MODIFIED="1368360487879"/>
</node>
<node TEXT="pocit, &#x17e;e to nem&#x16f;&#x17e;e nikomu &#x159;&#xed;ct" ID="ID_298501687" CREATED="1368360709890" MODIFIED="1368360722606">
<node TEXT="hlavn&#x11b; ne bl&#xed;zk&#xfd;m" ID="ID_1419768757" CREATED="1368360724773" MODIFIED="1368360736395"/>
</node>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="right" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<node TEXT="horkokrevn&#xed;" ID="ID_814620193" CREATED="1368360918955" MODIFIED="1368360922449"/>
<node TEXT="huben&#xed;" ID="ID_155670248" CREATED="1368360922778" MODIFIED="1368360925840"/>
<node TEXT="r&#xe1;di alkohol" ID="ID_599709666" CREATED="1368360926192" MODIFIED="1368360930356"/>
<node TEXT="pdobn&#xe9; jako u Fluoru" ID="ID_1154180229" CREATED="1368360930726" MODIFIED="1368360941913"/>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="right" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729"/>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="right" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690"/>
</node>
</map>
