<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Psorinum" ID="ID_625279936" CREATED="1336810515345" MODIFIED="1336813200726" COLOR="#cc0000" STYLE="fork" LINK="../../2.%20rocnik/2r-9.mm">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<hook NAME="MapStyle" max_node_width="600"/>
<node TEXT="svrab" POSITION="left" ID="ID_1736396532" CREATED="1336810520636" MODIFIED="1336814115868" COLOR="#999900">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="PSOR" POSITION="left" ID="ID_1556202469" CREATED="1336814117896" MODIFIED="1336814142173" COLOR="#996600">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Miazma" POSITION="left" ID="ID_1038648707" CREATED="1336816042617" MODIFIED="1336816052637" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="psorick&#xe9;" ID="ID_1556653803" CREATED="1336816053782" MODIFIED="1336816071222"/>
<node TEXT="nozoda" ID="ID_1608855736" CREATED="1336813207740" MODIFIED="1336814108381" COLOR="#659919">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
</node>
</node>
<node TEXT="T&#xe9;mata" POSITION="left" ID="ID_1965879541" CREATED="1336813210864" MODIFIED="1336813217992" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="archetyp bezdomovce" ID="ID_1130161631" CREATED="1336813234211" MODIFIED="1336813242274"/>
<node TEXT="&#x161;et&#x159;ivost" ID="ID_1085901901" CREATED="1336813242971" MODIFIED="1336813249418"/>
<node TEXT="obavy" ID="ID_360528639" CREATED="1336814774143" MODIFIED="1336817012948">
<font BOLD="true"/>
<node TEXT="co si po&#x10d;nu" ID="ID_1660115096" CREATED="1336814777541" MODIFIED="1336814781131"/>
<node TEXT="o z&#xe1;kladn&#xed; &#x17e;ivotn&#xed; pot&#x159;eby" ID="ID_1996468812" CREATED="1336814818130" MODIFIED="1336814829688">
<node TEXT="teplota" ID="ID_1239590121" CREATED="1336814856273" MODIFIED="1336814859848"/>
<node TEXT="j&#xed;dlo" ID="ID_1848987232" CREATED="1336814860305" MODIFIED="1336814871824"/>
<node TEXT="bydlen&#xed;" ID="ID_273407943" CREATED="1336814872464" MODIFIED="1336814875488"/>
<node TEXT="zaji&#x161;t&#x11b;n&#xed;" ID="ID_1115851444" CREATED="1336814875825" MODIFIED="1336814879948"/>
</node>
<node TEXT="nem&#x16f;&#x17e;e ovlivnit co se mu v &#x17e;ivot&#x11b; d&#x11b;je" ID="ID_947673478" CREATED="1336815679399" MODIFIED="1336815694797"/>
<node TEXT="nem&#xe1;m schopnost, zvl&#xe1;dnout z&#xe1;kladn&#xed; &#x17e;ivot" ID="ID_875669621" CREATED="1336815543015" MODIFIED="1336815711990"/>
</node>
<node TEXT="pochybnosti, &#xfa;zkosti" ID="ID_734090103" CREATED="1336816813442" MODIFIED="1336816821433"/>
<node TEXT="rigidita" ID="ID_703382611" CREATED="1336817024847" MODIFIED="1336817028304">
<node TEXT="trv&#xe1; na sv&#xe9; bezmocnosti" ID="ID_1703358785" CREATED="1336817069673" MODIFIED="1336817081515"/>
</node>
<node TEXT="zimoum&#x159;ivost" ID="ID_775997929" CREATED="1336814887262" MODIFIED="1336814893884">
<font BOLD="true"/>
</node>
<node TEXT="j&#xed;dlo" ID="ID_1911493665" CREATED="1336814894595" MODIFIED="1336814902376">
<node TEXT="&#x17e;ravost" ID="ID_734918464" CREATED="1336814904583" MODIFIED="1336814907845"/>
<node TEXT="m&#xe1; rychle hlad" ID="ID_1368066837" CREATED="1336814908230" MODIFIED="1336814912622"/>
</node>
<node TEXT="zaji&#x161;t&#x11b;n&#xed;" ID="ID_787642739" CREATED="1336814922578" MODIFIED="1336814928187">
<node TEXT="pen&#xed;ze" ID="ID_1543135912" CREATED="1336814930290" MODIFIED="1336814933292">
<node TEXT="pocit nedostatku" ID="ID_1274607885" CREATED="1336817270559" MODIFIED="1336817275761"/>
<node TEXT="pocit neschopnosti je vyd&#x11b;lat" ID="ID_305771956" CREATED="1336817276129" MODIFIED="1336817290217"/>
</node>
<node TEXT="spo&#x159;ivost" ID="ID_100587666" CREATED="1336814934536" MODIFIED="1336814938662">
<node TEXT="je to &#x161;koda to vyhodit" ID="ID_444410699" CREATED="1336814942718" MODIFIED="1336814949178"/>
</node>
<node TEXT="askeze" ID="ID_1508726626" CREATED="1336815352152" MODIFIED="1336815355825"/>
</node>
<node TEXT="strach o svoji schopnost vy&#x159;e&#x161;it situaci dob&#x159;e" ID="ID_1407981606" CREATED="1336813353179" MODIFIED="1336814793844">
<font BOLD="true"/>
</node>
<node TEXT="nez&#xe1;le&#x17e;&#xed; mu moc na sob&#x11b;" ID="ID_84118100" CREATED="1336815388662" MODIFIED="1336815403588"/>
<node TEXT="pozice tot&#xe1;ln&#xed; ob&#x11b;ti" ID="ID_506783184" CREATED="1336815607670" MODIFIED="1336815613968"/>
<node TEXT="p&#x159;ecitliv&#x11b;lost na vn&#x11b;j&#x161;&#xed; vlivy fyzick&#xe9;ho charakteru" ID="ID_1668546492" CREATED="1336815865458" MODIFIED="1336815882406"/>
<node TEXT="um&#xed;r&#xe1; na nedostatek uprost&#x159;ed hojnosti" ID="ID_46297123" CREATED="1336816520786" MODIFIED="1336816542116" COLOR="#659919">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="true"/>
<node TEXT="odd&#x11b;len&#xed; od Boha" ID="ID_467985721" CREATED="1336816543386" MODIFIED="1336816550361"/>
<node TEXT="neum&#xed; si to vz&#xed;t" ID="ID_684245839" CREATED="1336816550626" MODIFIED="1336816555051"/>
</node>
<node TEXT="n&#xed;zk&#xe9; sebev&#x11b;dom&#xed;" ID="ID_1525341563" CREATED="1336816981567" MODIFIED="1336816995626">
<font BOLD="true"/>
</node>
<node TEXT="m&#x11b; to tak vlastn&#x11b; sta&#x10d;&#xed;" ID="ID_1745020227" CREATED="1336817520969" MODIFIED="1336817537141">
<font BOLD="true"/>
</node>
<node TEXT="m&#x16f;&#x17e;e b&#xfd;t stav &#x17e;eny po rozvodu" ID="ID_1000801390" CREATED="1336817827934" MODIFIED="1336817883086"/>
<node TEXT="nec&#xed;t&#xed; s&#xed;lu vlastn&#xed;ho ega" ID="ID_1663318658" CREATED="1336818222135" MODIFIED="1336818230388">
<node TEXT="1. &#x10d;akra" ID="ID_1035467639" CREATED="1336818233585" MODIFIED="1336818237650"/>
</node>
</node>
<node TEXT="Video" POSITION="left" ID="ID_824703166" CREATED="1336813424109" MODIFIED="1336813429166" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="v &#x10d;ervnu p&#x159;&#xed;sel v kab&#xe1;tu a ve svetru" ID="ID_1835847570" CREATED="1336813430378" MODIFIED="1336813455090"/>
<node TEXT="tich&#xfd; hlas" ID="ID_40940775" CREATED="1336813455523" MODIFIED="1336813461704"/>
<node TEXT="obavy" ID="ID_1003368801" CREATED="1336813462313" MODIFIED="1336813659368">
<node TEXT="co kdy&#x17e; se protrhne p&#x159;ehrada" ID="ID_997302180" CREATED="1336813502832" MODIFIED="1336813518769">
<node TEXT="co si ssebou vezmu" ID="ID_705068510" CREATED="1336813627918" MODIFIED="1336813635269"/>
</node>
<node TEXT="co kdy&#x17e; se mi pokaz&#xed; auto" ID="ID_813250864" CREATED="1336813519170" MODIFIED="1336813535794"/>
<node TEXT="nem&#x16f;&#x17e;u j&#xed;t ven jen tak bez ni&#x10d;eho" ID="ID_410008759" CREATED="1336813920539" MODIFIED="1336813939774">
<node TEXT="jehla" ID="ID_1165814256" CREATED="1336814485464" MODIFIED="1336814488640"/>
<node TEXT="nit" ID="ID_1257868401" CREATED="1336814489119" MODIFIED="1336814490594"/>
<node TEXT="n&#x16f;&#x17e;ky" ID="ID_1069259469" CREATED="1336814490874" MODIFIED="1336814523466"/>
<node TEXT="sada kapesn&#xed;k&#x16f;" ID="ID_469171528" CREATED="1336814523803" MODIFIED="1336814528834"/>
</node>
<node TEXT="co kdy&#x17e; zmoknu" ID="ID_1307759869" CREATED="1336815969447" MODIFIED="1336815977554"/>
<node TEXT="chce b&#xfd;t p&#x159;ipravn&#xfd; na v&#x161;echno" ID="ID_1605630304" CREATED="1336813536674" MODIFIED="1336813548416"/>
<node TEXT="mus&#xed;m si sehnat n&#x11b;koho, kdo by m&#x11b; odt&#xe1;hnul, kdyby n&#x11b;co" ID="ID_1498272029" CREATED="1336813665172" MODIFIED="1336813707815"/>
<node TEXT="je pro m&#x11b; nejd&#x16f;le&#x17e;it&#x11b;&#x161;&#xed; abych m&#x11b;l rezervu, kdyby se n&#x11b;co stalo" ID="ID_180469046" CREATED="1336813708359" MODIFIED="1336813752611"/>
<node TEXT="cht&#x11b;l bych bydlet na hrad&#x11b;" ID="ID_259563440" CREATED="1336813752931" MODIFIED="1336813798820">
<node TEXT="vodn&#xed; p&#x159;&#xed;kopy" ID="ID_1356368598" CREATED="1336813800117" MODIFIED="1336813817959"/>
<node TEXT="minov&#xe9; pole" ID="ID_481640193" CREATED="1336813837220" MODIFIED="1336813842773"/>
<node TEXT="hradby" ID="ID_1502296069" CREATED="1336813843222" MODIFIED="1336813845905"/>
</node>
<node TEXT="nechci moc vyd&#x11b;l&#xe1;vat, aby m&#x11b; neunesli" ID="ID_657640479" CREATED="1336813847110" MODIFIED="1336813862707"/>
</node>
<node TEXT="zimoum&#x159;iv&#xfd;" ID="ID_1187671459" CREATED="1336814061043" MODIFIED="1336814072426">
<node TEXT="v&#x17e;dycky m&#xe1;m o jednu vrstvu v&#xed;c ne&#x17e; ostatn&#xed;" ID="ID_788812075" CREATED="1336814032120" MODIFIED="1336814050466"/>
</node>
<node TEXT="pozn&#xe1;mky na pou&#x17e;it&#xe9; ob&#xe1;lce z pr&#xe1;ce" ID="ID_775793756" CREATED="1336814317316" MODIFIED="1336814338579"/>
</node>
<node TEXT="Podobn&#xe9; l&#xe9;ky" POSITION="right" ID="ID_1780529194" CREATED="1336813251557" MODIFIED="1336813259895" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="ARS" ID="ID_1567744150" CREATED="1336813261187" MODIFIED="1336813264113">
<node TEXT="ego" ID="ID_583999985" CREATED="1336813266791" MODIFIED="1336813275263">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="chce prosadit sebe" ID="ID_1028676610" CREATED="1336813275749" MODIFIED="1336813284359">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="elegantn&#x11b; oble&#x10d;en&#xfd;" ID="ID_1079374516" CREATED="1336813285013" MODIFIED="1336813309445">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="&#x161;et&#x159;&#xed;lek" ID="ID_1223702121" CREATED="1336813310642" MODIFIED="1336813377165">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="SUPLH" ID="ID_290908614" CREATED="1336816335498" MODIFIED="1336816339092">
<node TEXT="ego" ID="ID_718869033" CREATED="1336816341658" MODIFIED="1336816344969">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="uk&#xe1;&#x17e;u v&#xe1;m jak se to m&#xe1; d&#x11b;lat" ID="ID_1451304695" CREATED="1336816345830" MODIFIED="1336816362364">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="boha nepot&#x159;ebuji" ID="ID_1756008257" CREATED="1336816454326" MODIFIED="1336816465074">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="spo&#x159;ivost" ID="ID_723346281" CREATED="1336816365349" MODIFIED="1336816370207">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="nedb&#xe1; na sebe" ID="ID_366979338" CREATED="1336816370884" MODIFIED="1336816389352">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node TEXT="Viz" POSITION="right" ID="ID_374970205" CREATED="1336815768605" MODIFIED="1336815913400" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Homeopatick&#xe1; miazmata" ID="ID_719303877" CREATED="1336815774822" MODIFIED="1336815921958">
<node TEXT="Ian Watson" ID="ID_1090396801" CREATED="1336815784338" MODIFIED="1336815788837"/>
</node>
</node>
<node TEXT="Trituace" POSITION="right" ID="ID_1828638585" CREATED="1336814080491" MODIFIED="1336814086883" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="C12" ID="ID_241066480" CREATED="1336814689214" MODIFIED="1336814691413"/>
<node TEXT="tlak na hrudi" ID="ID_1824874749" CREATED="1336814538976" MODIFIED="1336814547466">
<font ITALIC="true"/>
</node>
<node TEXT="&#x161;patn&#x11b; se mi d&#xfd;ch&#xe1;" ID="ID_324789695" CREATED="1336814547848" MODIFIED="1336814562567">
<font ITALIC="true"/>
</node>
<node TEXT="t&#xed;ha" ID="ID_927715323" CREATED="1336814562957" MODIFIED="1336814571971">
<font ITALIC="true"/>
</node>
<node TEXT="z&#xfa;&#x17e;il se mi prostor" ID="ID_1248791904" CREATED="1336815195525" MODIFIED="1336815201817"/>
<node TEXT="&#xfa;zkost" ID="ID_1278925012" CREATED="1336815202217" MODIFIED="1336815205307"/>
<node TEXT="zatuhl&#xe9; svaly kolem p&#xe1;te&#x159;e" ID="ID_1865072236" CREATED="1336815223392" MODIFIED="1336815287315"/>
<node TEXT="bolest zad" ID="ID_1815243516" CREATED="1336815289089" MODIFIED="1336815293283"/>
<node TEXT="sv&#x11b;d&#x11b;n&#xed;" ID="ID_778169506" CREATED="1336815293490" MODIFIED="1336815296427"/>
</node>
<node TEXT="Symptomy" POSITION="right" ID="ID_1791951006" CREATED="1336817550193" MODIFIED="1336817556036" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="k&#x16f;&#x17e;e" ID="ID_920155295" CREATED="1336817556998" MODIFIED="1336817574290">
<node TEXT="ekz&#xe9;my" ID="ID_904330567" CREATED="1336818634065" MODIFIED="1336818637735"/>
<node TEXT="vyr&#xe1;&#x17e;ky" ID="ID_1863940818" CREATED="1336818638232" MODIFIED="1336818643158"/>
<node TEXT="herpes" ID="ID_676958727" CREATED="1336818643398" MODIFIED="1336818645937"/>
<node TEXT="lupy" ID="ID_1827521162" CREATED="1336818646298" MODIFIED="1336818648457"/>
<node TEXT="..." ID="ID_716862106" CREATED="1336818648650" MODIFIED="1336818652480"/>
<node TEXT="such&#xe9;" ID="ID_510183753" CREATED="1336818655630" MODIFIED="1336818660192"/>
<node TEXT="sv&#x11b;div&#xe9;" ID="ID_871678141" CREATED="1336818660960" MODIFIED="1336818664096"/>
<node TEXT="zhor&#x161; v zim&#x11b;" ID="ID_44055747" CREATED="1336818664377" MODIFIED="1336818678826"/>
<node TEXT="vypad&#xe1; &#x161;pinav&#xe1;" ID="ID_1970056568" CREATED="1336818698078" MODIFIED="1336818704659"/>
<node TEXT="akn&#xe9;" ID="ID_1127462541" CREATED="1336818804042" MODIFIED="1336818806074"/>
</node>
<node TEXT="d&#xfd;chac&#xed; syt&#xe9;m" ID="ID_1367903077" CREATED="1336818502072" MODIFIED="1336818508001">
<node TEXT="asthma" ID="ID_1028147244" CREATED="1336818875715" MODIFIED="1336818877477"/>
<node TEXT="suh&#xfd; ka&#x161;el" ID="ID_1897504934" CREATED="1336818877837" MODIFIED="1336818883386">
<node TEXT="v zime" ID="ID_1759321268" CREATED="1336818884658" MODIFIED="1336818886046"/>
</node>
</node>
<node TEXT="u&#x161;i" ID="ID_1937983961" CREATED="1336818508376" MODIFIED="1336818510842">
<node TEXT="v&#xfd;toky z u&#x161;&#xed;" ID="ID_707528799" CREATED="1336818852225" MODIFIED="1336818857611"/>
<node TEXT="strupy za u&#x161;ima" ID="ID_867359464" CREATED="1336818859121" MODIFIED="1336818869415"/>
</node>
<node TEXT="nedostatek vit&#xe1;ln&#xed; reakce" ID="ID_1859819479" CREATED="1336818511154" MODIFIED="1336818572551">
<node TEXT="slabost" ID="ID_451183964" CREATED="1336818536603" MODIFIED="1336818539483"/>
<node TEXT="vys&#xed;lenost" ID="ID_48969447" CREATED="1336818539708" MODIFIED="1336818542574"/>
<node TEXT="dlouh&#xe1; rekonvalescence" ID="ID_1616916196" CREATED="1336818542783" MODIFIED="1336818555947"/>
<node TEXT="lehce se zran&#xed;" ID="ID_996249085" CREATED="1336818577734" MODIFIED="1336818581536"/>
<node TEXT="bled&#xed;" ID="ID_1563088454" CREATED="1336818581832" MODIFIED="1336818584961"/>
<node TEXT="k&#x159;ehc&#xed;" ID="ID_43933846" CREATED="1336818585529" MODIFIED="1336818588060"/>
<node TEXT="nezdrav&#xfd; vzhled" ID="ID_1311623367" CREATED="1336818588365" MODIFIED="1336818593889"/>
</node>
<node TEXT="zimoum&#x159;ivost" ID="ID_1066379330" CREATED="1336818605487" MODIFIED="1336818610874"/>
<node TEXT="bolesti hlavy" ID="ID_1423500067" CREATED="1336818757383" MODIFIED="1336818764717">
<node TEXT="zlep&#x161; j&#xed;dlem" ID="ID_93559956" CREATED="1336818765733" MODIFIED="1336818769761"/>
<node TEXT="v noci" ID="ID_1645305461" CREATED="1336818769994" MODIFIED="1336818771943"/>
</node>
<node TEXT="z&#xe1;pach" ID="ID_1339754056" CREATED="1336818730168" MODIFIED="1336818734225">
<node TEXT="v&#xfd;m&#x11b;&#x161;ky" ID="ID_809741280" CREATED="1336818735146" MODIFIED="1336818740054"/>
<node TEXT="dech" ID="ID_984160191" CREATED="1336818740487" MODIFIED="1336818742055"/>
<node TEXT="stolice" ID="ID_326401114" CREATED="1336818742368" MODIFIED="1336818744797"/>
<node TEXT="pot" ID="ID_672228174" CREATED="1336818745102" MODIFIED="1336818746871"/>
<node TEXT="hnis" ID="ID_332794440" CREATED="1336818747168" MODIFIED="1336818749241"/>
</node>
<node TEXT="neust&#xe1;l&#xe1; chu&#x165; k j&#xed;dlu" ID="ID_407746015" CREATED="1336818779386" MODIFIED="1336818787194">
<node TEXT="v noci" ID="ID_1937207926" CREATED="1336818790817" MODIFIED="1336818792804"/>
</node>
<node TEXT="sen&#xe1; r&#xfd;ma" ID="ID_1425612430" CREATED="1336818794041" MODIFIED="1336818799202"/>
<node TEXT="no&#x10d;n&#xed; pocen&#xed;" ID="ID_1386089099" CREATED="1336818889624" MODIFIED="1336818893661"/>
<node TEXT="stolice" ID="ID_175613629" CREATED="1336818811497" MODIFIED="1336818813637">
<node TEXT="pr&#x16f;jmy" ID="ID_1354229984" CREATED="1336818816025" MODIFIED="1336818820552">
<node TEXT="z&#xe1;pach" ID="ID_880757037" CREATED="1336818828085" MODIFIED="1336818830064"/>
</node>
<node TEXT="nejde vytla&#x10d;it" ID="ID_1635241196" CREATED="1336818831006" MODIFIED="1336818839608">
<node TEXT="p&#x159;esto &#x17e;e nen&#xed; tvrd&#xe1;" ID="ID_1547819380" CREATED="1336818840376" MODIFIED="1336818846713"/>
</node>
</node>
</node>
<node TEXT="Modality" POSITION="right" ID="ID_183411122" CREATED="1336815846145" MODIFIED="1336815850658" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="velk&#xe9; mno&#x17e;sv&#xed;" ID="ID_957812854" CREATED="1336815851792" MODIFIED="1336815857274"/>
<node TEXT="" ID="ID_107803202" CREATED="1336818612023" MODIFIED="1336818618205" COLOR="#000000">
<icon BUILTIN="smily_bad"/>
<node TEXT="chladem v jak&#xe9;koliv podob&#x11b;" ID="ID_93790590" CREATED="1336818619956" MODIFIED="1336818628501"/>
<node TEXT="studen&#xfd; &#x10d;erstv&#xfd; vzduch" ID="ID_1123350800" CREATED="1336818896396" MODIFIED="1336818904756"/>
<node TEXT="potla&#x10d;en&#xed; pocen&#xed;" ID="ID_807362721" CREATED="1336818905093" MODIFIED="1336818910008"/>
<node TEXT="v noci" ID="ID_822426157" CREATED="1336818914914" MODIFIED="1336818917637"/>
<node TEXT="p&#x159;i bou&#x159;ce" ID="ID_1571095455" CREATED="1336818917862" MODIFIED="1336818921926"/>
<node TEXT="v sed&#x11b;" ID="ID_857092120" CREATED="1336818922439" MODIFIED="1336818927599"/>
</node>
<node TEXT="" ID="ID_686743067" CREATED="1336818929187" MODIFIED="1336818931164" COLOR="#000000">
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="ksmiletris"/>
<node TEXT="l&#x11b;to" ID="ID_430035956" CREATED="1336818933078" MODIFIED="1336818936035"/>
<node TEXT="leh" ID="ID_366867283" CREATED="1336818937538" MODIFIED="1336818939021"/>
<node TEXT="pocen&#xed;" ID="ID_49619317" CREATED="1336818939318" MODIFIED="1336818941277"/>
</node>
</node>
<node TEXT="Rubriky" POSITION="right" ID="ID_1139228192" CREATED="1336818530232" MODIFIED="1336818533996" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</node>
</node>
</map>
