<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Green" ID="ID_835072547" CREATED="1341160126809" MODIFIED="1341160481394" LINK="../../Letn&#xed;%20&#x161;kola%202012%20-%20Podmitrov/L&#xe9;&#x10d;en&#xed;%20pomoc&#xed;%20barev.mm" COLOR="#cc0000" STYLE="fork">
<hook NAME="MapStyle" max_node_width="600"/>
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Trituace" POSITION="left" ID="ID_548169966" CREATED="1341160135341" MODIFIED="1341160485551" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="horko na srdci" ID="ID_1952746289" CREATED="1341160139632" MODIFIED="1341160157811">
<font ITALIC="true"/>
</node>
<node TEXT="pocit hloubky" ID="ID_506841113" CREATED="1341160147121" MODIFIED="1341160159106">
<font ITALIC="true"/>
</node>
<node TEXT="m&#xed;r" ID="ID_1611020847" CREATED="1341160164224" MODIFIED="1341160175617">
<font ITALIC="true"/>
</node>
<node TEXT="smutno" ID="ID_1602056179" CREATED="1341160620144" MODIFIED="1341160623082"/>
<node TEXT="cht&#x11b;l jsem vysko&#x10d;it a j&#xed;t do lesa" ID="ID_117240393" CREATED="1341160623810" MODIFIED="1341160722628"/>
<node TEXT="p&#x159;ijet&#xed;" ID="ID_653968624" CREATED="1341160744828" MODIFIED="1341160748603"/>
<node TEXT="sv&#x11b;&#x17e;est" ID="ID_1226846727" CREATED="1341160765507" MODIFIED="1341160771470"/>
</node>
<node TEXT="Info" POSITION="left" ID="ID_774307705" CREATED="1341160778370" MODIFIED="1341160973492" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="520-560nm" ID="ID_1012579783" CREATED="1341160820621" MODIFIED="1341160828221"/>
<node TEXT="t&#xf3;n" ID="ID_1184353564" CREATED="1341160834317" MODIFIED="1341160836991">
<node TEXT="F" ID="ID_1615058261" CREATED="1341160839934" MODIFIED="1341160841025"/>
</node>
<node TEXT="&#x10d;akra" ID="ID_1954167094" CREATED="1341160842151" MODIFIED="1341160845459">
<node TEXT="srdce" ID="ID_1946548247" CREATED="1341160846364" MODIFIED="1341160848045"/>
</node>
<node TEXT="planeta" ID="ID_1607269825" CREATED="1341160849074" MODIFIED="1341160851252">
<node TEXT="Venu&#x161;e" ID="ID_1437403675" CREATED="1341160852148" MODIFIED="1341160875942"/>
</node>
<node TEXT="miazma" ID="ID_1842163908" CREATED="1341160856180" MODIFIED="1341160858199">
<node TEXT="tuberkul&#xf3;zn&#xed;" ID="ID_1599564188" CREATED="1341160859008" MODIFIED="1341160865246"/>
</node>
<node TEXT="&#x17e;l&#xe1;za" ID="ID_1854430927" CREATED="1341161886438" MODIFIED="1341161889897">
<node TEXT="brzl&#xed;k" ID="ID_449338389" CREATED="1341161890818" MODIFIED="1341161893596"/>
</node>
<node TEXT="archetyp" ID="ID_1994031844" CREATED="1341161894929" MODIFIED="1341161898798">
<node TEXT="milenka" ID="ID_268357475" CREATED="1341161899807" MODIFIED="1341161901496"/>
</node>
<node TEXT="nejneutr&#xe1;ln&#x11b;j&#x161;&#xed; barva ze v&#x161;ech" ID="ID_965689815" CREATED="1341160902564" MODIFIED="1341160914736"/>
<node TEXT="pro o&#x10d;i nejp&#x159;&#xed;jemn&#x11b;j&#x161;&#xed; barva" ID="ID_1110876419" CREATED="1341160917973" MODIFIED="1341160933647"/>
<node TEXT="nedostatek zelen&#xe9; produkuje neklid a tou&#x17e;en&#xed;" ID="ID_1687104919" CREATED="1341160941139" MODIFIED="1341160958378"/>
<node TEXT="barva rozkladu" ID="ID_1012414301" CREATED="1341160982039" MODIFIED="1341160987253">
<node TEXT="rostliny za zelen&#xfd;m filtrem usychaj&#xed; a hnij&#xed;" ID="ID_1787217114" CREATED="1341160998926" MODIFIED="1341161021642"/>
</node>
<node TEXT="l&#xe9;k p&#x159;in&#xe1;&#x161;&#xed;" ID="ID_1444383493" CREATED="1341161099289" MODIFIED="1341161106640">
<node TEXT="klid" ID="ID_907945742" CREATED="1341161088363" MODIFIED="1341161089843"/>
<node TEXT="l&#xe9;&#x10d;&#xed; zlomen&#xe9; srdce" ID="ID_534547593" CREATED="1341161026658" MODIFIED="1341161032903"/>
<node TEXT="uvol&#x148;uje nap&#x11b;t&#xed;" ID="ID_1867434225" CREATED="1341161036243" MODIFIED="1341161048096"/>
<node TEXT="vn&#xe1;&#x161;&#xed; do &#x17e;ivota stabilitu" ID="ID_509948540" CREATED="1341161048513" MODIFIED="1341161058339"/>
<node TEXT="m&#xed;r radost" ID="ID_611168810" CREATED="1341161059282" MODIFIED="1341161063595"/>
<node TEXT="jednota" ID="ID_628400806" CREATED="1341161065625" MODIFIED="1341161067616"/>
<node TEXT="&#x10d;istota" ID="ID_26972348" CREATED="1341161067868" MODIFIED="1341161080100"/>
<node TEXT="vyrovnanost" ID="ID_1504659124" CREATED="1341161080828" MODIFIED="1341161084579"/>
<node TEXT="uvoln&#x11b;n&#xed;" ID="ID_1729828102" CREATED="1341161085403" MODIFIED="1341161088018"/>
</node>
<node TEXT="srdce je st&#x159;ed t&#x11b;la" ID="ID_1768799333" CREATED="1341161188284" MODIFIED="1341161195119">
<node TEXT="srde&#x10d;n&#xed; &#x10d;akra je st&#x159;ed na&#x161;eho energetick&#xe9;ho t&#x11b;la" ID="ID_1464404107" CREATED="1341161199970" MODIFIED="1341161224255">
<node TEXT="l&#xe1;ska" ID="ID_691306400" CREATED="1341161235091" MODIFIED="1341161237407"/>
</node>
</node>
</node>
<node TEXT="T&#xe9;mata" POSITION="left" ID="ID_1408850585" CREATED="1341160774841" MODIFIED="1341160976131" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="vyrovnan&#xe1;" ID="ID_1373129750" CREATED="1341161128168" MODIFIED="1341161137200">
<node TEXT="dok&#xe1;&#x17e;e l&#xe1;sku p&#x159;ij&#xed;mat i d&#xe1;vat" ID="ID_1044261919" CREATED="1341161138185" MODIFIED="1341161152707"/>
<node TEXT="dok&#xe1;&#x17e;e si uchr&#xe1;nit svoje srdce" ID="ID_1631960290" CREATED="1341161155376" MODIFIED="1341161165559"/>
</node>
<node TEXT="oslaben&#xe1;" ID="ID_1115742277" CREATED="1341161258233" MODIFIED="1341161262038">
<node TEXT="br&#xe1;n&#xed; se zm&#x11b;n&#xe1;m" ID="ID_596514685" CREATED="1341161262943" MODIFIED="1341161271614"/>
<node TEXT="nevyrovnan&#xfd;" ID="ID_1284993814" CREATED="1341161272030" MODIFIED="1341161276516"/>
<node TEXT="neklidn&#xfd;" ID="ID_1226741632" CREATED="1341161276837" MODIFIED="1341161280027"/>
<node TEXT="netrp&#x11b;liv&#xfd;" ID="ID_1351651036" CREATED="1341161280364" MODIFIED="1341161284468"/>
<node TEXT="nedok&#xe1;&#x17e;e post&#xe1;t" ID="ID_926747969" CREATED="1341161284797" MODIFIED="1341161292672"/>
<node TEXT="z&#xe1;vistiv&#xfd;" ID="ID_11350459" CREATED="1341161306021" MODIFIED="1341161310176"/>
<node TEXT="&#x17e;&#xe1;rliv&#xfd;" ID="ID_839750975" CREATED="1341161310936" MODIFIED="1341161315901">
<node TEXT="opak l&#xe1;sky" ID="ID_1554958433" CREATED="1341161317006" MODIFIED="1341161320023"/>
</node>
</node>
</node>
<node TEXT="Gree-aw" POSITION="right" ID="ID_247306029" CREATED="1341160487588" MODIFIED="1341160587777" COLOR="#996600">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Souvisej&#xed;c&#xed; l&#xe9;ky" POSITION="right" ID="ID_653048610" CREATED="1341160781399" MODIFIED="1341161353154" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Thuj" ID="ID_1133816270" CREATED="1341161771149" MODIFIED="1341161774535"/>
<node TEXT="Cacti" ID="ID_38114251" CREATED="1341161776078" MODIFIED="1341161778202"/>
<node TEXT="China" ID="ID_58211461" CREATED="1341161780208" MODIFIED="1341161781772"/>
<node TEXT="Cupr" ID="ID_227623347" CREATED="1341161785709" MODIFIED="1341161787427"/>
<node TEXT="Chrom" ID="ID_1889964461" CREATED="1341161788786" MODIFIED="1341161793642"/>
<node TEXT="Aur" ID="ID_1278603864" CREATED="1341161798875" MODIFIED="1341161800164"/>
<node TEXT="Digit" ID="ID_1554631728" CREATED="1341161805238" MODIFIED="1341161807561"/>
<node TEXT="Hydras" ID="ID_502516713" CREATED="1341161812786" MODIFIED="1341161816561"/>
<node TEXT="Anac" ID="ID_1299276720" CREATED="1341161817098" MODIFIED="1341161818680"/>
</node>
<node TEXT="Kontraindikace" POSITION="right" ID="ID_825875998" CREATED="1341160788768" MODIFIED="1341161352783" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Siln&#xe1; MS" ID="ID_834755133" CREATED="1341161596918" MODIFIED="1341161627454">
<node TEXT="detoxikace a odvod&#x148;ov&#xe1;n&#xed; zrychl&#xed; MS" ID="ID_823744009" CREATED="1341161628663" MODIFIED="1341161651568" COLOR="#999999">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="ztr&#xe1;ty tekutin" ID="ID_36449513" CREATED="1341161664983" MODIFIED="1341161670517">
<node TEXT="zvracen&#xed;" ID="ID_1910584475" CREATED="1341161671398" MODIFIED="1341161673932"/>
<node TEXT="pr&#x16f;jmy" ID="ID_865849494" CREATED="1341161674253" MODIFIED="1341161680323"/>
</node>
</node>
<node TEXT="Res" POSITION="right" ID="ID_396313163" CREATED="1341219683809" MODIFIED="1341219686562" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="GREEN:&#xa;This is the color of balance. It is neither hot nor cold, but neutral in the realm of visible color.  It is the most prevalent color in nature and soothes and comforts us when we are tired and weary. It relates to our emotional balance and the healing power of nature. It is the easiest color for the eye to see. The lens of the eye focuses green light almost exactly on the retina. It is a tonic for the eye, which explains why eye shades and sunglasses are in this color. Medieval engravers used green Beryl to contemplate on and relax their eyes after the intensity of their work.&#xa;Green is a natural detoxifier and helps drain the body of toxins. It acts as a diuretic for congestion, particularly to the heart. It is also the color associated with decay. There is duplicity to this color. On the one hand it is the color of life and also the color of decay.  Its dual nature is linked to its ability to fit both the top and bottom part of the visible spectrum.  Do not take this remedy without consulting your doctor if you have a serious medical problem.&#xa; &#xa;The Chakra: The Heart Chakra:  This is the center of the human energy system. Its emotional component, love, is the center of our life. It is concerned with all aspects of love and joy. It works on two levels, just as the color itself does. On one level it is the heart protector which acts as a shield against the negative and hurtful. It relates on a physical level to the Pericardium. On the second level this shield protects the purity and innocence of the human heart from exposure to harsh or unloving experiences. The heart protector provides a shelter for the heart itself and sustains the core of our being from hurt or abuse. Its positive archetype is the Lover, the person who loves life and others unconditionally. Its negative archetype is the Actor or Actress, who loves only through imitation and makes love a conditional experience.&#xa;The qualities of the Heart Chakra are: love, joy, peace, sister and brotherhood, and unity.&#xa; &#xa;Mentals: Green is the color for those who suffer from nervous tension. It is good for those who are highly strung and hysterical. It is has the ability to soothe shattered nerves, and helps create a more balanced nervous system so that clear thinking can occur. It provides a stronger sense of happiness and inner harmony.&#xa;Contraindications: It is not to be given to indolent people who need their energy. It is not indicated for night use because it acts as a diuretic and can keep people up running to the toilet. Do not take this remedy if you have a serious medical condition. Please consult your doctor before taking this remedy.&#xa; &#xa;Physicals: Green has been used successfully to drain excess fluid from the body. Where there is edema, inflammation, or congestion, this color removes toxic fluids that cause stagnation.  It has been used for PMS when women become engorged and retain fluids. It has helped with lumpy breasts and eased pain in the left breast.&#xa;It has been used along with heart remedies, for congestive heart failure to tonify the heart and stop fluid from building up around the heart. The relief is quick but not long lasting.&#xa;It has also been used for tired eyes and headaches where there is congestion. It can be used for inflammation of the breasts, testicles or soft tissue swelling.&#xa;Contraindications: If there has been a serious loss of fluid from diarrhea, vomiting, or kidney failure green is not indicated. It should not be given at night.&#xa;If you have a serious medical condition, please consult your doctor before using this treatment.&#xa; &#xa;General Symptoms:  Green has been used successfully for patients who lack stability and equilibrium in their emotional lives, who barely manage to cope with problems and when their resources are drained they are tired and exhausted. It is good for people undergoing major life changes. It helps to restore emotional balance and provides an opportunity for detachment.&#xa;Contraindications: It should not be given to people who need stimulation as it is more of a tranquiller and sedative than a stimulator. It is not to be given at night.&#xa;Do not take this remedy if you have a serious medical condition. Please consult your doctor." ID="ID_575641737" CREATED="1341219687614" MODIFIED="1341219700859"/>
</node>
<node TEXT="Symptomy" POSITION="left" ID="ID_1610801187" CREATED="1341160794557" MODIFIED="1341161352213" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="nevolnosti" ID="ID_558374780" CREATED="1341161327069" MODIFIED="1341161330893"/>
<node TEXT="podr&#xe1;&#x17e;d&#x11b;n&#xe9; nervy" ID="ID_1510931283" CREATED="1341161331373" MODIFIED="1341161341605"/>
<node TEXT="bolesti u srdce" ID="ID_1962751495" CREATED="1341161377101" MODIFIED="1341161381542">
<node TEXT="angina pektoris" ID="ID_74803738" CREATED="1341161385199" MODIFIED="1341161389285"/>
<node TEXT="arytmie" ID="ID_707568828" CREATED="1341161389566" MODIFIED="1341161392901"/>
<node TEXT="srde&#x10d;n&#xed; ed&#xe9;m" ID="ID_1743647504" CREATED="1341161393158" MODIFIED="1341161397565"/>
</node>
<node TEXT="bolesti p&#xe1;te&#x159;e" ID="ID_1244907847" CREATED="1341161423227" MODIFIED="1341161428690">
<node TEXT="v lumb&#xe1;ln&#xed; oblasti" ID="ID_86084091" CREATED="1341161429610" MODIFIED="1341161435315"/>
</node>
<node TEXT="detoxikace" ID="ID_1086741574" CREATED="1341161436226" MODIFIED="1341161439140">
<node TEXT="hnisy" ID="ID_211512667" CREATED="1341161486538" MODIFIED="1341161492329"/>
<node TEXT="jedy" ID="ID_1252592091" CREATED="1341161493010" MODIFIED="1341161494762"/>
<node TEXT="m&#x11b;stn&#xe1;n&#xed;" ID="ID_182524639" CREATED="1341161495211" MODIFIED="1341161498752"/>
</node>
<node TEXT="otoky" ID="ID_935267451" CREATED="1341161459982" MODIFIED="1341161468154">
<node TEXT="m&#x11b;kk&#xfd;ch tk&#xe1;n&#xed;" ID="ID_186568338" CREATED="1341161468979" MODIFIED="1341161479041">
<node TEXT="odvod&#x148;uje" ID="ID_1048352295" CREATED="1341161480881" MODIFIED="1341161484448"/>
</node>
</node>
<node TEXT="PMS" ID="ID_388535916" CREATED="1341161503131" MODIFIED="1341161504810"/>
<node TEXT="bolesti hlavy" ID="ID_1621502082" CREATED="1341161505340" MODIFIED="1341161520133">
<node TEXT="z p&#x159;epln&#x11b;n&#xed;" ID="ID_80998723" CREATED="1341161510081" MODIFIED="1341161513496"/>
</node>
<node TEXT="cysty v prsou" ID="ID_1940962126" CREATED="1341161522116" MODIFIED="1341161526462">
<node TEXT="lev&#xe9;" ID="ID_463443362" CREATED="1341161527223" MODIFIED="1341161529046"/>
</node>
<node TEXT="vrac&#xed; s&#xed;lu po stresu (z&#xe1;t&#x11b;&#x17e;i)" ID="ID_919663992" CREATED="1341161530827" MODIFIED="1341161550503"/>
<node TEXT="termin&#xe1;ln&#xed; st&#xe1;dia nemoci" ID="ID_430659678" CREATED="1341161553932" MODIFIED="1341161565362"/>
</node>
</node>
</map>
