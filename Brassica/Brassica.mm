<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Brassica" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1398436682660"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Trituace" STYLE_REF="Subtopic" POSITION="left" ID="ID_1275465186" CREATED="1357168572522" MODIFIED="1398807634800" HGAP="-30" VSHIFT="-20">
<node TEXT="pr&#xe1;zdn&#xe1;" ID="ID_796184990" CREATED="1398437070436" MODIFIED="1398437087530"/>
<node TEXT="pou&#x17e;&#xed;t" ID="ID_757079230" CREATED="1398437114865" MODIFIED="1398437121145"/>
<node TEXT="zneu&#x17e;&#xed;t" ID="ID_1055036458" CREATED="1398437121583" MODIFIED="1398437124900"/>
<node TEXT="vybrakovat" ID="ID_1864598057" CREATED="1398437125482" MODIFIED="1398437135217"/>
<node TEXT="monokultura" ID="ID_282231545" CREATED="1398437316324" MODIFIED="1398437320856"/>
<node TEXT="t&#x11b;&#x17e;k&#xe9; nohy" ID="ID_92296625" CREATED="1398437502336" MODIFIED="1398437543534">
<font BOLD="false" ITALIC="true"/>
</node>
<node TEXT="bolav&#xe1; kolena" ID="ID_1666667440" CREATED="1398437512358" MODIFIED="1398437546065">
<font BOLD="false" ITALIC="true"/>
</node>
<node TEXT="kdybych byla kombajn&#xe9;r, tak me drbne" ID="ID_500595153" CREATED="1398439550883" MODIFIED="1398807742901"/>
<node TEXT="Sherr/&#xa;v&#x11b;dma" ID="ID_660810728" CREATED="1398438503124" MODIFIED="1398807662354">
<font BOLD="true"/>
<node TEXT="c&#xed;t&#xed;m, &#x17e;e se podd&#xe1;v&#xe1;m" ID="ID_1700664927" CREATED="1398438506896" MODIFIED="1398438513881"/>
<node TEXT="nem&#xe1;m &#x161;anci n&#x11b;co zm&#x11b;nit" ID="ID_463789222" CREATED="1398438514329" MODIFIED="1398438523390"/>
<node TEXT="jsem bezv&#xfd;znamn&#xe1;" ID="ID_1075729207" CREATED="1398438523632" MODIFIED="1398438532454"/>
<node TEXT="jsem pr&#xe1;zdn&#xe1;" ID="ID_671005758" CREATED="1398438533000" MODIFIED="1398438536850"/>
<node TEXT="v&#x161;e vypad&#xe1; mechanicky" ID="ID_571342226" CREATED="1398438537092" MODIFIED="1398438545259"/>
<node TEXT="v&#x11b;ci jsou ploch&#xe9;" ID="ID_1508725455" CREATED="1398438545519" MODIFIED="1398438550479"/>
<node TEXT="p&#x159;ipad&#xe1;m si oby&#x10d;ejn&#xe1;" ID="ID_1293739463" CREATED="1398438550731" MODIFIED="1398438557034"/>
<node TEXT="nep&#x159;ita&#x17e;liv&#xe1;" ID="ID_1199369543" CREATED="1398438557374" MODIFIED="1398438566327"/>
<node TEXT="neu&#x17e;ite&#x10d;n&#xe1;" ID="ID_1045288784" CREATED="1398438566588" MODIFIED="1398438570982"/>
<node TEXT="jako bych m&#x11b;la vymyt&#xfd; mozek" ID="ID_1937793796" CREATED="1398438571376" MODIFIED="1398438584374"/>
<node TEXT="c&#xed;t&#xed;m se osam&#x11b;le" ID="ID_1960025981" CREATED="1398438611201" MODIFIED="1398438619146"/>
<node TEXT="m&#xe1;m touhu j&#xed;t z m&#x11b;sta" ID="ID_733896008" CREATED="1398438619620" MODIFIED="1398438629349">
<node TEXT="do p&#x159;&#xed;rody" ID="ID_308404308" CREATED="1398438631765" MODIFIED="1398438635898"/>
<node TEXT="do hor" ID="ID_1529246605" CREATED="1398438636176" MODIFIED="1398438638324"/>
<node TEXT="na farmu" ID="ID_441289313" CREATED="1398438638746" MODIFIED="1398438645212"/>
</node>
<node TEXT="jako bych nebyla nikdy milovan&#xe1;,&#xa;musela jsem b&#xfd;t prosp&#x11b;&#x161;n&#xe1;,&#xa;a potom odhozen&#xe1;" ID="ID_276209177" CREATED="1398438646594" MODIFIED="1398807893231"/>
<node TEXT="bezmoc" ID="ID_1922497589" CREATED="1398438679310" MODIFIED="1398438681726"/>
<node TEXT="jako bych &#x17e;ila v totalitn&#xed;m st&#xe1;t&#x11b;" ID="ID_265466478" CREATED="1398438684060" MODIFIED="1398438698650"/>
<node TEXT="jako by mi &#x159;&#xed;kali co si m&#xe1;m myslet" ID="ID_256515430" CREATED="1398438710565" MODIFIED="1398438735167"/>
<node TEXT="ztr&#xe1;c&#xed;m jskoukoliv individualitu" ID="ID_27289158" CREATED="1398438812988" MODIFIED="1398438826852"/>
<node TEXT="m&#xe1;m chu&#x165; &#x159;&#xed;ct, jsem tady!" ID="ID_692819723" CREATED="1398438827318" MODIFIED="1398438895153"/>
<node TEXT="m&#xe1;m strach &#x159;&#xed;ct pravdu, proto&#x17e;e mi vymyjou mozek" ID="ID_1581758543" CREATED="1398438837960" MODIFIED="1398438864093"/>
<node TEXT="monot&#xf3;n&#xed; my&#x161;lenky" ID="ID_233099215" CREATED="1398438865256" MODIFIED="1398438878149"/>
<node TEXT="chci b&#xfd;t n&#x11b;co zvl&#xe1;&#x161;tn&#xed;ho, t&#x159;eba ptakopysk" ID="ID_1917563572" CREATED="1398438898954" MODIFIED="1398438914517"/>
<node TEXT="c&#xed;t&#xed;m se bez radosti" ID="ID_1751619592" CREATED="1398438915081" MODIFIED="1398438925274"/>
<node TEXT="nem&#xe1;m impulzy" ID="ID_918669624" CREATED="1398438925534" MODIFIED="1398438934295"/>
<node TEXT="le&#x17e;&#xed;m doma a nikoho nezaj&#xed;m&#xe1;m" ID="ID_1988525765" CREATED="1398438934582" MODIFIED="1398438945866"/>
<node TEXT="c&#xed;t&#xed;m se jako pt&#xe1;k pokryt&#xfd; ropou" ID="ID_748317725" CREATED="1398438946367" MODIFIED="1398438964966"/>
<node TEXT="probl&#xe9;m se zhluboka nadechnout,&#xa;proto&#x17e;e vzduch je toxick&#xfd;" ID="ID_646982080" CREATED="1398438965191" MODIFIED="1398438986717"/>
</node>
</node>
<node TEXT="" POSITION="left" ID="ID_782779442" CREATED="1398807182686" MODIFIED="1398807432056" HGAP="-70" VSHIFT="70">
<hook URI="166567-original1-azsrp.jpg" SIZE="0.75" NAME="ExternalObject"/>
</node>
<node TEXT="Brassica napus oleifera" STYLE_REF="Remedy" POSITION="right" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1398436934228"/>
<node TEXT="&#x158;epka olejn&#xe1;" STYLE_REF="Remedy" POSITION="right" ID="ID_812257657" CREATED="1398436695039" MODIFIED="1398436730537"/>
<node TEXT="Brass" STYLE_REF="Abbrev" POSITION="right" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1398436867691"/>
<node TEXT="Podobn&#xe9; l&#xe9;ky" STYLE_REF="Subtopic" POSITION="right" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1357168625505"/>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="right" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690"/>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="right" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1357168653304">
<node TEXT="monokultura" ID="ID_4881250" CREATED="1398439598905" MODIFIED="1398439605069">
<font BOLD="true"/>
</node>
<node TEXT="lacin&#xfd;" ID="ID_1976066424" CREATED="1398439610017" MODIFIED="1398439616410"/>
<node TEXT="u&#x17e;ite&#x10d;n&#xfd;" ID="ID_546236809" CREATED="1398439617090" MODIFIED="1398439621786"/>
<node TEXT="mus&#xed;te c&#xed;tit to, co c&#xed;t&#xed; ostatn&#xed;" ID="ID_870862727" CREATED="1398439649996" MODIFIED="1398807778424"/>
<node TEXT="v&#x161;echno je ploch&#xe9;" ID="ID_1271323208" CREATED="1398439662795" MODIFIED="1398439671640"/>
<node TEXT="nevy&#x10d;n&#xed;vat" ID="ID_1437588977" CREATED="1398439672053" MODIFIED="1398807802681"/>
<node TEXT="c&#xed;t&#xed; se n&#xed;zk&#xfd;" ID="ID_1291996104" CREATED="1398439713660" MODIFIED="1398439720678"/>
<node TEXT="n&#x11b;co mu br&#xe1;n&#xed; ve v&#xfd;hledu" ID="ID_1078852643" CREATED="1398439721493" MODIFIED="1398439729907"/>
<node TEXT="touha ud&#x11b;lat n&#x11b;co zvl&#xe1;&#x161;tn&#xed;ho" ID="ID_188436018" CREATED="1398439737566" MODIFIED="1398439749701"/>
<node TEXT="cht&#x11b;j&#xed; b&#xfd;t milov&#xe1;n&#xed; individu&#xe1;ln&#x11b;, ne univerz&#xe1;ln&#x11b;" ID="ID_1264398722" CREATED="1398439750747" MODIFIED="1398439772054"/>
<node TEXT="cht&#x11b;j&#xed; b&#xfd;t chyt&#x159;ej&#x161;&#xed;, v&#x11b;t&#x161;&#xed;, jin&#xed;" ID="ID_1342644983" CREATED="1398439785723" MODIFIED="1398439803219"/>
<node TEXT="tou&#x17e;&#xed;m b&#xfd;t jin&#xe1;" ID="ID_843537303" CREATED="1398439811305" MODIFIED="1398439816403"/>
<node TEXT="pocit vymyt&#xe9;ho mozku" ID="ID_1112738396" CREATED="1398441149122" MODIFIED="1398441162674"/>
<node TEXT="nem&#xe1; s&#xed;lu ani n&#xe1;pad to zm&#x11b;nit" ID="ID_537465680" CREATED="1398441163247" MODIFIED="1398441773573"/>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="right" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1398807402921" VSHIFT="20">
<node TEXT="Mind; abdominal complaints, with" ID="ID_976933431" CREATED="1398805881553" MODIFIED="1398805881553"/>
<node TEXT="Mind; delusions, imaginations; stupid, he is" ID="ID_746239364" CREATED="1398805903151" MODIFIED="1398805903151"/>
<node TEXT="Mind; anticipation; ailments from, agg." ID="ID_643386531" CREATED="1398805934217" MODIFIED="1398805934217"/>
<node TEXT="Mind; anxiety; alone, being" ID="ID_1579021094" CREATED="1398805957838" MODIFIED="1398805957838"/>
<node TEXT="Mind; will; loss of" ID="ID_1316598034" CREATED="1398805981404" MODIFIED="1398805981404"/>
<node TEXT="Mind; anxiety; inspiration, deep; must" ID="ID_648681935" CREATED="1398806024134" MODIFIED="1398806024134"/>
<node TEXT="Mind; anxiety; nausea; with" ID="ID_146191523" CREATED="1398806040581" MODIFIED="1398806040581"/>
<node TEXT="Mind; anxiety; world, for the" ID="ID_978362164" CREATED="1398806053899" MODIFIED="1398806053899"/>
<node TEXT="Mind; approach of a person agg." ID="ID_1441733436" CREATED="1398806106175" MODIFIED="1398806106175"/>
<node TEXT="Mind; climb, desire to" ID="ID_1485285882" CREATED="1398806125965" MODIFIED="1398806125965"/>
<node TEXT="Mind; company; aversion to; seen, does not want to be" ID="ID_374345225" CREATED="1398806149366" MODIFIED="1398806149366"/>
<node TEXT="Mind; company; desire for; spoken to, but averse to being" ID="ID_1112914014" CREATED="1398806179064" MODIFIED="1398806179064"/>
<node TEXT="Mind; complaining; disease, of" ID="ID_1170759510" CREATED="1398806234742" MODIFIED="1398806234742"/>
<node TEXT="Mind; concentration; difficult; attempting to, on; vacant feeling, has a" ID="ID_3549004" CREATED="1398806287507" MODIFIED="1398806287507"/>
<node TEXT="Mind; concentration; difficult; studying, reading, while" ID="ID_831653720" CREATED="1398806297902" MODIFIED="1398806297902"/>
<node TEXT="Mind; country, desire for, to go into the" ID="ID_784680295" CREATED="1398806320409" MODIFIED="1398806320409"/>
<node TEXT="Mind; cut short, nails, desire to" ID="ID_682397745" CREATED="1398806342187" MODIFIED="1398806342187"/>
<node TEXT="Mind; delusions, imaginations; abused, being" ID="ID_1896901161" CREATED="1398806364431" MODIFIED="1398806364431"/>
<node TEXT="Mind; delusions, imaginations; abused, being; used and discarded, being" ID="ID_342967127" CREATED="1398806381753" MODIFIED="1398806381753"/>
<node TEXT="Mind; delusions, imaginations; animals, of; larger, are" ID="ID_11927126" CREATED="1398806402662" MODIFIED="1398806402662"/>
<node TEXT="Mind; delusions, imaginations; appreciated, that she is not" ID="ID_637340644" CREATED="1398806438269" MODIFIED="1398806438269"/>
<node TEXT="Mind; delusions, imaginations; calls; him, someone; wonders how this is possible because name is not known" ID="ID_954982752" CREATED="1398806467547" MODIFIED="1398806467547"/>
<node TEXT="Mind; delusions, imaginations; despised, is" ID="ID_1518394215" CREATED="1398806494298" MODIFIED="1398806494298"/>
<node TEXT="Mind; delusions, imaginations; lower, surrounding objects are, he is much taller" ID="ID_1501345759" CREATED="1398806521284" MODIFIED="1398806521284"/>
<node TEXT="Mind; delusions, imaginations; powerless, is" ID="ID_1203548817" CREATED="1398806532810" MODIFIED="1398806532810"/>
<node TEXT="Mind; delusions, imaginations; robotic, mechanical, feels" ID="ID_1645638084" CREATED="1398806552290" MODIFIED="1398806552290"/>
<node TEXT="Mind; delusions, imaginations; separated; world, from the, that he is" ID="ID_1367437563" CREATED="1398806564585" MODIFIED="1398806564585"/>
<node TEXT="Mind; delusions, imaginations; soul, souls; defeated, is" ID="ID_800254963" CREATED="1398806599950" MODIFIED="1398806599950"/>
<node TEXT="Mind; delusions, imaginations; subversive, obstructive, evasive, people are" ID="ID_1280837623" CREATED="1398806620468" MODIFIED="1398806620468"/>
<node TEXT="Mind; delusions, imaginations; tall, taller; he is" ID="ID_1289979612" CREATED="1398806629927" MODIFIED="1398806629927"/>
<node TEXT="Mind; delusions, imaginations; ugly, is" ID="ID_1252564492" CREATED="1398806641754" MODIFIED="1398806641754"/>
<node TEXT="Mind; delusions, imaginations; useless, is" ID="ID_711576540" CREATED="1398806652994" MODIFIED="1398806652994"/>
<node TEXT="Mind; different, desires to be, bizarre and" ID="ID_1269362084" CREATED="1398806686508" MODIFIED="1398806686508"/>
<node TEXT="Mind; discouraged; resignation" ID="ID_267324589" CREATED="1398806708288" MODIFIED="1398806708288"/>
<node TEXT="Mind; dreams; appealing, attempts to make unappealing things" ID="ID_1015161589" CREATED="1398806743611" MODIFIED="1398806743611"/>
<node TEXT="Mind; dreams; ascending a height; see everyone from the top, could" ID="ID_1051009991" CREATED="1398806764097" MODIFIED="1398806764097"/>
<node TEXT="Mind; dreams; changing; rapidly" ID="ID_1474404379" CREATED="1398806793531" MODIFIED="1398806793531"/>
<node TEXT="Mind; dreams; colorful" ID="ID_79638839" CREATED="1398806802591" MODIFIED="1398806802591"/>
<node TEXT="Mind; dreams; descending; escalators, unable to find way back up" ID="ID_512800432" CREATED="1398806822276" MODIFIED="1398806822276"/>
<node TEXT="Mind; dreams; high places" ID="ID_130909463" CREATED="1398806833821" MODIFIED="1398806833821"/>
<node TEXT="Mind; dreams; identity, of; lost" ID="ID_1328388905" CREATED="1398806848057" MODIFIED="1398806848057"/>
<node TEXT="Mind; dreams; identity, of; lost; confused with others, and" ID="ID_771704666" CREATED="1398806855733" MODIFIED="1398806855733"/>
<node TEXT="Mind; elevated, desires to be, above things blocking her vision" ID="ID_1945559154" CREATED="1398806876626" MODIFIED="1398806876626"/>
<node TEXT="Mind; emptiness of mind, sensation of" ID="ID_578059365" CREATED="1398806899917" MODIFIED="1398806899917"/>
<node TEXT="Mind; emptiness of mind, sensation of; brainwashed, feels" ID="ID_1479749974" CREATED="1398806904591" MODIFIED="1398806904591"/>
<node TEXT="Mind; fear; coition; thought of, in a woman, rape" ID="ID_567651942" CREATED="1398806933229" MODIFIED="1398807492397">
<edge COLOR="#808080"/>
</node>
<node TEXT="Mind; forsaken feeling; friendless, feels" ID="ID_1598589297" CREATED="1398806961404" MODIFIED="1398806961404"/>
<node TEXT="Mind; hunger, fasting agg." ID="ID_6024472" CREATED="1398806991413" MODIFIED="1398806991413"/>
<node TEXT="Mind; ideas; deficiency of" ID="ID_1132062838" CREATED="1398807006081" MODIFIED="1398807006081"/>
<node TEXT="Mind; indifference, apathy; taciturn" ID="ID_1815898174" CREATED="1398807024220" MODIFIED="1398807024220"/>
<node TEXT="Mind; irritability; causeless" ID="ID_1944365253" CREATED="1398807043806" MODIFIED="1398807043806"/>
<node TEXT="Mind; learning; difficult" ID="ID_208227486" CREATED="1398807064476" MODIFIED="1398807064476"/>
<node TEXT="Mind; monotony, overwhelming sensation of" ID="ID_1185467743" CREATED="1398807080700" MODIFIED="1398807080700"/>
<node TEXT="Mind; sadness; situation, unable to change" ID="ID_1272227879" CREATED="1398807108279" MODIFIED="1398807108279"/>
<node TEXT="Mind; thoughts; monotony of" ID="ID_1958680388" CREATED="1398807121753" MODIFIED="1398807121753"/>
<node TEXT="Mind; useless and unattractive, feels" ID="ID_760287412" CREATED="1398807136160" MODIFIED="1398807136160"/>
</node>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="left" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1398807601222" HGAP="-90" VSHIFT="30">
<node TEXT="vy&#x10d;erp&#xe1;v&#xe1; pole" ID="ID_493472184" CREATED="1398436774600" MODIFIED="1398436782281"/>
<node TEXT="pot&#x159;ebuje silnou podporu chemi&#xed;" ID="ID_965936846" CREATED="1398437287964" MODIFIED="1398437311933"/>
<node TEXT="kys. erukov&#xe1;" ID="ID_587166075" CREATED="1398437757761" MODIFIED="1398437778033">
<node TEXT="toxick&#xe1;" ID="ID_582078658" CREATED="1398437782737" MODIFIED="1398437789993"/>
<node TEXT="&#x161;lecht&#x11b;n&#xe1; &#x159;. neobsahuje" ID="ID_342982230" CREATED="1398437790548" MODIFIED="1398437823659"/>
</node>
<node TEXT="u&#x17e;ite&#x10d;n&#xe1;" ID="ID_116533352" CREATED="1398437826614" MODIFIED="1398437834253">
<node TEXT="olej" ID="ID_776310056" CREATED="1398437842492" MODIFIED="1398437845120"/>
<node TEXT="palivo" ID="ID_677908030" CREATED="1398437845568" MODIFIED="1398437848547"/>
<node TEXT="margar&#xed;ny" ID="ID_1866996438" CREATED="1398437849022" MODIFIED="1398437853974"/>
</node>
<node TEXT="levn&#xe1;" ID="ID_73145363" CREATED="1398437834987" MODIFIED="1398437838096"/>
<node TEXT="jedna z hlavn&#xed;ch rostlin pro gen in&#x17e;en&#xfd;rstv&#xed;" ID="ID_370700757" CREATED="1398437862869" MODIFIED="1398437883844"/>
<node TEXT="&#x10d;ele&#x10f;" ID="ID_972742810" CREATED="1398437898920" MODIFIED="1398437903965">
<node TEXT="brukvovit&#xe9;" ID="ID_444201871" CREATED="1398437885230" MODIFIED="1398437895880"/>
</node>
<node TEXT="proving" ID="ID_1698124886" CREATED="1398437908524" MODIFIED="1398437912357">
<node TEXT="Jeremy Sherr" ID="ID_1754211979" CREATED="1398437914532" MODIFIED="1398807985842">
<node TEXT="n&#xe1;ro&#x10d;n&#xfd; proving" ID="ID_1068278155" CREATED="1398437995215" MODIFIED="1398807999239"/>
<node TEXT="m&#x11b;li tendenci&#xa;to zpracovat v&#x11b;decky" ID="ID_983784495" CREATED="1398438021241" MODIFIED="1398807616596">
<node TEXT="odstranili zaj&#xed;mav&#xe9; symptomy" ID="ID_1390799091" CREATED="1398438073605" MODIFIED="1398438091473"/>
<node TEXT="v&#xfd;sledek byl pr&#xe1;zdn&#xfd; na informace" ID="ID_934378518" CREATED="1398438091930" MODIFIED="1398438117242"/>
</node>
<node TEXT="nakonec to bylo k ni&#x10d;emu" ID="ID_188241086" CREATED="1398439105812" MODIFIED="1398439139375">
<node TEXT="proving nakonec d&#x11b;lala n&#x11b;jak&#xe1; v&#x11b;dma" STYLE_REF="Comment" ID="ID_1384314210" CREATED="1398439324685" MODIFIED="1398807625887"/>
</node>
</node>
</node>
<node TEXT="monokultura bun&#x11b;k = rakovina" ID="ID_514124815" CREATED="1398439696156" MODIFIED="1398439708511"/>
<node TEXT="mo&#x17e;n&#xe1; i jako miazmatick&#xfd; l&#xe9;k" ID="ID_761258808" CREATED="1398441005847" MODIFIED="1398441021705"/>
</node>
</node>
</map>
