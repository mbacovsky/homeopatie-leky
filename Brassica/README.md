
Brassica
==============

![mapa](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Brassica/Brassica.svg)

 - [PDF](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Brassica/Brassica.pdf)
 - [Map](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Brassica/Brassica.mm)
 - [SVG](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Brassica/Brassica.svg)

Rubriky
-------
 - Mind; abdominal complaints, with 
 - Mind; anticipation; ailments from, agg. [[Ars](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/ArsenicumAlbum)]
 - Mind; anxiety; alone, being [[Ars](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/ArsenicumAlbum)]
 - Mind; anxiety; inspiration, deep; must 
 - Mind; anxiety; nausea; with 
 - Mind; anxiety; world, for the 
 - Mind; approach of a person agg. 
 - Mind; climb, desire to 
 - Mind; company; aversion to; seen, does not want to be 
 - Mind; company; desire for; spoken to, but averse to being 
 - Mind; complaining; disease, of 
 - Mind; concentration; difficult; attempting to, on; vacant feeling, has a 
 - Mind; concentration; difficult; studying, reading, while 
 - Mind; country, desire for, to go into the 
 - Mind; cut short, nails, desire to 
 - Mind; delusions, imaginations; abused, being 
 - Mind; delusions, imaginations; abused, being; used and discarded, being 
 - Mind; delusions, imaginations; animals, of; larger, are 
 - Mind; delusions, imaginations; appreciated, that she is not 
 - Mind; delusions, imaginations; calls; him, someone; wonders how this is possible because name is not known 
 - Mind; delusions, imaginations; despised, is 
 - Mind; delusions, imaginations; lower, surrounding objects are, he is much taller 
 - Mind; delusions, imaginations; powerless, is 
 - Mind; delusions, imaginations; robotic, mechanical, feels 
 - Mind; delusions, imaginations; separated; world, from the, that he is 
 - Mind; delusions, imaginations; soul, souls; defeated, is 
 - Mind; delusions, imaginations; stupid, he is 
 - Mind; delusions, imaginations; subversive, obstructive, evasive, people are 
 - Mind; delusions, imaginations; tall, taller; he is 
 - Mind; delusions, imaginations; ugly, is 
 - Mind; delusions, imaginations; useless, is 
 - Mind; different, desires to be, bizarre and 
 - Mind; discouraged; resignation [[Aq-mar](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Aqua marina)]
 - Mind; dreams; appealing, attempts to make unappealing things 
 - Mind; dreams; ascending a height; see everyone from the top, could 
 - Mind; dreams; changing; rapidly 
 - Mind; dreams; colorful 
 - Mind; dreams; descending; escalators, unable to find way back up 
 - Mind; dreams; high places 
 - Mind; dreams; identity, of; lost 
 - Mind; dreams; identity, of; lost; confused with others, and 
 - Mind; elevated, desires to be, above things blocking her vision 
 - Mind; emptiness of mind, sensation of 
 - Mind; emptiness of mind, sensation of; brainwashed, feels 
 - Mind; fear; coition; thought of, in a woman, rape 
 - Mind; forsaken feeling; friendless, feels 
 - Mind; hunger, fasting agg. 
 - Mind; ideas; deficiency of 
 - Mind; indifference, apathy; taciturn 
 - Mind; irritability; causeless 
 - Mind; learning; difficult 
 - Mind; monotony, overwhelming sensation of 
 - Mind; sadness; situation, unable to change 
 - Mind; thoughts; monotony of 
 - Mind; useless and unattractive, feels 
 - Mind; will; loss of 
    