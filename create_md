#!/usr/bin/ruby
require 'find'
require 'erb'
require 'nokogiri'

class Repertory
  def self.rep
    @repertory ||= Hash.new{|h, k| h[k] = []}
    @repertory
  end

  def self.abbrev
    @abbrevs ||= {}
    @abbrevs
  end
end

class RemedyPrinter
  include ERB::Util
  attr_accessor :remedy, :template, :upstream_repo_path

  def initialize(map_path)
    @map_dir = File.dirname(map_path)
    @remedy = File.basename(map_path, ".mm")
    @upstream_repo_path = 'https://bitbucket.org/mbacovsky/homeopatie-leky/'
    @template = remedy_template
    @abbrev = extract_abbrev(map_path)
    @rubrics = Repertory.rep.clone.select { |key, val| val && val.include?(@abbrev) }
    @rubrics.keys.each do |r|
      @rubrics[r] = @rubrics[r].clone
      @rubrics[r].delete_at(@rubrics[r].index(@abbrev))
      @rubrics[r] = @rubrics[r].map {|rem| "[#{rem}](#{File.join(@upstream_repo_path, Repertory.abbrev[rem])})" }
    end
  end

  def render()
    ERB.new(@template, nil, '<>').result(binding)
  end

  def save()
    file = File.join(@map_dir, 'README.md')
    puts "#{remedy} => #{file}"
    File.open(file, "w+") do |f|
      f.write(render)
    end
  end

  private

  def remedy_template()
    %{
<%= @remedy %>

==============

![mapa](<%= File.join(@upstream_repo_path,'raw/master',@remedy,@remedy+'.svg').gsub(' ', '%20') %>)

 - [PDF](<%= File.join(@upstream_repo_path,'raw/master',@remedy,@remedy+'.pdf').gsub(' ', '%20') %>)
 - [Map](<%= File.join(@upstream_repo_path,'raw/master',@remedy,@remedy+'.mm').gsub(' ', '%20') %>)
 - [SVG](<%= File.join(@upstream_repo_path,'raw/master',@remedy,@remedy+'.svg').gsub(' ', '%20') %>)

<% if !@rubrics.keys.empty? %>
Rubriky
-------
<% @rubrics.keys.sort.each do |rub| %>
 - <%= rub %> <% if !@rubrics[rub].empty? %>[<%= @rubrics[rub].join(', ') %>]<% end %>
<% end %>
<% end %>
    }
  end


  def extract_abbrev(file)
    doc = Nokogiri::XML(open(file))
    nodes = doc.xpath('//node[@STYLE_REF="Abbrev"]')
    nodes.first['TEXT']
  end
end

def extract_rubrics(file)
  doc = Nokogiri::XML(open(file))
  nodes = doc.xpath('//node[@STYLE_REF="Abbrev"]')
  abbrev = nodes.first['TEXT']
  Repertory.abbrev[abbrev] = "/src/master/#{File.basename(file, ".mm")}"
  puts abbrev
  rubrics = doc.xpath('//node[@TEXT="Rubriky"]')
  unless rubrics.empty?
    rubrics.first.children.each do |r|
      if r['TEXT']
        puts r['TEXT']
        Repertory.rep[r['TEXT']] << abbrev unless Repertory.rep[r['TEXT']].include?(abbrev)
      end
    end
  end
end


Find.find('.') do |path|
  extract_rubrics(path) if path =~ /.*\.mm/ && !path.include?('.backup')
end
Find.find('.') do |path|
  RemedyPrinter.new(path).save if path =~ /.*\.mm/ && !path.include?('.backup')
end
#Repertory.rep.each { |r, rems| puts "#{r} [#{rems.join(', ')}]"}
