<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Violet" ID="ID_338122036" CREATED="1341239301242" MODIFIED="1341239371415" LINK="../../Letn&#xed;%20&#x161;kola%202012%20-%20Podmitrov/L&#xe9;&#x10d;en&#xed;%20pomoc&#xed;%20barev.mm" COLOR="#cc0000" STYLE="fork">
<hook NAME="MapStyle" max_node_width="600"/>
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="T&#xe9;mata" POSITION="right" ID="ID_1836670580" CREATED="1341240630009" MODIFIED="1341240633870" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="&#xfa;tulnost" ID="ID_182152877" CREATED="1341240789545" MODIFIED="1341240793509"/>
<node TEXT="duchovn&#xed; pohled na &#x17e;ivot" ID="ID_1929660165" CREATED="1341240812395" MODIFIED="1341240831536"/>
<node TEXT="pom&#xe1;h&#xe1; v citov&#xe9;m r&#x16f;stu" ID="ID_971536731" CREATED="1341240837665" MODIFIED="1341240847265"/>
<node TEXT="&#x10d;ist&#xed; auru" ID="ID_454757747" CREATED="1341240848785" MODIFIED="1341240852806"/>
<node TEXT="anestettikum" ID="ID_735195851" CREATED="1341240859152" MODIFIED="1341240864067"/>
<node TEXT="pom&#xe1;h&#xe1; zvl&#xe1;dat bolest" ID="ID_172898339" CREATED="1341240864268" MODIFIED="1341240871033"/>
<node TEXT="navozuje sp&#xe1;nek" ID="ID_1364409135" CREATED="1341240871266" MODIFIED="1341240876620"/>
<node TEXT="komunikace s duchovn&#xed;mi sv&#x11b;ty" ID="ID_884086772" CREATED="1341240877413" MODIFIED="1341240886672"/>
<node TEXT="v&#xed;ra ve vy&#x161;&#x161;&#xed; moc" ID="ID_385544591" CREATED="1341240891845" MODIFIED="1341240898851"/>
<node TEXT="estetick&#xe9; vn&#xed;m&#xe1;n&#xed;" ID="ID_1639503616" CREATED="1341240905307" MODIFIED="1341240911463"/>
<node TEXT="harmonie" ID="ID_551082235" CREATED="1341240919125" MODIFIED="1341240921562"/>
<node TEXT="klid" ID="ID_517840814" CREATED="1341240921810" MODIFIED="1341240922707"/>
<node TEXT="ticho" ID="ID_1618457874" CREATED="1341240922932" MODIFIED="1341240924499"/>
<node TEXT="vyrovnanost" ID="ID_1937107531" CREATED="1341240924716" MODIFIED="1341240927612"/>
<node TEXT="osv&#xed;cen&#xed;" ID="ID_1540533437" CREATED="1341240927869" MODIFIED="1341240930907"/>
<node TEXT="spiritu&#xe1;ln&#xed; pravda" ID="ID_316095808" CREATED="1341240931124" MODIFIED="1341240935700"/>
<node TEXT="n&#xe1;siln&#xe9; (rychl&#xe9;) otev&#x159;en&#xed; m&#x16f;&#x17e;e zp&#x16f;sobit &#x161;ok a kolaps nervov&#xe9;ho syst&#xe9;mu" ID="ID_585366227" CREATED="1341240941807" MODIFIED="1341240977529"/>
<node TEXT="nedostatek" ID="ID_1674613493" CREATED="1341240980716" MODIFIED="1341241104477">
<font BOLD="true"/>
<node TEXT="neschopnout p&#x159;ijmout zodpov&#x11b;dnost" ID="ID_231881912" CREATED="1341240984472" MODIFIED="1341240995263"/>
<node TEXT="snaha z&#x16f;stat d&#x11b;tmi" ID="ID_429124459" CREATED="1341240995608" MODIFIED="1341241001238"/>
<node TEXT="p&#x159;ipoutanost k rodi&#x10d;&#x16f;m" ID="ID_822951036" CREATED="1341241001471" MODIFIED="1341241010273">
<node TEXT="matce" ID="ID_1666073586" CREATED="1341241011010" MODIFIED="1341241012375"/>
</node>
</node>
<node TEXT="nadbytek" ID="ID_797137594" CREATED="1341241015604" MODIFIED="1341241105094">
<font BOLD="true"/>
<node TEXT="zmatek" ID="ID_57234159" CREATED="1341241019630" MODIFIED="1341241021191"/>
<node TEXT="rozlad&#x11b;n&#xed; ega" ID="ID_929750234" CREATED="1341241021424" MODIFIED="1341241025599"/>
<node TEXT="ztr&#xe1;ta vlastn&#xed; identity" ID="ID_120377517" CREATED="1341241036791" MODIFIED="1341241049585"/>
<node TEXT="podr&#xe1;&#x17e;d&#x11b;n&#xed;" ID="ID_1311132539" CREATED="1341241050082" MODIFIED="1341241054234"/>
<node TEXT="ned&#x16f;tklivost" ID="ID_1352087209" CREATED="1341241054467" MODIFIED="1341241057993"/>
<node TEXT="nedok&#xe1;&#x17e;e si trvat na sv&#xe9;m" ID="ID_1198888015" CREATED="1341241067340" MODIFIED="1341241077324"/>
<node TEXT="vzn&#xe1;&#x161;&#xed; se a je mimo" ID="ID_1089195502" CREATED="1341241260289" MODIFIED="1341241267782"/>
<node TEXT="nen&#xed; pln&#x11b; inkarnov&#xe1;n" ID="ID_558233530" CREATED="1341241279199" MODIFIED="1341241287494"/>
<node TEXT="nen&#xed; ve hmot&#x11b;" ID="ID_839176851" CREATED="1341241287927" MODIFIED="1341241292577"/>
<node TEXT="nem&#xe1; dost fyzick&#xe9; s&#xed;ly" ID="ID_1456661424" CREATED="1341241292866" MODIFIED="1341241299532"/>
<node TEXT="ztr&#xe1;c&#xed; individualitu, ego" ID="ID_1873625409" CREATED="1341241303885" MODIFIED="1341241314821"/>
<node TEXT="p&#x159;ecitliv&#x11b;lost a&#x17e; ztr&#xe1;ta rozumu" ID="ID_1407803980" CREATED="1341241338132" MODIFIED="1341241359077"/>
</node>
<node TEXT="d&#xe1;v&#xe1;" ID="ID_12164097" CREATED="1341241374228" MODIFIED="1341241377248">
<font BOLD="true"/>
<node TEXT="klid" ID="ID_879907144" CREATED="1341241378119" MODIFIED="1341241381647"/>
<node TEXT="&#xfa;levu" ID="ID_1871646573" CREATED="1341241382296" MODIFIED="1341241384597"/>
<node TEXT="pom&#xe1;h&#xe1; naj&#xed;t duchovn&#xed; cestu" ID="ID_1347979430" CREATED="1341241396717" MODIFIED="1341241407551"/>
<node TEXT="&#xfa;t&#x11b;chu p&#x159;ecitliv&#x11b;l&#xfd;m lidem" ID="ID_388895490" CREATED="1341241432099" MODIFIED="1341241442003"/>
<node TEXT="otev&#x159;&#xed;t spiritu&#xe1;ln&#xed; centra" ID="ID_1352078034" CREATED="1341241555449" MODIFIED="1341241565209"/>
<node TEXT="submisivn&#xed; lid&#xe9;" ID="ID_1940834233" CREATED="1341241565498" MODIFIED="1341241574441"/>
<node TEXT="podr&#xe1;&#x17e;d&#x11b;n&#xed; z nerozhodnosti" ID="ID_1745164505" CREATED="1341241575497" MODIFIED="1341241586974"/>
<node TEXT="pro pasivn&#xed;" ID="ID_321946723" CREATED="1341241589332" MODIFIED="1341241600429"/>
<node TEXT="pro plach&#xe9;" ID="ID_1668524119" CREATED="1341241601022" MODIFIED="1341241604127"/>
<node TEXT="pro nesm&#x11b;l&#xe9;" ID="ID_489600291" CREATED="1341241604592" MODIFIED="1341241608351"/>
<node TEXT="pro introverty" ID="ID_1881373745" CREATED="1341241609335" MODIFIED="1341241614294"/>
<node TEXT="pro lidi co necht&#x11b;j&#xed; nic &#x159;e&#x161;it, s nik&#xfd;m mluvit" ID="ID_90265774" CREATED="1341241615526" MODIFIED="1341241630694">
<node TEXT="samot&#xe1;&#x159;i" ID="ID_104094725" CREATED="1341241641425" MODIFIED="1341241644621"/>
</node>
<node TEXT="pro egoisty a narcisty" ID="ID_148842080" CREATED="1341241674409" MODIFIED="1341241681961"/>
<node TEXT="na noc" ID="ID_1308720498" CREATED="1341241688872" MODIFIED="1341241690985"/>
</node>
</node>
<node TEXT="Viol-aw" POSITION="left" ID="ID_1223240912" CREATED="1341241949386" MODIFIED="1341241969452" COLOR="#996600">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Trituace" POSITION="left" ID="ID_1680990862" CREATED="1341239372484" MODIFIED="1341240254576" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="svoboda" ID="ID_214072643" CREATED="1341240256414" MODIFIED="1341240524797"/>
<node TEXT="splynut&#xed;" ID="ID_72172982" CREATED="1341240525173" MODIFIED="1341240528419"/>
<node TEXT="v&#x161;echno je jedno" ID="ID_1418377272" CREATED="1341240529036" MODIFIED="1341240534848"/>
<node TEXT="vesel&#xe1; debata o mouch&#xe1;ch a exkrementech" ID="ID_182350733" CREATED="1341240535657" MODIFIED="1341240567252"/>
<node TEXT="radost" ID="ID_615586284" CREATED="1341240544675" MODIFIED="1341240547362"/>
<node TEXT="lehkost" ID="ID_1425275563" CREATED="1341240547683" MODIFIED="1341240550643"/>
<node TEXT="sd&#xed;len&#xed;" ID="ID_1714981002" CREATED="1341240550964" MODIFIED="1341240555103"/>
<node TEXT="inspirace" ID="ID_113252888" CREATED="1341240573798" MODIFIED="1341240576810"/>
<node TEXT="mimo&#x10d;asovost" ID="ID_120443476" CREATED="1341240606608" MODIFIED="1341240621648">
<font ITALIC="true"/>
</node>
</node>
<node TEXT="Info" POSITION="left" ID="ID_829790279" CREATED="1341239393506" MODIFIED="1341240255080" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="&#x10d;akra" ID="ID_1077014286" CREATED="1341240691103" MODIFIED="1341240694466">
<node TEXT="korunn&#xed;" ID="ID_1698934443" CREATED="1341240697632" MODIFIED="1341240700463"/>
</node>
<node TEXT="&#x17e;l&#xe1;za" ID="ID_1981439411" CREATED="1341240702799" MODIFIED="1341240705435">
<node TEXT="hypof&#xed;za" ID="ID_1757706606" CREATED="1341240706325" MODIFIED="1341240709601"/>
</node>
<node TEXT="380-430nm" ID="ID_160120290" CREATED="1341240710823" MODIFIED="1341240720018"/>
<node TEXT="nejvy&#x161;&#x161;&#xed; frekvence, kterou oko dok&#xe1;&#x17e;e vn&#xed;mat" ID="ID_556411634" CREATED="1341240722072" MODIFIED="1341240748010"/>
<node TEXT="mystick&#xe1; barva" ID="ID_1359039619" CREATED="1341240748659" MODIFIED="1341240754055"/>
<node TEXT="spiritualita a kr&#xe1;sa" ID="ID_1500476028" CREATED="1341240754297" MODIFIED="1341240761385"/>
<node TEXT="t&#xf3;n" ID="ID_733496496" CREATED="1341240762113" MODIFIED="1341240765780">
<node TEXT="H" ID="ID_14155804" CREATED="1341240766765" MODIFIED="1341240767641"/>
</node>
<node TEXT="vhodn&#xe1; pro star&#x161;&#xed; lidi" ID="ID_1258983632" CREATED="1341241417470" MODIFIED="1341241427007"/>
</node>
<node TEXT="Souvisej&#xed;c&#xed; l&#xe9;ky" POSITION="left" ID="ID_1240533932" CREATED="1341241780689" MODIFIED="1341241805738" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="slou&#x10d;eniny manganu a ho&#x159;&#x10d;&#xed;ku" ID="ID_1757913125" CREATED="1341241786004" MODIFIED="1341241801351"/>
<node TEXT="flouridy" ID="ID_252086216" CREATED="1341241808731" MODIFIED="1341241814999"/>
<node TEXT="ametist" ID="ID_1778512403" CREATED="1341241815400" MODIFIED="1341241817641"/>
<node TEXT="Hydrogenium" ID="ID_1191887984" CREATED="1341241841775" MODIFIED="1341241846914"/>
<node TEXT="opi&#xe1;ty" ID="ID_860148389" CREATED="1341241847338" MODIFIED="1341241891678"/>
<node TEXT="vz&#xe1;cn&#xe9; plyny" ID="ID_1481679538" CREATED="1341241893045" MODIFIED="1341241899359"/>
<node TEXT="iris" ID="ID_1614843775" CREATED="1341241900095" MODIFIED="1341241901759"/>
<node TEXT="viola" ID="ID_1797595124" CREATED="1341241903366" MODIFIED="1341241904908"/>
</node>
<node TEXT="Src" POSITION="left" ID="ID_342699078" CREATED="1341241994214" MODIFIED="1341242034929" COLOR="#0066cc" STYLE="fork" HGAP="-47" VSHIFT="31">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="VIOLET&#xa; &#xa;This color is associated with serenity, beauty and spirituality. It soothes calms and brings comfort, and on the physical plane it acts as a purifier and anesthetic.  It is associated with the Pineal gland located at the top of the skull. Oriental teachers share a belief that this gland opens the Crown center, or the gateway to the spiritual realm. Violet is found in nature amongst the most delicate and beautiful flowers of spring and summer.  It is sometimes disorienting to physical reality and can create a sensation of &#x201c;otherworldliness&#x201d; or disorientation.&#xa; &#xa;Chakra: The Crown Chakra:&#xa; This center sits at the top of the head. It governs our communication with the spirit realm, and becomes strengthened with meditation, inner reflection and prayer. It governs our aesthetic sense and connects us with that place within ourselves where we find tranquility and serenity. When the Crown Chakra is impaired it affects our health by creating such problems as epilepsy, brain tumors, strokes and disjointed mental patterns. When there are physical problems in this chakra there is often a problem with growth, maturity and development.&#xa;The archetype that empowers this chakra is called The Guru. It is when we become our wise teacher and learn from our life experiences that we live in the Source, and the Source lives within us. The negative archetype is called the Egotist, and refers to the person who thinks they are responsible for everything that happens. There is no spiritual connection to a higher Source at all.&#xa;The qualities of the Crown Chakra are: serenity, beauty and bliss.&#xa; &#xa;Mental: Violet gives a person a spiritual or higher outlook in life. It can ease our egotism and is useful when people struggle with personal growth issues. &#xa;Contraindications: Use judiciously with people who do not have developed egos as it may make them uncertain and create instability. Do not use this remedy if you have a serious medical condition without consulting your doctor.&#xa; &#xa;Physical: This color can be used to help with disorders such as alcoholism, epilepsy, and neurosis. It is good for stopping pains and can be used for menstrual cramps, headaches, and pain around the head, neck and shoulders.&#xa;Contraindicated: It is not to be used by people who are too sensitive to life. It can make them feel uneasy or irritable. Do not use this remedy if you have a serious medical condition. Please consult your doctor.&#xa; &#xa;General Symptoms: Violet can be used to open up realms of spiritual understanding, ease the emotions, and bring the gift of peace and spiritual insight in to a person&#x2019;s life. Violet can be emotional stability to violent minds and relief to neurotic states of anxiety and chronic worry.&#xa;Contraindications: Use in limited dosage. Give the remedy and wait to see how it affects a person before repeating it. Be cautious with overly sensitive people. Do not use this remedy if you have a serious medical condition. Please consult your doctor." ID="ID_534370849" CREATED="1341242000168" MODIFIED="1341242003082"/>
</node>
<node TEXT="Symptomy" POSITION="right" ID="ID_905800938" CREATED="1341241082770" MODIFIED="1341241699407" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="nevolnost" ID="ID_1674855997" CREATED="1341241086463" MODIFIED="1341241088772"/>
<node TEXT="dezorientace" ID="ID_1372526895" CREATED="1341241089013" MODIFIED="1341241092394"/>
<node TEXT="bolesti hlavy" ID="ID_529059773" CREATED="1341241092635" MODIFIED="1341241095820"/>
<node TEXT="nervov&#xfd; syst&#xe9;mu" ID="ID_370196707" CREATED="1341241155539" MODIFIED="1341241184100">
<node TEXT="mozek" ID="ID_407508079" CREATED="1341241132631" MODIFIED="1341241134469">
<node TEXT="epilepsie" ID="ID_1812180848" CREATED="1341241118326" MODIFIED="1341241121921"/>
<node TEXT="mrtvice" ID="ID_30071322" CREATED="1341241122130" MODIFIED="1341241124923"/>
<node TEXT="n&#xe1;dory" ID="ID_1169758816" CREATED="1341241125140" MODIFIED="1341241128542"/>
</node>
<node TEXT="neur&#xf3;zy" ID="ID_1610782640" CREATED="1341241149308" MODIFIED="1341241155042"/>
</node>
<node TEXT="konflikt mezi spiritualitou a emocemi" ID="ID_967403068" CREATED="1341241189030" MODIFIED="1341241210096">
<node TEXT="bolest hlavy" ID="ID_1971335334" CREATED="1341241212607" MODIFIED="1341241221022"/>
</node>
<node TEXT="MS" ID="ID_7242169" CREATED="1341241223689" MODIFIED="1341241225805">
<node TEXT="bolesti" ID="ID_852117438" CREATED="1341241226886" MODIFIED="1341241230584"/>
<node TEXT="k&#x159;e&#x10d;e" ID="ID_537867846" CREATED="1341241231088" MODIFIED="1341241234348"/>
</node>
<node TEXT="oblast ramen" ID="ID_711848712" CREATED="1341241235878" MODIFIED="1341241241676">
<node TEXT="bolesti" ID="ID_1319920573" CREATED="1341241242421" MODIFIED="1341241244420"/>
</node>
<node TEXT="z&#xe1;vislosti" ID="ID_262055413" CREATED="1341241245148" MODIFIED="1341241248110">
<node TEXT="alkoholismus" ID="ID_1978588868" CREATED="1341241248721" MODIFIED="1341241255595"/>
</node>
</node>
<node TEXT="Kontraindikace" POSITION="right" ID="ID_1234750839" CREATED="1341241699664" MODIFIED="1341241764893" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="m&#xe1;lo zako&#x159;en&#x11b;n&#xfd; &#x10d;lov&#x11b;k" ID="ID_122229107" CREATED="1341241705494" MODIFIED="1341241735689">
<node TEXT="mohl by ulet&#x11b;t" ID="ID_833141538" CREATED="1341241737692" MODIFIED="1341241742483"/>
</node>
<node TEXT="slab&#xe9; ego" ID="ID_272141834" CREATED="1341241743553" MODIFIED="1341241747656"/>
<node TEXT="alopatick&#xe9; l&#xe9;ky" ID="ID_1581091928" CREATED="1341241766034" MODIFIED="1341241772541"/>
</node>
</node>
</map>
