<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Carcinosin Cum Cuprum" ID="ID_1238349912" CREATED="1321120676896" MODIFIED="1321396526399" COLOR="#cc0000" STYLE="bubble" HGAP="-196" VSHIFT="71">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="hide_edge" COLOR="#808080" WIDTH="thin"/>
<hook NAME="MapStyle" max_node_width="600"/>
<node TEXT="CARC-C-C" POSITION="right" ID="ID_1411897909" CREATED="1321121477644" MODIFIED="1321121876873" COLOR="#996600" STYLE="fork">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
<edge STYLE="bezier"/>
</node>
<node TEXT="Info" POSITION="right" ID="ID_1657644968" CREATED="1321397413248" MODIFIED="1321397420279" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="je tvrd&#x161;&#xed; ne&#x17e; CARC" ID="ID_697330008" CREATED="1321172290567" MODIFIED="1321172301729">
<node TEXT="je v n&#x11b;m c&#xed;tit slo&#x17e;ka kovu" ID="ID_1418937511" CREATED="1321172302833" MODIFIED="1321172313892"/>
</node>
<node TEXT="m&#xe1; v&#x11b;t&#x161;&#xed; ambice ne&#x17e; CARC" ID="ID_1794189515" CREATED="1321121584233" MODIFIED="1321121598156">
<node TEXT="CARC &#x10d;ek&#xe1; na pochvalu" ID="ID_1713228324" CREATED="1321121615296" MODIFIED="1321121624639"/>
<node TEXT="CARC-C-C si pro pochvalu p&#x159;ijde" ID="ID_938266552" CREATED="1321121625208" MODIFIED="1321121647848"/>
</node>
</node>
<node TEXT="T&#xe9;mata" POSITION="right" ID="ID_1322291130" CREATED="1321120874440" MODIFIED="1321121097721" COLOR="#0066cc" STYLE="fork" HGAP="28" VSHIFT="-2">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="n&#xed;zk&#xe9; sebev&#x11b;dom&#xed; s vysokou v&#x16f;l&#xed;" ID="ID_1833960327" CREATED="1321120996166" MODIFIED="1321172349647" COLOR="#659919">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="true"/>
</node>
<node TEXT="s rakovinou cht&#x11b;j&#xed; bojovat" ID="ID_1405132185" CREATED="1321121011549" MODIFIED="1321121308634">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
<node TEXT="nikdy to nevzd&#xe1;m" ID="ID_1182288049" CREATED="1321121214619" MODIFIED="1321121294374">
<font BOLD="true"/>
</node>
<node TEXT="mus&#xed; b&#xfd;t siln&#x11b;j&#x161;&#xed; ne&#x17e; nemoc" ID="ID_1587505330" CREATED="1321121150749" MODIFIED="1321121163257"/>
</node>
<node TEXT="jde na to silou" ID="ID_112742577" CREATED="1321121176810" MODIFIED="1321121183624">
<node TEXT="vztek" ID="ID_385038117" CREATED="1321121184663" MODIFIED="1321121188953"/>
<node TEXT="podr&#xe1;&#x17e;d&#x11b;n&#xed;" ID="ID_1949926509" CREATED="1321121189202" MODIFIED="1321121195719"/>
<node TEXT="tvrdohlavost" ID="ID_566582600" CREATED="1321121085930" MODIFIED="1321121091284"/>
</node>
<node TEXT="n&#xed;zk&#xe9; sebev&#x11b;dom&#xed; se sna&#x17e;&#xed; zakr&#xfd;t" ID="ID_1979338405" CREATED="1321172044356" MODIFIED="1321172080392">
<font BOLD="true"/>
<node TEXT="vynakl&#xe1;d&#xe1; na to poustu energie" ID="ID_170026532" CREATED="1321172081063" MODIFIED="1321172091326"/>
<node TEXT="sna&#x17e;&#xed; se dok&#xe1;zat, &#x17e;e to tak nen&#xed;" ID="ID_967367996" CREATED="1321172091590" MODIFIED="1321172103237"/>
<node TEXT="nejt&#x11b;&#x17e;&#x161;&#xed; je p&#x159;iznat, &#x17e;e n&#x11b;&#x10d;eho nen&#xed; schopen" ID="ID_1258770075" CREATED="1321121262302" MODIFIED="1321121281784"/>
<node TEXT="sna&#x17e;&#xed; se dok&#xe1;zat ostatn&#xed;m, &#x17e;e je dobr&#xfd;" ID="ID_1480630531" CREATED="1321121137233" MODIFIED="1321121149237"/>
<node TEXT="chce v&#x161;echno zvl&#xe1;dnout" ID="ID_260360588" CREATED="1321172236363" MODIFIED="1321172244201"/>
<node TEXT="nezvl&#xe1;dat je pro n&#x11b;ho nep&#x159;ijateln&#xe9;" ID="ID_1085751082" CREATED="1321172246311" MODIFIED="1321172258919"/>
</node>
</node>
<node TEXT="Podobn&#xe9; l&#xe9;ky" POSITION="right" ID="ID_1385521409" CREATED="1321121724467" MODIFIED="1321172396803" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="CARC" ID="ID_1676438058" CREATED="1321172398962" MODIFIED="1321172400982">
<node TEXT="m&#xe9;n&#x11b; strukturovan&#xfd;" ID="ID_841641366" CREATED="1321172402238" MODIFIED="1321172504838">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="m&#xe9;n&#x11b; rigidn&#xed;" ID="ID_1438777681" CREATED="1321172444327" MODIFIED="1321172506120">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="m&#xe9;n&#x11b; tvrdohlav&#xfd;" ID="ID_1503496460" CREATED="1321172455264" MODIFIED="1321172506796">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="tolerantn&#x11b;j&#x161;&#xed; k ur&#xe1;&#x17e;k&#xe1;m" ID="ID_59026425" CREATED="1321172523510" MODIFIED="1321172541836">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node TEXT="CUPR" ID="ID_1666676981" CREATED="1321172571750" MODIFIED="1321172577485">
<node TEXT="chyb&#xed; snaha zavd&#x11b;&#x10d;it se" ID="ID_1610296435" CREATED="1321172578829" MODIFIED="1321172633285">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="chyb&#xed; slab&#xe9; sebev&#x11b;dom&#xed;" ID="ID_489100720" CREATED="1321172607405" MODIFIED="1321172633864">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="dok&#xe1;&#x17e;e &#x159;&#xed;ct ne" ID="ID_149390900" CREATED="1321172624624" MODIFIED="1321172634252">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="chyb&#xed; soucit a starost o druh&#xe9;" ID="ID_704620394" CREATED="1321172635152" MODIFIED="1321172660701">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="v&#xe1;le&#x10d;n&#xed;k" ID="ID_888388717" CREATED="1321172676113" MODIFIED="1321172704291">
<icon BUILTIN="button_cancel"/>
<node TEXT="strategie" ID="ID_36447416" CREATED="1321172691740" MODIFIED="1321172694718"/>
<node TEXT="obrann&#xe9; mechanismy" ID="ID_347848086" CREATED="1321172695007" MODIFIED="1321172700658"/>
</node>
</node>
</node>
</node>
</map>
