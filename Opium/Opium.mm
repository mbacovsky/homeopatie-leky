<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Opium" STYLE_REF="Main Topic" ID="ID_697977062" CREATED="1331314707225" MODIFIED="1354743588196" LINK="../2.%20rocnik/2r-7.mm"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="OP" POSITION="left" ID="ID_1543893251" CREATED="1331764242159" MODIFIED="1331764247448" COLOR="#996600">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Opium" POSITION="left" ID="ID_1276857674" CREATED="1331764247914" MODIFIED="1331764255425" COLOR="#999900">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Miazma" POSITION="left" ID="ID_280598606" CREATED="1331390684381" MODIFIED="1331390688674" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Karcinogen&#xed;" ID="ID_179771826" CREATED="1331390689929" MODIFIED="1331390695290"/>
</node>
<node TEXT="Podobn&#xe9; l&#xe9;ky" POSITION="left" ID="ID_159031141" CREATED="1331377960292" MODIFIED="1331377985175" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="ACON" ID="ID_1443378274" CREATED="1331377967104" MODIFIED="1331377968941">
<node TEXT="tady a te&#x10f;" ID="ID_153495997" CREATED="1331377970189" MODIFIED="1331377978179">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="strach nabud&#xed;" ID="ID_1180548954" CREATED="1331767079301" MODIFIED="1331767088072">
<icon BUILTIN="button_cancel"/>
<node TEXT="OP strach paralizuje" ID="ID_573293809" CREATED="1331767096218" MODIFIED="1331767102837" COLOR="#999999">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
<node TEXT="PLB" ID="ID_1607317307" CREATED="1331378842779" MODIFIED="1331378895700">
<node TEXT="m&#xe1; v&#x16f;li" ID="ID_1401575418" CREATED="1331378852415" MODIFIED="1331378868402">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node TEXT="HELL" ID="ID_1913535058" CREATED="1331378896475" MODIFIED="1331378899152">
<node TEXT="jde dol&#x16f;" ID="ID_1289310782" CREATED="1331378902581" MODIFIED="1331378909684">
<icon BUILTIN="button_cancel"/>
<node TEXT="OP jde nahoru" ID="ID_1507621373" CREATED="1331378911313" MODIFIED="1331378920040" COLOR="#999999">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
</node>
<node TEXT="T&#xe9;mata" POSITION="left" ID="ID_729425179" CREATED="1331373025103" MODIFIED="1331766867307" COLOR="#0066cc" STYLE="fork" VSHIFT="-9">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="bez v&#x16f;le" ID="ID_1744602504" CREATED="1331380507657" MODIFIED="1331380518112">
<font BOLD="true"/>
</node>
<node TEXT="lehkov&#xe1;&#x17e;n&#xfd;" ID="ID_1783193041" CREATED="1331314753388" MODIFIED="1331314758259"/>
<node TEXT="vize negativn&#xed;" ID="ID_1622549165" CREATED="1331371534182" MODIFIED="1331371539948"/>
<node TEXT="reakce po traumatech" ID="ID_1613095819" CREATED="1331379316186" MODIFIED="1331379321996">
<node TEXT="&#x161;ok" ID="ID_1057048334" CREATED="1331377020844" MODIFIED="1331379417341">
<font BOLD="true"/>
</node>
<node TEXT="&#xfa;raz" ID="ID_151801023" CREATED="1331377043435" MODIFIED="1331379406526">
<font BOLD="true"/>
<node TEXT="&#x10d;asto hlavy" ID="ID_285894006" CREATED="1331378169232" MODIFIED="1331378192180">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="sv&#x11b;dectv&#xed; v&#xe1;le&#x10d;n&#xfd;ch hr&#x16f;z" ID="ID_361200773" CREATED="1331379329201" MODIFIED="1331379338701"/>
<node TEXT="leknut&#xed;" ID="ID_1736747255" CREATED="1331379394323" MODIFIED="1331379397954"/>
<node TEXT="d&#x11b;siv&#xfd; z&#xe1;&#x17e;itek matky v t&#x11b;hotenstv&#xed;" ID="ID_1364635752" CREATED="1331767232313" MODIFIED="1331767251112"/>
</node>
<node TEXT="ztr&#xe1;ta komunikace mezi jednotliv&#xfd;mi energetick&#xfd;mi t&#x11b;ly" ID="ID_1448116778" CREATED="1331376979011" MODIFIED="1331377017942"/>
<node TEXT="n&#x11b;jak&#xe1; situace mu p&#x159;ipomene trauma z d&#x159;&#xed;v&#x11b;j&#x161;ka" ID="ID_1422595863" CREATED="1331377699508" MODIFIED="1331377875671">
<font BOLD="true"/>
</node>
<node TEXT="&#x159;e&#x161;&#xed; t&#xe9;ma, kter&#xe9; bylo" ID="ID_785922966" CREATED="1331314971542" MODIFIED="1331315000322"/>
<node TEXT="vztek" ID="ID_693304147" CREATED="1331377876277" MODIFIED="1331377882570"/>
<node TEXT="protivn&#xfd;" ID="ID_1978614247" CREATED="1331766231461" MODIFIED="1331766238733"/>
<node TEXT="strach" ID="ID_1413808885" CREATED="1331377886709" MODIFIED="1331766950825">
<font BOLD="true"/>
<node TEXT="&#xfa;zkost" ID="ID_957430204" CREATED="1331377883034" MODIFIED="1331377886323"/>
<node TEXT="paralizov&#xe1;n strachem" ID="ID_885239663" CREATED="1331767055903" MODIFIED="1331767066090"/>
</node>
<node TEXT="ztr&#xe1;ta mor&#xe1;ln&#xed;ch hodnot" ID="ID_747728682" CREATED="1331379031588" MODIFIED="1331766060370">
<font BOLD="true"/>
<node TEXT="m&#x16f;&#x17e;e cht&#xed;t i zab&#xed;t" ID="ID_1895469995" CREATED="1331379042725" MODIFIED="1331379121320"/>
<node TEXT="lhan&#xed;" ID="ID_350654297" CREATED="1331379178042" MODIFIED="1331379180670">
<node TEXT="Mind; liar" ID="ID_1567426789" CREATED="1331379192974" MODIFIED="1331379196061"/>
</node>
<node TEXT="lhostejn&#xfd; k utrpen&#xed;" ID="ID_1875114604" CREATED="1331379101236" MODIFIED="1331379111333"/>
</node>
<node TEXT="rychl&#xe9; st&#x159;&#xed;d&#xe1;n&#xed; polarit" ID="ID_325303692" CREATED="1331379435563" MODIFIED="1331379443986"/>
<node TEXT="na hranici &#x17e;ivota" ID="ID_34230415" CREATED="1331767319307" MODIFIED="1331767325647">
<node TEXT="sta&#x159;&#xed; lid&#xe9;" ID="ID_1832218603" CREATED="1331379444555" MODIFIED="1331767334612"/>
<node TEXT="d&#x11b;ti" ID="ID_433746205" CREATED="1331767335101" MODIFIED="1331767337328">
<node TEXT="narod&#xed; se sp&#xed;c&#xed;" ID="ID_1684214842" CREATED="1331767344966" MODIFIED="1331767382356"/>
<node TEXT="apatick&#xe9;" ID="ID_927218066" CREATED="1331767382693" MODIFIED="1331767398434"/>
<node TEXT="nesaj&#xed;" ID="ID_1946433050" CREATED="1331767399617" MODIFIED="1331767401677"/>
<node TEXT="us&#xed;naj&#xed; p&#x159;i kojen&#xed;" ID="ID_951993792" CREATED="1331767402165" MODIFIED="1331767421867"/>
<node TEXT="&#x161;patn&#x11b; nab&#xed;raj&#xed; v&#xe1;hu" ID="ID_83649727" CREATED="1331767422116" MODIFIED="1331767432191"/>
<node TEXT="pupe&#x10d;n&#xed; k&#xfd;la" ID="ID_816942254" CREATED="1331767458581" MODIFIED="1331767463176"/>
<node TEXT="z&#xe1;cpa" ID="ID_1470531507" CREATED="1331767463737" MODIFIED="1331767466141"/>
</node>
<node TEXT="du&#x161;e se usazuje nebo opu&#x161;t&#xed; sv&#x11b;t" ID="ID_887169169" CREATED="1331379470357" MODIFIED="1331379483602"/>
</node>
<node TEXT="tvrd&#xed;, &#x17e;e je mu dob&#x159;e i kdy&#x17e; trp&#xed;" ID="ID_1883425102" CREATED="1331766318010" MODIFIED="1331766346318"/>
<node TEXT="zpomalen&#xfd;" ID="ID_1427826530" CREATED="1331379377958" MODIFIED="1331379382382">
<node TEXT="ospalost" ID="ID_866806450" CREATED="1331379364346" MODIFIED="1331379366518"/>
<node TEXT="skeln&#xfd; pohled" ID="ID_1274202999" CREATED="1331379355689" MODIFIED="1331379359962"/>
</node>
<node TEXT="jako n&#xe1;sledek alkoholismu" ID="ID_238564209" CREATED="1331380384889" MODIFIED="1331380397175"/>
<node TEXT="Seagalov&#xe9;" ID="ID_1774121623" CREATED="1331388670261" MODIFIED="1331390435121" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Strach z extravagance" ID="ID_761053637" CREATED="1331388678949" MODIFIED="1331767007582">
<font BOLD="true"/>
<node TEXT="extravagance=mez, hranice" ID="ID_1293872800" CREATED="1331314718897" MODIFIED="1331314741233"/>
<node ID="ID_717246500" CREATED="1331314760189" MODIFIED="1331767028303"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      strach p&#345;ibl&#237;&#382;it se k n&#283;jak&#233;mu mezn&#237;ku,
    </p>
    <p>
      kter&#253; jsem si vyt&#253;&#269;il
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="lehkov&#xe1;&#x17e;nost" ID="ID_755190049" CREATED="1331388717653" MODIFIED="1331388724667"/>
<node TEXT="lhostejnost" ID="ID_1293380656" CREATED="1331388743966" MODIFIED="1331388761494"/>
<node TEXT="apatie" ID="ID_1183300911" CREATED="1331388755444" MODIFIED="1331388757767"/>
<node TEXT="nejsou si jist&#xed; sv&#xfd;m stavem" ID="ID_428009410" CREATED="1331388853024" MODIFIED="1331388915709"/>
<node TEXT="odvaha" ID="ID_902537323" CREATED="1331388916277" MODIFIED="1331388920652">
<node TEXT="i zbab&#x11b;lost" ID="ID_1077211748" CREATED="1331388922468" MODIFIED="1331388926888"/>
</node>
<node TEXT="hranice m&#x16f;&#x17e;e b&#xfd;t reprezentov&#xe1;na i &#x10d;asov&#xfd;m &#xfa;dajem" ID="ID_1949705246" CREATED="1331389039744" MODIFIED="1331389159553"/>
<node TEXT="ochutn&#xe1;m jen trochu, to mi p&#x159;ece nem&#x16f;&#x17e;e nic ud&#x11b;lat" ID="ID_1224724375" CREATED="1331389657988" MODIFIED="1331389676466"/>
<node TEXT="m&#xe1; strach ze v&#x161;eho, &#x10d;eho je moc" ID="ID_1807719398" CREATED="1331390003076" MODIFIED="1331390020996"/>
<node TEXT=" strach, kdy se za&#x10d;neme bl&#xed;&#x17e;it k n&#x11b;jak&#xe9; sv&#xe9; hranici, kterou jsem si stanovil" ID="ID_307399034" CREATED="1331388687457" MODIFIED="1331388715062"/>
<node TEXT="u&#x17e; to trva x dn&#xed;, u&#x17e; to nebudu d&#xe1;l tolerovat" ID="ID_801652870" CREATED="1331389170553" MODIFIED="1331389185129"/>
</node>
</node>
<node TEXT="Symptomy" POSITION="left" ID="ID_437787721" CREATED="1331377613370" MODIFIED="1331377618746" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="po &#x161;oku" ID="ID_443582662" CREATED="1331377619879" MODIFIED="1331377636603">
<node TEXT="paral&#xfd;za" ID="ID_914639861" CREATED="1331377637724" MODIFIED="1331377641136"/>
</node>
<node TEXT="spav&#xe1; apatick&#xe1; miminka" ID="ID_507567718" CREATED="1331377779231" MODIFIED="1331377789508"/>
<node TEXT="z&#xe1;cpa" ID="ID_933932085" CREATED="1331378064350" MODIFIED="1331379222545">
<font BOLD="true"/>
<node TEXT="u d&#x11b;t&#xed;" ID="ID_439963038" CREATED="1331379223992" MODIFIED="1331379227577"/>
</node>
<node TEXT="pr&#x16f;jem" ID="ID_496355969" CREATED="1331379229237" MODIFIED="1331379232974"/>
<node TEXT="stav komatu" ID="ID_1052679473" CREATED="1331378211042" MODIFIED="1331378215184">
<node TEXT="v&#x11b;dom&#xed; ode&#x161;lo jinam" ID="ID_1918606333" CREATED="1331378218341" MODIFIED="1331378225474"/>
</node>
<node TEXT="stavy po MM" ID="ID_1165588103" CREATED="1331378726234" MODIFIED="1331378730002"/>
<node TEXT="vn&#xed;m&#xe1;n&#xed; bolesti" ID="ID_409203528" CREATED="1331378956741" MODIFIED="1331379694567">
<font BOLD="true"/>
<node TEXT="sn&#xed;&#x17e;en&#xe9;" ID="ID_923727804" CREATED="1331378988505" MODIFIED="1331379005011">
<font BOLD="true"/>
</node>
<node TEXT="zv&#xfd;&#x161;en&#xe9;" ID="ID_1953878636" CREATED="1331378999268" MODIFIED="1331379003370"/>
</node>
<node TEXT="k&#x159;e&#x10d;e" ID="ID_813047658" CREATED="1331379207602" MODIFIED="1331379298467">
<font BOLD="true"/>
<node TEXT="z&#xe1;&#x161;kuby" ID="ID_832516645" CREATED="1331379261283" MODIFIED="1331379265345"/>
<node TEXT="paral&#xfd;za" ID="ID_1488224768" CREATED="1331379265817" MODIFIED="1331379268344"/>
<node TEXT="necitlivost kon&#x10d;etin" ID="ID_1647555031" CREATED="1331379268696" MODIFIED="1331379277173"/>
<node TEXT="epileptick&#xe9; stavy" ID="ID_580437572" CREATED="1331379240000" MODIFIED="1331379245911"/>
<node TEXT="astmatick&#xe9; stavy" ID="ID_1464664299" CREATED="1331379234627" MODIFIED="1331379239520"/>
</node>
<node TEXT="chroptiv&#xe9; d&#xfd;ch&#xe1;n&#xed;" ID="ID_1997556037" CREATED="1331379247670" MODIFIED="1331379259322"/>
<node TEXT="&#x10d;erven&#xfd; obli&#x10d;ej a&#x17e; do modra" ID="ID_1322851502" CREATED="1331379343016" MODIFIED="1331379355424"/>
<node TEXT="tepl&#xfd; pot" ID="ID_337009244" CREATED="1331379559566" MODIFIED="1331379566705"/>
<node TEXT="ochabnut&#xed;, a&#x17e; mu vis&#xed; spodn&#xed; &#x10d;elist" ID="ID_340859117" CREATED="1331379573486" MODIFIED="1331379586892"/>
<node TEXT="samovoln&#xe1; stolice" ID="ID_1624581013" CREATED="1331379608034" MODIFIED="1331379613569">
<node TEXT="po leknut&#xed;" ID="ID_94720316" CREATED="1331379614458" MODIFIED="1331379617507"/>
</node>
<node TEXT="nespavost" ID="ID_1673355164" CREATED="1331379618736" MODIFIED="1331379621536"/>
<node TEXT="sluchov&#xe1; p&#x159;ecitliv&#x11b;lost" ID="ID_173185536" CREATED="1331379642791" MODIFIED="1331379656659"/>
<node TEXT="no&#x10d;n&#xed; d&#x11b;sy" ID="ID_1977016740" CREATED="1331379657868" MODIFIED="1331379661836"/>
</node>
<node TEXT="Info" POSITION="right" ID="ID_89783737" CREATED="1331372848636" MODIFIED="1331766872002" COLOR="#0066cc" STYLE="fork" VSHIFT="-9">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="makovice se na&#x159;ez&#xe1;vaj&#xed;" ID="ID_1224016963" CREATED="1331372856727" MODIFIED="1331372899848">
<node TEXT="zaschl&#xe1; &#x161;&#x165;&#xe1;va se ze&#x161;krab&#xe1;v&#xe1; a zpracov&#xe1;v&#xe1;" ID="ID_1440073408" CREATED="1331372901950" MODIFIED="1331372943342"/>
</node>
<node TEXT="surovina k v&#xfd;rob&#x11b; heroinu" ID="ID_507017402" CREATED="1331373217662" MODIFIED="1331765736089"/>
<node TEXT="obsahuje alkaloidy" ID="ID_96351591" CREATED="1331765748885" MODIFIED="1331765755089">
<node TEXT="morfin" ID="ID_625373561" CREATED="1331765756489" MODIFIED="1331765759074"/>
<node TEXT="kodein" ID="ID_1241576155" CREATED="1331765759451" MODIFIED="1331765760975"/>
<node TEXT="papaverin" ID="ID_1125480752" CREATED="1331765761225" MODIFIED="1331765764148"/>
<node TEXT="..." ID="ID_623540925" CREATED="1331765769230" MODIFIED="1331765770416"/>
</node>
<node TEXT="akutn&#xed; intoxikace" ID="ID_244730319" CREATED="1331376548221" MODIFIED="1331376553690">
<node TEXT="pomal&#xfd; puls" ID="ID_94040133" CREATED="1331376448043" MODIFIED="1331376451234"/>
<node TEXT="pomal&#xe9; d&#xfd;ch&#xe1;n&#xed;" ID="ID_1407395446" CREATED="1331376442508" MODIFIED="1331376447802"/>
<node TEXT="inkontinence sv&#x11b;ra&#x10d;&#x16f;" ID="ID_310171482" CREATED="1331376430012" MODIFIED="1331376459671">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="tepl&#xe9; pocen&#xed;" ID="ID_1314361187" CREATED="1331376423395" MODIFIED="1331376429700"/>
<node TEXT="komat&#xf3;zn&#xed; sp&#xe1;nek s cefalickou kongesc&#xed;" ID="ID_53898909" CREATED="1331376384548" MODIFIED="1331376404090"/>
</node>
<node TEXT="chronick&#xe1; intoxikace" ID="ID_921748007" CREATED="1331376468524" MODIFIED="1331376476673">
<node TEXT="nejd&#x159;&#xed;ve" ID="ID_1482549096" CREATED="1331376507629" MODIFIED="1331376511750">
<node TEXT="euforie" ID="ID_1006544232" CREATED="1331376486045" MODIFIED="1331376491591"/>
<node TEXT="hyperaktivita" ID="ID_1155127505" CREATED="1331376491944" MODIFIED="1331376505863"/>
</node>
<node TEXT="pozd&#x11b;ji" ID="ID_1500449549" CREATED="1331376516472" MODIFIED="1331376519224">
<node TEXT="zpomalen&#xed;" ID="ID_1309927930" CREATED="1331376520041" MODIFIED="1331376540515"/>
<node TEXT="otup&#x11b;lost" ID="ID_199457406" CREATED="1331376522534" MODIFIED="1331376525394"/>
<node TEXT="necitlivost" ID="ID_1441849194" CREATED="1331376526394" MODIFIED="1331376529801"/>
<node TEXT="nebolestivost" ID="ID_1602437836" CREATED="1331376530017" MODIFIED="1331376533461"/>
<node TEXT="zpomalen&#xed; &#x10d;innosti st&#x159;ev" ID="ID_1161087824" CREATED="1331376565696" MODIFIED="1331376576550"/>
<node TEXT="pocen&#xed;" ID="ID_1991913864" CREATED="1331376576870" MODIFIED="1331376579121"/>
<node TEXT="&#xfa;tlum" ID="ID_970611967" CREATED="1331376623329" MODIFIED="1331376638433">
<node TEXT="pocit, &#x17e;e se nic ned&#x11b;je" ID="ID_1120501850" CREATED="1331376646410" MODIFIED="1331376661510"/>
<node TEXT="nic ne&#x159;e&#x161;&#xed;" ID="ID_1930504262" CREATED="1331376661904" MODIFIED="1331376665544"/>
<node TEXT="nem&#xe1; strach" ID="ID_1676813313" CREATED="1331376665865" MODIFIED="1331376668925"/>
<node TEXT="apatie" ID="ID_1734666739" CREATED="1331376669146" MODIFIED="1331376673066"/>
<node TEXT="lhostejnost" ID="ID_654745302" CREATED="1331376673515" MODIFIED="1331376676761"/>
<node TEXT="nefunguje mu v&#x16f;le" ID="ID_1683202136" CREATED="1331376678840" MODIFIED="1331376683201"/>
</node>
<node TEXT="nev&#x11b;dom&#xed; si sebe sama" ID="ID_1613318072" CREATED="1331376948719" MODIFIED="1331376964002"/>
</node>
</node>
<node TEXT="&#x10d;asto jsou pot&#x159;eba vy&#x161;&#x161;&#xed; potence" ID="ID_1143630187" CREATED="1331377803119" MODIFIED="1331377812466"/>
<node TEXT="akutn&#xed; i chronick&#xe9; stavy" ID="ID_524299641" CREATED="1331377820746" MODIFIED="1331377829002"/>
<node TEXT="u &#x17e;en po epidur&#xe1;ln&#xed; anestezii se &#x10d;asto narod&#xed; d&#x11b;ti ve stavu opia" ID="ID_415402422" CREATED="1331378009776" MODIFIED="1331378035512"/>
<node TEXT="" ID="ID_1532401058" CREATED="1331767193554" MODIFIED="1331767193554"/>
<node ID="ID_97612256" CREATED="1331764553732" MODIFIED="1331766788941"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      pou&#382;&#237;v&#225; se k paliaci bolesti, po vymizen&#237; &#250;&#269;inku nast&#225;v&#225; dramatick&#233; zhor&#353;en&#237;
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1600206175" CREATED="1331766775238" MODIFIED="1331766799140"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Riziko z&#225;vislosti je velmi vysok&#233;
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1182433788" CREATED="1331766799144" MODIFIED="1331766799146"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Komplikace z&#225;vislosti jsou &#269;asto z&#225;va&#382;n&#283;j&#353;&#237; ne&#382; p&#367;vodn&#237; probl&#233;m
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Trituace" POSITION="right" ID="ID_1415110123" CREATED="1331372953000" MODIFIED="1331372961686" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="30C" ID="ID_1757780602" CREATED="1331372979725" MODIFIED="1331372988237"/>
<node TEXT="bolesti hlavy" ID="ID_1498244841" CREATED="1331373196705" MODIFIED="1331373523874">
<font ITALIC="true"/>
<node TEXT="&#x10d;elo" ID="ID_125770911" CREATED="1331373456373" MODIFIED="1331373525967">
<font ITALIC="true"/>
</node>
<node TEXT="za o&#x10d;ima" ID="ID_885389276" CREATED="1331373460669" MODIFIED="1331373527535">
<font ITALIC="true"/>
</node>
</node>
<node TEXT="nausea" ID="ID_771154788" CREATED="1331373282351" MODIFIED="1331373521956">
<font ITALIC="true"/>
</node>
<node TEXT="t&#xe1;hne to nahoru" ID="ID_1438673084" CREATED="1331373475909" MODIFIED="1331373520388">
<font ITALIC="true"/>
<node TEXT="v p&#xe1;te&#x159;i" ID="ID_1409269103" CREATED="1331373491611" MODIFIED="1331374155857">
<font ITALIC="true"/>
</node>
<node TEXT="do hlavy" ID="ID_1541572883" CREATED="1331373497872" MODIFIED="1331373532309">
<font ITALIC="true"/>
</node>
</node>
<node TEXT="lehkost" ID="ID_1889109557" CREATED="1331373579655" MODIFIED="1331373582447"/>
<node TEXT="radost" ID="ID_3811814" CREATED="1331373614003" MODIFIED="1331373669559"/>
<node TEXT="otup&#x11b;lost" ID="ID_533900059" CREATED="1331373669872" MODIFIED="1331373673667"/>
<node TEXT="mal&#xe1;tnost" ID="ID_1799038739" CREATED="1331373684898" MODIFIED="1331373689319"/>
<node TEXT="t&#x11b;&#x17e;i&#x161;t&#x11b; v makovici" ID="ID_1379316776" CREATED="1331374128861" MODIFIED="1331374139083"/>
</node>
<node TEXT="Rubriky" POSITION="right" ID="ID_772196255" CREATED="1331377483968" MODIFIED="1331766880066" COLOR="#0066cc" STYLE="fork" VSHIFT="7">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Mind; shock, ailments from" ID="ID_1870771855" CREATED="1331377574168" MODIFIED="1331377576402"/>
<node TEXT="Mind; liar" ID="ID_627496662" CREATED="1331379200388" MODIFIED="1331767496002">
<font BOLD="false"/>
</node>
<node TEXT="Mind; fear; extravagant, of commiting something" ID="ID_20285898" CREATED="1331762692486" MODIFIED="1331762694185"/>
<node TEXT="Mind; well; says he is, when very sick" ID="ID_1040434822" CREATED="1331762718418" MODIFIED="1331762719468"/>
<node TEXT="Mind; indifference, apathy; complain, does not" ID="ID_390514847" CREATED="1331762746581" MODIFIED="1331762747649"/>
<node TEXT="Mind; indifference, apathy; pleasure, to" ID="ID_380219473" CREATED="1331762757602" MODIFIED="1331762758610"/>
<node TEXT="Mind; indifference, apathy; suffering, to" ID="ID_1451545947" CREATED="1331762769335" MODIFIED="1331762770716"/>
<node TEXT="Mind; indifference, apathy; everything, to" ID="ID_933361990" CREATED="1331762780315" MODIFIED="1331762786621"/>
<node TEXT="Mind; indifference, apathy; complete" ID="ID_946192955" CREATED="1331762792804" MODIFIED="1331762793935"/>
<node TEXT="Mind; audacity" ID="ID_1450412833" CREATED="1331762810124" MODIFIED="1331762812159"/>
<node TEXT="Mind; whimpering" ID="ID_1886109261" CREATED="1331762828458" MODIFIED="1331762830277"/>
<node TEXT="Mind; comprehension; loss of" ID="ID_1187785817" CREATED="1331762849879" MODIFIED="1331762851666"/>
<node TEXT="Mind; starting, startled; fright or fear, after" ID="ID_117922740" CREATED="1331762894122" MODIFIED="1331762899472"/>
<node TEXT="Mind; fear; fright, after" ID="ID_634195856" CREATED="1331762915469" MODIFIED="1331762916689"/>
<node TEXT="Mind; anxiety; fright; fear of fright still remaining" ID="ID_497537051" CREATED="1331762939783" MODIFIED="1331762941076"/>
<node TEXT="Mind; fright, fear agg., ailments from; accident, from sight of an" ID="ID_732242355" CREATED="1331762992692" MODIFIED="1331762995054"/>
<node TEXT="Mind; anger; ailments from, agg.; fear or fright, with" ID="ID_1169266519" CREATED="1331763028603" MODIFIED="1331763029899"/>
<node TEXT="Mind; anger; fright, fear agg." ID="ID_1578728730" CREATED="1331763042096" MODIFIED="1331763043372"/>
<node TEXT="Mind; absorbed, buried in thought" ID="ID_499760059" CREATED="1331763061541" MODIFIED="1331763062929"/>
<node TEXT="Mind; anguish; shock from injury, in" ID="ID_871063998" CREATED="1331763086847" MODIFIED="1331763088035"/>
<node TEXT="Mind; anxiety; future, about" ID="ID_1905266578" CREATED="1331763128990" MODIFIED="1331763130314"/>
<node TEXT="Mind; apoplexy, after" ID="ID_479079323" CREATED="1331763163519" MODIFIED="1331763164619"/>
<node TEXT="Mind; coma vigil" ID="ID_599516337" CREATED="1331763184195" MODIFIED="1331763185272"/>
<node TEXT="Mind; confusion of mind" ID="ID_1912318312" CREATED="1331763209805" MODIFIED="1331763210914"/>
<node TEXT="Mind; day-dreaming" ID="ID_1951725359" CREATED="1331763224513" MODIFIED="1331763225731"/>
<node TEXT="Mind; death; ailments from, agg.; confrontation of, from, in children" ID="ID_50123190" CREATED="1331763276889" MODIFIED="1331763278253"/>
<node TEXT="Mind; deceitful, sly" ID="ID_1760351199" CREATED="1331763299163" MODIFIED="1331763300207"/>
<node TEXT="Mind; despair; masturbation tendency, with" ID="ID_1082989632" CREATED="1331763328708" MODIFIED="1331763330089"/>
<node TEXT="Mind; dream, as in a" ID="ID_717581164" CREATED="1331763388942" MODIFIED="1331763390258"/>
<node TEXT="Mind; grief; offenses, from long past" ID="ID_394465860" CREATED="1331763424268" MODIFIED="1331763425387"/>
<node TEXT="Mind; injuries, accidents, ailments from" ID="ID_1924322574" CREATED="1331763453742" MODIFIED="1331763454827"/>
<node TEXT="Mind; liar; never speaks the truth, does not know what she is saying" ID="ID_1638522035" CREATED="1331763473537" MODIFIED="1331763474758"/>
<node TEXT="Mind; mirth, hilarity, liveliness" ID="ID_433357210" CREATED="1331763498339" MODIFIED="1331763499503"/>
<node TEXT="Mind; recognize; does not; people, anyone; relatives, his" ID="ID_918054743" CREATED="1331763517601" MODIFIED="1331763518790"/>
<node TEXT="Mind; stupor; apoplexy, after" ID="ID_1794575521" CREATED="1331763537093" MODIFIED="1331763538264"/>
<node TEXT="Mind; unconsciousness, coma; brain complaints, in" ID="ID_476522393" CREATED="1331763558213" MODIFIED="1331763559407"/>
<node TEXT="Mind; unconsciousness, coma; uremic coma" ID="ID_1183081222" CREATED="1331763580909" MODIFIED="1331763585202"/>
</node>
<node TEXT="Modality" POSITION="left" ID="ID_1766335677" CREATED="1331379540222" MODIFIED="1331766860963" COLOR="#0066cc" STYLE="fork" HGAP="21" VSHIFT="12">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="" ID="ID_139644859" CREATED="1331379544337" MODIFIED="1331379550006" COLOR="#000000">
<icon BUILTIN="smily_bad"/>
<node TEXT="teplem" ID="ID_1526678371" CREATED="1331379551433" MODIFIED="1331379554145"/>
<node TEXT="b&#x11b;hem a po sp&#xe1;nku" ID="ID_856924384" CREATED="1331766447406" MODIFIED="1331766456723"/>
<node TEXT="stimulanty" ID="ID_721385288" CREATED="1331766469256" MODIFIED="1331766474491"/>
<node TEXT="pocen&#xed;m" ID="ID_1845184161" CREATED="1331766475147" MODIFIED="1331766478395"/>
</node>
<node TEXT="" ID="ID_1022436301" CREATED="1331766490191" MODIFIED="1331766493465" COLOR="#000000">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="12"/>
<node TEXT="chlad" ID="ID_141294108" CREATED="1331766494910" MODIFIED="1331766507263"/>
<node TEXT="proch&#xe1;zka" ID="ID_172588309" CREATED="1331766507600" MODIFIED="1331766511898"/>
<node TEXT="&#x10d;ern&#xe1; k&#xe1;va" ID="ID_1833346752" CREATED="1331766513704" MODIFIED="1331766520803"/>
</node>
</node>
<node POSITION="left" ID="ID_1494138060" CREATED="1331764158720" MODIFIED="1331766847819" HGAP="12" VSHIFT="22"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <img src="op01.jpg"/>
  </body>
</html>
</richcontent>
</node>
<node POSITION="right" ID="ID_1012367991" CREATED="1331764180673" MODIFIED="1331766852177" HGAP="21" VSHIFT="12"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <img src="op02.jpg"/>
  </body>
</html>
</richcontent>
</node>
</node>
</map>
