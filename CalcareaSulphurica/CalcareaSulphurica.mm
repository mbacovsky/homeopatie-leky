<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Calcarea Sulphurica" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1362850913278"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Zdroj" STYLE_REF="Remedy" POSITION="right" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1357168559629"/>
<node TEXT="Calc-s" STYLE_REF="Abbrev" POSITION="right" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1362851074349"/>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="right" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389">
<node TEXT="Sulph" ID="ID_698090548" CREATED="1362851115063" MODIFIED="1362851118991">
<node TEXT="&#x161;aty" ID="ID_340153221" CREATED="1362851120047" MODIFIED="1362851122725"/>
<node TEXT="ego" ID="ID_1587473296" CREATED="1362851123038" MODIFIED="1362851287354">
<font BOLD="true"/>
</node>
<node TEXT="z&#xe1;jem o kr&#xe1;su" ID="ID_397197901" CREATED="1362851127061" MODIFIED="1362851158369"/>
</node>
<node TEXT="Calc" ID="ID_1339358915" CREATED="1362851163879" MODIFIED="1362851167826">
<node TEXT="&#xfa;sil&#xed;" ID="ID_1360938095" CREATED="1362852709756" MODIFIED="1362852714676"/>
</node>
<node TEXT="calc zv&#xfd;razn&#xed;" ID="ID_1205369111" CREATED="1362851393610" MODIFIED="1362851403265">
<node TEXT="n&#xed;zk&#xe9; seb&#x11b;dom&#xed;" ID="ID_215894902" CREATED="1362851404481" MODIFIED="1362851410027"/>
</node>
<node TEXT="podobn&#xe9; jako Chicory z bachovek" ID="ID_218840524" CREATED="1362854569808" MODIFIED="1362854596662"/>
</node>
<node TEXT="Trituace" STYLE_REF="Subtopic" POSITION="right" ID="ID_1275465186" CREATED="1357168572522" MODIFIED="1357168576351">
<node TEXT="&#xfa;pln&#x11b; na to ka&#x161;lu" ID="ID_675525567" CREATED="1362851030751" MODIFIED="1362851043093"/>
</node>
<node TEXT="Podobn&#xe9; l&#xe9;ky" STYLE_REF="Subtopic" POSITION="right" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1357168625505">
<node TEXT="Fluor" ID="ID_1840829967" CREATED="1362851992152" MODIFIED="1362851997863">
<node TEXT="elegantn&#xed;" STYLE_REF="Differs" ID="ID_1276594062" CREATED="1362852000787" MODIFIED="1362852013444"/>
</node>
<node TEXT="Sulph" ID="ID_448563185" CREATED="1362852040157" MODIFIED="1362852045916">
<node TEXT="nat&#xe1;hne na sebe kde co a je mu to jedno&#xa;(Calc-s za to bude cht&#xed;t ocen&#x11b;n&#xed;)" STYLE_REF="Differs" ID="ID_1002567696" CREATED="1362852050046" MODIFIED="1362852114646"/>
</node>
<node TEXT="Pallad" ID="ID_812881654" CREATED="1362852419832" MODIFIED="1362852423990">
<node TEXT="tou&#x17e;&#xed; po ocen&#x11b;n&#xed;" STYLE_REF="Same" ID="ID_1396586462" CREATED="1362852425438" MODIFIED="1362852437856">
<node TEXT="ale nen&#xed; to na s&#xed;lu" STYLE_REF="Comment" ID="ID_973723326" CREATED="1362852467027" MODIFIED="1362852477304"/>
<node TEXT="Calc-s lamentuje &#x17e;e nen&#xed; oce&#x148;ov&#xe1;na" STYLE_REF="Comment" ID="ID_155070231" CREATED="1362852479120" MODIFIED="1362852528094"/>
</node>
</node>
</node>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="right" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1357168653304">
<node TEXT="sebev&#x11b;dom&#xe1; &#x161;keble" ID="ID_959480369" CREATED="1362851357956" MODIFIED="1362851364910"/>
<node TEXT="machrov&#xe1;n&#xed;" ID="ID_1659598997" CREATED="1362851793804" MODIFIED="1362851797698">
<node TEXT="usiluje o to, abyste si v&#x161;imli, &#x17e;e je dobr&#xfd;" ID="ID_1568133746" CREATED="1362851759686" MODIFIED="1362851779130"/>
<node TEXT="&#x10d;echr&#xe1; pe&#x159;&#xed;" ID="ID_1459451701" CREATED="1362851742387" MODIFIED="1362851755108"/>
<node TEXT="moc j&#xed; to nep&#x16f;jde" ID="ID_1251849562" CREATED="1362851802653" MODIFIED="1362851818252"/>
<node TEXT="je v tom neohrabanost" ID="ID_907185182" CREATED="1362851818724" MODIFIED="1362851823889"/>
</node>
<node TEXT="chce se prosazovat i kdy&#x17e; je mimo" ID="ID_3360919" CREATED="1362852289351" MODIFIED="1362852297546"/>
<node TEXT="&#x17e;&#xe1;rlivost" ID="ID_1999256472" CREATED="1362852298187" MODIFIED="1362852306575">
<node TEXT="sourozenci" ID="ID_1574115859" CREATED="1362852312138" MODIFIED="1362852317180"/>
</node>
<node TEXT="tvrdohlavost" ID="ID_825580292" CREATED="1362852306976" MODIFIED="1362852310533"/>
<node TEXT="urputnost" ID="ID_1498171683" CREATED="1362852330623" MODIFIED="1362852333586"/>
<node TEXT="nevyb&#xed;rav&#x11b; si &#x159;&#xed;k&#xe1; o pochvalu" ID="ID_428917468" CREATED="1362854548774" MODIFIED="1362854559775"/>
<node TEXT="kritiku nesnese" ID="ID_1103543380" CREATED="1362852379445" MODIFIED="1362852384835">
<node TEXT="ur&#xe1;&#x17e;livost" ID="ID_378588717" CREATED="1362852373206" MODIFIED="1362852378182"/>
<node TEXT="na&#x161;tv&#xe1;n&#xed;" ID="ID_552324790" CREATED="1362852388872" MODIFIED="1362852398396"/>
<node TEXT="pochvalu hled&#xe1; jinde" ID="ID_1406505302" CREATED="1362852400833" MODIFIED="1362852406834"/>
</node>
<node TEXT="nesna&#x17e;&#xed; se b&#xfd;t dobr&#xe1;" ID="ID_1867484390" CREATED="1362852723495" MODIFIED="1362852734841">
<node TEXT="sna&#x17e;&#xed; se p&#x159;esv&#x11b;d&#x10d;it, &#x17e;e je dobr&#xe1;" ID="ID_1488207838" CREATED="1362852736073" MODIFIED="1362852751911"/>
<node TEXT="z pozice chudinky" ID="ID_942405243" CREATED="1362854279824" MODIFIED="1362854291608"/>
</node>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="right" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347"/>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="right" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729">
<node TEXT="Mysl&#xed; pomalu, zvl&#xe1;&#x161;&#x165; kdy&#x17e; maj&#xed; myslet rychle" ID="ID_668271210" CREATED="1362850964970" MODIFIED="1362850984073"/>
<node TEXT="Lamentuje, &#x17e;e nen&#xed; oce&#x148;ov&#xe1;n" ID="ID_88020466" CREATED="1362851441434" MODIFIED="1362851452541"/>
</node>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="right" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690"/>
</node>
</map>
