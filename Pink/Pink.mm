<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Pink" ID="ID_1663572557" CREATED="1341213828956" MODIFIED="1341213858439" LINK="../../Letn&#xed;%20&#x161;kola%202012%20-%20Podmitrov/L&#xe9;&#x10d;en&#xed;%20pomoc&#xed;%20barev.mm" COLOR="#cc0000" STYLE="fork">
<hook NAME="MapStyle" max_node_width="600"/>
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Trituace" POSITION="right" ID="ID_418556961" CREATED="1341213869153" MODIFIED="1341216145904" COLOR="#0066cc" STYLE="fork" HGAP="7" VSHIFT="-15">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="klid" ID="ID_409331951" CREATED="1341213899133" MODIFIED="1341213961350">
<font ITALIC="true"/>
</node>
<node TEXT="bl&#xed;zkost" ID="ID_1682910904" CREATED="1341213918709" MODIFIED="1341213965979">
<font ITALIC="true"/>
</node>
<node TEXT="objet&#xed;" ID="ID_545209893" CREATED="1341213966412" MODIFIED="1341213972209">
<font ITALIC="true"/>
</node>
<node TEXT="propojen&#xed;" ID="ID_304938771" CREATED="1341213972623" MODIFIED="1341213977079">
<font ITALIC="true"/>
</node>
<node TEXT="bu&#x161;en&#xed; srdce" ID="ID_1459355676" CREATED="1341214001969" MODIFIED="1341214008117">
<font ITALIC="true"/>
</node>
<node TEXT="smutek" ID="ID_1552127060" CREATED="1341214008443" MODIFIED="1341214012474">
<font ITALIC="true"/>
</node>
<node TEXT="jemnost" ID="ID_772920276" CREATED="1341214593412" MODIFIED="1341214595757"/>
<node TEXT="k&#x159;ehkost" ID="ID_328051528" CREATED="1341214596070" MODIFIED="1341214599986">
<node TEXT="sametov&#xe1;" ID="ID_136577742" CREATED="1341214606272" MODIFIED="1341214614118"/>
<node TEXT="mot&#xfd;l&#xed; k&#x159;&#xed;dla" ID="ID_550665408" CREATED="1341214614574" MODIFIED="1341214620063"/>
</node>
<node TEXT="chu&#x165; mluvit" ID="ID_1856328477" CREATED="1341214623713" MODIFIED="1341214628211"/>
<node TEXT="sd&#xed;len&#xed;" ID="ID_18596663" CREATED="1341214637598" MODIFIED="1341214640518"/>
<node TEXT="bolest hlavy" ID="ID_504413742" CREATED="1341214644474" MODIFIED="1341214652255"/>
<node TEXT="strach" ID="ID_886024641" CREATED="1341214671787" MODIFIED="1341214674262"/>
<node TEXT="lehkost" ID="ID_1338089279" CREATED="1341214686095" MODIFIED="1341214693200"/>
<node TEXT="tlak v hlav&#x11b;" ID="ID_1613365937" CREATED="1341214698626" MODIFIED="1341214702423"/>
<node TEXT="n&#x11b;ha" ID="ID_1785612311" CREATED="1341214748389" MODIFIED="1341214750979"/>
<node TEXT="bezpodm&#xed;ne&#x10d;n&#xe1; l&#xe1;ska" ID="ID_798453482" CREATED="1341214751396" MODIFIED="1341214759861"/>
<node TEXT="mate&#x159;stv&#xed;" ID="ID_971394351" CREATED="1341214761108" MODIFIED="1341214764335"/>
</node>
<node TEXT="T&#xe9;mata" POSITION="right" ID="ID_1180545678" CREATED="1341213860531" MODIFIED="1341216153750" COLOR="#0066cc" STYLE="fork" HGAP="25" VSHIFT="28">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="n&#x11b;&#x17e;nost" ID="ID_619010248" CREATED="1341214888158" MODIFIED="1341214892254"/>
<node TEXT="dotyky" ID="ID_1827393006" CREATED="1341214892719" MODIFIED="1341214896444"/>
<node TEXT="mate&#x159;sk&#xe1; l&#xe1;ska" ID="ID_908163069" CREATED="1341214913191" MODIFIED="1341214917671"/>
<node TEXT="p&#xe9;&#x10d;e" ID="ID_1540468311" CREATED="1341214918096" MODIFIED="1341214920830"/>
<node TEXT="jednota" ID="ID_1566980619" CREATED="1341214921527" MODIFIED="1341214923185"/>
<node TEXT="n&#x11b;ha" ID="ID_563324964" CREATED="1341214923514" MODIFIED="1341214925186"/>
<node TEXT="radost" ID="ID_267403758" CREATED="1341214925435" MODIFIED="1341214927718">
<node TEXT="obecn&#x11b;" ID="ID_1424913111" CREATED="1341214928519" MODIFIED="1341214930786"/>
<node TEXT="ze &#x17e;ivota" ID="ID_878943726" CREATED="1341214931043" MODIFIED="1341214933799"/>
</node>
<node TEXT="lehkost" ID="ID_975847823" CREATED="1341214935384" MODIFIED="1341214938927"/>
<node TEXT="jemnost" ID="ID_357128288" CREATED="1341214939296" MODIFIED="1341214941697"/>
<node TEXT="sladkost" ID="ID_684791313" CREATED="1341214946236" MODIFIED="1341214948068"/>
<node TEXT="naivita" ID="ID_1709323343" CREATED="1341214953078" MODIFIED="1341214955120"/>
<node TEXT="&#x10d;istota" ID="ID_1848377464" CREATED="1341214963772" MODIFIED="1341214966904"/>
<node TEXT="nevinost lidsk&#xe9;ho srdce" ID="ID_563693346" CREATED="1341214973063" MODIFIED="1341214980089"/>
<node TEXT="k&#x159;ehkost" ID="ID_314005863" CREATED="1341215008106" MODIFIED="1341215012481"/>
<node TEXT="d&#xe1;v&#xe1;/l&#xe9;&#x10d;&#xed;" ID="ID_42222765" CREATED="1341215046659" MODIFIED="1341215391968">
<font BOLD="true"/>
<node TEXT="schopnost d&#xe1;vat i p&#x159;ij&#xed;mat l&#xe1;sku" ID="ID_1857216295" CREATED="1341215022125" MODIFIED="1341215041280"/>
<node TEXT="vitalitu" ID="ID_1770607184" CREATED="1341215339794" MODIFIED="1341215344326"/>
<node TEXT="pocit radosti" ID="ID_287259283" CREATED="1341215489001" MODIFIED="1341215494370"/>
<node TEXT="zm&#x11b;k&#x10d;en&#xed; tvrdosti/rozumovosti" ID="ID_1027442430" CREATED="1341215626636" MODIFIED="1341215649819"/>
</node>
<node TEXT="dostaek" ID="ID_477396711" CREATED="1341214896813" MODIFIED="1341215051994">
<font BOLD="true"/>
<node TEXT="vitalita" ID="ID_437699956" CREATED="1341214905324" MODIFIED="1341214908696"/>
<node TEXT="&#x17e;ivot" ID="ID_1106715677" CREATED="1341214909153" MODIFIED="1341214911630"/>
<node TEXT="probl&#xe9;my m&#xe9;n&#x11b; akutn&#xed; a jdou l&#xe9;pe zvl&#xe1;dat" ID="ID_632961644" CREATED="1341215269669" MODIFIED="1341215284058"/>
</node>
<node TEXT="nedostatek" ID="ID_510760310" CREATED="1341215285376" MODIFIED="1341215290692">
<font BOLD="true"/>
<node TEXT="ko&#x17e;n&#xed;" ID="ID_7572697" CREATED="1341215291508" MODIFIED="1341215309070">
<node TEXT="vyr&#xe1;&#x17e;ky" ID="ID_1082458772" CREATED="1341215300706" MODIFIED="1341215304552"/>
<node TEXT="ekz&#xe9;my" ID="ID_1602229421" CREATED="1341215310475" MODIFIED="1341215313908"/>
</node>
<node TEXT="pocit, &#x17e;e nejsme milov&#xe1;ni" ID="ID_565167802" CREATED="1341215315051" MODIFIED="1341215323586"/>
<node TEXT="zatvrzelost" ID="ID_161717661" CREATED="1341215418497" MODIFIED="1341215421288"/>
<node TEXT="&#x17e;al z l&#xe1;sky" ID="ID_1505465593" CREATED="1341215436825" MODIFIED="1341215441040"/>
<node TEXT="vy&#x10d;erp&#xe1;n&#xed;" ID="ID_96797748" CREATED="1341215445546" MODIFIED="1341215449565"/>
<node TEXT="ztr&#xe1;ta mate&#x159;sk&#xe9; l&#xe1;sky" ID="ID_1903009276" CREATED="1341215456437" MODIFIED="1341215464561"/>
</node>
</node>
<node TEXT="Pink-aw" POSITION="left" ID="ID_508150687" CREATED="1341215092279" MODIFIED="1341216141782" COLOR="#996600" HGAP="3" VSHIFT="44">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Symptomy" POSITION="left" ID="ID_1142794031" CREATED="1341215327045" MODIFIED="1341215333706" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="srdce" ID="ID_1618183792" CREATED="1341215334609" MODIFIED="1341215336334"/>
<node TEXT="traumatick&#xfd; porod" ID="ID_1129636480" CREATED="1341215399580" MODIFIED="1341215404885"/>
<node TEXT="t&#x11b;&#x17e;k&#xe9; &#x17e;ivotn&#xed; situace" ID="ID_1531669124" CREATED="1341215406828" MODIFIED="1341215415732"/>
<node TEXT="&#x161;oky" ID="ID_1434252147" CREATED="1341215365749" MODIFIED="1341215367587"/>
<node TEXT="vy&#x10d;erp&#xe1;n&#xed;" ID="ID_1303058909" CREATED="1341215367821" MODIFIED="1341215371680"/>
</node>
<node TEXT="Info" POSITION="left" ID="ID_1859660337" CREATED="1341213866076" MODIFIED="1341213893492" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="t&#xf3;n" ID="ID_1172728722" CREATED="1341214854263" MODIFIED="1341214858273">
<node TEXT="nen&#xed;" ID="ID_1095031586" CREATED="1341214859578" MODIFIED="1341214861561"/>
</node>
<node TEXT="obl&#xed;ben&#xe1;  barva mal&#xfd;ch d&#x11b;t&#xed;" ID="ID_1079835863" CREATED="1341214863936" MODIFIED="1341214879809"/>
<node TEXT="protip&#xf3;l rozumu" ID="ID_259799003" CREATED="1341215479185" MODIFIED="1341215484436"/>
</node>
<node TEXT="Souvisej&#xed;c&#xed; l&#xe9;ky" POSITION="left" ID="ID_422263686" CREATED="1341213878445" MODIFIED="1341213894040" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="phos" ID="ID_1043254845" CREATED="1341215534873" MODIFIED="1341215537926"/>
<node TEXT="phosphorick&#xe9; soli" ID="ID_544279814" CREATED="1341215538630" MODIFIED="1341215551317"/>
<node TEXT="phos-ac" ID="ID_758805935" CREATED="1341215551821" MODIFIED="1341215558887"/>
<node TEXT="Ign" ID="ID_714871988" CREATED="1341215579579" MODIFIED="1341215582497"/>
<node TEXT="muriatick&#xe9; soli" ID="ID_1537281046" CREATED="1341215583218" MODIFIED="1341215592029"/>
<node TEXT="ml&#xe9;ka" ID="ID_987973574" CREATED="1341215592510" MODIFIED="1341215595170"/>
<node TEXT="impatiens" ID="ID_1622598358" CREATED="1341215599374" MODIFIED="1341215602290"/>
<node TEXT="puls" ID="ID_904000137" CREATED="1341215603753" MODIFIED="1341215605329"/>
<node TEXT="kali-ph" ID="ID_1307915428" CREATED="1341215652904" MODIFIED="1341215656727"/>
</node>
<node TEXT="Res" POSITION="left" ID="ID_980720746" CREATED="1341214819036" MODIFIED="1341216051575" COLOR="#0066cc" STYLE="fork" HGAP="-35" VSHIFT="-7">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="PINK&#xa;This is the color of universal mother love. It is often the favorite of children and is a color associated with love of all sorts. It pertains to the purity and innocence of the human heart and helps our capacity to give and receive love. It refers to the rosiness and vitality of life. It is suggestive of what is sweet, good and vital in our relationships.  It makes people feel good to be around pink. They find their problems are less acute, and they manage their problems better.&#xa; &#xa;Chakra: The Heart Chakra&#xa;Pink is another aspect of the love energy which flows through the Heart Chakra. It helps us develop qualities of love and releases our pain, sorrow and grief at the same time. The Heart Chakra is fueled by love; it is its reason for functioning and our reason for living.&#xa;We nourish the heart, both its physical and spiritual aspects, by cultivating a loving outlook and a lovely way of being in the world. Each time we come from love we radiate that energy out into the world around us. People who love their lives, their work, their friends and relationships actually glow with the essence of love. They spread that goodness and teach us all to love ourselves, one another and the world around us.&#xa; &#xa; &#xa;Mental: Pink can be used to add an emotional element to the harsh light of the mind. It offers sweetness and a refreshing quality to the hard edges of our lives.&#xa;Contraindications: Do not take this remedy if you have a serious medical condition. Please consult your doctor.&#xa; &#xa; &#xa;Physical: Pink can be used for heart problems and is excellent for reviving vitality without the aggression of red or orange. It can be used to help overcome shock, such as trauma, injury or grief. It is a general toner and helps people through difficult times of change when they feel tired, fed up or exhausted. It can be used for skin conditions which suggest a person may be somatizing feelings of rejection and feeling unloved.&#xa;Contraindications: Do not use this remedy if you have a serious medical condition. Please consult your doctor.&#xa; &#xa; &#xa;General Symptoms: Pink is a color associated with love. Whenever there is a hardening or an emotional closing down, the color can add softness and a gentle quality to a person&#x2019;s life. It is good for heartache, loss and emotional suffering, especially in association with loss of motherly or feminine love.&#xa;Contraindications: Pink is not only a woman&#x2019;s remedy. It can be given to any sex, of any age, whenever there is loss of love or grief. When combined with orange it adds vitality and a sense of joy to any situation. Do not take this remedy if you have a serious medical condition without consulting your doctor." ID="ID_723988709" CREATED="1341214844164" MODIFIED="1341214847988"/>
</node>
</node>
</map>
