<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Orange" ID="ID_613443310" CREATED="1341148806726" MODIFIED="1341151852597" LINK="../../Letn&#xed;%20&#x161;kola%202012%20-%20Podmitrov/L&#xe9;&#x10d;en&#xed;%20pomoc&#xed;%20barev.mm" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<hook NAME="MapStyle" max_node_width="600"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Oran-aw" POSITION="left" ID="ID_1861520444" CREATED="1341155215542" MODIFIED="1341155221925" COLOR="#996600">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Info" POSITION="left" ID="ID_962757124" CREATED="1341149794612" MODIFIED="1341150934297" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="590-625nm" ID="ID_1367658082" CREATED="1341149798000" MODIFIED="1341149809069"/>
<node TEXT="t&#xf3;n D" ID="ID_191174584" CREATED="1341149816539" MODIFIED="1341149819800"/>
<node TEXT="miazma" ID="ID_1121058501" CREATED="1341149820856" MODIFIED="1341149824960">
<node TEXT="sykotick&#xe9;" ID="ID_526503421" CREATED="1341149825825" MODIFIED="1341149828692"/>
</node>
<node TEXT="&#x10d;akra" ID="ID_29387787" CREATED="1341149883960" MODIFIED="1341149887200">
<node TEXT="sakr&#xe1;ln&#xed;" ID="ID_283858148" CREATED="1341149888536" MODIFIED="1341149891709"/>
<node TEXT="c&#xed;sa&#x159;/cisa&#x159;ovna" ID="ID_1142685858" CREATED="1341149892125" MODIFIED="1341149905308"/>
</node>
<node TEXT="kovy" ID="ID_1242516162" CREATED="1341149906993" MODIFIED="1341149910370">
<node TEXT="m&#x11b;&#x10f;" ID="ID_986844136" CREATED="1341149911139" MODIFIED="1341149913591"/>
<node TEXT="mosaz" ID="ID_1427573679" CREATED="1341149915673" MODIFIED="1341149917119"/>
<node TEXT="bronz" ID="ID_1705616236" CREATED="1341149917432" MODIFIED="1341149919146"/>
</node>
<node TEXT="kameny" ID="ID_1201552573" CREATED="1341149920368" MODIFIED="1341149922187">
<node TEXT="oran&#x17e;ov&#xe9;" ID="ID_1393380495" CREATED="1341149923252" MODIFIED="1341149951932"/>
<node TEXT="jantar" ID="ID_1557495913" CREATED="1341149927439" MODIFIED="1341149932685"/>
<node TEXT="citr&#xed;n" ID="ID_1035708577" CREATED="1341149933477" MODIFIED="1341149937781"/>
<node TEXT="topaz" ID="ID_293382671" CREATED="1341149938900" MODIFIED="1341149940746"/>
<node TEXT="karneol" ID="ID_701161091" CREATED="1341149997896" MODIFIED="1341150000281"/>
</node>
<node TEXT="&#x17e;l&#xe1;za" ID="ID_1853403045" CREATED="1341150002269" MODIFIED="1341150005161">
<node TEXT="nadledvinky" ID="ID_1623518033" CREATED="1341150006058" MODIFIED="1341150009752"/>
</node>
</node>
<node TEXT="T&#xe9;mata" POSITION="left" ID="ID_716893715" CREATED="1341149835738" MODIFIED="1341150933405" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="chce v&#xed;t&#x11b;zit" ID="ID_389536791" CREATED="1341149839791" MODIFIED="1341149843826"/>
<node TEXT="onemocn&#x11b;n&#xed; z p&#x159;em&#xed;ry n&#xe1;mahy" ID="ID_429220683" CREATED="1341149844851" MODIFIED="1341149854169"/>
<node TEXT="vitalita" ID="ID_4627193" CREATED="1341150087227" MODIFIED="1341150089602"/>
<node TEXT="radost" ID="ID_432071272" CREATED="1341150089899" MODIFIED="1341150091870"/>
<node TEXT="&#x161;t&#x11b;st&#xed;" ID="ID_1999280142" CREATED="1341150092231" MODIFIED="1341150094498"/>
<node TEXT="pot&#x11b;&#x161;en&#xed;" ID="ID_1938042379" CREATED="1341150094779" MODIFIED="1341150098193"/>
<node TEXT="sm&#xed;ch" ID="ID_1641734511" CREATED="1341150099416" MODIFIED="1341150100942"/>
<node TEXT="blahobyt" ID="ID_878875988" CREATED="1341150106487" MODIFIED="1341150110296"/>
<node TEXT="pen&#xed;ze" ID="ID_1996625585" CREATED="1341150110944" MODIFIED="1341150113027"/>
<node TEXT="dobr&#xfd; pocit ze &#x17e;ivota" ID="ID_218250143" CREATED="1341150113468" MODIFIED="1341150121225"/>
<node TEXT="uvoln&#x11b;nost" ID="ID_1313767899" CREATED="1341150122009" MODIFIED="1341150125873"/>
<node TEXT="vztah k sob&#x11b; sam&#xe9;mu" ID="ID_1054099379" CREATED="1341150134006" MODIFIED="1341150141240">
<node TEXT="jak se o sebe star&#xe1;me, m&#xed;ra komfortu" ID="ID_1986859195" CREATED="1341150150435" MODIFIED="1341150170751" COLOR="#999999">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="radost" ID="ID_1033185651" CREATED="1341150177458" MODIFIED="1341150185460"/>
<node TEXT="&#x10d;istota" ID="ID_1226126445" CREATED="1341150185925" MODIFIED="1341150189076"/>
<node TEXT="kvalita j&#xed;dla" ID="ID_60880292" CREATED="1341150189381" MODIFIED="1341150196013"/>
<node TEXT="sp&#xe1;nek" ID="ID_1986316836" CREATED="1341150196366" MODIFIED="1341150198911"/>
</node>
<node TEXT="zdrav&#xe1; chu&#x165; do &#x17e;ivota" ID="ID_1569871923" CREATED="1341150205979" MODIFIED="1341150212748"/>
<node TEXT="p&#x159;em&#xed;ra" ID="ID_198220620" CREATED="1341150215274" MODIFIED="1341150226837">
<node TEXT="chtivost" ID="ID_686311919" CREATED="1341150227678" MODIFIED="1341150230699"/>
<node TEXT="&#x17e;ravost" ID="ID_445666548" CREATED="1341150231044" MODIFIED="1341150234436"/>
<node TEXT="alkoholismus" ID="ID_1724328999" CREATED="1341150234716" MODIFIED="1341150241835"/>
</node>
<node TEXT="spole&#x10d;en&#x161;t&#xed;" ID="ID_1632276457" CREATED="1341150243161" MODIFIED="1341150247750"/>
<node TEXT="tou&#x17e;&#xed; po bl&#xed;zkosti druh&#xfd;ch" ID="ID_1153472363" CREATED="1341150248015" MODIFIED="1341150256856"/>
<node TEXT="p&#x159;&#xe1;tel&#x161;t&#xed;" ID="ID_1661722907" CREATED="1341150257153" MODIFIED="1341150263097"/>
<node TEXT="extroverti" ID="ID_1219332440" CREATED="1341150263497" MODIFIED="1341150268137"/>
<node TEXT="chu&#x165; k j&#xed;dlu" ID="ID_990991559" CREATED="1341150269871" MODIFIED="1341150276420"/>
<node TEXT="poruchy p&#x159;&#xed;jmu potravy" ID="ID_642285340" CREATED="1341150356572" MODIFIED="1341150363520">
<node TEXT="anorexie" ID="ID_4915383" CREATED="1341150364198" MODIFIED="1341150366615"/>
<node TEXT="bul&#xed;mie" ID="ID_157716152" CREATED="1341150367104" MODIFIED="1341150369536"/>
<node TEXT="&#x17e;ravost" ID="ID_1528150195" CREATED="1341150369898" MODIFIED="1341150373821"/>
</node>
<node TEXT="sexualita" ID="ID_93239263" CREATED="1341150468346" MODIFIED="1341150470741">
<node TEXT="disfunkce" ID="ID_1190190617" CREATED="1341150472143" MODIFIED="1341150475912"/>
<node TEXT="neplodnost" ID="ID_54117950" CREATED="1341150476185" MODIFIED="1341150479283">
<node TEXT="ot&#x11b;hotn&#x11b;n&#xed;" ID="ID_1136783180" CREATED="1341150480092" MODIFIED="1341150484678"/>
<node TEXT="potraty" ID="ID_1749810272" CREATED="1341150487755" MODIFIED="1341150490195"/>
</node>
<node TEXT="impotence" ID="ID_486157161" CREATED="1341150491121" MODIFIED="1341150493997"/>
<node TEXT="frigidita" ID="ID_1856949723" CREATED="1341150494303" MODIFIED="1341150496547"/>
<node TEXT="n&#xed;zk&#xe9; libido" ID="ID_353498064" CREATED="1341150496892" MODIFIED="1341150501420"/>
<node TEXT="obt&#xed;&#x17e;n&#xe9; porody" ID="ID_1230106983" CREATED="1341150515127" MODIFIED="1341150520422"/>
<node TEXT="p&#x159;ed&#x10d;asn&#xe1; ejakulace" ID="ID_1896421472" CREATED="1341150520711" MODIFIED="1341150527340"/>
<node TEXT="menstruace" ID="ID_1034862583" CREATED="1341150527629" MODIFIED="1341150531965">
<node TEXT="nepravidelnosti" ID="ID_470179813" CREATED="1341150532638" MODIFIED="1341150536396"/>
<node TEXT="bolestivost" ID="ID_1475292679" CREATED="1341150536637" MODIFIED="1341150540244"/>
</node>
</node>
<node TEXT="rovnov&#xe1;ha vody v organismu" ID="ID_1318118372" CREATED="1341150632464" MODIFIED="1341150639924">
<node TEXT="otoky" ID="ID_769986929" CREATED="1341150641292" MODIFIED="1341150644734"/>
<node TEXT="retence" ID="ID_370320772" CREATED="1341150645007" MODIFIED="1341150646912"/>
</node>
<node TEXT="p&#x159;i nedostatku" ID="ID_94249024" CREATED="1341150783058" MODIFIED="1341150787748">
<node TEXT="&#xfa;nava" ID="ID_336511227" CREATED="1341150788781" MODIFIED="1341150792000"/>
<node TEXT="vy&#x10d;erp&#xe1;n&#xed;" ID="ID_1596566547" CREATED="1341150792241" MODIFIED="1341150795829"/>
<node TEXT="vyho&#x159;en&#xed;" ID="ID_1294253440" CREATED="1341150796189" MODIFIED="1341150800730"/>
<node TEXT="co pat&#x159;&#xed; n&#xe1;m, d&#xe1;v&#xe1;me duh&#xfd;m" ID="ID_1867667908" CREATED="1341150827298" MODIFIED="1341150841404"/>
</node>
</node>
<node TEXT="Souvisej&#xed;c&#xed; l&#xe9;ky" POSITION="left" ID="ID_1780757394" CREATED="1341152034756" MODIFIED="1341152107282" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Bell-p" ID="ID_350595554" CREATED="1341152028646" MODIFIED="1341152031838"/>
<node TEXT="Rhus-t" ID="ID_698538832" CREATED="1341152048790" MODIFIED="1341152054230"/>
<node TEXT="Beril" ID="ID_631813721" CREATED="1341152054751" MODIFIED="1341152058056"/>
<node TEXT="Sel" ID="ID_338939012" CREATED="1341152058425" MODIFIED="1341152061771"/>
<node TEXT="Tita" ID="ID_528423794" CREATED="1341152062643" MODIFIED="1341152066566"/>
<node TEXT="Wolfr" ID="ID_1703924161" CREATED="1341152067038" MODIFIED="1341152070032"/>
<node TEXT="Natria" ID="ID_960368789" CREATED="1341152070393" MODIFIED="1341152081735"/>
<node TEXT="Calen" ID="ID_950157609" CREATED="1341152018786" MODIFIED="1341152021843"/>
<node TEXT="Berb" ID="ID_744579637" CREATED="1341152013276" MODIFIED="1341152018137"/>
<node TEXT="Puls" ID="ID_140305660" CREATED="1341151998105" MODIFIED="1341152000214"/>
<node TEXT="Calc" ID="ID_1516965907" CREATED="1341152000775" MODIFIED="1341152002477"/>
<node TEXT="Nitr-ac" ID="ID_831286150" CREATED="1341152002958" MODIFIED="1341152008775"/>
<node TEXT="Nitr" ID="ID_1623148359" CREATED="1341152009408" MODIFIED="1341152027699"/>
</node>
<node TEXT="Trituace" POSITION="right" ID="ID_365300419" CREATED="1341148812159" MODIFIED="1341150942195" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="neklid" ID="ID_435675110" CREATED="1341148816818" MODIFIED="1341148827285">
<font ITALIC="true"/>
</node>
<node TEXT="brn&#x11b;n&#xed;" ID="ID_764046404" CREATED="1341148827659" MODIFIED="1341148833384">
<font ITALIC="true"/>
</node>
<node TEXT="aktivita" ID="ID_445815780" CREATED="1341148843632" MODIFIED="1341148848173">
<font ITALIC="true"/>
</node>
<node TEXT="spl&#xed;n" ID="ID_265634148" CREATED="1341149119926" MODIFIED="1341149123886">
<font ITALIC="true"/>
<node TEXT="potom" ID="ID_1885548679" CREATED="1341149725073" MODIFIED="1341149730001" COLOR="#999999">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="oran&#x17e;ov&#xed; mni&#x161;i" ID="ID_437068757" CREATED="1341149610889" MODIFIED="1341149622909"/>
<node TEXT="pozitivn&#xed;" ID="ID_1194703570" CREATED="1341149626401" MODIFIED="1341149629341"/>
<node TEXT="horko" ID="ID_1036362731" CREATED="1341149682155" MODIFIED="1341149684568">
<node TEXT="v podb&#x159;i&#x161;ku" ID="ID_270456779" CREATED="1341149697997" MODIFIED="1341149706979"/>
</node>
</node>
<node TEXT="Symptomy" POSITION="right" ID="ID_985780586" CREATED="1341150851223" MODIFIED="1341150932597" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="alergie" ID="ID_972357159" CREATED="1341150855331" MODIFIED="1341150857963"/>
<node TEXT="slab&#xe1; obrana organismu" ID="ID_1785636567" CREATED="1341150877281" MODIFIED="1341150884500"/>
<node TEXT="touha po j&#xed;dle" ID="ID_140111854" CREATED="1341150884773" MODIFIED="1341150889005"/>
<node TEXT="touha po &#x10d;okol&#xe1;d&#x11b;" ID="ID_1509175587" CREATED="1341150892497" MODIFIED="1341150898273"/>
<node TEXT="touha po p&#x159;ijet&#xed;" ID="ID_1993155027" CREATED="1341150898666" MODIFIED="1341150906273"/>
<node TEXT="hormon&#xe1;ln&#xed; disbalance" ID="ID_812806479" CREATED="1341150961987" MODIFIED="1341150984897">
<node TEXT="PMS" ID="ID_1187305855" CREATED="1341150985698" MODIFIED="1341150987161"/>
<node TEXT="opo&#x17e;d&#x11b;n&#xe1; MS" ID="ID_261296571" CREATED="1341150987474" MODIFIED="1341150993579"/>
</node>
<node TEXT="l&#xed;n&#xe1; st&#x159;eva" ID="ID_1934279729" CREATED="1341151813372" MODIFIED="1341151813373" LINK="../Leky/Orange/Orange.mm"/>
<node TEXT="poopera&#x10d;n&#xed; stavy" ID="ID_885166503" CREATED="1341151132499" MODIFIED="1341151138804"/>
<node TEXT="chronick&#xfd; &#xfa;navov&#xfd; syndrom" ID="ID_1363638432" CREATED="1341151181332" MODIFIED="1341151191653"/>
<node TEXT="diuretikum" ID="ID_1239273632" CREATED="1341151197577" MODIFIED="1341151200851"/>
<node TEXT="autoimunitn&#xed; onemocn&#x11b;n&#xed;" ID="ID_1330812445" CREATED="1341151209223" MODIFIED="1341151216906">
<node TEXT="AIDS" ID="ID_647906709" CREATED="1341151221536" MODIFIED="1341151223477"/>
<node TEXT="HIV" ID="ID_1783200222" CREATED="1341151224038" MODIFIED="1341151226508"/>
<node TEXT="RS" ID="ID_1624048796" CREATED="1341151227357" MODIFIED="1341151236315"/>
<node TEXT="lupus" ID="ID_651982933" CREATED="1341151239256" MODIFIED="1341151241141"/>
</node>
<node TEXT="deprese" ID="ID_492041968" CREATED="1341151347229" MODIFIED="1341151349598"/>
<node TEXT="&#x161;patn&#xe9; n&#xe1;lady" ID="ID_1399489586" CREATED="1341151349911" MODIFIED="1341151355070"/>
<node TEXT="negativita" ID="ID_1714148624" CREATED="1341151355423" MODIFIED="1341151358201"/>
<node TEXT="zoufalstv&#xed;" ID="ID_544640948" CREATED="1341151358586" MODIFIED="1341151361776"/>
<node TEXT="sebevra&#x17e;edn&#xe9; sklony" ID="ID_874153254" CREATED="1341151362053" MODIFIED="1341151367783">
<node TEXT="zab&#xed;r&#xe1; rychle" ID="ID_1282076373" CREATED="1341151369902" MODIFIED="1341151375808"/>
</node>
<node TEXT="tich&#xfd; zak&#x159;iknut&#xfd; &#x10d;lov&#x11b;k" ID="ID_1152533957" CREATED="1341151377333" MODIFIED="1341151391125">
<node TEXT="postr&#xe1;d&#xe1; radost a energii" ID="ID_1986738778" CREATED="1341151396280" MODIFIED="1341151406249"/>
</node>
<node TEXT="p&#x159;&#xed;sn&#xfd; rigidn&#xed; zap&#x161;kl&#xed;k" ID="ID_1050089281" CREATED="1341151408581" MODIFIED="1341151421626"/>
</node>
<node TEXT="Kontraindikace" POSITION="right" ID="ID_1775293206" CREATED="1341151430308" MODIFIED="1341151436681" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="ned&#xe1;v&#xe1; se na noc" ID="ID_908315628" CREATED="1341151437836" MODIFIED="1341151444155"/>
<node TEXT="nespavost" ID="ID_1750490243" CREATED="1341151485933" MODIFIED="1341151488728"/>
<node TEXT="krv&#xe1;cen&#xed;" ID="ID_1488801144" CREATED="1341151554796" MODIFIED="1341151558369"/>
<node TEXT="siln&#xe1; MS" ID="ID_78319674" CREATED="1341151558874" MODIFIED="1341151561536"/>
<node TEXT="energeti&#x10d;t&#xed; hyperaktivn&#xed; lid&#xe9;" ID="ID_39774732" CREATED="1341151564963" MODIFIED="1341151584611"/>
<node TEXT="vztek" ID="ID_642678836" CREATED="1341151585254" MODIFIED="1341151587690"/>
<node TEXT="neklid" ID="ID_861975039" CREATED="1341151587996" MODIFIED="1341151589560"/>
<node TEXT="&#xfa;zkost" ID="ID_1266572837" CREATED="1341151589848" MODIFIED="1341151597945"/>
</node>
<node TEXT="Res" POSITION="right" ID="ID_1174220492" CREATED="1341215223799" MODIFIED="1341215230062" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="ORANGE:  This color works to enliven, rejuvenate and heal. It is the color of youth and vitality. We can tap into its energy whenever we eat food that is orange, such as squash, citrus, carrots. This food carries the vibration of the color and feeds the energy center that has to do with movement, emotions, and well being.&#xa;The homeopathic color remedy orange is used to revivify and revitalize worn down and exhausted systems. It focuses on the life energy systems of the body and supplies energy to the sexual organs which create life.&#xa;Orange is a stimulating color that offers excitement and joy as part of its components. When we need a lift this is the color to use to raise our spirits and move us forward. If you have a serious medical condition, please consult your doctor.&#xa; &#xa;Chakra: The Sacral Chakra is governed by the color orange. It stimulates this vital center located two inches below the navel and two inches inside the body. It brings healing energy, vitality and warmth to the lower abdomen. It deals with the issues of ease, well being, abundance and acceptance. Whenever we are not connected with the truth of this center we fall into the Martyr archetype where we become too strict and punishing, leaching sweetness out of our lives. When we find balance we embody the Empress or Emperor archetype. This archetype has a proportionate sense of balance and sweetness in its life which makes them joyful and easy to be around.&#xa;The qualities of the Sacral Chakra are:  physical well being, self acceptance, ease and pleasure, and abundance.&#xa; &#xa;Mentals: This is a remedy suited for anyone lacking a sense of personal well being. It offers people who have a negative attitude towards their bodies and sexuality an opportunity to open up and develop a healthy degree of self acceptance. It brings a real sense of pleasure and stimulates an appetite for life. It can also stimulate a sense of creativity, playfulness and fun.&#xa;Contraindications: Orange is not to be used by people who are persistently restless or anxious, nor should it be used at night. If you have a serious medical condition, please consult your doctor.&#xa; &#xa;Physicals: Orange is a remedy that can be used for bowel problems, infertility, period irregularities, slow onset of menstruation, anorexia, appetite disorders, when people have little or no appetite or are having food cravings. It is good for post viral infections, auto-immune deficiency disease and the after effects of shock or trauma. If you have any of these conditions please consult your doctor.&#xa;Contraindications: It should not be used by people who have an excess of rage or anger. It is not suited to someone with an abundance of energy or who have trouble resting or finding a quiet space. It can over-stimulate their vitality and lead to restlessness. Consult your doctor if you have a serious condition before attempting this treatment.&#xa; &#xa;General Symptoms:  This is a color that can be used to boost the immune system by repeated use for several weeks in a medium or low potency. It has helped people to regain their energy when they have been stressed or exhausted.&#xa;Contraindications: Do not give to people who are overly stimulated or who have a serious medical condition and are on medication." ID="ID_872252923" CREATED="1341215257978" MODIFIED="1341215262324"/>
</node>
</node>
</map>
