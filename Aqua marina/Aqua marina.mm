<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Aqua marina" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1398503481499"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Mo&#x159;sk&#xe1; voda" STYLE_REF="Remedy" POSITION="left" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1398506426291"/>
<node TEXT="Aq-mar" STYLE_REF="Abbrev" POSITION="left" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1398506442891"/>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="left" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1398811640882" VSHIFT="-10">
<node TEXT="mo&#x159;sk&#xe1; voda" ID="ID_628910470" CREATED="1398504129936" MODIFIED="1398504136662"/>
<node TEXT="obsahuje t&#xe9;m&#x11b;&#x159; v&#x161;echny prvky per soustavy" ID="ID_1285987058" CREATED="1398504136993" MODIFIED="1398504152257"/>
</node>
<node TEXT="Trituace" STYLE_REF="Subtopic" POSITION="left" ID="ID_1275465186" CREATED="1357168572522" MODIFIED="1398811636571" VSHIFT="-10">
<node TEXT="30C" ID="ID_1248096963" CREATED="1398504158816" MODIFIED="1398504162119"/>
<node TEXT="bolest hlavy" ID="ID_1131116202" CREATED="1398503485218" MODIFIED="1398503503012">
<node TEXT="temeno" ID="ID_874087474" CREATED="1398503611107" MODIFIED="1398503614646"/>
</node>
<node TEXT="nen&#xed; takov&#xfd; smutek jako u Nat-m" ID="ID_1300095870" CREATED="1398503617717" MODIFIED="1398503637266"/>
<node TEXT="sjednoceni s &#x17e;ivlem" ID="ID_250645404" CREATED="1398503679029" MODIFIED="1398503686611"/>
<node TEXT="tlak na &#x161;t&#xed;tn&#xe9; &#x17e;l&#xe1;ze" ID="ID_1997118741" CREATED="1398503704957" MODIFIED="1398503713339"/>
<node TEXT="nesoust&#x159;ed&#x11b;nost" ID="ID_931218299" CREATED="1398503714261" MODIFIED="1398503718911"/>
<node TEXT="teplo" ID="ID_342483592" CREATED="1398503719546" MODIFIED="1398503960781">
<font ITALIC="true"/>
</node>
<node TEXT="brn&#xed; mi p&#x159;edn&#xed; zuby" ID="ID_737799546" CREATED="1398503961553" MODIFIED="1398503978046">
<font ITALIC="true"/>
</node>
<node TEXT="pocit m&#x11b;kka" ID="ID_247461517" CREATED="1398503981355" MODIFIED="1398504005525">
<font ITALIC="true"/>
</node>
<node TEXT="klid" ID="ID_541070722" CREATED="1398503997150" MODIFIED="1398504002500">
<font ITALIC="true"/>
</node>
<node TEXT="chv&#x11b;n&#xed; v horn&#xed; polovin&#x11b; t&#x11b;la" ID="ID_222554446" CREATED="1398504035475" MODIFIED="1398504085349">
<font ITALIC="true"/>
</node>
<node TEXT="bolest hlavy v &#x10d;ele" ID="ID_1448738748" CREATED="1398504065725" MODIFIED="1398504081584">
<font ITALIC="true"/>
</node>
<node TEXT="svoboda" ID="ID_564527228" CREATED="1398504169141" MODIFIED="1398504183170"/>
<node TEXT="s&#xed;la" ID="ID_1253355120" CREATED="1398504174024" MODIFIED="1398504176303"/>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="left" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<node TEXT="solen&#xed;" ID="ID_1708547756" CREATED="1398505869168" MODIFIED="1398505879579">
<node TEXT="osol&#xed; si j&#xed;dlo ani&#x17e; by ochutnal" ID="ID_646884786" CREATED="1398505881566" MODIFIED="1398505892202"/>
</node>
<node TEXT="vhodn&#xfd; l&#xe9;k pro puber&#x165;&#xe1;ky" ID="ID_1841248394" CREATED="1398507150139" MODIFIED="1398507160265"/>
<node TEXT="z&#xe1;vrat&#x11b;" ID="ID_315324464" CREATED="1398517879949" MODIFIED="1398517890127"/>
<node TEXT="to&#x10d;en&#xed; hlavy" ID="ID_287639019" CREATED="1398517891395" MODIFIED="1398517911388"/>
<node TEXT="bolesti ve sp&#xe1;nc&#xed;ch" ID="ID_904934093" CREATED="1398517915039" MODIFIED="1398517920532"/>
<node TEXT="o&#x10d;i" ID="ID_823370152" CREATED="1398517952923" MODIFIED="1398517958108">
<node TEXT="p&#xe1;len&#xed;" ID="ID_429397472" CREATED="1398517959682" MODIFIED="1398517962808"/>
</node>
<node TEXT="&#xfa;sta" ID="ID_1279143561" CREATED="1398518513837" MODIFIED="1398518516528">
<node TEXT="bolestiv&#xe9; patro" ID="ID_1093074936" CREATED="1398518519658" MODIFIED="1398518524511"/>
</node>
<node TEXT="bolesti" ID="ID_475269783" CREATED="1398518534182" MODIFIED="1398518537764">
<node TEXT="p&#xe1;liv&#xe9;" ID="ID_60269502" CREATED="1398518538946" MODIFIED="1398518541218"/>
<node TEXT="mandle" ID="ID_144544587" CREATED="1398518542460" MODIFIED="1398518545662"/>
<node TEXT="kone&#x10d;n&#xed;k" ID="ID_797532624" CREATED="1398518546110" MODIFIED="1398518549169"/>
<node TEXT="svaly" ID="ID_324499154" CREATED="1398518717074" MODIFIED="1398518719279"/>
</node>
<node TEXT="z&#xe1;da" ID="ID_532907622" CREATED="1398518596326" MODIFIED="1398518609876">
<node TEXT="bolesti" ID="ID_1231897612" CREATED="1398518613988" MODIFIED="1398518621509"/>
<node TEXT="mezi lopatkami" ID="ID_1365353030" CREATED="1398518628792" MODIFIED="1398518633159"/>
</node>
<node TEXT="vyr&#xe1;&#x17e;ky" ID="ID_887024775" CREATED="1398518643759" MODIFIED="1398518649362">
<node TEXT="sv&#x11b;div&#xe9;" ID="ID_900576128" CREATED="1398518651688" MODIFIED="1398518658503"/>
<node TEXT="na kon&#x10d;etin&#xe1;ch" ID="ID_89059698" CREATED="1398518658951" MODIFIED="1398518663382"/>
</node>
<node TEXT="averze k vod&#x11b;" ID="ID_1857483956" CREATED="1398518698252" MODIFIED="1398518703000"/>
<node TEXT="TBC" ID="ID_306975584" CREATED="1398518732741" MODIFIED="1398518735120"/>
</node>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="left" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690">
<node TEXT="" STYLE_REF="Agg" ID="ID_1855539537" CREATED="1398811739491" MODIFIED="1398811743361">
<node TEXT="o&#x10d;ek&#xe1;v&#xe1;n&#xed;" ID="ID_958161286" CREATED="1398811744251" MODIFIED="1398811767338"/>
<node TEXT="obchodov&#xe1;n&#xed;, finan&#x10d;n&#xed; ztr&#xe1;ta" ID="ID_1504342676" CREATED="1398811768099" MODIFIED="1398811797050"/>
</node>
<node TEXT="" STYLE_REF="Amel" ID="ID_752563174" CREATED="1398811845326" MODIFIED="1398811847479">
<node TEXT="samota" ID="ID_1314694365" CREATED="1398811848370" MODIFIED="1398811851053"/>
<node TEXT="pocen&#xed;" ID="ID_709317690" CREATED="1398811851967" MODIFIED="1398811853946"/>
<node TEXT="j&#xed;dlo" ID="ID_1974542286" CREATED="1398811854278" MODIFIED="1398811856628"/>
<node TEXT="pohyb" ID="ID_1396832839" CREATED="1398811867427" MODIFIED="1398811869616"/>
</node>
</node>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="left" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1398811528815" HGAP="-160" VSHIFT="30">
<node TEXT="m&#xe1; pocit,&#xa;&#x17e;e je jin&#xe1; ne&#x17e; ostatn&#xed;" ID="ID_98780280" CREATED="1398504641027" MODIFIED="1398504850201">
<node TEXT="jin&#xe9; hodnoty" ID="ID_1724023878" CREATED="1398504654741" MODIFIED="1398504658883"/>
<node TEXT="nepovy&#x161;uje se" ID="ID_772763423" CREATED="1398504741035" MODIFIED="1398504776543"/>
<node TEXT="&#x10d;&#xed;m je jeho patologie v&#x11b;t&#x161;&#xed;,&#xa;t&#xed;m v&#x11b;t&#x161;&#xed; z toho m&#xe1; mindr&#xe1;k" ID="ID_743186456" CREATED="1398504811883" MODIFIED="1398504831645">
<node TEXT="chce svou odli&#x161;nost uk&#xe1;zat" ID="ID_1260510944" CREATED="1398504917812" MODIFIED="1398504941579">
<node TEXT="ale jenom n&#x11b;komu,&#xa;kdo je ho schopen pochopit" STYLE_REF="Comment" ID="ID_1296973308" CREATED="1398504943199" MODIFIED="1398811607145"/>
</node>
<node TEXT="st&#xe1;hne se" ID="ID_1332729720" CREATED="1398504949671" MODIFIED="1398504955434"/>
</node>
<node TEXT="chce aby se na&#x161;el aspo&#x148; n&#x11b;kdo, kdo mu bude rozum&#x11b;t" ID="ID_1028610318" CREATED="1398505272216" MODIFIED="1398505286457"/>
<node TEXT="m&#x16f;&#x17e;e se st&#xe1;t ob&#x11b;t&#xed; &#x161;ikany" ID="ID_1748708905" CREATED="1398505225997" MODIFIED="1398505244049"/>
<node TEXT="je si v&#x11b;dom&#xe1; toho, &#x17e;e ji ostatn&#xed; pozoruj&#xed;" ID="ID_134239134" CREATED="1398505682836" MODIFIED="1398505701448">
<node TEXT="prost&#x11b; to tak je" STYLE_REF="Comment" ID="ID_472268303" CREATED="1398505703524" MODIFIED="1398505712380"/>
<node TEXT="Calc, Bar-c" STYLE_REF="Differs" ID="ID_1770945961" CREATED="1398505718137" MODIFIED="1398505802322">
<node TEXT="sna&#x17e;&#xed; se tomu zabr&#xe1;nit" STYLE_REF="Comment" ID="ID_302819183" CREATED="1398505734003" MODIFIED="1398811586203"/>
<node TEXT="boj&#xed; se, &#x17e;e je ostatn&#xed; pozoruj&#xed;" STYLE_REF="Comment" ID="ID_1292187789" CREATED="1398505787113" MODIFIED="1398811588948"/>
</node>
</node>
<node TEXT="tou&#x17e;&#xed; aby byla p&#x159;ijata" ID="ID_1746378353" CREATED="1398505902153" MODIFIED="1398505911867">
<font BOLD="true"/>
</node>
<node TEXT="poj&#x10f;te ke m&#x11b;, r&#xe1;d v&#xe1;m uk&#xe1;&#x17e;u sv&#x16f;j sv&#x11b;t" ID="ID_1247074096" CREATED="1398506075583" MODIFIED="1398506091869"/>
<node TEXT="nem&#xe1; pot&#x159;ebu se p&#x159;izp&#x16f;sobovat" ID="ID_1392099395" CREATED="1398506118921" MODIFIED="1398506131671"/>
<node TEXT="m&#xe1; tendenci se fixovat na lidi, kte&#x159;&#xed; mu rozum&#xed;" ID="ID_1729591767" CREATED="1398506456977" MODIFIED="1398506474413">
<node TEXT="i kdy&#x17e; u&#x17e; tomu tak nen&#xed;" STYLE_REF="Comment" ID="ID_1115227171" CREATED="1398506484288" MODIFIED="1398506499783"/>
</node>
</node>
<node TEXT="p&#x159;itahuj&#xed; ho" ID="ID_1926862094" CREATED="1398505037033" MODIFIED="1398505048646">
<node TEXT="alternativn&#xed;, ezoterick&#xe9; sm&#x11b;ry" ID="ID_968552258" CREATED="1398505050355" MODIFIED="1398505060954"/>
</node>
<node TEXT="uzav&#x159;enost" ID="ID_1995886970" CREATED="1398505078160" MODIFIED="1398505081908">
<node TEXT="na prvn&#xed; kontakt mnohem p&#x159;&#xed;jemn&#x11b;j&#x161;&#xed; ne&#x17e; Nat-m" ID="ID_112887669" CREATED="1398505109564" MODIFIED="1398505133908"/>
<node TEXT="vypadaj&#xed; hodn&#xed;" ID="ID_1648573473" CREATED="1398505186208" MODIFIED="1398505190530"/>
<node TEXT="lehce z&#x17e;en&#x161;til&#xed;" ID="ID_1706157731" CREATED="1398505191041" MODIFIED="1398505204463"/>
<node TEXT="empati&#x10d;t&#xed;" ID="ID_107301128" CREATED="1398505206412" MODIFIED="1398505214676"/>
<node TEXT="vyhovuje jim samota" ID="ID_1807526762" CREATED="1398507256567" MODIFIED="1398507262205">
<node TEXT="spole&#x10d;nost nevyhled&#xe1;vaj&#xed;" ID="ID_500569682" CREATED="1398507263719" MODIFIED="1398507271920"/>
</node>
</node>
<node TEXT="&quot;kdy&#x17e; se mnou chcete n&#x11b;co m&#xed;t, mus&#xed;te poznat, kdo jsem&quot;" ID="ID_427886189" CREATED="1398506362718" MODIFIED="1398506384908"/>
<node TEXT="tvrdohlavost" ID="ID_625869412" CREATED="1398506901357" MODIFIED="1398506905457">
<node TEXT="stoj&#xed; si za sv&#xfd;m" ID="ID_618827113" CREATED="1398506895070" MODIFIED="1398506899557"/>
</node>
<node TEXT="jemn&#xe1; a zatvrzel&#xe1;" ID="ID_23784594" CREATED="1398507088517" MODIFIED="1398507094493"/>
<node TEXT="nekonfliktn&#xed;" ID="ID_1816439346" CREATED="1398507094932" MODIFIED="1398507100974"/>
</node>
<node TEXT="Podobn&#xe9; l&#xe9;ky" STYLE_REF="Subtopic" POSITION="right" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1398811626651" HGAP="-120" VSHIFT="-10">
<node TEXT="Nat-m" ID="ID_1077691046" CREATED="1398504549518" MODIFIED="1398504554719">
<node TEXT="vyrobeno odpa&#x159;ov&#xe1;n&#xed;m mo&#x159;sk&#xe9; vody" STYLE_REF="Comment" ID="ID_1238290795" CREATED="1398504559260" MODIFIED="1398504573893"/>
<node TEXT="zran&#x11b;n&#xe9;" STYLE_REF="Differs" ID="ID_1288190126" CREATED="1398504576147" MODIFIED="1398504583848"/>
<node TEXT="nepochopen&#xe9;" STYLE_REF="Differs" ID="ID_579441002" CREATED="1398504585405" MODIFIED="1398504597520"/>
<node TEXT="nechov&#xe1; se tak aby byl p&#x159;ijat" STYLE_REF="Differs" ID="ID_1280483775" CREATED="1398505936578" MODIFIED="1398505947281">
<node TEXT="fixace na svoje zran&#x11b;n&#xed;" STYLE_REF="Comment" ID="ID_812728938" CREATED="1398505957691" MODIFIED="1398505967435"/>
</node>
<node TEXT="nechce se sd&#xed;let" STYLE_REF="Differs" ID="ID_1552977012" CREATED="1398505984748" MODIFIED="1398506008334"/>
<node TEXT="m&#xed;&#x148; plach&#xe1;" STYLE_REF="Differs" ID="ID_370411149" CREATED="1398506659794" MODIFIED="1398506779574"/>
</node>
<node TEXT="Nat-c" ID="ID_1891199042" CREATED="1398506252620" MODIFIED="1398506259975"/>
<node TEXT="Phos" ID="ID_1267577203" CREATED="1398506861100" MODIFIED="1398506865506"/>
<node TEXT="Kali-p" ID="ID_327909957" CREATED="1398506867733" MODIFIED="1398506873150">
<node TEXT="&#x159;&#xe1;d" STYLE_REF="Differs" ID="ID_1566888464" CREATED="1398507048201" MODIFIED="1398507056485"/>
<node TEXT="p&#x159;izp&#x16f;sobivost" STYLE_REF="Differs" ID="ID_797006823" CREATED="1398507060064" MODIFIED="1398507066793"/>
</node>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="right" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729">
<node TEXT="Mind; morning; agg." ID="ID_1432820634" CREATED="1398507390092" MODIFIED="1398507390092"/>
<node TEXT="Mind; afternoon; agg." ID="ID_126486075" CREATED="1398507407868" MODIFIED="1398507407868"/>
<node TEXT="Mind; absent-mindedness" ID="ID_1961927566" CREATED="1398507426166" MODIFIED="1398507426166"/>
<node TEXT="Mind; alone; amel." ID="ID_1638991299" CREATED="1398507438463" MODIFIED="1398507438463"/>
<node TEXT="Mind; company; amel." ID="ID_1045320933" CREATED="1398507454204" MODIFIED="1398507454204"/>
<node TEXT="Mind; company; aversion to" ID="ID_1418855433" CREATED="1398507459605" MODIFIED="1398507459605"/>
<node TEXT="Mind; anxiety; drinks, cold, amel.; ice cold" ID="ID_645935738" CREATED="1398507481627" MODIFIED="1398507481627"/>
<node TEXT="Mind; anxiety; motion; amel." ID="ID_1977345978" CREATED="1398507498683" MODIFIED="1398507498683"/>
<node TEXT="Mind; anxiety; time is set, when a" ID="ID_336941807" CREATED="1398507516032" MODIFIED="1398507516032"/>
<node TEXT="Mind; anxiety; wind on head amel." ID="ID_1503697674" CREATED="1398507530734" MODIFIED="1398507530734"/>
<node TEXT="Mind; anxiety; money, financial matters, about" ID="ID_1535994553" CREATED="1398507552495" MODIFIED="1398507552495"/>
<node TEXT="Mind; confidence; want of self" ID="ID_853698869" CREATED="1398507574274" MODIFIED="1398507574274"/>
<node TEXT="Mind; contented; oneself, with" ID="ID_1384440754" CREATED="1398507598549" MODIFIED="1398507598549"/>
<node TEXT="Mind; delusions, imaginations; failure, he is a" ID="ID_1048868164" CREATED="1398507619716" MODIFIED="1398507619716"/>
<node TEXT="Mind; delusions, imaginations; insignificant, he is" ID="ID_1240220117" CREATED="1398507629852" MODIFIED="1398507629852"/>
<node TEXT="Mind; delusions, imaginations; tormented, he is" ID="ID_864893680" CREATED="1398507636645" MODIFIED="1398507636645"/>
<node TEXT="Mind; delusions, imaginations; watched, that she is being" ID="ID_248895341" CREATED="1398507646363" MODIFIED="1398507646363"/>
<node TEXT="Mind; discouraged; resignation" ID="ID_430188685" CREATED="1398507672505" MODIFIED="1398507672505"/>
<node TEXT="Mind; dreams; amorous; advances, unwanted" ID="ID_192347159" CREATED="1398507690526" MODIFIED="1398507690526"/>
<node TEXT="Mind; dreams; romantic" ID="ID_940366448" CREATED="1398507706057" MODIFIED="1398507706057"/>
<node TEXT="Mind; dreams; water" ID="ID_1325231505" CREATED="1398507713934" MODIFIED="1398507713934"/>
<node TEXT="Mind; fancies; lascivious" ID="ID_1945063607" CREATED="1398507729938" MODIFIED="1398507729938"/>
<node TEXT="Mind; fear; looked at" ID="ID_1050664469" CREATED="1398507745976" MODIFIED="1398507745976"/>
<node TEXT="Mind; fear; observed, of her condition being" ID="ID_1201529223" CREATED="1398507765649" MODIFIED="1398507765649"/>
<node TEXT="Mind; forsaken feeling" ID="ID_1993966834" CREATED="1398507798116" MODIFIED="1398507798116"/>
<node TEXT="Mind; grief; prolonged" ID="ID_1243360430" CREATED="1398507860480" MODIFIED="1398507860480"/>
<node TEXT="Mind; grief; resigned" ID="ID_1109291914" CREATED="1398507875321" MODIFIED="1398507875321"/>
<node TEXT="Mind; homesickness, nostalgia; weep, with inclination to" ID="ID_163164928" CREATED="1398507895093" MODIFIED="1398507895093"/>
<node TEXT="Mind; independence" ID="ID_1361546378" CREATED="1398507909577" MODIFIED="1398507909577"/>
<node TEXT="Mind; insecurity" ID="ID_781657206" CREATED="1398507920826" MODIFIED="1398507920826"/>
<node TEXT="Mind; introverted" ID="ID_876690524" CREATED="1398507931371" MODIFIED="1398507931371"/>
<node TEXT="Mind; looked at; cannot bear to be, agg." ID="ID_1678161499" CREATED="1398507945765" MODIFIED="1398507945765"/>
<node TEXT="Mind; prostration of mind; morning" ID="ID_1232578252" CREATED="1398507961215" MODIFIED="1398507961215"/>
<node TEXT="Mind; reserved; displeasure" ID="ID_394483827" CREATED="1398507976982" MODIFIED="1398507976982"/>
<node TEXT="Mind; sadness; others, for" ID="ID_1412355214" CREATED="1398507993480" MODIFIED="1398507993480"/>
<node TEXT="Mind; self-satisfied" ID="ID_1092482632" CREATED="1398508011529" MODIFIED="1398508011529"/>
<node TEXT="Mind; self-sufficient" ID="ID_1430174145" CREATED="1398508025244" MODIFIED="1398508025244"/>
<node TEXT="Mind; selflessness" ID="ID_1110584906" CREATED="1398508033009" MODIFIED="1398508033009"/>
<node TEXT="Mind; vulnerable, emotionally" ID="ID_1883165426" CREATED="1398508047190" MODIFIED="1398508047190"/>
<node TEXT="Mind; timidity; bashful" ID="ID_523407181" CREATED="1398508060089" MODIFIED="1398508060089"/>
</node>
<node TEXT="" POSITION="right" ID="ID_631753618" CREATED="1398811478750" MODIFIED="1398811504151">
<hook URI="aqua-marina.jpg" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</map>
