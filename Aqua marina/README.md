
Aqua marina
==============

![mapa](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Aqua%20marina/Aqua%20marina.svg)

 - [PDF](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Aqua%20marina/Aqua%20marina.pdf)
 - [Map](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Aqua%20marina/Aqua%20marina.mm)
 - [SVG](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Aqua%20marina/Aqua%20marina.svg)

Rubriky
-------
 - Mind; absent-mindedness 
 - Mind; afternoon; agg. [[Aster](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Asterias rubens)]
 - Mind; alone; amel. 
 - Mind; anxiety; drinks, cold, amel.; ice cold 
 - Mind; anxiety; money, financial matters, about [[Onc-t](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Onchorynchus)]
 - Mind; anxiety; motion; amel. 
 - Mind; anxiety; time is set, when a [[Arg-n](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/ArgentumNitricum)]
 - Mind; anxiety; wind on head amel. 
 - Mind; company; amel. 
 - Mind; company; aversion to 
 - Mind; confidence; want of self 
 - Mind; contented; oneself, with 
 - Mind; delusions, imaginations; failure, he is a [[Onc-t](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Onchorynchus)]
 - Mind; delusions, imaginations; insignificant, he is 
 - Mind; delusions, imaginations; tormented, he is 
 - Mind; delusions, imaginations; watched, that she is being 
 - Mind; discouraged; resignation [[Brass](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Brassica)]
 - Mind; dreams; amorous; advances, unwanted 
 - Mind; dreams; romantic 
 - Mind; dreams; water 
 - Mind; fancies; lascivious [[Lac-cpr](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Lac caprinum)]
 - Mind; fear; looked at 
 - Mind; fear; observed, of her condition being 
 - Mind; forsaken feeling 
 - Mind; grief; prolonged 
 - Mind; grief; resigned 
 - Mind; homesickness, nostalgia; weep, with inclination to 
 - Mind; independence 
 - Mind; insecurity 
 - Mind; introverted [[Alum](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Alumina)]
 - Mind; looked at; cannot bear to be, agg. [[Tarent](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/TarentulaHispanica)]
 - Mind; morning; agg. 
 - Mind; prostration of mind; morning 
 - Mind; reserved; displeasure 
 - Mind; sadness; others, for 
 - Mind; self-satisfied 
 - Mind; self-sufficient 
 - Mind; selflessness 
 - Mind; timidity; bashful 
 - Mind; vulnerable, emotionally 
    