
Homarus gammarus
==============

![mapa](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Homarus%20gammarus/Homarus%20gammarus.svg)

 - [PDF](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Homarus%20gammarus/Homarus%20gammarus.pdf)
 - [Map](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Homarus%20gammarus/Homarus%20gammarus.mm)
 - [SVG](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Homarus%20gammarus/Homarus%20gammarus.svg)

Rubriky
-------
 - Generalities; mucous membranes 
 - Generalities; touch; agg. 
 - Generalities; weakness; morning 
 - Generalities; weakness; motion; agg. [[Murx](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Murex purpurea)]
 - Generalities; weariness; morning; waking, on 
 - Mind; anger; violent [[Onc-t](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Onchorynchus)]
 - Mind; delusions, imaginations 
 - Mind; delusions, imaginations; move, moving; he cannot 
 - Mind; dullness 
 - Mind; frightened easily 
 - Mind; rage, fury [[Astac](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Astacus fluviatilis)]
 - Mind; restlessness, nervousness; move; aversion to 
 - Stomach; pain 
    