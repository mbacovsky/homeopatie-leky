<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Homarus gammarus" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1398597547954"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="left" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1399065624871" VSHIFT="-20">
<node TEXT="siln&#xe1; pov&#x11b;r&#x10d;ivost" ID="ID_1207158580" CREATED="1398597817538" MODIFIED="1398597827156"/>
<node TEXT="druh ochrany" ID="ID_240004186" CREATED="1398597872730" MODIFIED="1398597882251">
<node TEXT="ne&#x17e;iv&#xfd;" ID="ID_265944903" CREATED="1398597918130" MODIFIED="1398597928336"/>
<node TEXT="neskute&#x10d;n&#xfd;" ID="ID_261943468" CREATED="1398597928704" MODIFIED="1398597933904"/>
<node TEXT="talismany" ID="ID_1940385429" CREATED="1398597934862" MODIFIED="1398597944609"/>
<node TEXT="amulety" ID="ID_168115845" CREATED="1398597945084" MODIFIED="1398597949143"/>
<node TEXT="n&#x11b;co temn&#xe9;ho" ID="ID_1822684024" CREATED="1398598053941" MODIFIED="1398598060814"/>
<node TEXT="je to stra&#x161;ideln&#xe9;, ale chr&#xe1;n&#xed; ho to" ID="ID_1718193201" CREATED="1398598061379" MODIFIED="1398598073858"/>
</node>
<node TEXT="n&#x11b;kdo, kdo v&#x11b;&#x159;&#xed; sv&#xfd;m st&#xed;n&#x16f;m" ID="ID_972282287" CREATED="1398598008546" MODIFIED="1398598020297"/>
<node TEXT="st&#xed;ny" ID="ID_118019191" CREATED="1398598189433" MODIFIED="1398598206328">
<node TEXT="v r&#x16f;zn&#xfd;ch podob&#xe1;ch a v&#xfd;znamech" ID="ID_1689897223" CREATED="1398598210543" MODIFIED="1398598224624"/>
</node>
<node TEXT="nevyzr&#xe1;lost" ID="ID_1058215834" CREATED="1398598236989" MODIFIED="1398598242401">
<node TEXT="nedosp&#x11b;lost" ID="ID_1020336974" CREATED="1398598243736" MODIFIED="1398598247455"/>
<node TEXT="d&#x11b;tskost" ID="ID_968392750" CREATED="1398598247841" MODIFIED="1398598250805"/>
</node>
<node TEXT="bezpe&#x10d;n&#xe9; prost&#x159;ed&#xed;" ID="ID_1663787436" CREATED="1398599086640" MODIFIED="1398599093414">
<node TEXT="omezen&#xe9;" ID="ID_1954688085" CREATED="1398599094516" MODIFIED="1398599105396"/>
</node>
<node TEXT="uspokojen&#xed; &#x17e;ivotn&#xed;ch pot&#x159;eb" ID="ID_177842642" CREATED="1398599181818" MODIFIED="1398599190259">
<node TEXT="j&#xed;dlo" ID="ID_1106345258" CREATED="1398599191523" MODIFIED="1398599193800"/>
<node TEXT="domov" ID="ID_205019350" CREATED="1398599194141" MODIFIED="1398599196088"/>
<node TEXT="pen&#xed;ze" ID="ID_2645494" CREATED="1398599196331" MODIFIED="1398599198516"/>
<node TEXT="bez velk&#xfd;ch ambic&#xed;" ID="ID_1551250775" CREATED="1398599198911" MODIFIED="1398599206960"/>
</node>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="left" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<node TEXT="mluv&#xed; s imagin&#xe1;ln&#xed;mi v&#x11b;cmi nebo p&#x159;&#xe1;tely" ID="ID_233176665" CREATED="1398597829131" MODIFIED="1398597843785"/>
<node TEXT="siln&#xfd; strach ze tmy" ID="ID_729098249" CREATED="1398597988902" MODIFIED="1398597996103"/>
<node TEXT="b&#xfd;vaj&#xed; zdrav&#xed;" ID="ID_1419301514" CREATED="1398598388293" MODIFIED="1398598414715"/>
<node TEXT="ochablost" ID="ID_419543077" CREATED="1398598942054" MODIFIED="1398598951782"/>
<node TEXT="obezita" ID="ID_402541740" CREATED="1398598952168" MODIFIED="1398598955332">
<node TEXT="i se tak vn&#xed;m&#xe1;" ID="ID_70119403" CREATED="1398598970639" MODIFIED="1398598977362"/>
</node>
<node TEXT="pocen&#xed;" ID="ID_1703735737" CREATED="1398598955807" MODIFIED="1398598958001"/>
<node TEXT="ml&#xe9;ko" ID="ID_803660455" CREATED="1398599028326" MODIFIED="1398599031114">
<node TEXT="kojen&#xed; m&#x16f;&#x17e;e b&#xfd;t&#xa;jako prvotn&#xed; trauma" ID="ID_611852576" CREATED="1398599034579" MODIFIED="1398599075769">
<node TEXT="o&#x10d;ek&#xe1;v&#xe1; ochranu" ID="ID_810569608" CREATED="1398599052982" MODIFIED="1398599061041"/>
<node TEXT="z ml&#xe9;ka je mu zle" ID="ID_286623557" CREATED="1398599062303" MODIFIED="1398599069434"/>
</node>
</node>
<node TEXT="prsa" ID="ID_1300889028" CREATED="1398599120360" MODIFIED="1398599125845">
<node TEXT="otoky prsou" ID="ID_902653844" CREATED="1398599127798" MODIFIED="1398599143806">
<node TEXT="u d&#x11b;t&#xed;" ID="ID_134805926" CREATED="1398599145260" MODIFIED="1398599149330"/>
<node TEXT="u &#x17e;en p&#x159;ed MS" ID="ID_1096717239" CREATED="1398599150570" MODIFIED="1398599154727"/>
<node TEXT="mastodynie" ID="ID_150601821" CREATED="1398599155086" MODIFIED="1398599175355"/>
</node>
</node>
<node TEXT="bipol&#xe1;rn&#xed; porucha" ID="ID_809457186" CREATED="1398599284305" MODIFIED="1398599292020"/>
<node TEXT="k&#x159;e&#x10d;e" ID="ID_694655468" CREATED="1398600368471" MODIFIED="1398600375362">
<node TEXT="za&#x17e;&#xed;vac&#xed;ho traktu" ID="ID_304856098" CREATED="1398600376536" MODIFIED="1398600385329"/>
</node>
</node>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="left" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690">
<node TEXT="" STYLE_REF="Agg" ID="ID_668275457" CREATED="1398598985929" MODIFIED="1398598991435">
<node TEXT="t&#x11b;sn&#xe9; oble&#x10d;en&#xed;" ID="ID_1426502754" CREATED="1398598994628" MODIFIED="1398599002621">
<node TEXT="pono&#x17e;ky" ID="ID_777488056" CREATED="1398599004985" MODIFIED="1398599011006"/>
<node TEXT="boty" ID="ID_1915748828" CREATED="1398599012125" MODIFIED="1398599014174"/>
<node TEXT="bundy" ID="ID_413298982" CREATED="1398599018227" MODIFIED="1398599020550"/>
</node>
<node TEXT="ml&#xe9;ko" ID="ID_535256240" CREATED="1398599022238" MODIFIED="1398599025142"/>
<node TEXT="pohyb" ID="ID_911419346" CREATED="1399064951708" MODIFIED="1399064954280"/>
<node TEXT="dotek" ID="ID_651322294" CREATED="1399065007676" MODIFIED="1399065009935"/>
</node>
<node TEXT="" STYLE_REF="Amel" ID="ID_910853286" CREATED="1399065346973" MODIFIED="1399065349009">
<node TEXT="j&#xed;dlo" ID="ID_460432709" CREATED="1399065351115" MODIFIED="1399065354497"/>
<node TEXT="chladn&#xfd; vzduch" ID="ID_754455975" CREATED="1399065354909" MODIFIED="1399065360325"/>
</node>
</node>
<node TEXT="Humr evropsk&#xfd;" STYLE_REF="Remedy" POSITION="right" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1399065254922"/>
<node TEXT="Hom" STYLE_REF="Abbrev" POSITION="right" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1398597620591"/>
<node TEXT="Podobn&#xe9; l&#xe9;ky" STYLE_REF="Subtopic" POSITION="right" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1357168625505">
<node TEXT="Calc-c" ID="ID_926968445" CREATED="1398599111954" MODIFIED="1398599114589"/>
</node>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="right" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389">
<node TEXT="velk&#xe1; klepeta" ID="ID_1163366166" CREATED="1398597563539" MODIFIED="1398597572590">
<node TEXT="jedno v&#xfd;razn&#x11b; v&#x11b;t&#x161;&#xed;" ID="ID_1502925545" CREATED="1398597573737" MODIFIED="1398597582556">
<node TEXT="dr&#x17e;&#xed;" ID="ID_947214743" CREATED="1399064162762" MODIFIED="1399064167607"/>
<node TEXT="drt&#xed;" ID="ID_984649548" CREATED="1399064168153" MODIFIED="1399064171355"/>
</node>
<node TEXT="druh&#xe9; st&#x159;&#xed;h&#xe1;" ID="ID_427794338" CREATED="1399064174614" MODIFIED="1399064189428"/>
</node>
<node TEXT="plave dozadu" ID="ID_1004399644" CREATED="1398597586429" MODIFIED="1398597590655"/>
<node TEXT="p&#x159;edch&#x16f;dce &#x161;korpiona" ID="ID_1093006124" CREATED="1398597590933" MODIFIED="1398597601511">
<node TEXT="ocas se zm&#x11b;nil na &#x17e;ihadlo" STYLE_REF="Comment" ID="ID_224241409" CREATED="1398597605029" MODIFIED="1398597615903"/>
</node>
<node TEXT="exoskeleton" ID="ID_762537294" CREATED="1399064204605" MODIFIED="1399064208074">
<node TEXT="modr&#xfd;" ID="ID_478917573" CREATED="1399064201076" MODIFIED="1399064204327"/>
<node TEXT="svl&#xe9;k&#xe1;" ID="ID_765983394" CREATED="1399064211939" MODIFIED="1399064216921"/>
</node>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="right" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729">
<node TEXT="Mind; delusions, imaginations; move, moving; he cannot" ID="ID_462120677" CREATED="1399064750612" MODIFIED="1399064750612"/>
<node TEXT="Mind; restlessness, nervousness; move; aversion to" ID="ID_1598440520" CREATED="1399064758454" MODIFIED="1399064758454"/>
<node TEXT="Mind; anger; violent" ID="ID_33199006" CREATED="1399064785756" MODIFIED="1399064785756"/>
<node TEXT="Mind; delusions, imaginations" ID="ID_1178292190" CREATED="1399064803403" MODIFIED="1399064803403"/>
<node TEXT="Mind; dullness" ID="ID_193122979" CREATED="1399064808703" MODIFIED="1399064808703"/>
<node TEXT="Mind; rage, fury" ID="ID_1954091682" CREATED="1399064816009" MODIFIED="1399064816009"/>
<node TEXT="Mind; frightened easily" ID="ID_769449918" CREATED="1399064821983" MODIFIED="1399064821983"/>
<node TEXT="Generalities; weariness; morning; waking, on" ID="ID_609787999" CREATED="1399064928535" MODIFIED="1399064928535"/>
<node TEXT="Generalities; weakness; motion; agg." ID="ID_927413864" CREATED="1399064938423" MODIFIED="1399064938423"/>
<node TEXT="Generalities; weakness; morning" ID="ID_1788257984" CREATED="1399064946674" MODIFIED="1399064946674"/>
<node TEXT="Generalities; touch; agg." ID="ID_389613597" CREATED="1399065003001" MODIFIED="1399065003001"/>
<node TEXT="Generalities; mucous membranes" ID="ID_831334060" CREATED="1399065082800" MODIFIED="1399065082800"/>
<node TEXT="Stomach; pain" ID="ID_1753112114" CREATED="1399065132561" MODIFIED="1399065132561"/>
</node>
<node TEXT="" POSITION="right" ID="ID_1014947168" CREATED="1399064615176" MODIFIED="1399065153953" HGAP="-80" VSHIFT="10">
<hook URI="Homarus-gammarus.jpg" SIZE="0.8321775" NAME="ExternalObject"/>
</node>
</node>
</map>
