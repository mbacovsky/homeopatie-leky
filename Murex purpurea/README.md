
Murex purpurea
==============

![mapa](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Murex%20purpurea/Murex%20purpurea.svg)

 - [PDF](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Murex%20purpurea/Murex%20purpurea.pdf)
 - [Map](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Murex%20purpurea/Murex%20purpurea.mm)
 - [SVG](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Murex%20purpurea/Murex%20purpurea.svg)

Rubriky
-------
 - Generalities; emptiness, hollow sensation; internal 
 - Generalities; faintness, fainting 
 - Generalities; faintness, fainting; frequent 
 - Generalities; pain; weakness, with 
 - Generalities; weakness; exertion; agg. 
 - Generalities; weakness; motion; agg. [[Hom](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Homarus gammarus)]
 - Generalities; weakness; muscles 
 - Generalities; weakness; organs 
 - Mind; affectionate [[Carc](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Carcinosin)]
 - Mind; affectionate; active 
 - Mind; alternating states; physical 
 - Mind; amorous disposition 
 - Mind; anguish; abdominal complaints, in 
 - Mind; anguish; menses; before 
 - Mind; climacteric period, in [[Aster](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Asterias rubens)]
 - Mind; delusions, imaginations; head; floating 
 - Mind; dreams; frightful, nightmare 
 - Mind; dreams; vexatious 
 - Mind; dreams; water; sea, ocean, of 
 - Mind; impatience; pain, from; herself and others, at 
 - Mind; insanity, madness; erotic, sexual 
 - Mind; lasciviousness, lustfulness; women at every touch 
 - Mind; menses; before 
 - Mind; menses; suppressed, from 
 - Mind; no; cannot say 
 - Mind; nymphomania; climacteric period, in 
 - Mind; nymphomania; menses; suppressed 
 - Mind; pain; agg. 
 - Mind; restlessness, nervousness; women, in 
 - Mind; sadness; leucorrhea amel. 
 - Mind; sadness; menses; before 
 - Mind; sadness; sleepiness, with 
 - Mind; sensitive, oversensitive; touch, to; genitals, of 
 - Mind; sleepiness, with 
 - Mind; starting, startled [[Cor-r](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Corallium rubrum), [Spong](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Spongia tosta)]
 - Mind; weeping, tearful mood; incessantly 
    