<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Murex purpurea" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1398525254502"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="left" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389">
<node TEXT="ulita m&#xe1; ostnat&#xe9; v&#xfd;b&#x11b;&#x17e;ky" ID="ID_1295521805" CREATED="1398525501513" MODIFIED="1398525648569">
<node TEXT="br&#xe1;n&#xed; ostatn&#xed;m v p&#x159;ibl&#xed;&#x17e;en&#xed;" ID="ID_723863938" CREATED="1398525665513" MODIFIED="1398525688536"/>
</node>
<node TEXT="pl&#x17e;" ID="ID_698361240" CREATED="1398526273170" MODIFIED="1398526276682"/>
<node TEXT="m&#xe1; nechr&#xe1;n&#x11b;n&#xe9; m&#xed;sto" ID="ID_1318585312" CREATED="1398526288156" MODIFIED="1398526296522"/>
<node TEXT="d&#x11b;lalo se z nich barvivo" ID="ID_392245997" CREATED="1398527308743" MODIFIED="1398527315230"/>
</node>
<node TEXT="Trituace" STYLE_REF="Subtopic" POSITION="left" ID="ID_1275465186" CREATED="1357168572522" MODIFIED="1357168576351">
<node TEXT="siln&#xfd; tlak v lev&#xe9;m uchu" ID="ID_423993593" CREATED="1398525827591" MODIFIED="1398525839692">
<font ITALIC="true"/>
</node>
<node TEXT="mra&#x10d;&#xed;m se" ID="ID_623656922" CREATED="1398525842558" MODIFIED="1398525860536">
<font ITALIC="true"/>
</node>
<node TEXT="na&#x161;tv&#xe1;n&#xed;" ID="ID_840384940" CREATED="1398525870931" MODIFIED="1398525884034">
<font ITALIC="true"/>
</node>
<node TEXT="k&#x159;ehk&#xfd;" ID="ID_1460799386" CREATED="1398526016475" MODIFIED="1398526027616"/>
<node TEXT="poklidn&#xfd;" ID="ID_886253274" CREATED="1398526028047" MODIFIED="1398526031314"/>
</node>
<node TEXT="Podobn&#xe9; l&#xe9;ky" STYLE_REF="Subtopic" POSITION="left" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1357168625505">
<node TEXT="Cocc" ID="ID_1875559311" CREATED="1398527802687" MODIFIED="1398527808139">
<node TEXT="Pot&#x159;eba pe&#x10d;ovat jde do mnohem v&#x11b;t&#x161;&#xed; hloubky" STYLE_REF="Differs" ID="ID_866903058" CREATED="1398528304993" MODIFIED="1398528879657"/>
</node>
<node TEXT="Calc-m" ID="ID_97421957" CREATED="1398527808605" MODIFIED="1398527812235"/>
<node TEXT="Calc-s" ID="ID_1196043435" CREATED="1398528202660" MODIFIED="1398528209587"/>
<node TEXT="Ambr" ID="ID_1093064507" CREATED="1398528181279" MODIFIED="1398528184862">
<node TEXT="nevyzr&#xe1;lost" STYLE_REF="Same" ID="ID_1204350967" CREATED="1398528186922" MODIFIED="1398528195990"/>
</node>
</node>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="left" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690">
<node TEXT="" STYLE_REF="Agg" ID="ID_1269897519" CREATED="1398529302928" MODIFIED="1398529305363">
<node TEXT="MS" ID="ID_44589710" CREATED="1398529308590" MODIFIED="1398529310642"/>
<node TEXT="ve stoje" ID="ID_1285009604" CREATED="1398529315079" MODIFIED="1398529317525"/>
</node>
<node TEXT="" STYLE_REF="Amel" ID="ID_1267274484" CREATED="1398529326761" MODIFIED="1398529328323">
<node TEXT="zk&#x159;&#xed;&#x17e;en&#xed;m nohou" ID="ID_927269085" CREATED="1398529329836" MODIFIED="1398529338119"/>
</node>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="left" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1398642906709" VSHIFT="20">
<node TEXT="Mind; alternating states; physical" ID="ID_1652664765" CREATED="1398529461795" MODIFIED="1398529461795"/>
<node TEXT="Mind; affectionate" ID="ID_203380205" CREATED="1398529480426" MODIFIED="1398529480426"/>
<node TEXT="Mind; affectionate; active" ID="ID_157094384" CREATED="1398529490917" MODIFIED="1398529490917"/>
<node TEXT="Mind; amorous disposition" ID="ID_172454912" CREATED="1398529514260" MODIFIED="1398529514260"/>
<node TEXT="Mind; anguish; abdominal complaints, in" ID="ID_627591944" CREATED="1398529543384" MODIFIED="1398529543384"/>
<node TEXT="Mind; anguish; menses; before" ID="ID_1924947642" CREATED="1398529551142" MODIFIED="1398529551142"/>
<node TEXT="Mind; climacteric period, in" ID="ID_1600384635" CREATED="1398529567381" MODIFIED="1398529567381"/>
<node TEXT="Mind; delusions, imaginations; head; floating" ID="ID_627197050" CREATED="1398529600256" MODIFIED="1398529600256"/>
<node TEXT="Mind; dreams; frightful, nightmare" ID="ID_1067655894" CREATED="1398529622397" MODIFIED="1398529622397"/>
<node TEXT="Mind; dreams; water; sea, ocean, of" ID="ID_1106234802" CREATED="1398529690897" MODIFIED="1398529690897"/>
<node TEXT="Mind; dreams; vexatious" ID="ID_1470024434" CREATED="1398529709449" MODIFIED="1398529709449">
<node TEXT="otravn&#xe9;" STYLE_REF="Comment" ID="ID_1986082457" CREATED="1398529745509" MODIFIED="1398529751672"/>
</node>
<node TEXT="Mind; impatience; pain, from; herself and others, at" ID="ID_1110623332" CREATED="1398529738470" MODIFIED="1398529738470"/>
<node TEXT="Mind; insanity, madness; erotic, sexual" ID="ID_1021108240" CREATED="1398529844607" MODIFIED="1398529844607"/>
<node TEXT="Mind; lasciviousness, lustfulness; women at every touch" ID="ID_947486400" CREATED="1398529876542" MODIFIED="1398529876542"/>
<node TEXT="Mind; menses; before" ID="ID_1779705872" CREATED="1398529900241" MODIFIED="1398529900241"/>
<node TEXT="Mind; menses; suppressed, from" ID="ID_1055700910" CREATED="1398529908772" MODIFIED="1398529908772"/>
<node TEXT="Mind; no; cannot say" ID="ID_1012497673" CREATED="1398529922650" MODIFIED="1398529922650"/>
<node TEXT="Mind; nymphomania; climacteric period, in" ID="ID_1468346012" CREATED="1398529939442" MODIFIED="1398529939442"/>
<node TEXT="Mind; nymphomania; menses; suppressed" ID="ID_1383051548" CREATED="1398529958561" MODIFIED="1398529958561"/>
<node TEXT="Mind; pain; agg." ID="ID_1840780449" CREATED="1398529972299" MODIFIED="1398529972299"/>
<node TEXT="Mind; restlessness, nervousness; women, in" ID="ID_1180936551" CREATED="1398529992378" MODIFIED="1398529992378"/>
<node TEXT="Mind; sadness; leucorrhea amel." ID="ID_249605268" CREATED="1398530012121" MODIFIED="1398530012121"/>
<node TEXT="Mind; sadness; menses; before" ID="ID_189625653" CREATED="1398530028618" MODIFIED="1398530028618"/>
<node TEXT="Mind; sadness; sleepiness, with" ID="ID_1054266560" CREATED="1398530038787" MODIFIED="1398530038787"/>
<node TEXT="Mind; sensitive, oversensitive; touch, to; genitals, of" ID="ID_722137183" CREATED="1398530058395" MODIFIED="1398530058395"/>
<node TEXT="Mind; sleepiness, with" ID="ID_1943155362" CREATED="1398530073850" MODIFIED="1398530073850"/>
<node TEXT="Mind; starting, startled" ID="ID_837499584" CREATED="1398530088892" MODIFIED="1398530088892">
<node TEXT="vyd&#x11b;&#x161;en&#xfd;" STYLE_REF="Comment" ID="ID_584824495" CREATED="1398530218427" MODIFIED="1398530223440"/>
</node>
<node TEXT="Mind; weeping, tearful mood; incessantly" ID="ID_752697094" CREATED="1398530264165" MODIFIED="1398530264165"/>
<node TEXT="Generalities; emptiness, hollow sensation; internal" ID="ID_1949867970" CREATED="1398530305622" MODIFIED="1398530305622"/>
<node TEXT="Generalities; faintness, fainting" ID="ID_1246226552" CREATED="1398530360199" MODIFIED="1398530360199"/>
<node TEXT="Generalities; faintness, fainting; frequent" ID="ID_31718923" CREATED="1398530371115" MODIFIED="1398530371115"/>
<node TEXT="Generalities; pain; weakness, with" ID="ID_1104828625" CREATED="1398530410364" MODIFIED="1398530410364"/>
<node TEXT="Generalities; weakness; exertion; agg." ID="ID_440422374" CREATED="1398530421352" MODIFIED="1398530421352"/>
<node TEXT="Generalities; weakness; motion; agg." ID="ID_1734173075" CREATED="1398530429858" MODIFIED="1398530429858"/>
<node TEXT="Generalities; weakness; organs" ID="ID_1497382586" CREATED="1398530445162" MODIFIED="1398530445162"/>
<node TEXT="Generalities; weakness; muscles" ID="ID_280311128" CREATED="1398530450316" MODIFIED="1398530450316"/>
</node>
<node TEXT="Murx" STYLE_REF="Abbrev" POSITION="right" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1398525480146"/>
<node TEXT="Ostranka jadersk&#xe1;" STYLE_REF="Remedy" POSITION="right" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1398642762332"/>
<node TEXT="Miazma" STYLE_REF="Subtopic" POSITION="right" ID="ID_31190142" CREATED="1357168609677" MODIFIED="1357168614539"/>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="right" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1357168653304">
<node TEXT="siln&#xfd; pohlavn&#xed; pud" ID="ID_1880291984" CREATED="1398525529248" MODIFIED="1398525619546"/>
<node TEXT="udr&#x17e;et si odstup" ID="ID_1506892764" CREATED="1398526327603" MODIFIED="1398527169065">
<font BOLD="true"/>
<node TEXT="&#x159;e&#x161;&#xed; jak si koho p&#x159;ipustit a nep&#x159;ipustit" ID="ID_114782825" CREATED="1398526300810" MODIFIED="1398526314708"/>
<node TEXT="d&#xe1;v&#xe1;n&#xed; je pro n&#x11b; zp&#x16f;sob jak si udr&#x17e;et odstup" ID="ID_669122775" CREATED="1398526369410" MODIFIED="1398526385250"/>
<node TEXT="odv&#xe1;d&#x11b;n&#xed; pozornosti" ID="ID_920763903" CREATED="1398526420982" MODIFIED="1398526435965"/>
<node TEXT="&#x160;eherez&#xe1;da" ID="ID_1001130403" CREATED="1398526460843" MODIFIED="1398526474722"/>
<node TEXT="pln&#xed; sv&#xe9; povinosti na 130%, aby nezavdal podn&#x11b;t ke kritice" ID="ID_627197033" CREATED="1398526596488" MODIFIED="1398527167717">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1892997003" STARTINCLINATION="257;0;" ENDINCLINATION="257;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font BOLD="true"/>
</node>
<node TEXT="dok&#xe1;&#x17e;e se zcela vy&#x10d;erpat" ID="ID_1892997003" CREATED="1398526769652" MODIFIED="1398526786074">
<node TEXT="bl&#xed;zko komatu" STYLE_REF="Comment" ID="ID_244546290" CREATED="1398527116792" MODIFIED="1398527139025"/>
</node>
<node TEXT="pe&#x10d;uje aby p&#x159;e&#x17e;il" ID="ID_403882199" CREATED="1398528324779" MODIFIED="1398528333548"/>
<node TEXT="pe&#x10d;uje, aby to bylo vid&#x11b;t" ID="ID_1767063198" CREATED="1398528481332" MODIFIED="1398528489702"/>
</node>
<node TEXT="aktivita se st&#x159;&#xed;d&#xe1; s vy&#x10d;erp&#xe1;n&#xed;m" ID="ID_855197219" CREATED="1398527022756" MODIFIED="1398527033347">
<node TEXT="neda&#x159;&#xed; se mu naj&#xed;t rovnov&#xe1;hu mezi v&#xfd;dejem a p&#x159;&#xed;jmem" ID="ID_1205206943" CREATED="1398527039583" MODIFIED="1398527070542"/>
</node>
<node TEXT="p&#x16f;sob&#xed; m&#xed;rn&#xfd;m a&#x17e; poddajn&#xfd;m dojmem" ID="ID_944976215" CREATED="1398527149269" MODIFIED="1398527163985"/>
<node TEXT="je d&#x16f;le&#x17e;it&#xe9;, aby jejich pe&#x10d;ov&#xe1;n&#xed; a star&#xe1;n&#xed; vid&#x11b;lo jejich okol&#xed;" ID="ID_1077177782" CREATED="1398527173813" MODIFIED="1398527205082">
<font BOLD="true"/>
</node>
<node TEXT="d&#x11b;l&#xe1;m, co m&#x16f;&#x17e;u, ale m&#xe1;m pocit, &#x17e;e bych m&#x11b;la d&#x11b;lat v&#xed;c" ID="ID_1811067763" CREATED="1398527251077" MODIFIED="1398527270554"/>
<node TEXT="m&#xe1; pocit, &#x17e;e mu jeho okol&#xed; energii bere" ID="ID_668819806" CREATED="1398527363181" MODIFIED="1398527377881"/>
<node TEXT="dojem nezralosti" ID="ID_1824447489" CREATED="1398527477844" MODIFIED="1398527490172">
<node TEXT="jako by nebyl schopn&#xfd; dosp&#x11b;t" ID="ID_351094398" CREATED="1398527491543" MODIFIED="1398527508872"/>
</node>
<node TEXT="po&#x159;eba b&#xfd;t atraktivn&#xed;" ID="ID_243948164" CREATED="1398527509853" MODIFIED="1398527520664">
<node TEXT="db&#xe1; na sv&#x16f;j vzhled" ID="ID_1839581230" CREATED="1398527523842" MODIFIED="1398527542059"/>
</node>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="right" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<node TEXT="gyn" ID="ID_1041959101" CREATED="1398529253405" MODIFIED="1398529255249">
<node TEXT="d&#x11b;loha" ID="ID_31484109" CREATED="1398526082343" MODIFIED="1398526092690">
<node TEXT="pokles" ID="ID_1434238616" CREATED="1398529189579" MODIFIED="1398529191861"/>
<node TEXT="p&#x159;ekrven&#xed;" ID="ID_662659032" CREATED="1398529192640" MODIFIED="1398529197353"/>
<node TEXT="krv&#xe1;cen&#xed;" ID="ID_1142352808" CREATED="1398529197667" MODIFIED="1398529202789"/>
<node TEXT="bolesti" ID="ID_1649191578" CREATED="1398529203129" MODIFIED="1398529205302"/>
<node TEXT="tlaky" ID="ID_1798824110" CREATED="1398529205544" MODIFIED="1398529207626"/>
<node TEXT="myomy" ID="ID_684934135" CREATED="1398529245239" MODIFIED="1398529250494"/>
<node TEXT="onkologie" ID="ID_509948049" CREATED="1398529259783" MODIFIED="1398529268119"/>
</node>
<node TEXT="MS" ID="ID_498743457" CREATED="1398529221267" MODIFIED="1398529223250">
<node TEXT="v&#x161;echny patologie" ID="ID_1113752183" CREATED="1398529224630" MODIFIED="1398529241851"/>
</node>
<node TEXT="extr&#xe9;mn&#xed; vzru&#x161;ivost" ID="ID_323638805" CREATED="1398529342587" MODIFIED="1398529350543"/>
<node TEXT="na uvoln&#x11b;n&#xed; p&#xe1;nevn&#xed;ch kloub&#x16f; v t&#x11b;hotenstvi" ID="ID_1029758986" CREATED="1398529352985" MODIFIED="1398529369704">
<node TEXT="Ambr" ID="ID_1306695360" CREATED="1398529371280" MODIFIED="1398529373888"/>
<node TEXT="Trill-p" ID="ID_1522210602" CREATED="1398529374291" MODIFIED="1398529380541"/>
</node>
</node>
<node TEXT="bud&#xed; se" ID="ID_1136076835" CREATED="1398529279184" MODIFIED="1398529285555">
<node TEXT="s pot&#x159;ebou mo&#x10d;it" ID="ID_172325579" CREATED="1398529287051" MODIFIED="1398529294203"/>
<node TEXT="hladem" ID="ID_1306552646" CREATED="1398529294696" MODIFIED="1398529296819"/>
</node>
</node>
<node TEXT="" POSITION="right" ID="ID_335298005" CREATED="1398642847589" MODIFIED="1398642864118">
<hook URI="brandaris_long_3s708.jpg" SIZE="0.7894737" NAME="ExternalObject"/>
</node>
</node>
</map>
