<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Lanthan" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1374748954830"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Zdroj" STYLE_REF="Remedy" POSITION="right" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1357168559629"/>
<node TEXT="Zkratka" STYLE_REF="Abbrev" POSITION="right" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1357168569896"/>
<node TEXT="Miazma" STYLE_REF="Subtopic" POSITION="right" ID="ID_31190142" CREATED="1357168609677" MODIFIED="1357168614539"/>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="right" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389">
<node TEXT="&#x10d;ast&#x11b;ji u mu&#x17e;&#x16f;" ID="ID_1493946134" CREATED="1374749754202" MODIFIED="1374749762985"/>
</node>
<node TEXT="Trituace" STYLE_REF="Subtopic" POSITION="right" ID="ID_1275465186" CREATED="1357168572522" MODIFIED="1357168576351"/>
<node TEXT="Podobn&#xe9; l&#xe9;ky" STYLE_REF="Subtopic" POSITION="right" ID="ID_1469527899" CREATED="1357168616263" MODIFIED="1357168625505"/>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="right" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1357168653304">
<node TEXT="3. st&#xe1;dium" ID="ID_809422448" CREATED="1374748960193" MODIFIED="1374748977507">
<node TEXT="hled&#xe1; a zkou&#x161;&#xed; bez toho aby v&#x11b;d&#x11b;l, co skute&#x10d;n&#x11b; pot&#x159;ebuje" ID="ID_1065134428" CREATED="1374748978388" MODIFIED="1374749009060"/>
<node TEXT="v&#xe1;h&#xe1;n&#xed;" ID="ID_1715029930" CREATED="1374749017165" MODIFIED="1374749022276"/>
<node TEXT="zkou&#x161;en&#xed;" ID="ID_759527307" CREATED="1374749023356" MODIFIED="1374749028894"/>
<node TEXT="hled&#xe1;n&#xed;" ID="ID_714364279" CREATED="1374749029279" MODIFIED="1374749034636"/>
<node TEXT="porovn&#xe1;v&#xe1;n&#xed;" ID="ID_1368660697" CREATED="1374749035228" MODIFIED="1374749040281"/>
<node TEXT="zkoum&#xe1;n&#xed;" ID="ID_1362916555" CREATED="1374749040754" MODIFIED="1374749049824"/>
<node TEXT="pochybnosti" ID="ID_1208629505" CREATED="1374749051471" MODIFIED="1374749061861"/>
<node TEXT="nerozhodn&#xfd;" ID="ID_1192381023" CREATED="1374749062932" MODIFIED="1374749070688"/>
</node>
<node TEXT="pochybuje" ID="ID_1560820693" CREATED="1374749088085" MODIFIED="1374749095211"/>
<node TEXT="cht&#x11b;l by, ale je si v&#x11b;dom svoj&#xed; slabosti" ID="ID_1946666612" CREATED="1374749095459" MODIFIED="1374749105686">
<node TEXT="&#x10d;asto d&#x16f;sledek ovl&#xe1;d&#xe1;n&#xed; nebo poni&#x17e;ov&#xe1;n&#xed;" STYLE_REF="Comment" ID="ID_1403493816" CREATED="1374749118692" MODIFIED="1374749138779"/>
</node>
<node TEXT="vzd&#xe1;v&#xe1;n&#xed; se p&#x159;i sebemen&#x161;&#xed;m proti&#x159;e&#x10d;en&#xed;" ID="ID_1292267914" CREATED="1374749421851" MODIFIED="1374749475110"/>
<node TEXT="strach" ID="ID_575840703" CREATED="1374749480152" MODIFIED="1374749482284">
<node TEXT="z v&#xfd;&#x161;ek" ID="ID_1916282482" CREATED="1374749483149" MODIFIED="1374749487506"/>
<node TEXT="n&#xe1;bo&#x17e;enstv&#xed;" ID="ID_865161397" CREATED="1374749487771" MODIFIED="1374749495006"/>
<node TEXT="sebevra&#x17e;da" ID="ID_1008495150" CREATED="1374749512932" MODIFIED="1374749518060"/>
<node TEXT="B&#x16f;h" ID="ID_495455542" CREATED="1374749518388" MODIFIED="1374749521938"/>
</node>
<node TEXT="osam&#x11b;n&#xed;" ID="ID_1393362082" CREATED="1374749523159" MODIFIED="1374749525864"/>
<node TEXT="pov&#xfd;&#x161;enost" ID="ID_1123852607" CREATED="1374749526136" MODIFIED="1374749529866"/>
<node TEXT="m&#xe1;nie" ID="ID_72806573" CREATED="1374749530131" MODIFIED="1374749532784"/>
<node TEXT="st&#xe1;hne se do ulity p&#x159;i minim&#xe1;ln&#xed; p&#x159;ek&#xe1;&#x17e;ce" ID="ID_1276540507" CREATED="1374749546860" MODIFIED="1374749559353"/>
<node TEXT="&#x17e;ertov&#xe1;n&#xed;" ID="ID_90390809" CREATED="1374749586132" MODIFIED="1374749590287">
<node TEXT="kv&#x16f;li vlastn&#xed; neschopnosti" ID="ID_147610458" CREATED="1374749591135" MODIFIED="1374749597939"/>
</node>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="right" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<node TEXT="t&#x159;es v hn&#x11b;vu" ID="ID_1920736137" CREATED="1374749676044" MODIFIED="1374749684969"/>
<node TEXT="&#x17e;l&#xe1;zy" ID="ID_118417324" CREATED="1374749685441" MODIFIED="1374749696480">
<node TEXT="pohlavn&#xed;" ID="ID_1420732929" CREATED="1374749697704" MODIFIED="1374749703298">
<node TEXT="nesestoupl&#xe1; varlata" ID="ID_1559957664" CREATED="1374749704146" MODIFIED="1374749711336"/>
<node TEXT="neplodnost" ID="ID_1513906764" CREATED="1374749711728" MODIFIED="1374749715650"/>
</node>
</node>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="right" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729"/>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="right" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690">
<node TEXT="" STYLE_REF="Amel" ID="ID_1545691212" CREATED="1374749766604" MODIFIED="1374749772550">
<node TEXT="j&#xed;dlem" ID="ID_1119742032" CREATED="1374749774111" MODIFIED="1374749776519"/>
</node>
<node TEXT="" STYLE_REF="Agg" ID="ID_655592895" CREATED="1374749777349" MODIFIED="1374749781094">
<node TEXT="hladem" ID="ID_438259020" CREATED="1374749782639" MODIFIED="1374749784961"/>
<node TEXT="drogy" ID="ID_427613623" CREATED="1374749801374" MODIFIED="1374749805944"/>
</node>
</node>
</node>
</map>
