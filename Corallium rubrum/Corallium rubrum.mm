<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Corallium rubrum" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1399061280497"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="left" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1399063225551" HGAP="-180" VSHIFT="-10">
<node TEXT="pot&#x159;ebuje oporu" ID="ID_1642623080" CREATED="1398595196820" MODIFIED="1398596364801">
<font BOLD="true"/>
<node TEXT="vyhled&#xe1;v&#xe1; n&#x11b;koho, kdo je siln&#x11b;j&#x161;&#xed;" ID="ID_750300703" CREATED="1398595222041" MODIFIED="1398595234773"/>
<node TEXT="autoritu" ID="ID_153713750" CREATED="1398595242642" MODIFIED="1398595245506"/>
<node TEXT="rodi&#x10d;" ID="ID_1507583108" CREATED="1398595284243" MODIFIED="1398595287450"/>
<node TEXT="mistr" ID="ID_333651002" CREATED="1398595287881" MODIFIED="1398595295568"/>
<node TEXT="vy&#x161;&#x161;&#xed; entity" ID="ID_1820166164" CREATED="1398595299128" MODIFIED="1398595304222">
<node TEXT="b&#x16f;h, kter&#xfd; d&#xe1;v&#xe1; pravidla&#xa;a za to poskytuje ochranu" ID="ID_651648900" CREATED="1398595549197" MODIFIED="1398595577037"/>
</node>
<node TEXT="sna&#x17e;&#xed; se vytvo&#x159;it bezpe&#x10d;n&#xe9; z&#xe1;zem&#xed;" ID="ID_481776343" CREATED="1398595910760" MODIFIED="1398595926358"/>
</node>
<node TEXT="strach" ID="ID_1514132509" CREATED="1398596195027" MODIFIED="1398596198307">
<node TEXT="z bolesti" ID="ID_1834110691" CREATED="1398596178709" MODIFIED="1398596206179"/>
<node TEXT="z krve" ID="ID_224517291" CREATED="1398596169818" MODIFIED="1398596224785"/>
<node TEXT="ze smrti" ID="ID_1143011050" CREATED="1398595743674" MODIFIED="1398596376715"/>
</node>
<node TEXT="nebezpe&#x10d;&#xed; &#x10d;&#xed;h&#xe1; z vn&#x11b;j&#x161;ku" ID="ID_1007374543" CREATED="1398595794118" MODIFIED="1398595802946">
<node TEXT="za jejich probl&#xe9;my m&#x16f;&#x17e;e n&#x11b;kdo jin&#xfd;" ID="ID_1705165429" CREATED="1398596090189" MODIFIED="1398596102391"/>
</node>
<node TEXT="st&#x11b;&#x17e;ov&#xe1;n&#xed; si" ID="ID_64635671" CREATED="1398595812388" MODIFIED="1398595822905">
<font BOLD="true"/>
<node TEXT="&quot;nen&#xed; to f&#xe9;r&quot;" ID="ID_1352665623" CREATED="1398596537161" MODIFIED="1398596545331"/>
</node>
<node TEXT="h&#xe1;dav&#xfd;" ID="ID_1042822118" CREATED="1398596552964" MODIFIED="1398596556472"/>
<node TEXT="vztek" ID="ID_79937888" CREATED="1398596144020" MODIFIED="1398596146298"/>
</node>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="left" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1357168629389">
<node TEXT="chr&#xe1;n&#xed; proti sm&#x16f;le a zlu" ID="ID_1432448036" CREATED="1398594985788" MODIFIED="1398595022744"/>
<node TEXT="citlivost na v&#xfd;kyvy teplot" ID="ID_391337231" CREATED="1398595046157" MODIFIED="1398595055584"/>
<node TEXT="citlivost na mno&#x17e;stv&#xed; sv&#x11b;tla" ID="ID_1156553709" CREATED="1398595056014" MODIFIED="1398595064242"/>
<node TEXT="pot&#x159;ebuje &#x10d;istou vodu" ID="ID_1590909044" CREATED="1398595064476" MODIFIED="1398595075559"/>
<node TEXT="k&#x159;ehk&#xfd;" ID="ID_717860290" CREATED="1398595078001" MODIFIED="1398595081695"/>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="left" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<node TEXT="ka&#x161;el" ID="ID_1240869682" CREATED="1398595125914" MODIFIED="1398596821858">
<font BOLD="true"/>
<node TEXT="intenzivn&#xed;" ID="ID_1047520886" CREATED="1398595130168" MODIFIED="1398595133649"/>
<node TEXT="k&#x159;e&#x10d;ovit&#xfd;" ID="ID_189586939" CREATED="1398595133918" MODIFIED="1398595138057"/>
<node TEXT="&#x10d;ern&#xfd; ka&#x161;el" ID="ID_990228040" CREATED="1398595138559" MODIFIED="1398595143039"/>
<node TEXT="z&#xe1;chvatovit&#xfd;" ID="ID_1703190903" CREATED="1398596881188" MODIFIED="1398596886388"/>
<node TEXT="zvracen&#xed; hlenu" ID="ID_1373585177" CREATED="1398596888294" MODIFIED="1398596892824"/>
<node TEXT="vy&#x10d;erp&#xe1;n&#xed;" ID="ID_545713593" CREATED="1398596893147" MODIFIED="1398596897287"/>
<node TEXT="dramatick&#xfd; pr&#x16f;b&#x11b;h" ID="ID_462503633" CREATED="1398596897637" MODIFIED="1398596910230"/>
</node>
<node TEXT="d&#x11b;ti, kter&#xe9; vychov&#xe1;vali prarodi&#x10d;e" ID="ID_1329995122" CREATED="1398595680501" MODIFIED="1398595700018"/>
<node TEXT="mluv&#xed; sprost&#x11b;" ID="ID_1061200414" CREATED="1398595853606" MODIFIED="1398595859241">
<node TEXT="dod&#xe1;v&#xe1; si t&#xed;m odvahu, autoritu" ID="ID_1475895816" CREATED="1398595860647" MODIFIED="1398595871640"/>
<node TEXT="kdy&#x17e; c&#xed;t&#xed; bolest" ID="ID_1471816245" CREATED="1398596994327" MODIFIED="1398596999163"/>
</node>
<node TEXT="zuby" ID="ID_1449663236" CREATED="1398596227168" MODIFIED="1398596229164">
<node TEXT="kazy" ID="ID_747433378" CREATED="1398596230490" MODIFIED="1398596232493"/>
</node>
<node TEXT="kosti" ID="ID_1997815958" CREATED="1398596235821" MODIFIED="1398596237554">
<node TEXT="&#x161;patn&#x11b; sr&#x16f;staj&#xed;c&#xed; zlomeniny" ID="ID_769124967" CREATED="1398596239139" MODIFIED="1398596251718"/>
</node>
<node TEXT="krv&#xe1;cen&#xed;" ID="ID_1042494710" CREATED="1398596254331" MODIFIED="1398596257456"/>
<node TEXT="opatrnost" ID="ID_415674414" CREATED="1398596744645" MODIFIED="1398596748622">
<node TEXT="p&#x159;ecitliv&#x11b;lost na bolest" ID="ID_1884110012" CREATED="1398596749852" MODIFIED="1398596766539"/>
</node>
<node TEXT="strach z bolesti" ID="ID_1319662582" CREATED="1398596784107" MODIFIED="1398596795811">
<node TEXT="br&#xe1;n&#xed; porou nebo ot&#x11b;hotn&#x11b;n&#xed;" ID="ID_1931091596" CREATED="1398596796869" MODIFIED="1398596811589"/>
</node>
<node TEXT="nos" ID="ID_183247095" CREATED="1398596839338" MODIFIED="1398596841808">
<node TEXT="such&#xe1; r&#xfd;ma" ID="ID_93841824" CREATED="1398596829641" MODIFIED="1398596834366"/>
<node TEXT="ucpan&#xfd; nos" ID="ID_741852943" CREATED="1398596843754" MODIFIED="1398596852843"/>
<node TEXT="hlen st&#xe9;k&#xe1; do krku a nut&#xed; ka&#x161;lat" ID="ID_1635890184" CREATED="1398596856144" MODIFIED="1399062407186"/>
</node>
</node>
<node TEXT="" POSITION="left" ID="ID_1683921987" CREATED="1399061574639" MODIFIED="1399063220077" HGAP="-210" VSHIFT="50">
<hook URI="12966801IG.jpg" SIZE="0.75" NAME="ExternalObject"/>
</node>
<node TEXT="Kor&#xe1;l" STYLE_REF="Remedy" POSITION="right" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1398595162742"/>
<node TEXT="Cor-r" STYLE_REF="Abbrev" POSITION="right" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1398595043352"/>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="right" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690">
<node TEXT="" STYLE_REF="Agg" ID="ID_1939398199" CREATED="1398596916872" MODIFIED="1398596918261">
<node TEXT="studen&#xfd; vzduch" ID="ID_1131331173" CREATED="1398596919239" MODIFIED="1399063146124">
<font BOLD="false"/>
</node>
<node TEXT="alkohol" ID="ID_1908181084" CREATED="1398596934721" MODIFIED="1398596937894">
<node TEXT="pivo" ID="ID_886655096" CREATED="1398596930837" MODIFIED="1398596932931"/>
<node TEXT="v&#xed;no" ID="ID_1637816409" CREATED="1398596928589" MODIFIED="1398596930549"/>
</node>
<node TEXT="v noci" ID="ID_1250603443" CREATED="1398596946758" MODIFIED="1398596948832"/>
<node TEXT="bolest" ID="ID_1966032784" CREATED="1398596986889" MODIFIED="1398596989199"/>
</node>
<node TEXT="" STYLE_REF="Amel" ID="ID_1712856062" CREATED="1398596950251" MODIFIED="1398596951410">
<node TEXT="p&#x159;ikryt&#xed;m" ID="ID_617420630" CREATED="1398596952078" MODIFIED="1398596956118"/>
</node>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="right" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1399063255782" VSHIFT="10">
<node TEXT="Mind; cursing, swearing, desires; amel." ID="ID_598604185" CREATED="1399061902245" MODIFIED="1399061902245"/>
<node TEXT="Mind; cursing, swearing, desires; pains, at" ID="ID_340306747" CREATED="1399061909148" MODIFIED="1399061909148"/>
<node TEXT="Mind; delirium; maniacal; night" ID="ID_1352091653" CREATED="1399061915589" MODIFIED="1399061915589"/>
<node TEXT="Mind; muttering; pulse, with quick" ID="ID_47873365" CREATED="1399061923089" MODIFIED="1399061923089"/>
<node TEXT="Mind; quarrelsomeness, scolding; pains; during" ID="ID_1489792669" CREATED="1399061953334" MODIFIED="1399061953334"/>
<node TEXT="Mind; night; agg." ID="ID_1692695027" CREATED="1399062012952" MODIFIED="1399062012952"/>
<node TEXT="Mind; complaining" ID="ID_1284682610" CREATED="1399062482127" MODIFIED="1399062482127"/>
<node TEXT="Mind; confusion of mind; alcohol, alcoholic drinks agg." ID="ID_571689618" CREATED="1399062502250" MODIFIED="1399062502250"/>
<node TEXT="Mind; delirium; maniacal" ID="ID_969979703" CREATED="1399062545240" MODIFIED="1399062545240"/>
<node TEXT="Mind; dreams; anxious" ID="ID_1691082913" CREATED="1399062563129" MODIFIED="1399062563129"/>
<node TEXT="Mind; lamenting, bemoaning, wailing" ID="ID_89421338" CREATED="1399062573932" MODIFIED="1399062573932"/>
<node TEXT="Mind; starting, startled" ID="ID_1832963048" CREATED="1399062586692" MODIFIED="1399062586692"/>
<node TEXT="Mind; psychological themes; self-appreciation; increased" ID="ID_712308319" CREATED="1399062732641" MODIFIED="1399062732641"/>
<node TEXT="Mind; fear; suffering, of" ID="ID_926468458" CREATED="1399062759639" MODIFIED="1399062759639"/>
<node TEXT="Mind; fear; pain, of" ID="ID_501946638" CREATED="1399062767173" MODIFIED="1399062767173"/>
<node TEXT="Mind; fear; rain, of" ID="ID_596787140" CREATED="1399062775260" MODIFIED="1399062775260"/>
<node TEXT="Mind; slander, disposition to" ID="ID_1871432168" CREATED="1399062839080" MODIFIED="1399062839080"/>
<node TEXT="Cough; convulsive, spasmodic" ID="ID_1387976435" CREATED="1399062884158" MODIFIED="1399062884158"/>
<node TEXT="Cough; paroxysmal" ID="ID_1939560335" CREATED="1399062893028" MODIFIED="1399062893028"/>
<node TEXT="Cough; whooping" ID="ID_789089435" CREATED="1399062949900" MODIFIED="1399062949900"/>
<node TEXT="Cough; short" ID="ID_443826540" CREATED="1399062956691" MODIFIED="1399062956691"/>
<node TEXT="Cough; suffocative" ID="ID_1939594763" CREATED="1399062968745" MODIFIED="1399062968745"/>
<node TEXT="Cough; paroxysmal; follow one another quickly" ID="ID_24276582" CREATED="1399062992913" MODIFIED="1399062992913"/>
<node TEXT="Cough; paroxysmal; short coughs" ID="ID_805627452" CREATED="1399063004676" MODIFIED="1399063004676"/>
<node TEXT="Cough; ringing" ID="ID_552988219" CREATED="1399063009941" MODIFIED="1399063009941"/>
<node TEXT="Nose; epistaxis, hemorrhage" ID="ID_311879764" CREATED="1399063058182" MODIFIED="1399063058182"/>
<node TEXT="Nose; inspiration agg." ID="ID_1105708768" CREATED="1399063067283" MODIFIED="1399063067283"/>
<node TEXT="Nose; coldness; internal; inspiration, on" ID="ID_959657533" CREATED="1399063076677" MODIFIED="1399063076677"/>
<node TEXT="Nose; discharge; profuse" ID="ID_519948256" CREATED="1399063094591" MODIFIED="1399063094591"/>
</node>
<node TEXT="" POSITION="right" ID="ID_1197371193" CREATED="1399061598299" MODIFIED="1399061620397">
<hook URI="corallium.gif" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</map>
