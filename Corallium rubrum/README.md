
Corallium rubrum
==============

![mapa](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Corallium%20rubrum/Corallium%20rubrum.svg)

 - [PDF](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Corallium%20rubrum/Corallium%20rubrum.pdf)
 - [Map](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Corallium%20rubrum/Corallium%20rubrum.mm)
 - [SVG](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Corallium%20rubrum/Corallium%20rubrum.svg)

Rubriky
-------
 - Cough; convulsive, spasmodic 
 - Cough; paroxysmal 
 - Cough; paroxysmal; follow one another quickly 
 - Cough; paroxysmal; short coughs 
 - Cough; ringing 
 - Cough; short 
 - Cough; suffocative 
 - Cough; whooping 
 - Mind; complaining 
 - Mind; confusion of mind; alcohol, alcoholic drinks agg. 
 - Mind; cursing, swearing, desires; amel. 
 - Mind; cursing, swearing, desires; pains, at 
 - Mind; delirium; maniacal 
 - Mind; delirium; maniacal; night 
 - Mind; dreams; anxious 
 - Mind; fear; pain, of 
 - Mind; fear; rain, of 
 - Mind; fear; suffering, of 
 - Mind; lamenting, bemoaning, wailing [[Ars](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/ArsenicumAlbum)]
 - Mind; muttering; pulse, with quick 
 - Mind; night; agg. [[Aster](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Asterias rubens)]
 - Mind; psychological themes; self-appreciation; increased 
 - Mind; quarrelsomeness, scolding; pains; during 
 - Mind; slander, disposition to 
 - Mind; starting, startled [[Murx](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Murex purpurea), [Spong](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Spongia tosta)]
 - Nose; coldness; internal; inspiration, on 
 - Nose; discharge; profuse 
 - Nose; epistaxis, hemorrhage 
 - Nose; inspiration agg. 
    