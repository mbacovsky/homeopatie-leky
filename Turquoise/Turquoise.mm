<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Turquoise" ID="ID_1484695409" CREATED="1341217574474" MODIFIED="1341217618120" LINK="../../Letn&#xed;%20&#x161;kola%202012%20-%20Podmitrov/L&#xe9;&#x10d;en&#xed;%20pomoc&#xed;%20barev.mm" COLOR="#cc0000" STYLE="fork">
<hook NAME="MapStyle" max_node_width="600"/>
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Turq-aw" POSITION="right" ID="ID_976239870" CREATED="1341217738318" MODIFIED="1341217745489" COLOR="#996600">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Trituace" POSITION="right" ID="ID_1376139038" CREATED="1341217658433" MODIFIED="1341217689193" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="ve vod&#x11b;" ID="ID_1221517496" CREATED="1341217750302" MODIFIED="1341217866242">
<font ITALIC="true"/>
</node>
<node TEXT="izolace" ID="ID_469467029" CREATED="1341217866552" MODIFIED="1341217872688">
<font ITALIC="true"/>
</node>
<node TEXT="individualita" ID="ID_1905944382" CREATED="1341217872959" MODIFIED="1341217878781">
<font ITALIC="true"/>
</node>
<node TEXT="odstup" ID="ID_748091808" CREATED="1341217879595" MODIFIED="1341217925543">
<font ITALIC="true"/>
</node>
<node TEXT="stoupan&#xed;" ID="ID_671514186" CREATED="1341218345170" MODIFIED="1341218355335">
<font ITALIC="true"/>
</node>
<node TEXT="lehkost" ID="ID_270469750" CREATED="1341218350301" MODIFIED="1341218357054">
<font ITALIC="true"/>
</node>
<node TEXT="sta&#x17e;en&#xe9; hrdlo" ID="ID_263793519" CREATED="1341218358954" MODIFIED="1341218367984">
<font ITALIC="true"/>
</node>
<node TEXT="nabudilo to" ID="ID_508952157" CREATED="1341218482307" MODIFIED="1341218489788">
<node TEXT="ud&#x11b;l&#xe1;m spoustu pr&#xe1;ce" ID="ID_1993988780" CREATED="1341218490629" MODIFIED="1341218498433"/>
</node>
<node TEXT="zklidnilo m&#x11b; to" ID="ID_565582090" CREATED="1341218500014" MODIFIED="1341218505194"/>
<node TEXT="hloubka" ID="ID_1418971158" CREATED="1341218507097" MODIFIED="1341218516296"/>
<node TEXT="odpout&#xe1;n&#xed;" ID="ID_285132118" CREATED="1341218520667" MODIFIED="1341218524806"/>
<node TEXT="&#x161;t&#x11b;bet&#xe1;me" ID="ID_1773149786" CREATED="1341218588878" MODIFIED="1341218597167"/>
</node>
<node TEXT="Info" POSITION="right" ID="ID_1413089747" CREATED="1341217716245" MODIFIED="1341217726211" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="podobn&#xe9; &#xfa;&#x10d;inky jako bled&#x11b; modr&#xe1; s trochou zelen&#xe9;" ID="ID_1747042373" CREATED="1341218639009" MODIFIED="1341218661594"/>
<node TEXT="miazma" ID="ID_1152098544" CREATED="1341218662003" MODIFIED="1341218664925">
<node TEXT="rakovinn&#xe9;" ID="ID_911062896" CREATED="1341218665742" MODIFIED="1341218668274"/>
</node>
<node TEXT="t&#xf3;n" ID="ID_1776514328" CREATED="1341219098720" MODIFIED="1341219102137">
<node TEXT="G" ID="ID_584146461" CREATED="1341219103202" MODIFIED="1341219104134"/>
</node>
</node>
<node TEXT="Kotraindikace" POSITION="right" ID="ID_1113719588" CREATED="1341219218627" MODIFIED="1341219225929" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="n&#xed;zk&#xfd; tlak (?)" ID="ID_804238522" CREATED="1341219226773" MODIFIED="1341219295032"/>
<node TEXT="alopatick&#xe9; l&#xe9;ky" ID="ID_1092804493" CREATED="1341219231318" MODIFIED="1341219237393">
<node TEXT="ATB" ID="ID_536687346" CREATED="1341219239025" MODIFIED="1341219246550"/>
</node>
</node>
<node TEXT="Res" POSITION="right" ID="ID_569720889" CREATED="1341217620346" MODIFIED="1341219631936" COLOR="#0066cc" STYLE="fork" HGAP="-17" VSHIFT="22">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="TURQUOISE&#xa;This is the color of creative expression. It is a mixture of green and indigo. It represents all forms of creativity, communication, truth and will power. It is the color associated with spiritual healing and values. It is associated with the veil of the Virgin Mary. Many cultures prize this color and value the stone by the same name. The color received its name from the turquoise blue seas around Turkey.&#xa;Turquoise has the ability to soothe our nerves and relieve the body of physical pain. It works well for many conditions from tooth ache to sore throat. It can calm an inflamed earache and ease congestion in the sinuses. If you have a serious medical condition, please consult your doctor before taking this remedy.&#xa; &#xa; &#xa;Chakra: The Throat Chakra: This is the center for personal expression. It is related to our capacity to express our needs and be creative with our life energy. Matters of personal will power are affected by the strength of this chakra. Will allows us to strengthen our character and develop maturity and presence. &#xa;This chakra is also meant to be the center of truth. This applies to how we express our truth as well as how we ingest it in terms of our consciousness and awareness.  The Throat Chakra controls the sense of hearing and speaking. When we hear the truth it resonates in our being. When we speak the truth we open a channel for healing between ourselves and others.&#xa;It s archetype is the Communicator, that person who expresses their truth to the best of their ability. The negative aspect of this color is represented by the archetype of the Silent Child, the person who suppresses their feelings and is afraid to say their truth.&#xa;The qualities of this color are: integrity, communication, will, truth, both your personal truth and the higher truth, and creativity.&#xa; &#xa;Mentals: The mental symptoms associated with Turquoise all focus around our ability to hear and express the truth. People have resistance to saying what they think or feel and many opportunities for healing are lost because of fear of repercussions. This remedy can be used to heal closed minds with fixed ideas and when people have difficulties hearing what is being said to them.&#xa;Contraindications: Do not take this remedy if you have a serious medical condition without consulting your doctor.&#xa; &#xa;Physicals: This remedy can bring healing to inflammation, particularly around the throat and gums. It has been used successfully to alleviate toothache. It can be used for mouth ulcers, ear infections, sore throats, and bronchial inflammations. It is good for treating obesity when the thyroid is under active and the patient has low energy, tires quickly and is apathetic.&#xa;Contraindications: It is not recommended for people on medication or taking drugs. It would be best to wait for several weeks after taking medication to take this color remedy. If you have a serious medical condition, please consult your doctor before taking this treatment.&#xa; &#xa;General Symptoms: Wherever there is a problem with self expression and a need to stimulate communication skills this remedy works very well. It strengthens the Throat Chakra that may have been weakened by allopathic or recreational drugs, over or under eating, smoking or drinking.  Any time the will needs to be engaged this color works to strengthen and fortify a person.&#xa;Contraindications:  This remedy is not a substitute for communication, but it helps to relieve tension in this sphere where it is blocked. Please consult your doctor if you have a serious medical condition before taking this remedy." ID="ID_1190354070" CREATED="1341217648382" MODIFIED="1341217657358"/>
</node>
<node TEXT="T&#xe9;mata" POSITION="left" ID="ID_1533101483" CREATED="1341217662596" MODIFIED="1341217689741" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="nedostatek" ID="ID_256681078" CREATED="1341218695665" MODIFIED="1341218700252">
<font BOLD="true"/>
<node TEXT="&#x17e;ivot nenapln&#x11b;n&#xfd; nebo mevyj&#xe1;d&#x159;en&#xfd;" ID="ID_1753201266" CREATED="1341218676824" MODIFIED="1341218693319"/>
<node TEXT="strach &#x159;&#xed;kat pravdu" ID="ID_1025746800" CREATED="1341218999751" MODIFIED="1341219006417"/>
<node TEXT="&#x161;&#xed;&#x159;en&#xed; pomluv" ID="ID_607199986" CREATED="1341219006715" MODIFIED="1341219010845"/>
<node TEXT="klejeme" ID="ID_1914826947" CREATED="1341219011086" MODIFIED="1341219013983"/>
<node TEXT="hrub&#xe1; slova" ID="ID_64264102" CREATED="1341219014423" MODIFIED="1341219020639"/>
<node TEXT="sebevyj&#xe1;d&#x159;en&#xed;" ID="ID_548832500" CREATED="1341219143539" MODIFIED="1341219148189"/>
<node TEXT="neschopnost se za sebe postavit" ID="ID_890732732" CREATED="1341219150696" MODIFIED="1341219159152"/>
<node TEXT="obt&#xed;&#x17e;n&#xe1; komunikace" ID="ID_1679836710" CREATED="1341219160096" MODIFIED="1341219166406"/>
<node TEXT="slab&#xe1; v&#x16f;le" ID="ID_1632776105" CREATED="1341219166679" MODIFIED="1341219169649"/>
<node TEXT="podl&#xe9;h&#xe1; z&#xe1;vislostem" ID="ID_864421824" CREATED="1341219169931" MODIFIED="1341219179574"/>
</node>
<node TEXT="d&#xe1;v&#xe1;" ID="ID_1246962010" CREATED="1341218719282" MODIFIED="1341218746872">
<font BOLD="true"/>
<node TEXT="spojen&#xed; s vlastn&#xed; du&#x161;&#xed;" ID="ID_266815362" CREATED="1341218723041" MODIFIED="1341218730303"/>
<node TEXT="schopnost sly&#x161;et sv&#x16f;j vnit&#x159;n&#xed; hlas" ID="ID_47801044" CREATED="1341218730672" MODIFIED="1341218744991"/>
<node TEXT="pocit celistvosti" ID="ID_170824104" CREATED="1341218778077" MODIFIED="1341218782178"/>
<node TEXT="pru&#x17e;nost" ID="ID_1882864282" CREATED="1341218785423" MODIFIED="1341218788588"/>
<node TEXT="schopnost se p&#x159;izp&#x16f;sobit" ID="ID_618053067" CREATED="1341218793503" MODIFIED="1341218802354"/>
<node TEXT="hloubka" ID="ID_531748228" CREATED="1341218806889" MODIFIED="1341218809336"/>
<node TEXT="svoboda" ID="ID_1929780194" CREATED="1341218809601" MODIFIED="1341218811738"/>
<node TEXT="ntegrita" ID="ID_906589593" CREATED="1341218975371" MODIFIED="1341218977432"/>
<node TEXT="pevnost osobnosti" ID="ID_1530950009" CREATED="1341218977681" MODIFIED="1341218986153"/>
<node TEXT="schopnost &#x17e;&#xed;t podle toho co hl&#xe1;s&#xe1;me" ID="ID_480307102" CREATED="1341218986722" MODIFIED="1341218995690"/>
</node>
<node TEXT="nekone&#x10d;nost nebe a mo&#x159;e" ID="ID_437179497" CREATED="1341218703952" MODIFIED="1341218710914"/>
<node TEXT="komunikace" ID="ID_1832707520" CREATED="1341218753680" MODIFIED="1341218756163"/>
<node TEXT="kreativita" ID="ID_522149848" CREATED="1341218756436" MODIFIED="1341218759262"/>
<node TEXT="svobodn&#xe1; v&#x16f;le" ID="ID_1176660617" CREATED="1341218826430" MODIFIED="1341218831655">
<node TEXT="dok&#xe1;&#x17e;eme odolat tomu co n&#xe1;m &#x161;kod&#xed;" ID="ID_15167178" CREATED="1341218832952" MODIFIED="1341218847247"/>
<node TEXT="drogy" ID="ID_728259717" CREATED="1341218863688" MODIFIED="1341218867699"/>
<node TEXT="alkohol" ID="ID_1124166737" CREATED="1341218867996" MODIFIED="1341218869870"/>
<node TEXT="l&#xe9;ky" ID="ID_1991112199" CREATED="1341218936750" MODIFIED="1341218938679"/>
<node TEXT="j&#xed;dlo" ID="ID_1252327778" CREATED="1341218940029" MODIFIED="1341218941499"/>
</node>
<node TEXT="vyjad&#x159;en&#xed;" ID="ID_1464031499" CREATED="1341218759759" MODIFIED="1341218763825">
<node TEXT="emoce" ID="ID_723428393" CREATED="1341218764483" MODIFIED="1341218765704"/>
<node TEXT="my&#x161;lenky" ID="ID_1292661240" CREATED="1341218766049" MODIFIED="1341218768771"/>
<node TEXT="n&#xe1;zory" ID="ID_936185868" CREATED="1341218768969" MODIFIED="1341218771077"/>
</node>
<node TEXT="naslouch&#xe1;n&#xed;" ID="ID_1543954122" CREATED="1341218898887" MODIFIED="1341218902527">
<node TEXT="sob&#x11b;" ID="ID_1796744770" CREATED="1341218903216" MODIFIED="1341218904639"/>
<node TEXT="druh&#xfd;" ID="ID_15282215" CREATED="1341218904944" MODIFIED="1341218906674"/>
</node>
<node TEXT="st&#x159;edisko pravdy" ID="ID_620697788" CREATED="1341218922720" MODIFIED="1341218927153">
<node TEXT="na&#x161;&#xed;" ID="ID_884507345" CREATED="1341218929544" MODIFIED="1341218947153"/>
<node TEXT="vy&#x161;&#x161;&#xed;" ID="ID_1607181454" CREATED="1341218947498" MODIFIED="1341218949807"/>
<node TEXT="bo&#x17e;&#xed; &#xfa;sta" ID="ID_1081718768" CREATED="1341218950120" MODIFIED="1341218955713"/>
<node TEXT="mor&#xe1;lka" ID="ID_1131296077" CREATED="1341218956034" MODIFIED="1341218958323"/>
</node>
<node TEXT="hlas" ID="ID_1545002468" CREATED="1341218872988" MODIFIED="1341219197112">
<node TEXT="jak to &#x159;&#xed;k&#xe1;me" ID="ID_1318599376" CREATED="1341218880601" MODIFIED="1341218893751"/>
<node TEXT="co &#x159;&#xed;k&#xe1;me" ID="ID_625840776" CREATED="1341218916473" MODIFIED="1341218920252"/>
<node TEXT="ovliv&#x148;uje t&#xf3;n" ID="ID_1921747138" CREATED="1341219198813" MODIFIED="1341219206534"/>
</node>
</node>
<node TEXT="Symptomy" POSITION="left" ID="ID_1835759830" CREATED="1341217719626" MODIFIED="1341217726612" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="&#x161;t&#xed;tn&#xe1; &#x17e;l&#xe1;za" ID="ID_1728227000" CREATED="1341219028985" MODIFIED="1341219035552">
<node TEXT="hyper" ID="ID_1393295781" CREATED="1341219036433" MODIFIED="1341219039756"/>
<node TEXT="hypo" ID="ID_1481134935" CREATED="1341219040309" MODIFIED="1341219041982"/>
</node>
<node TEXT="u&#x161;i" ID="ID_1060824879" CREATED="1341219048718" MODIFIED="1341219050856"/>
<node TEXT="&#x10d;elisti" ID="ID_1965975696" CREATED="1341219051145" MODIFIED="1341219053809"/>
<node TEXT="&#xfa;sta" ID="ID_99570365" CREATED="1341219054043" MODIFIED="1341219056993"/>
<node TEXT="d&#xe1;sn&#x11b;" ID="ID_1065444872" CREATED="1341219057266" MODIFIED="1341219059423"/>
<node TEXT="jazyk" ID="ID_407999731" CREATED="1341219059872" MODIFIED="1341219061645"/>
<node TEXT="zuby" ID="ID_1522276317" CREATED="1341219061839" MODIFIED="1341219065684"/>
<node TEXT="hrdlo" ID="ID_678686575" CREATED="1341219065901" MODIFIED="1341219068418"/>
<node TEXT="tvorba hlenu" ID="ID_1620167187" CREATED="1341219068659" MODIFIED="1341219075715">
<node TEXT="z&#xe1;n&#x11b;ty" ID="ID_1667668207" CREATED="1341219111286" MODIFIED="1341219113952"/>
<node TEXT="uklidn&#x11b;n&#xed;" ID="ID_394143726" CREATED="1341219114200" MODIFIED="1341219116909"/>
<node TEXT="vysok&#xfd; krevn&#xed; tlak" ID="ID_1701316880" CREATED="1341219117142" MODIFIED="1341219138152"/>
</node>
</node>
<node TEXT="Souvisej&#xed;c&#xed; l&#xe9;ky" POSITION="left" ID="ID_1184275718" CREATED="1341217678620" MODIFIED="1341219581237" COLOR="#0066cc" STYLE="fork" HGAP="17" VSHIFT="9">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Alum" ID="ID_320953877" CREATED="1341219479094" MODIFIED="1341219482704"/>
<node TEXT="Stan" ID="ID_808623581" CREATED="1341219483121" MODIFIED="1341219485199"/>
<node TEXT="Ign" ID="ID_1394246949" CREATED="1341219485488" MODIFIED="1341219487272"/>
<node TEXT="Caust" ID="ID_875265061" CREATED="1341219489702" MODIFIED="1341219491314"/>
<node TEXT="Acon" ID="ID_1304426241" CREATED="1341219493185" MODIFIED="1341219494758"/>
<node TEXT="Arg" ID="ID_1267839889" CREATED="1341219498411" MODIFIED="1341219499915"/>
<node TEXT="tub" ID="ID_1268615316" CREATED="1341219504518" MODIFIED="1341219508434"/>
<node TEXT="Beril" ID="ID_385759036" CREATED="1341219510503" MODIFIED="1341219512035"/>
<node TEXT="lith" ID="ID_721909993" CREATED="1341219518060" MODIFIED="1341219519522"/>
<node TEXT="Caladium" ID="ID_1824383017" CREATED="1341219524971" MODIFIED="1341219529156"/>
<node TEXT="Phos" ID="ID_454356283" CREATED="1341219530499" MODIFIED="1341219532633"/>
</node>
</node>
</map>
