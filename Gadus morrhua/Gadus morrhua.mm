<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Gadus morrhua" STYLE_REF="Main Topic" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1398601340866"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode TEXT="Main Topic" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subtopic" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Subsubtopic" COLOR="#659919" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Remedy" COLOR="#626218" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Abbrev" COLOR="#a96628" BACKGROUND_COLOR="#ffffff" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</stylenode>
<stylenode TEXT="Agg" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="smily_bad"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Amel" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="0" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Differs" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Same" COLOR="#000000" STYLE="as_parent" MAX_WIDTH="600">
<icon BUILTIN="button_ok"/>
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode TEXT="Comment" COLOR="#666666" STYLE="as_parent" MAX_WIDTH="600">
<font NAME="SansSerif" SIZE="8" BOLD="false" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Gadus morrhua oleum" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" POSITION="left" ID="ID_56956964" CREATED="1398602796139" MODIFIED="1399069974612" HGAP="-50" VSHIFT="-20">
<node TEXT="olej z tres&#x10d;&#xed;ch jater" STYLE_REF="Remedy" ID="ID_963051056" CREATED="1398602801795" MODIFIED="1399068899225"/>
<node TEXT="v&#xed;c bolen&#xed;" ID="ID_1243294802" CREATED="1398602804631" MODIFIED="1398602811684"/>
<node TEXT="v&#xed;c pot&#xed;&#x17e;&#xed; se za&#x17e;&#xed;vac&#xed;m traktem" ID="ID_1055260843" CREATED="1398602812078" MODIFIED="1398602822231"/>
<node TEXT="l&#xe9;ze" ID="ID_84119191" CREATED="1398602822572" MODIFIED="1398602825221"/>
<node TEXT="paraziti" ID="ID_842579150" CREATED="1398602825589" MODIFIED="1398602827865"/>
<node TEXT="v&#x11b;t&#x161;&#xed; se&#x161;lost" ID="ID_471910772" CREATED="1398602828179" MODIFIED="1398602834477"/>
<node TEXT="v&#x11b;t&#x161;&#xed; vy&#x10d;erp&#xe1;n&#xed;" ID="ID_1268208791" CREATED="1398602845935" MODIFIED="1398602853209"/>
<node TEXT="za&#x17e;&#xed;vac&#xed; trakt" ID="ID_1362959619" CREATED="1398603202787" MODIFIED="1398603207959">
<node TEXT="parazit&#xe9;" ID="ID_861047096" CREATED="1398603195485" MODIFIED="1398603198816"/>
<node TEXT="pr&#x16f;jmy" ID="ID_232846228" CREATED="1398603191350" MODIFIED="1398603195198"/>
</node>
<node TEXT="sn&#xed;&#x17e;en&#xe9; vn&#xed;m&#xe1;n&#xed;" ID="ID_1140400287" CREATED="1398602854480" MODIFIED="1398602879443">
<node TEXT="sluch" ID="ID_1194951761" CREATED="1398602880572" MODIFIED="1398602884021"/>
<node TEXT="&#x10d;ich" ID="ID_1692596199" CREATED="1398602884434" MODIFIED="1398602885992"/>
<node TEXT="hmat" ID="ID_168989328" CREATED="1398602886234" MODIFIED="1398602887327"/>
<node TEXT="chu&#x165;" ID="ID_1733838369" CREATED="1398602889323" MODIFIED="1398602891642"/>
</node>
</node>
<node TEXT="" POSITION="left" ID="ID_1042105978" CREATED="1399068908422" MODIFIED="1399069988682" HGAP="-150" VSHIFT="120">
<hook URI="atlantic-cod-dieter-craasmann_frompewwebsite.jpg" SIZE="0.65430754" NAME="ExternalObject"/>
</node>
<node TEXT="Treska (kr&#x10d;n&#xed; obratel)" STYLE_REF="Remedy" POSITION="right" ID="ID_114546884" CREATED="1357168539414" MODIFIED="1399070026214" HGAP="-60" VSHIFT="20"/>
<node TEXT="Gad" STYLE_REF="Abbrev" POSITION="right" ID="ID_683843007" CREATED="1357168564164" MODIFIED="1399069966835" HGAP="-60" VSHIFT="10"/>
<node TEXT="Info" STYLE_REF="Subtopic" POSITION="right" ID="ID_288129864" CREATED="1357168626031" MODIFIED="1399069964181" HGAP="-30" VSHIFT="-20">
<node TEXT="pr&#xe1;&#x161;ek z kost&#xed; se u&#x17e;&#xed;val na l&#xe9;&#x10d;bu k&#x159;ivice" ID="ID_765020824" CREATED="1398601366821" MODIFIED="1398601378597"/>
<node TEXT="&#x17e;ije v hejnu, ale v r&#xe1;mci hejna neprob&#xed;h&#xe1; &#x17e;&#xe1;dn&#xe1; interakce" ID="ID_1514967348" CREATED="1398601601325" MODIFIED="1398601618631"/>
<node TEXT="individuality, kter&#xe9; jsou v jednom hejn&#x11b;" ID="ID_1832715830" CREATED="1398601626968" MODIFIED="1398601639367"/>
</node>
<node TEXT="T&#xe9;mata" STYLE_REF="Subtopic" POSITION="right" ID="ID_531910115" CREATED="1357168649335" MODIFIED="1399069957608" HGAP="-50">
<node TEXT="slab&#x11b; integrovan&#xfd; do spole&#x10d;nosti" ID="ID_817987872" CREATED="1398601436010" MODIFIED="1398601450423"/>
<node TEXT="odta&#x17e;itost" ID="ID_448918929" CREATED="1398601509155" MODIFIED="1398601512413">
<node TEXT="necht&#x11b;j&#xed; m&#xed;t nic spole&#x10d;n&#xe9;ho s prost&#x159;ed&#xed;m ve kter&#xe9;m &#x17e;ij&#xed;" ID="ID_1602602549" CREATED="1398601516908" MODIFIED="1398601534235"/>
<node TEXT="p&#x159;st&#xe1;vaj&#xed; komunikovat" ID="ID_1334057361" CREATED="1398601567283" MODIFIED="1398601576333"/>
<node TEXT="omezuj&#xed; vztahy" ID="ID_1215596526" CREATED="1398601576719" MODIFIED="1398601581383">
<node TEXT="k rodin&#x11b;" ID="ID_242198293" CREATED="1398601582745" MODIFIED="1398601585636"/>
<node TEXT="ke komunit&#x11b;" ID="ID_425032106" CREATED="1398601586075" MODIFIED="1398601590593"/>
</node>
<node TEXT="odst&#x159;ih&#xe1;v&#xe1;n&#xed; se od reality" ID="ID_819692008" CREATED="1398601652499" MODIFIED="1398601661178"/>
<node TEXT="necht&#x11b;j&#xed; p&#x159;ij&#xed;mat potravu" ID="ID_1478765491" CREATED="1398601661456" MODIFIED="1398601674861">
<node TEXT="jeden z prvn&#xed;ch symptom&#x16f;" STYLE_REF="Comment" ID="ID_1469506708" CREATED="1398601677028" MODIFIED="1398601695284"/>
<node TEXT="zpravidla ne anorexie,&#xa;kde jde &#x10d;asto o manipulaci" STYLE_REF="Comment" ID="ID_1543642066" CREATED="1398601787106" MODIFIED="1398601830129"/>
</node>
<node TEXT="nereaguj&#xed; na vn&#x11b;j&#x161;&#xed; podn&#x11b;ty" ID="ID_1159978204" CREATED="1398601837390" MODIFIED="1398601846785"/>
<node TEXT="stahuj&#xed; se do sebe" ID="ID_1184751195" CREATED="1398601847207" MODIFIED="1398601852572"/>
<node TEXT="p&#x159;est&#xe1;vaj&#xed; sly&#x161;et" ID="ID_496451824" CREATED="1398601859371" MODIFIED="1398601875505"/>
<node TEXT="neschopnost se vyrovnat s prost&#x159;ed&#xed;m, ve kter&#xe9;m &#x17e;ij&#xed;" ID="ID_815522077" CREATED="1398602137506" MODIFIED="1398602155264"/>
</node>
</node>
<node TEXT="Symptomy" STYLE_REF="Subtopic" POSITION="right" ID="ID_1863360914" CREATED="1357168653548" MODIFIED="1357168658347">
<node TEXT="autismus" ID="ID_1810424083" CREATED="1398601458314" MODIFIED="1398601468861"/>
<node TEXT="maniodepresivn&#xed; syndrom" ID="ID_1687318220" CREATED="1398601472746" MODIFIED="1398601478941"/>
<node TEXT="p&#x159;ij&#xed;m&#xe1; minimum potravy, aby p&#x159;e&#x17e;il" ID="ID_1357819578" CREATED="1398601703062" MODIFIED="1398601739642"/>
<node TEXT="sluch" ID="ID_973244820" CREATED="1398601999199" MODIFIED="1398602001836">
<node TEXT="ztr&#xe1;ta" ID="ID_974097192" CREATED="1398602003118" MODIFIED="1398602005408"/>
</node>
<node TEXT="sta&#x159;eck&#xe1; demence" ID="ID_934979318" CREATED="1398602295115" MODIFIED="1398602312836"/>
<node TEXT="alergie" ID="ID_1533611443" CREATED="1398602705935" MODIFIED="1398602708357"/>
</node>
<node TEXT="Modality" STYLE_REF="Subtopic" POSITION="right" ID="ID_1285296051" CREATED="1357168663285" MODIFIED="1357168667690">
<node TEXT="" STYLE_REF="Agg" ID="ID_459077114" CREATED="1398602686090" MODIFIED="1398602697370">
<node TEXT="sliunce" ID="ID_1255211995" CREATED="1398602688804" MODIFIED="1398602692796"/>
<node TEXT="hluk" ID="ID_1609472737" CREATED="1399069865940" MODIFIED="1399069870026"/>
<node TEXT="chlad" ID="ID_1686436409" CREATED="1399069870670" MODIFIED="1399069872760"/>
</node>
</node>
<node TEXT="Rubriky" STYLE_REF="Subtopic" POSITION="right" ID="ID_952483042" CREATED="1357168659026" MODIFIED="1357168662729">
<node TEXT="Mind; express oneself; difficult" ID="ID_609529247" CREATED="1398602277219" MODIFIED="1398602277219"/>
<node TEXT="Mind; forgetfulness" ID="ID_345536313" CREATED="1398602290476" MODIFIED="1398602290476"/>
<node TEXT="Mind; pessimist" ID="ID_1699193285" CREATED="1398602326573" MODIFIED="1398602326573"/>
<node TEXT="Mind; slowness" ID="ID_193524123" CREATED="1398602335660" MODIFIED="1398602335660"/>
<node TEXT="Mind; thoughts; vanishing, unable to think" ID="ID_775259151" CREATED="1398602351545" MODIFIED="1398602351545"/>
<node TEXT="Mind; sadness" ID="ID_819095003" CREATED="1398602362749" MODIFIED="1398602362749"/>
<node TEXT="Mind; helplessness; paroxysmal" ID="ID_402530912" CREATED="1399069420457" MODIFIED="1399069420457"/>
<node TEXT="Generalities; pain; sore, bruised; external" ID="ID_320098698" CREATED="1399069493754" MODIFIED="1399069493754"/>
<node TEXT="Mind; death; desires" ID="ID_1357119457" CREATED="1399069524700" MODIFIED="1399069524700"/>
<node TEXT="Mind; forgetfulness; words while speaking, of, word hunting" ID="ID_601672794" CREATED="1399069556641" MODIFIED="1399069556641"/>
<node TEXT="Mind; indifference, apathy" ID="ID_1745442036" CREATED="1399069574478" MODIFIED="1399069574478"/>
<node TEXT="Hearing; impaired" ID="ID_1211803752" CREATED="1399069622738" MODIFIED="1399069622738"/>
<node TEXT="Vision; weak" ID="ID_209915235" CREATED="1399069629101" MODIFIED="1399069629101"/>
<node TEXT="Speech &amp; voice; weak voice" ID="ID_333940920" CREATED="1399069663876" MODIFIED="1399069663876"/>
<node TEXT="Clinical; diabetes; insipidus" ID="ID_378688358" CREATED="1399069714009" MODIFIED="1399069714009"/>
<node TEXT="Clinical; tuberculosis" ID="ID_678700376" CREATED="1399069719066" MODIFIED="1399069719066"/>
<node TEXT="Generalities; weakness; afternoon" ID="ID_640416429" CREATED="1399069732955" MODIFIED="1399069732955"/>
<node TEXT="Generalities; pain; sore, bruised" ID="ID_1688601261" CREATED="1399069755710" MODIFIED="1399069755710"/>
<node TEXT="Stomach; appetite; diminished" ID="ID_944233409" CREATED="1399069786826" MODIFIED="1399069786826"/>
<node TEXT="Rectum; diarrhea" ID="ID_1089969057" CREATED="1399069816288" MODIFIED="1399069816288"/>
</node>
</node>
</map>
