
Gadus morrhua
==============

![mapa](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Gadus%20morrhua/Gadus%20morrhua.svg)

 - [PDF](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Gadus%20morrhua/Gadus%20morrhua.pdf)
 - [Map](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Gadus%20morrhua/Gadus%20morrhua.mm)
 - [SVG](https://bitbucket.org/mbacovsky/homeopatie-leky/raw/master/Gadus%20morrhua/Gadus%20morrhua.svg)

Rubriky
-------
 - Clinical; diabetes; insipidus 
 - Clinical; tuberculosis 
 - Generalities; pain; sore, bruised 
 - Generalities; pain; sore, bruised; external 
 - Generalities; weakness; afternoon 
 - Hearing; impaired 
 - Mind; death; desires 
 - Mind; express oneself; difficult 
 - Mind; forgetfulness 
 - Mind; forgetfulness; words while speaking, of, word hunting 
 - Mind; helplessness; paroxysmal 
 - Mind; indifference, apathy [[Alum](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Alumina)]
 - Mind; pessimist 
 - Mind; sadness 
 - Mind; slowness [[Alum](https://bitbucket.org/mbacovsky/homeopatie-leky/src/master/Alumina)]
 - Mind; thoughts; vanishing, unable to think 
 - Rectum; diarrhea 
 - Speech & voice; weak voice 
 - Stomach; appetite; diminished 
 - Vision; weak 
    