<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Cannabis Indica" ID="ID_1103547851" CREATED="1331371541129" MODIFIED="1331717634522" LINK="../2.%20rocnik/2r-7.mm" COLOR="#cc0000" STYLE="fork">
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="20"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Miazma" POSITION="left" ID="ID_826451028" CREATED="1331403558887" MODIFIED="1331682714018" COLOR="#0066cc" STYLE="fork" HGAP="3">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="sykotick&#xe9;" ID="ID_1689564126" CREATED="1331403563341" MODIFIED="1331403567111"/>
</node>
<node TEXT="Podobn&#xe9; l&#xe9;ky" POSITION="left" ID="ID_1409998937" CREATED="1331404598277" MODIFIED="1331683363673" COLOR="#0066cc" STYLE="fork" HGAP="12">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="OP" ID="ID_679986740" CREATED="1331404606346" MODIFIED="1331404609752">
<node TEXT="&#x10d;as plyne p&#x159;&#xed;li&#x161; rychle" ID="ID_611382389" CREATED="1331404611296" MODIFIED="1331404633316">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="lh&#xe1;&#x159; s nedostatkem mor&#xe1;lky" ID="ID_576808088" CREATED="1331404721173" MODIFIED="1331404736349" COLOR="#000000">
<icon BUILTIN="button_cancel"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node TEXT="onemocn&#x11b;n&#xed; z &#x161;oku" ID="ID_1144864594" CREATED="1331404737225" MODIFIED="1331404749336">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node TEXT="PHOS" ID="ID_1174714810" CREATED="1357920443462" MODIFIED="1357920449463">
<node TEXT="jde j&#xed; sp&#xed;&#x161; o z&#xe1;bavu ne&#x17e; o lidi" ID="ID_279613765" CREATED="1357920450519" MODIFIED="1357920485073">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node TEXT="T&#xe9;mata" POSITION="left" ID="ID_1967945458" CREATED="1331401634043" MODIFIED="1331682535920" COLOR="#0066cc" STYLE="fork" HGAP="-8" VSHIFT="-14">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="" ID="ID_875902067" CREATED="1331401648369" MODIFIED="1331401654588" COLOR="#000000">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node TEXT="pozitivni vize" ID="ID_1442675078" CREATED="1331371575461" MODIFIED="1331401703169">
<font BOLD="true"/>
<node TEXT="vytv&#xe1;&#x159;&#xed; si fantazie" ID="ID_1900317985" CREATED="1331403697742" MODIFIED="1331403705262"/>
</node>
<node TEXT="chichot&#xe1;n&#xed;" ID="ID_1059348889" CREATED="1331401813687" MODIFIED="1331401831742">
<font BOLD="true"/>
</node>
<node TEXT="p&#x159;ecitliv&#x11b;l&#xed;" ID="ID_1530431382" CREATED="1331403607561" MODIFIED="1331403612800">
<node TEXT="sv&#x11b;t vn&#xed;m&#xe1; jako tvrd&#xfd; a chladn&#xfd;" ID="ID_862794775" CREATED="1331682388444" MODIFIED="1331682403814"/>
</node>
<node TEXT="pocit odd&#x11b;len&#xed;" ID="ID_885848269" CREATED="1331403613372" MODIFIED="1331682298308">
<font BOLD="true"/>
<node TEXT="od vn&#x11b;j&#x161;&#xed;ho sv&#x11b;ta" ID="ID_1776598174" CREATED="1331682299794" MODIFIED="1331682795656">
<font BOLD="true"/>
<node TEXT="pozoruje sv&#x11b;t z bezpe&#x10d;&#xed; sklen&#x11b;n&#xe9; klece" ID="ID_277448416" CREATED="1331403624113" MODIFIED="1331403636128"/>
<node TEXT="zakr&#xfd;v&#xe1; neschopnost &#x10d;elit rizik&#x16f;m sv&#x11b;ta" ID="ID_144053173" CREATED="1331403581064" MODIFIED="1331682686927"/>
<node TEXT="touha uniknout do vysn&#x11b;n&#xe9; reality" ID="ID_1352591212" CREATED="1331682744166" MODIFIED="1331683338596">
<font BOLD="true"/>
</node>
<node TEXT="nemaj&#xed; odvahu se pustit mimo bezpe&#x10d;&#xed;" ID="ID_1325393822" CREATED="1331403668145" MODIFIED="1331403697157"/>
<node TEXT="odkl&#xe1;daj&#xed; na potom" ID="ID_1004565757" CREATED="1331404567019" MODIFIED="1331404572820"/>
<node TEXT="hippie" ID="ID_1515501482" CREATED="1331404988464" MODIFIED="1331404990964"/>
<node TEXT="hled&#xe1;n&#xed; identity" ID="ID_1752495795" CREATED="1331683340839" MODIFIED="1331683347509">
<font BOLD="true"/>
</node>
</node>
<node TEXT="mysl od t&#x11b;la" ID="ID_1275377789" CREATED="1331682312756" MODIFIED="1331683042769"/>
<node TEXT="osam&#x11b;lost" ID="ID_1954895142" CREATED="1331682329125" MODIFIED="1331682334844"/>
</node>
<node TEXT="dob&#x159;e zabezpe&#x10d;en&#xed;" ID="ID_1206232268" CREATED="1331403645227" MODIFIED="1331678095566"/>
<node TEXT="v&#x161;echno vn&#xed;m&#xe1; nadsazen&#x11b;" ID="ID_934909706" CREATED="1331403707419" MODIFIED="1331403718405">
<node TEXT="co je p&#x159;&#xed;jemn&#xe9; vn&#xed;m&#xe1; p&#x159;&#xed;jemn&#x11b;ji" ID="ID_1219458031" CREATED="1331403735436" MODIFIED="1331403760832"/>
<node TEXT="nep&#x159;&#xed;jemn&#xe9; je nep&#x159;&#xed;jemn&#x11b;j&#x161;&#xed;" ID="ID_1568618049" CREATED="1331403774483" MODIFIED="1331403785973"/>
<node TEXT="vzd&#xe1;lenosti" ID="ID_808609132" CREATED="1331403789417" MODIFIED="1331403793823"/>
<node TEXT="&#x10d;as" ID="ID_102671896" CREATED="1331679882614" MODIFIED="1331679884965"/>
<node TEXT="barvy" ID="ID_562966098" CREATED="1331679911716" MODIFIED="1331679918758"/>
</node>
<node TEXT="p&#x159;ehnan&#xe1; v&#xfd;&#x159;e&#x10d;nost" ID="ID_1632705542" CREATED="1331404033355" MODIFIED="1331404042037"/>
<node TEXT="siln&#xe1; sexualita" ID="ID_471563872" CREATED="1331404063989" MODIFIED="1331404073482"/>
<node TEXT="st&#x159;&#xed;d&#xe1;n&#xed; polarit" ID="ID_789887450" CREATED="1331404541127" MODIFIED="1331678177722">
<font BOLD="true"/>
</node>
<node TEXT="zm&#x11b;n&#x11b;n&#xe9; vn&#xed;m&#xe1;n&#xed; t&#x11b;la" ID="ID_1834938286" CREATED="1331680192396" MODIFIED="1331680214829">
<node TEXT="l&#xe9;t&#xe1;n&#xed;, vzn&#xe1;&#x161;en&#xed; se" ID="ID_1814513064" CREATED="1331404556714" MODIFIED="1331404566507"/>
<node TEXT="hlas jako by nebyl jeho" ID="ID_640367017" CREATED="1331680136402" MODIFIED="1331680142932"/>
<node TEXT="&#x10d;&#xe1;sti t&#x11b;la maj&#xed; nespr&#xe1;vn&#xe9; proporce" ID="ID_1888446478" CREATED="1331680231494" MODIFIED="1331680242442"/>
<node TEXT="lehkost t&#x11b;la" ID="ID_808733687" CREATED="1331680255718" MODIFIED="1331680262718"/>
</node>
<node TEXT="&quot;rostlinn&#xfd; ekvivalent Phos&quot;" ID="ID_179328904" CREATED="1357921531089" MODIFIED="1357921556223"/>
</node>
<node TEXT="Symptomy" POSITION="left" ID="ID_150929932" CREATED="1331403850450" MODIFIED="1331403855628" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="neurologick&#xe9; pot&#xed;&#x17e;e" ID="ID_1732179529" CREATED="1331403856465" MODIFIED="1331403862477"/>
<node TEXT="&#x161;patn&#xe1; pam&#x11b;&#x165;" ID="ID_1311374067" CREATED="1331403863277" MODIFIED="1331403876347">
<node TEXT="zapom&#x11b;tlivost" ID="ID_781314001" CREATED="1331404053339" MODIFIED="1331404057780"/>
</node>
<node TEXT="sucho v &#xfa;stech" ID="ID_745849266" CREATED="1331403876923" MODIFIED="1331403883336"/>
<node TEXT="&#x17e;&#xed;ze&#x148;" ID="ID_210931939" CREATED="1331403883593" MODIFIED="1331403888629"/>
<node TEXT="obli&#x10d;ej" ID="ID_1276556139" CREATED="1331404413508" MODIFIED="1331404416738">
<node TEXT="bled&#xfd; vpadl&#xfd;" ID="ID_1535986012" CREATED="1331403889006" MODIFIED="1331404421092"/>
<node TEXT="fialov&#xed; kdy&#x17e; se sm&#x11b;je" ID="ID_738196615" CREATED="1331404421956" MODIFIED="1331404456932"/>
</node>
<node TEXT="v&#xfd;raz" ID="ID_842736484" CREATED="1331403898276" MODIFIED="1331403901663">
<node TEXT="&#x161;&#xed;len&#xfd;" ID="ID_1293072871" CREATED="1331403902559" MODIFIED="1331403905712"/>
<node TEXT="ospal&#xfd;" ID="ID_1181362895" CREATED="1331403905977" MODIFIED="1331403908420"/>
</node>
<node TEXT="vl&#x10d;&#xed; chu&#x165; k j&#xed;dlu" ID="ID_101561028" CREATED="1331404114030" MODIFIED="1331404121935"/>
<node TEXT="mo&#x10d;ov&#xe9; cesty" ID="ID_220744342" CREATED="1331404135167" MODIFIED="1331404142015">
<node TEXT="ukap&#xe1;v&#xe1;n&#xed;" ID="ID_1462086018" CREATED="1331404143056" MODIFIED="1331404149301"/>
<node TEXT="dlouho trv&#xe1; ne&#x17e; se mo&#x10d; spust&#xed;" ID="ID_1544853799" CREATED="1331404149774" MODIFIED="1331404159759"/>
<node TEXT="p&#xe1;liv&#xe9; nebo bodav&#xe9; bolesti v mo&#x10d;. trubici" ID="ID_125353201" CREATED="1331404160016" MODIFIED="1331404175025"/>
</node>
<node TEXT="bolest ledviny" ID="ID_1176445642" CREATED="1331404186713" MODIFIED="1331404191481">
<node TEXT="tup&#xe1;" ID="ID_1323214906" CREATED="1331404192457" MODIFIED="1331404194376"/>
<node TEXT="p&#x159;i sm&#xed;chu" ID="ID_1718910455" CREATED="1331404194776" MODIFIED="1331404198242"/>
</node>
<node TEXT="pocit, jako by sed&#x11b;l na bal&#xf3;n&#x11b;" ID="ID_245758676" CREATED="1331404199248" MODIFIED="1331404219138"/>
<node TEXT="no&#x10d;n&#xed; m&#x16f;ry" ID="ID_1685071812" CREATED="1331404085489" MODIFIED="1331404090160">
<node TEXT="mrtv&#xed;" ID="ID_1075347844" CREATED="1331404091025" MODIFIED="1331404093661"/>
<node TEXT="nebezpe&#x10d;&#xed;" ID="ID_1917421223" CREATED="1331404094637" MODIFIED="1331404099701"/>
<node TEXT="sk&#x159;&#xed;paj&#xed; zuby ze sna" ID="ID_1577236174" CREATED="1331404100086" MODIFIED="1331404109745"/>
</node>
<node TEXT="strach" ID="ID_560023088" CREATED="1331404000431" MODIFIED="1331681831488">
<font BOLD="true"/>
<node TEXT="&#x17e;e ztrat&#xed; kontrolu" ID="ID_1099685102" CREATED="1331404006610" MODIFIED="1331404012377"/>
<node TEXT="ze &#x161;&#xed;lenstv&#xed;" ID="ID_521446869" CREATED="1331404012616" MODIFIED="1331404017178"/>
<node TEXT="ze tmy" ID="ID_950707962" CREATED="1331404017891" MODIFIED="1331681837745">
<font BOLD="true"/>
</node>
<node TEXT="vzduchu" ID="ID_1628879018" CREATED="1331404023664" MODIFIED="1331404026193"/>
<node TEXT="smrti" ID="ID_593916981" CREATED="1331404026474" MODIFIED="1331404028918"/>
</node>
<node TEXT="pot" ID="ID_1968230811" CREATED="1331404374368" MODIFIED="1331404382456">
<node TEXT="tv&#xe1;&#x159;" ID="ID_1989917903" CREATED="1331404384679" MODIFIED="1331404387156"/>
<node TEXT="lep&#xed;c&#xed; kapky" ID="ID_951174702" CREATED="1331404387669" MODIFIED="1331404393518"/>
</node>
<node TEXT="halucinace" ID="ID_1793924507" CREATED="1331679992966" MODIFIED="1331680006079">
<node TEXT="zrakov&#xe9;" ID="ID_338420635" CREATED="1331680006905" MODIFIED="1331680010738"/>
<node TEXT="sluchov&#xe9;" ID="ID_824423052" CREATED="1331680011266" MODIFIED="1331680017223"/>
</node>
</node>
<node TEXT="Modality" POSITION="left" ID="ID_1026310362" CREATED="1331403936071" MODIFIED="1331682041067" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="" ID="ID_531459215" CREATED="1331403939748" MODIFIED="1331403941581" COLOR="#000000">
<icon BUILTIN="smily_bad"/>
<node TEXT="Odpoledne" ID="ID_874895224" CREATED="1331403942603" MODIFIED="1331403945267"/>
<node TEXT="b&#x11b;hem sp&#xe1;nku" ID="ID_446953692" CREATED="1331403945508" MODIFIED="1331403949800"/>
<node TEXT="p&#x159;i vst&#xe1;v&#xe1;n&#xed;" ID="ID_98604000" CREATED="1331403952501" MODIFIED="1331403957624"/>
<node TEXT="v noci" ID="ID_878707048" CREATED="1331404315415" MODIFIED="1331404320999"/>
<node TEXT="k&#xe1;va" ID="ID_1531773510" CREATED="1331679737215" MODIFIED="1331679745881"/>
<node TEXT="alkohol" ID="ID_1630956184" CREATED="1331679746625" MODIFIED="1331679748705"/>
</node>
<node TEXT="" ID="ID_1606784365" CREATED="1331403961912" MODIFIED="1331403963286" COLOR="#000000">
<icon BUILTIN="ksmiletris"/>
<font NAME="SansSerif" SIZE="12"/>
<node TEXT="odpo&#x10d;inkem" ID="ID_197085238" CREATED="1331403964298" MODIFIED="1331403968081"/>
<node TEXT="proch&#xe1;zka na &#x10d;erstv&#xe9;m vzduchu" ID="ID_1084299138" CREATED="1331403969632" MODIFIED="1331680289907">
<font BOLD="true"/>
<node TEXT="uvoln&#xed; ment&#xe1;ln&#xed; symptomy" ID="ID_1298289142" CREATED="1331680291162" MODIFIED="1331680303628" COLOR="#999999">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="studen&#xe1; voda" ID="ID_817515810" CREATED="1331679710138" MODIFIED="1331679726183"/>
</node>
</node>
<node TEXT="CANN-I" POSITION="right" ID="ID_1422228314" CREATED="1331680867159" MODIFIED="1331680924030" COLOR="#996600">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Konop&#xed; Indick&#xe9;" POSITION="right" ID="ID_1381263853" CREATED="1331681584078" MODIFIED="1331681614768" COLOR="#999900">
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
</node>
<node TEXT="Trituace" POSITION="right" ID="ID_125775072" CREATED="1331401712891" MODIFIED="1331682445063" COLOR="#0066cc" STYLE="fork" HGAP="21" VSHIFT="-9">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="v&#x161;ichni se sm&#x11b;j&#xed;" ID="ID_1525056036" CREATED="1331401737149" MODIFIED="1331680427462">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="nikdo nechce hl&#xe1;sit symptomy" ID="ID_367007502" CREATED="1331401771762" MODIFIED="1331680404673"/>
</node>
<node TEXT="Info" POSITION="right" ID="ID_599689432" CREATED="1331401705089" MODIFIED="1331401710011" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="droga se vyr&#xe1;b&#xed; usu&#x161;en&#xed;m sam&#x10d;&#xed;ch kv&#x11b;tenstv&#xed; s obsahem nad 0.3%THC" ID="ID_657933417" CREATED="1331401954401" MODIFIED="1331682085824"/>
<node TEXT="kanabioidy" ID="ID_661776597" CREATED="1331402013063" MODIFIED="1331402021145"/>
<node TEXT="THC" ID="ID_1736675354" CREATED="1331403553571" MODIFIED="1331403555911">
<node TEXT="tetrahydrocannabinol" ID="ID_1039687330" CREATED="1331403532894" MODIFIED="1331678239806"/>
</node>
<node TEXT="mno&#x17e;stv&#xed; THC se m&#x11b;n&#xed; podle kultivaru, pohlav&#xed; a um&#xed;st&#x11b;n&#xed; na rostlin&#x11b;" ID="ID_1116534812" CREATED="1331402025050" MODIFIED="1331402053512"/>
<node TEXT="d&#xe1; se u&#x17e;&#xed;vat n&#x11b;kolika zp&#x16f;soby" ID="ID_1903040550" CREATED="1331402090119" MODIFIED="1331402176762">
<node TEXT="ve dvou" ID="ID_365308285" CREATED="1331402131541" MODIFIED="1331402135674"/>
<node TEXT="&#x10d;ist&#xe1; nebo sm&#xed;chan&#xe1; s tab&#xe1;kem" ID="ID_1785079990" CREATED="1331402136338" MODIFIED="1331402154612"/>
<node TEXT="marihuanov&#xe9; ml&#xe9;ko" ID="ID_682372897" CREATED="1331402156139" MODIFIED="1331402162968"/>
</node>
<node TEXT="&#xfa;&#x10d;inky" ID="ID_94052146" CREATED="1331402406680" MODIFIED="1331677819357" COLOR="#659919">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<node TEXT="pocit dobr&#xe9; pohody" ID="ID_1894185945" CREATED="1331402414969" MODIFIED="1331681612601">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="hlad a chu&#x165; na sladk&#xe9;" ID="ID_1318889887" CREATED="1331402429952" MODIFIED="1331402441169"/>
<node TEXT="zm&#x11b;ny ve smysl. vn&#xed;m&#xe1;n&#xed;" ID="ID_339757019" CREATED="1331402451186" MODIFIED="1331402459393"/>
<node TEXT="pronikav&#xe9; zm&#x11b;ny ve zp&#x16f;sobu my&#x161;len&#xed; a jeho vyjad&#x159;ov&#xe1;n&#xed;" ID="ID_128236711" CREATED="1331402461847" MODIFIED="1331402479416"/>
<node TEXT="prohlubuje se pro&#x17e;itek z hudby" ID="ID_1068917720" CREATED="1331402504177" MODIFIED="1331402516778"/>
<node TEXT="zrakov&#xe9; &#x10d;ichov&#xe9; a sluchov&#xe9; zm&#x11b;ny" ID="ID_1098073492" CREATED="1331402518456" MODIFIED="1331402531198"/>
<node TEXT="vysm&#xe1;tost" ID="ID_1715424310" CREATED="1331402531639" MODIFIED="1331402546407">
<node TEXT="z&#xe1;chvaty sm&#xed;chu se nedaj&#xed; zastavit" ID="ID_1670777720" CREATED="1331402547480" MODIFIED="1331402556788"/>
</node>
<node ID="ID_1119287573" CREATED="1331402558161" MODIFIED="1331682647050"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      euforii st&#345;&#237;d&#225; pocit pr&#225;zdnoty, kdy &#269;lov&#283;k nemysl&#237; v&#367;bec na nic,
    </p>
    <p>
      nebo chce jen sp&#225;t
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="sn&#xed;&#x17e;en&#xe1; citlivost kon&#x10d;etin" ID="ID_1616171240" CREATED="1331402599060" MODIFIED="1331402609329"/>
<node TEXT="zkreslen&#xe9; vid&#x11b;n&#xed;" ID="ID_1166996147" CREATED="1331402609625" MODIFIED="1331402614628"/>
<node TEXT="pocit, &#x17e;e &#x10d;as plyne n&#x11b;kolikan&#xe1;sobn&#x11b; pomaleji" ID="ID_406209274" CREATED="1331402615157" MODIFIED="1331402634246"/>
<node TEXT="pocit, &#x17e;e va&#x161;e t&#x11b;lo chod&#xed; samo od sebe" ID="ID_1391112103" CREATED="1331402635259" MODIFIED="1331402645707"/>
<node TEXT="tikavost sval&#x16f;, nebo pocit, &#x17e;e v&#xe1;s svaly tla&#x10d;&#xed;" ID="ID_1893864895" CREATED="1331402668353" MODIFIED="1331402686256"/>
<node TEXT="p&#x159;i poz&#x159;en&#xed;" ID="ID_1115604382" CREATED="1331402687184" MODIFIED="1331402702494">
<node TEXT="efekt nastupuje po hodin&#x11b;" ID="ID_1245771983" CREATED="1331402704094" MODIFIED="1331402720145"/>
<node TEXT="trv&#xe1; d&#xe9;le, a&#x17e; 12 hodin" ID="ID_363602270" CREATED="1331402720985" MODIFIED="1331402733905"/>
</node>
<node TEXT="stihomam" ID="ID_1722253505" CREATED="1331402750095" MODIFIED="1331402785529"/>
<node TEXT="nel&#xe9;&#x10d;iteln&#xe9; po&#x161;kozen&#xed; plic" ID="ID_1587908605" CREATED="1331403237539" MODIFIED="1331403245534"/>
<node TEXT="zp&#x16f;sobuje st&#xe1;rnut&#xed; bun&#x11b;k" ID="ID_1456438709" CREATED="1331403283091" MODIFIED="1331403294529"/>
<node TEXT="&#xfa;pln&#xe9; odbour&#xe1;n&#xed; metabol&#xe1;t&#x16f; THC z jednoho jointu trv&#xe1; a&#x17e; 130 dn&#xed;" ID="ID_1718786171" CREATED="1331403319490" MODIFIED="1331403362320"/>
</node>
<node TEXT="terapie" ID="ID_1533640650" CREATED="1331402787629" MODIFIED="1331677821288" COLOR="#659919">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<node TEXT="protirakovinn&#xe9; p&#x16f;soben&#xed;" ID="ID_1215680439" CREATED="1331402799329" MODIFIED="1331402807991"/>
<node TEXT="sedativn&#xed; &#xfa;&#x10d;inek" ID="ID_1969846055" CREATED="1331402808240" MODIFIED="1331402814712"/>
<node TEXT="neuroleptikum" ID="ID_107313759" CREATED="1331402815073" MODIFIED="1331402819379"/>
<node TEXT="antiametikum" ID="ID_1829651858" CREATED="1331402820339" MODIFIED="1331402825892"/>
<node TEXT="spasmolytikum" ID="ID_1198281467" CREATED="1331402826541" MODIFIED="1331402831605"/>
<node TEXT="bronchospasmolytikum" ID="ID_944880674" CREATED="1331402832828" MODIFIED="1331402841258"/>
<node TEXT="kardiokorigens" ID="ID_710976346" CREATED="1331402841619" MODIFIED="1331402853774"/>
<node TEXT="zm&#xed;r&#x148;uje" ID="ID_1328001423" CREATED="1331402856507" MODIFIED="1331402872232">
<node TEXT="nechutenstv&#xed;" ID="ID_1002828877" CREATED="1331402873281" MODIFIED="1331402877029"/>
<node TEXT="k&#x159;e&#x10d;e" ID="ID_22067845" CREATED="1331402977369" MODIFIED="1331402980444"/>
<node TEXT="bolesti kloub&#x16f; a p&#xe1;te&#x159;e" ID="ID_797642125" CREATED="1331402877341" MODIFIED="1331402884758"/>
<node TEXT="alergie" ID="ID_1217387138" CREATED="1331402885135" MODIFIED="1331402887871"/>
<node TEXT="psychotick&#xe9; stavy" ID="ID_1446133114" CREATED="1331402888120" MODIFIED="1331402894910"/>
</node>
<node TEXT="parkinsonova ch." ID="ID_1516022619" CREATED="1331402898986" MODIFIED="1331402907216"/>
<node TEXT="alzheimerova ch." ID="ID_960114578" CREATED="1331402907864" MODIFIED="1331402920323"/>
<node TEXT="RS" ID="ID_906823897" CREATED="1331402920556" MODIFIED="1331402922365"/>
<node TEXT="tachykardie" ID="ID_477413767" CREATED="1331402922607" MODIFIED="1331402926277"/>
<node TEXT="angina pectoris" ID="ID_1731685428" CREATED="1331402926590" MODIFIED="1331402933152"/>
<node TEXT="migreny" ID="ID_820941522" CREATED="1331402933425" MODIFIED="1331402937808"/>
<node TEXT="maniodeprese" ID="ID_1165691317" CREATED="1331402939565" MODIFIED="1331402943026"/>
<node TEXT="epilepsie" ID="ID_1052613838" CREATED="1331402945823" MODIFIED="1331402948700"/>
<node TEXT="anorexie" ID="ID_1273713297" CREATED="1331402953908" MODIFIED="1331402957177"/>
<node TEXT="glaukom" ID="ID_655792529" CREATED="1331402964136" MODIFIED="1331402966936"/>
<node TEXT="antibakteri&#xe1;ln&#xed;" ID="ID_567426497" CREATED="1331402986674" MODIFIED="1331402991575"/>
<node TEXT="fungicidn&#xed;" ID="ID_1723499026" CREATED="1331402992024" MODIFIED="1331402996766"/>
<node TEXT="virostatick&#xe9;" ID="ID_1394999498" CREATED="1331402997071" MODIFIED="1331403000780"/>
</node>
<node TEXT="k l&#xe9;&#x10d;en&#xed; se pou&#x17e;&#xed;v&#xe1;" ID="ID_703759809" CREATED="1331403021556" MODIFIED="1331403033861">
<node TEXT="vaporizace" ID="ID_870200869" CREATED="1331403035325" MODIFIED="1331403041864"/>
<node TEXT="rekt&#xe1;ln&#xed; &#x10d;&#xed;pky" ID="ID_1529957396" CREATED="1331403042153" MODIFIED="1331403048797"/>
<node TEXT="mast" ID="ID_966040536" CREATED="1331403049286" MODIFIED="1331403051725"/>
<node TEXT="THC a&#x17e; 20%" ID="ID_1047957644" CREATED="1331403094266" MODIFIED="1331403110864"/>
</node>
<node TEXT="a&#x17e; 80% u&#x17e;ivatel&#x16f; se dostane do obrazu l&#xe9;ku" ID="ID_580328062" CREATED="1331404250138" MODIFIED="1331404306593"/>
</node>
<node POSITION="left" ID="ID_1701040922" CREATED="1331680474518" MODIFIED="1331680849342" HGAP="-45" VSHIFT="21"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <img src="cann-i01.jpg"/>
  </body>
</html>
</richcontent>
</node>
<node POSITION="right" ID="ID_1877801557" CREATED="1331680806471" MODIFIED="1331680851702" HGAP="-20" VSHIFT="31"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <img src="cann-i02.jpg"/>
  </body>
</html>
</richcontent>
</node>
</node>
</map>
