<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Vernix Caseosa" ID="ID_689297696" CREATED="1347698996925" MODIFIED="1347797785447" LINK="../../3r-1.mm" COLOR="#cc0000" STYLE="fork">
<hook NAME="MapStyle" max_node_width="600"/>
<font NAME="Liberation Sans" SIZE="24" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="T&#xe9;mata" POSITION="left" ID="ID_460920044" CREATED="1347797791084" MODIFIED="1347797797722" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="oslaben&#xe9; ego" ID="ID_1781981845" CREATED="1347699038264" MODIFIED="1347699047089"/>
<node TEXT="nedostatek l&#xe1;sky" ID="ID_970423011" CREATED="1347699047505" MODIFIED="1347699055101"/>
<node TEXT="pocit, jako by byli prostupn&#xed;" ID="ID_721687153" CREATED="1347797804617" MODIFIED="1347797815419">
<node TEXT="jako be se jich v&#x161;e dot&#xfd;kalo" ID="ID_1041178321" CREATED="1347797818952" MODIFIED="1347797826973"/>
</node>
<node TEXT="udr&#x17e;et si svou integritu" ID="ID_1203950316" CREATED="1347798032545" MODIFIED="1347798045218"/>
<node TEXT="chr&#xe1;n&#xed; na&#x161;i energii, aby byla odd&#x11b;lena od ostatn&#xed;ch lid&#xed;" ID="ID_1446397815" CREATED="1347798056881" MODIFIED="1347798106029"/>
<node TEXT="pocit nedostate&#x10d;n&#xe9; ochrany" ID="ID_888986980" CREATED="1347798128731" MODIFIED="1347798135841"/>
<node TEXT="extr&#xe9;mn&#xed; vn&#xed;mavost" ID="ID_1686486333" CREATED="1347798148428" MODIFIED="1347798154393">
<node TEXT="c&#xed;t&#xed; co c&#xed;t&#xed; ostatn&#xed;" ID="ID_1477927418" CREATED="1347798161940" MODIFIED="1347798167668"/>
<node TEXT="nedok&#xe1;&#x17e;&#xed; to vypnout" ID="ID_1252794606" CREATED="1347798179886" MODIFIED="1347798187204"/>
</node>
<node TEXT="probl&#xe9;m s ochranou vlastn&#xed;ch hranic" ID="ID_723415911" CREATED="1347798223562" MODIFIED="1347798234334"/>
<node TEXT="probl&#xe9;my s hranicemi" ID="ID_699875095" CREATED="1347798265998" MODIFIED="1347798279219">
<node TEXT="nem&#x16f;&#x17e;ou sledovat zpr&#xe1;vy" ID="ID_962737499" CREATED="1347798280068" MODIFIED="1347798345201"/>
<node TEXT="p&#x159;ecitliv&#x11b;lost na jin&#xe9; energie" ID="ID_1026525069" CREATED="1347798290762" MODIFIED="1347798306041"/>
<node TEXT="navazuj&#xed; jenom povrchn&#xed; kontakty" ID="ID_674276112" CREATED="1347798312337" MODIFIED="1347798331441">
<node TEXT="u&#x17e; se jen stran&#xed; a br&#xe1;n&#xed;" ID="ID_838512110" CREATED="1347798332115" MODIFIED="1347798340095"/>
</node>
<node TEXT="um&#x11b;le se odd&#x11b;luje od sv&#x11b;ta a lid&#xed;" ID="ID_244046726" CREATED="1347799072431" MODIFIED="1347799085217"/>
<node TEXT="lehk&#xe1; ob&#x11b;&#x165; manipulace" ID="ID_1773441274" CREATED="1347798346900" MODIFIED="1347798358766"/>
<node TEXT="okoln&#xed; sv&#x11b;t je pln&#xfd; nebezpe&#x10d;&#xed;" ID="ID_1159605746" CREATED="1347798367561" MODIFIED="1347798377586">
<node TEXT="hrub&#xfd;" ID="ID_991481977" CREATED="1347798378755" MODIFIED="1347798381917"/>
<node TEXT="&#x161;kared&#xfd;" ID="ID_1340945228" CREATED="1347798382446" MODIFIED="1347798385909"/>
</node>
<node TEXT="p&#x159;ehnan&#xe9; obavy o vn&#x11b;j&#x161;&#xed; sv&#x11b;t a vn&#x11b;j&#x161;&#xed; ud&#xe1;losti" ID="ID_442557112" CREATED="1347798510388" MODIFIED="1347798525739"/>
<node TEXT="nedok&#xe1;&#x17e;e pochopit pro&#x10d; se to d&#x11b;je" ID="ID_25169629" CREATED="1347798562187" MODIFIED="1347798585617"/>
<node TEXT="nedostetk ochrany" ID="ID_1397213153" CREATED="1347799785310" MODIFIED="1347799791812">
<node TEXT="&#xfa;zkosti" ID="ID_801521550" CREATED="1347800000199" MODIFIED="1347800004723"/>
<node TEXT="vyh&#xfd;b&#xe1; se spole&#x10d;nosti" ID="ID_164822421" CREATED="1347800011626" MODIFIED="1347800018527"/>
<node TEXT="z&#x16f;st&#xe1;v&#xe1; doma" ID="ID_945890261" CREATED="1347800021860" MODIFIED="1347800026575"/>
</node>
<node TEXT="pocit, &#x17e;e je bez k&#x16f;&#x17e;e, nah&#xfd;" ID="ID_904631557" CREATED="1347799808171" MODIFIED="1347799818350"/>
<node TEXT="p&#x159;ecitliv&#x11b;lost na v&#x161;echno" ID="ID_1689838716" CREATED="1347799818710" MODIFIED="1347799830884">
<node TEXT="pachy" ID="ID_120660329" CREATED="1347799868421" MODIFIED="1347799870889"/>
<node TEXT="hluk" ID="ID_1995987873" CREATED="1347799871457" MODIFIED="1347799875254"/>
<node TEXT="dotyk" ID="ID_59584264" CREATED="1347799875639" MODIFIED="1347799877868"/>
<node TEXT="sign&#xe1;ly" ID="ID_1655495253" CREATED="1347799878285" MODIFIED="1347799884000"/>
<node TEXT="pocity ostatn&#xed;ch" ID="ID_180211260" CREATED="1347800067394" MODIFIED="1347800071766"/>
</node>
<node TEXT="jeho st&#x159;ed je ve vn&#x11b;j&#x161;&#xed;m sv&#x11b;t&#x11b;" ID="ID_1746677449" CREATED="1347799930898" MODIFIED="1347799945098"/>
<node TEXT="starost o probl&#xe9;my ostatn&#xed;ch" ID="ID_569240943" CREATED="1347799948210" MODIFIED="1347799958189"/>
</node>
<node TEXT="nev&#x11b;dom&#xed; sebe sama" ID="ID_1398135917" CREATED="1347799974523" MODIFIED="1347799980154">
<node TEXT="pocit, &#x17e;e ztratil s&#xe1;m sebe" ID="ID_878384200" CREATED="1347799980939" MODIFIED="1347799991374"/>
</node>
<node TEXT="chyb&#xed; jasn&#xe1; hranice mezi realitou a fantazi&#xed;" ID="ID_803734072" CREATED="1347800081661" MODIFIED="1347800100011"/>
<node TEXT="slab&#xe1; v&#x16f;le" ID="ID_1898203164" CREATED="1347800100476" MODIFIED="1347800104508"/>
<node TEXT="nev&#xed; co chce" ID="ID_1210952904" CREATED="1347800109655" MODIFIED="1347800112996"/>
<node TEXT="pocit viny" ID="ID_1419681928" CREATED="1347800113261" MODIFIED="1347800115664">
<node TEXT="neud&#x11b;lal toho pro ostatn&#xed; dost" ID="ID_1593431578" CREATED="1347800117024" MODIFIED="1347800126455"/>
<node TEXT="nic nem&#x16f;&#x17e;e odm&#xed;tnout" ID="ID_389950915" CREATED="1347800127088" MODIFIED="1347800133097"/>
</node>
<node TEXT="po l&#xe9;ku schopen p&#x159;ijmout realitu takovou jak&#xe1; je" ID="ID_1044439221" CREATED="1347798587278" MODIFIED="1347798666707"/>
<node TEXT="strach ve tm&#x11b;" ID="ID_171252984" CREATED="1347800032561" MODIFIED="1347800036123"/>
</node>
<node TEXT="Symptomy" POSITION="right" ID="ID_371336856" CREATED="1347799779697" MODIFIED="1347799784829" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="hlava" ID="ID_1685603820" CREATED="1347799888983" MODIFIED="1347799890508">
<node TEXT="pln&#xe1;" ID="ID_7227495" CREATED="1347799891317" MODIFIED="1347799893044"/>
<node TEXT="nem&#x16f;&#x17e;e si p&#x159;estat d&#x11b;lat starosti" ID="ID_1605990775" CREATED="1347799893445" MODIFIED="1347799908513"/>
</node>
<node TEXT="nespavost" ID="ID_487512984" CREATED="1347800140359" MODIFIED="1347800143730">
<node TEXT="obt&#xed;&#x17e;n&#xfd; sp&#xe1;nek" ID="ID_526909457" CREATED="1347800145082" MODIFIED="1347800159441"/>
<node TEXT="p&#x159;&#xed;li&#x161; mnoho absorbovan&#xfd;ch v&#x11b;m&#x16f;" ID="ID_1833538521" CREATED="1347800160721" MODIFIED="1347800171948"/>
</node>
<node TEXT="vy&#x10d;erp&#xe1;n&#xed;" ID="ID_1124996634" CREATED="1347800187027" MODIFIED="1347800193466">
<node TEXT="energie mu unik&#xe1;" ID="ID_713994505" CREATED="1347800194315" MODIFIED="1347800199256"/>
<node TEXT="je naru&#x161;ov&#xe1;na neharmonick&#xfd;mi energiemi" ID="ID_911790056" CREATED="1347800199512" MODIFIED="1347800216014"/>
</node>
<node TEXT="p&#x159;ehnan&#xe1; reaktivnost" ID="ID_767192947" CREATED="1347800231334" MODIFIED="1347800238924">
<node TEXT="d&#xed;ky &#x161;patn&#xe9;mu vyhodnocen&#xed; situace" ID="ID_389035120" CREATED="1347800243016" MODIFIED="1347800253080"/>
</node>
</node>
<node TEXT="DD" POSITION="right" ID="ID_1254083874" CREATED="1347798667928" MODIFIED="1347798670751" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Phos" ID="ID_1049333672" CREATED="1347798671696" MODIFIED="1347798674704">
<node TEXT="miner&#xe1;l" ID="ID_622514468" CREATED="1347798677241" MODIFIED="1347798687058">
<icon BUILTIN="button_cancel"/>
<node TEXT="vernix m&#xe1; v&#x11b;t&#x161;&#xed; spiritu&#xe1;ln&#xed; hloubku" ID="ID_1038287437" CREATED="1347798689901" MODIFIED="1347798714392"/>
</node>
</node>
</node>
<node TEXT="Info" POSITION="right" ID="ID_316431083" CREATED="1347797830062" MODIFIED="1347797833406" COLOR="#0066cc" STYLE="fork">
<font NAME="Nimbus Sans L" SIZE="14" BOLD="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="d&#x11b;tsk&#xfd; m&#xe1;zek" ID="ID_1973923193" CREATED="1347699014294" MODIFIED="1347699031718" COLOR="#999999">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
</map>
